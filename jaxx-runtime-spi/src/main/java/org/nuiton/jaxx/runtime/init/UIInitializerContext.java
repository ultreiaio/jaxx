package org.nuiton.jaxx.runtime.init;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.JComponent;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Context to collect components at init time.
 * <p>
 * Created on 09/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class UIInitializerContext<U extends JAXXObject> {

    private static final Logger log = LogManager.getLogger(UIInitializerContext.class);
    private final U ui;
    private final String prefix;
    private final Class<?>[] types;
    private final ImmutableSet.Builder<String> doNotBlockComponentIds;
    private final ImmutableSet.Builder<Class<?>> componentsToKeep;
    private final ArrayListMultimap<String, JComponent> focusComponents = ArrayListMultimap.create();

    private final ImmutableList.Builder<Object> dependencies;
    private final SingletonSupplier<ArrayListMultimap<Class<?>, Object>> typedComponents;
    private Init initState;

    public static ArrayListMultimap<Class<?>, Object> components(JAXXObject ui) {
        ArrayListMultimap<Class<?>, Object> result = ArrayListMultimap.create();
        ui.get$objectMap().values().forEach((v) -> result.put(v.getClass(), v));
        return result;
    }

    public static <O> Stream<O> onComponents(Class<O> type, ArrayListMultimap<Class<?>, Object> components) {
        return loadComponents(type, components).stream();
    }

    @SuppressWarnings("unchecked")
    public static <O> List<O> loadComponents(Class<O> type, ArrayListMultimap<Class<?>, Object> components) {
        ImmutableList.Builder<O> result = ImmutableList.builder();
        Set<Map.Entry<Class<?>, Collection<Object>>> entries = components.asMap().entrySet();
        entries.stream().filter(k -> type.isAssignableFrom(k.getKey())).forEach(k -> result.addAll((Collection<O>) k.getValue()));
        return result.build();
    }

    public UIInitializerContext(U ui, Class<?>... types) {
        this.ui = Objects.requireNonNull(ui);
        this.prefix = "[" + ui.getClass().getSimpleName() + "] ";
        this.types = types;
        this.doNotBlockComponentIds = ImmutableSet.builder();
        this.dependencies = ImmutableList.builder();
        this.componentsToKeep = ImmutableSet.builder();
        this.typedComponents = SingletonSupplier.of(this::init);
    }

    /**
     * Created on 09/12/2020.
     *
     * @author Tony Chemit - dev@tchemit.fr
     * @since 8.0.1
     */
    public enum Init {
        FIRST_PASS,
        SECOND_PASS
    }

    public U getUi() {
        return ui;
    }

    public ArrayListMultimap<Class<?>, Object> init() {
        ArrayListMultimap<Class<?>, Object> typedComponents = ArrayListMultimap.create();
        ui.get$objectMap().forEach((k, v) -> {
            if (v == null) {
                //FIXME This probably means a JAXX bug?
                log.warn(String.format("%s Null JAXX object with id: %s", prefix, k));
                return;
            }
            typedComponents.put(v.getClass(), v);
        });
        return typedComponents;
    }

    public String getPrefix() {
        return prefix;
    }

    public void registerDependencies(Object... dependencies) {
        this.dependencies.addAll(Arrays.asList(dependencies));
        for (Object dep : dependencies) {
            ui.setContextValue(dep);
        }
    }

    public void addDoNotBlockComponentId(String editor) {
        doNotBlockComponentIds.add(editor);
    }

    public ImmutableSet<String> getDoNotBlockComponentIds() {
        return doNotBlockComponentIds.build();
    }

    public ImmutableList<Object> getDependencies() {
        return dependencies.build();
    }

    public ArrayListMultimap<String, JComponent> getFocusComponents() {
        return focusComponents;
    }

    public ImmutableSet<Class<?>> getComponentsToKeep() {
        return componentsToKeep.build();
    }

    public void addFocusComponents(String name, Collection<JComponent> collection) {
        focusComponents.putAll(name, collection);
    }

    public void addFocusComponent(String name, JComponent editor) {
        focusComponents.put(name, editor);
    }

    public UIInitializerContext<U> startFirstPass() {
        log.info(String.format("%sInit widgets - first pass", prefix));
        this.initState = Init.FIRST_PASS;
        return this;
    }

    public UIInitializerContext<U> startSecondPass() {
        log.info(String.format("%sInit widgets - second pass", prefix));
        this.initState = Init.SECOND_PASS;
        return this;
    }

    public <C> UIInitializerContext<U> onComponents(Class<C> componentType, boolean keep, Consumer<C> consumer) {
        if (keep) {
            componentsToKeep.add(componentType);
        }
        getComponents(componentType).forEach(consumer);
        return this;
    }

    public <C> UIInitializerContext<U> onComponents(Class<C> componentType, Consumer<C> consumer) {
        return onComponents(componentType, false, consumer);
    }

    public <C> UIInitializerContext<U> onSubComponents(Class<C> componentType, boolean keep, Consumer<C> consumer) {
        Set<Class<?>> subComponentsTypes = getSubComponentsTypes(componentType);
        if (keep) {
            componentsToKeep.addAll(subComponentsTypes);
        }
        getSubComponents(componentType, subComponentsTypes).forEach(consumer);
        return this;
    }

    public <C> UIInitializerContext<U> onSubComponents(Class<C> componentType, Consumer<C> consumer) {
        return onSubComponents(componentType, false, consumer);
    }

    public void checkFirstPass() {
        check(Init.FIRST_PASS);
    }

    public void checkSecondPass() {
        check(Init.SECOND_PASS);
    }

    public void check(Init initState) {
        if (!Objects.equals(initState, this.initState)) {
            throw new IllegalStateException("Must be in " + this + " but still on " + initState);
        }
    }

    @SuppressWarnings("unchecked")
    public <C> Stream<C> getComponents(Class<C> componentType) {
        return (Stream<C>) typedComponents.get().get(componentType).stream();
    }

    @SuppressWarnings("unchecked")
    public <C> Stream<C> getSubComponents(Class<C> componentType, Set<Class<?>> componentTypes) {
        ArrayListMultimap<Class<?>, Object> multimap = typedComponents.get();
        return (Stream<C>) multimap.asMap().entrySet().stream().filter(e -> componentTypes.contains(e.getKey())).flatMap(e -> e.getValue().stream()).distinct();
    }

    public <C> Set<Class<?>> getSubComponentsTypes(Class<C> componentType) {
        ArrayListMultimap<Class<?>, Object> multimap = typedComponents.get();
        return multimap.asMap().keySet().stream().filter(componentType::isAssignableFrom).collect(Collectors.toSet());

    }

    public UIInitializerContext<U> apply(Runnable consumer) {
        consumer.run();
        return this;
    }

    public ArrayListMultimap<Class<?>, Object> getKeptComponents() {
        ArrayListMultimap<Class<?>, Object> result = ArrayListMultimap.create();
        getComponentsToKeep().forEach(type -> result.putAll(type, typedComponents.get().get(type)));
        return result;
    }
}
