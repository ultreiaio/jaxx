/*
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.awt.Container;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The <code>JAXXObject</code> interface is implemented by all classes
 * produced by the JAXX compiler.
 */
public interface JAXXObject extends JAXXContext, Serializable {

    /**
     * Initialize a {@link JAXXObject}.
     *
     * This method is always
     * @param ui jaxx object to initialize
     * @param createHandler to create handler (can return {@code null}, for ui without handler)
     * @param createComponents to create components
     * @param registerDataBindings to register data bindings
     * @param finalizeCreateComponents to finalize components
     * @param registerActions to register actions
     * @param applyDataBindings to apply data bindings
     * @param setProperties to set properties
     * @param finalizeInitialize to finalize init
     */
    static void initialize(JAXXObject ui,
                           Supplier<UIHandler<? extends JAXXObject>> createHandler,
                           Runnable createComponents,
                           Runnable registerDataBindings,
                           Runnable finalizeCreateComponents,
                           Runnable registerActions,
                           Runnable applyDataBindings,
                           Runnable setProperties,
                           Runnable finalizeInitialize) {
        @SuppressWarnings("unchecked") UIHandler<JAXXObject> handler = (UIHandler<JAXXObject>) createHandler.get();
        if (handler != null) {
            handler.beforeInit(ui);
        }
        createComponents.run();
        registerDataBindings.run();
        finalizeCreateComponents.run();
        registerActions.run();
        if (handler != null) {
            handler.registerActions(ui);
        }
        applyDataBindings.run();
        setProperties.run();
        finalizeInitialize.run();
        if (handler != null) {
            handler.afterInit(ui);
        }
    }

    /**
     * Retrieves an object defined in an XML tag by its ID.
     *
     * @param id the id of the component to retrieve
     * @return the object
     */
    Object getObjectById(String id);

    /**
     * Pretrieves the dictonary of knwon objects indexed by their ids.
     *
     * @return the dictonary of objects.
     */
    Map<String, Object> get$objectMap();

    /**
     * @return the {@link JAXXContext} attached to the object
     */
    JAXXContext getDelegateContext();

    /**
     * @return all the databinding registred on the jaxx object
     */
    JAXXBinding[] getDataBindings();

    /**
     * Register a new binding in the jaxx object.
     *
     * @param binding the binding to add
     */
    void registerDataBinding(JAXXBinding binding);

    /**
     * Apply the data bind by name and then process it.
     *
     * @param id the id of the databinding
     */
    void applyDataBinding(String id);

    /**
     * Processes a data binding by name.  Data binding names are comprised of an object ID and a property name:
     * for example, the data binding in the tag <code>&lt;JLabel id='label' text='{foo.getText()}'/&gt;</code> is
     * named <code>"label.text"</code>.  Processing a data binding causes it to reevaluate its expression, in this
     * case <code>foo.getText()</code>.
     *
     * @param dest the name of the data binding to run
     */
    void processDataBinding(String dest);

    /**
     * Processes a data binding by name.  Data binding names are comprised of an object ID and a property name:
     * for example, the data binding in the tag <code>&lt;JLabel id='label' text='{foo.getText()}'/&gt;</code> is
     * named <code>"label.text"</code>.  Processing a data binding causes it to reevaluate its expression, in this
     * case <code>foo.getText()</code>.
     *
     * @param dest  the name of the data binding to run
     * @param force flag to force binding, even if already on run
     */
    void processDataBinding(String dest, boolean force);

    /**
     * Remove a databinding by name.
     *
     * @param id the name of databinding to remove
     */
    void removeDataBinding(String id);

    /**
     * Obtain a binding given his id.
     *
     * @param bindingId the id of the binding
     * @return the binding, or {@code null} if not found.
     * @see JAXXBinding
     * @since 2.4.2
     */
    JAXXBinding getDataBinding(String bindingId);

//    /**
//     * All <code>JAXXObject</code> implements are capable of broadcasting <code>PropertyChangeEvent</code>, and
//     * furthermore (for technical reasons) must allow code in outside packages, specifically the JAXX runtime,
//     * to trigger these events.
//     *
//     * @param name     the name of the property which changed
//     * @param oldValue the old value of the property
//     * @param newValue the new value of the property
//     */
//    void firePropertyChange(String name, Object oldValue, Object newValue);

    /**
     * Register a general {@link PropertyChangeListener}.
     *
     * @param listener the listener to register
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Register a  {@link PropertyChangeListener}. for the given {@code propertyName}.
     *
     * @param property the property name to listen
     * @param listener the listener to register
     */
    void addPropertyChangeListener(String property, PropertyChangeListener listener);

    /**
     * Unregister a general {@link PropertyChangeListener}.
     *
     * @param listener the listener to unregister
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Unregister a  {@link PropertyChangeListener}. for the given {@code propertyName}.
     *
     * @param property the property name to listen
     * @param listener the listener to unregister
     */
    void removePropertyChangeListener(String property, PropertyChangeListener listener);

    /**
     * Return parent's container corresponding to the Class clazz
     *
     * @param <O>   type of container to obtain from context
     * @param clazz clazz desired
     * @return parent's container
     */
    <O extends Container> O getParentContainer(Class<O> clazz);

    /**
     * Return parent's container corresponding to the Class clazz
     *
     * @param <O>   type of container to obtain from context
     * @param top   the top container
     * @param clazz desired
     * @return parent's container
     */
    <O extends Container> O getParentContainer(Object top, Class<O> clazz);
}
