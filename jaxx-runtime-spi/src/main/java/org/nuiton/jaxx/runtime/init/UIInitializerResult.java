package org.nuiton.jaxx.runtime.init;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;

import javax.swing.JComponent;
import java.util.List;
import java.util.stream.Stream;

/**
 * Result of init.
 * <p>
 * Created on 10/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class UIInitializerResult {

    private final ArrayListMultimap<String, JComponent> focusComponents;
    private final ImmutableList<Object> dependencies;
    private final ArrayListMultimap<Class<?>, Object> components;

    public UIInitializerResult(UIInitializerContext<?> context) {
        this.focusComponents = context.getFocusComponents();
        this.dependencies = context.getDependencies();
        this.components = context.getKeptComponents();
    }

    public ImmutableList<Object> getDependencies() {
        return dependencies;
    }

    public ArrayListMultimap<String, JComponent> getFocusComponents() {
        return focusComponents;
    }

    public ArrayListMultimap<Class<?>, Object> getComponents() {
        return components;
    }

    @SuppressWarnings("unchecked")
    public <C> Stream<C> getComponents(Class<C> componentType) {
        return (Stream<C>) components.get(componentType).stream();
    }

    @SuppressWarnings("unchecked")
    public <C> Stream<C> getSubComponents(Class<C> componentType) {
        return (Stream<C>) components.asMap().entrySet().stream().filter(e -> componentType.isAssignableFrom(e.getKey())).flatMap(e -> e.getValue().stream());
    }

    @SuppressWarnings("unchecked")
    public <C> List<C> getComponentsList(Class<C> componentType) {
        return (List<C>) components.get(componentType);
    }

}
