package org.nuiton.jaxx.runtime.bean;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Contract to add on any bean editor.
 *
 * When Jaxx compiles, it will then try to set any available bean defined in a {@code beanScope} attribute on a ancestor component.
 *
 * Created on 31/01/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public interface BeanScopeAware {

    Object getBean();

    void setBean(Object bean);

}
