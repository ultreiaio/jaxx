package org.nuiton.jaxx.runtime.spi;

/*
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;

/**
 * Created on 11/26/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.6
 */
public interface UIHandler<UI extends JAXXObject> {

    /**
     * Invoke by Jaxx java file in {@code $initialize} method before {@code $initialize_01_createComponents} method invocation.
     * <b>Note:</b> this is the very first method invoked in {@code $initialize} method
     *
     * @param ui the ui
     */
    default void beforeInit(UI ui) {
        // do nothing
    }

    /**
     * Invoke by Jaxx java file in {@code $initialize} method after {@code $initialize_02_registerActions} method invocation.
     *
     * @param ui the ui
     */
    default void registerActions(UI ui) {
        // do nothing
    }

    /**
     * Invoke by Jaxx java file in {@code $initialize} method after {@code $initialize_06_finalizeInitialize} method invocation.
     *
     * <b>Note:</b> this is the very last method invoked in {@code $initialize} method
     *
     * @param ui the ui
     */
    default void afterInit(UI ui) {
        // do nothing
    }
}
