package org.nuiton.jaxx.runtime.init;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;

import javax.swing.JComponent;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Created on 10/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public abstract class UIInitializerSupport<UI extends JComponent & JAXXObject, C extends UIInitializerContext<UI>> {

    private static final String CLIENT_PROPERTY_SKIP_BINDING_TO_BEAN = "skipBindingToBean";
    private static final String CLIENT_PROPERTY_SKIP_BINDING_FROM_BEAN = "skipBindingFromBean";
    protected final C initializerContext;

    public static boolean isBindingToBean(JComponent editor) {
        Boolean skip = (Boolean) editor.getClientProperty(CLIENT_PROPERTY_SKIP_BINDING_TO_BEAN);
        return skip == null || !skip;
    }

    public static boolean isBindingFromBean(JComponent editor) {
        Boolean skip = (Boolean) editor.getClientProperty(CLIENT_PROPERTY_SKIP_BINDING_FROM_BEAN);
        return skip == null || !skip;
    }

    public static <J extends JComponent & BeanScopeAware, O> void bindFromBean(J editor, String property, Function<J, O> valueGetter, BiConsumer<J, O> valueSetter) {
        JavaBean bean = (JavaBean) editor.getBean();
        if (property != null && bean != null) {
            if (isBindingFromBean(editor)) {
                bean.addPropertyChangeListener(property, e -> {
                    O oldValue = valueGetter.apply(editor);
                    @SuppressWarnings("unchecked") O newValue = (O) e.getNewValue();
                    if (!Objects.equals(oldValue, newValue)) {
                        valueSetter.accept(editor, newValue);
                    }
                });
            }
        }
    }

    public UIInitializerSupport(UI ui, Class<?>... types) {
        initializerContext = createIInitializerContext(ui, types);
    }

    protected abstract void initUI(C initializerContext);

    protected abstract C createIInitializerContext(UI ui, Class<?>... types);

    public UIInitializerResult initUI() {
        initUI(initializerContext);
        return build(initializerContext);
    }

    protected UIInitializerResult build(C initializerContext) {
        return new UIInitializerResult(initializerContext);
    }

    public void registerDependencies(Object... dependencies) {
        initializerContext.registerDependencies(dependencies);
    }
}
