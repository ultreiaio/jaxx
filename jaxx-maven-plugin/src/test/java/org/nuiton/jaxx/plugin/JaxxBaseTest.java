/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import com.google.common.collect.ImmutableSet;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nModuleConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;
import org.nuiton.jaxx.compiler.decorators.DefaultCompiledObjectDecorator;
import org.nuiton.jaxx.runtime.context.DefaultJAXXContext;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.plugin.MojoTestRule;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base test case for a jaxx:generate goal.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see GenerateMojo
 */
public abstract class JaxxBaseTest {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(JaxxBaseTest.class);

    @Rule
    public final MojoTestRule<GenerateMojo> rule = new MojoTestRule<GenerateMojo>(getClass(), "generate") {

        @Override
        protected void setUpMojo(GenerateMojo mojo, File pomFile) throws Exception {

            try {
//                    MavenProject project = mojo.getProject();
//                    if (project == null) {
//                        log.debug("init maven project");
//                        ProjectBuilder projectBuilder = lookup(ProjectBuilder.class);
//                        ProjectBuildingRequest projectBuildingRequest = new DefaultProjectBuildingRequest();
//                        projectBuildingRequest.setRepositorySession(new DefaultRepositorySystemSession());
//                        projectBuildingRequest.setResolveDependencies(true);
//                        DefaultArtifactRepositoryFactory lookup = (DefaultArtifactRepositoryFactory)lookup(ArtifactRepositoryFactory.class);
//                        projectBuildingRequest.setLocalRepository(lookup.createArtifactRepository("yo",getTestDirectory().getAbsolutePath(),ArtifactRepositoryFactory.DEFAULT_LAYOUT_ID,null,null ));
//                        ProjectBuildingResult projectBuildingResult = projectBuilder.build(pomFile, projectBuildingRequest);
//                        project = projectBuildingResult.getProject();
//                        mojo.setProject(project);
//                    }
                super.setUpMojo(mojo, pomFile);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error(e);
                }
            }
            mojo.setEncoding("UTF-8");
            mojo.validatorFactoryFQN = SwingValidator.class.getName();
            mojo.jaxxContextFQN = DefaultJAXXContext.class.getName();
            mojo.compilerFQN = JAXXCompiler.class.getName();
            mojo.defaultDecoratorFQN = DefaultCompiledObjectDecorator.class.getName();

            I18nModuleConfiguration i18nModuleConfiguration = new I18nModuleConfiguration(
                    mojo.getProject().getGroupId(),
                    mojo.getProject().getArtifactId(),
                    mojo.getTargetDirectory().getParentFile().toPath().resolve("i18n"),
                    mojo.getTargetDirectory().getParentFile().toPath().resolve("i18n"),
                    mojo.getTargetDirectory().getParentFile().toPath().resolve("i18n"),
                    mojo.getTargetDirectory().getParentFile().toPath().resolve("i18n"),
                    ImmutableSet.of(Locale.FRANCE),
                    StandardCharsets.UTF_8,
                    false,
                    true,
                    I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION
            );
            I18nModuleConfiguration.store(mojo.getProject().getProperties(), i18nModuleConfiguration);
            mojo.setI18nModule(I18nModule.forGetter(mojo.getProject().getProperties()));
        }

        @Override
        public boolean initMojo(GenerateMojo mojo) {
            return super.initMojo(mojo);
        }
    };

//    @Override
//    protected String getGoalName(String methodName) {
//        return "generate";
//    }
//
//    @Override
//    protected void setUpMojo(GenerateMojo mojo, File pomFile) throws Exception {
//        try {
//            super.setUpMojo(mojo, pomFile);
//        } catch (Exception e) {
//            if (log.isErrorEnabled()) {
//                log.error(e);
//            }
//        }
//        mojo.setEncoding("UTF-8");
//        mojo.validatorFactoryFQN= SwingValidator.class.getName();
//        mojo.jaxxContextFQN = DefaultJAXXContext.class.getName();
//        mojo.compilerFQN = JAXXCompiler.class.getName();
//        mojo.defaultDecoratorFQN = DefaultCompiledObjectDecorator.class.getName();
//    }

    protected void checkPattern(GenerateMojo mojo,
                                String pattern,
                                boolean required,
                                String... files) throws IOException {
        if (files.length == 0) {
            files = mojo.files;
        }
        for (String file : files) {
            // check we have a the required/forbidden pattern
            File f = new File(mojo.outJava, file.substring(0, file.length() - 4) + "java");
            if (mojo.isVerbose()) {
                log.info("check generated file " + f);
            }

            assertTrue("generated file " + f + " was not found...", f.exists());
            String content = new String(Files.readAllBytes(f.toPath()), StandardCharsets.UTF_8);

            String errorMessage = required ? "could not find the pattern : " : "should not have found pattern :";
            assertEquals(errorMessage + pattern + " in file " + f, required, content.contains(pattern));
        }
    }

    public GenerateMojo getMojo() {
        return rule.getMojo();
    }

    protected void assertNumberJaxxFiles(int expectedNbFiles) {
        if (expectedNbFiles == 0) {
            assertTrue(getMojo().files == null || getMojo().files.length == 0);
        } else {
            assertEquals(expectedNbFiles, getMojo().files.length);
        }
    }

    @SuppressWarnings("unchecked")
    protected void assertError(JAXXEngine engine, String file, int nbCompiler) {

        JAXXCompilerFile[] compilers = engine.getCompiledFiles();
        assertEquals(nbCompiler, compilers.length);
        List<String> errors = engine.getErrors();
        assertTrue("should have found at least one error for " + file, errors != null && !errors.isEmpty());
    }
}
