<!--
  #%L
  JAXX :: Maven plugin
  %%
  Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<Application title="Validation.jaxx">

  <!-- models -->
  <Model id='model'/>
  <Model id='model2'/>
  <Identity id='identity'/>

  <!-- errors model -->
  <jaxx.runtime.validator.swing.SwingValidatorMessageListModel id='errors'
                                                               onContentsChanged='ok.setEnabled(errors.isEmpty())'/>

  <!-- validators -->
  <BeanValidator id='validator' bean='model' errorListModel='errors'>
    <field name="text"/>
    <field name="text2"/>
    <field name="ratio"/>
  </BeanValidator>
  <BeanValidator id='validator2' bean='model2' errorListModel='errors'
                 uiClass="org.nuiton.jaxx.validator.swing.ui.IconValidationUI">
    <field name="text" component="_text"/>
    <field name="text2" component="_text2"/>
    <field name="ratio" component="_ratio"/>
  </BeanValidator>
  <BeanValidator id='validator3' autoField='true' bean='identity' errorListModel='errors'
                 uiClass="org.nuiton.jaxx.validator.swing.ui.TranslucentValidationUI">
    <field name="email" component="email2"/>
  </BeanValidator>

  <Table fill='both'>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Form")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='text' text='{model.getText()}'
                            onKeyReleased='model.setText(text.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='text2' text='{model.getText2()}'
                            onKeyReleased='model.setText2(text2.getText())'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JSlider id='ratio' minimum='0' maximum='100' value='{model.getRatio()}'
                         onStateChanged='model.setRatio(ratio.getValue())'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Model")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model.getText()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model.getText2()}'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JLabel text='{model.getRatio()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Form2")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='_text' text='{model2.getText()}'
                            onKeyReleased='model2.setText(_text.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='_text2' text='{model2.getText2()}'
                            onKeyReleased='model2.setText2(_text2.getText())'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JSlider id='_ratio' minimum='0' maximum='100' value='{model2.getRatio()}'
                         onStateChanged='model2.setRatio(_ratio.getValue())'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Model2")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model2.getText()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model2.getText2()}'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JLabel text='{model2.getRatio()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Identify Form")}'
                layout='{new GridLayout()}' width='250' height='140'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='FirstName:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='firstName' text='{identity.getFirstName()}'
                            onKeyReleased='identity.setFirstName(firstName.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='LastName:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='lastName' text='{identity.getLastName()}'
                            onKeyReleased='identity.setLastName(lastName.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Email:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='email2' text='{identity.getEmail()}'
                            onKeyReleased='identity.setEmail(email2.getText())'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Age:'/>
              </cell>
              <cell>
                <JSlider id='age' minimum='0' maximum='100' value='{identity.getAge()}'
                         onStateChanged='identity.setAge(age.getValue())'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Identity Model")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='FirstName:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getFirstName()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='LastName:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getLastName()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Email:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getEmail()}'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Age:'/>
              </cell>
              <cell>
                <JLabel text='{identity.getAge()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell columns='2' fill="both">
        <JPanel border='{BorderFactory.createTitledBorder("Errors")}' layout='{new GridLayout()}' height='200'
                width='500'>
          <JScrollPane>
            <JList model='{errors}'/>
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell columns='2' fill="both">
        <JPanel layout='{new GridLayout(1,2,0,0)}'>
          <JButton text='cancel' onActionPerformed='dispose()'/>
          <JButton id='ok' text='valid' onActionPerformed='dispose()'/>
        </JPanel>
      </cell>
    </row>

  </Table>
</Application>
