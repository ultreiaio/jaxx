package org.nuiton.jaxx.runtime.application.action.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.application.action.ActionExecutor;
import org.nuiton.jaxx.runtime.application.action.ActionWorker;

import java.util.EventObject;

/**
 * Created by tchemit on 28/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActionExecutorEvent extends EventObject {

    private final ActionExecutor executor;

    public ActionExecutorEvent(ActionWorker source, ActionExecutor executor) {
        super(source);
        this.executor = executor;
    }

    @Override
    public ActionWorker getSource() {
        return (ActionWorker) super.getSource();
    }

    public ActionExecutor getExecutor() {
        return executor;
    }
}
