package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.application.action.ActionExecutor;

import java.io.Closeable;

/**
 * Created by tchemit on 26/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ApplicationBootInitializer<Config extends ApplicationConfiguration, Context extends ApplicationContext<Config, Context>> extends Closeable {

    /**
     * @return arguments of boot (options used by configuration)
     */
    String[] getArgs();

    /**
     * @return {@code true} if jvm should be halt on shutdown (used for tests...).
     * @see ApplicationBoot#exit(int)
     */
    boolean haltOnExit();

    /**
     * Create configuration.
     *
     * @param boot incoming boot
     * @return initialized configuration
     */
    Config createConfiguration(ApplicationBoot<Config, Context> boot) throws ApplicationBootInitializerException;

    /**
     * Create context.
     *
     * @param boot          incoming boot
     * @param configuration incoming configuration
     * @return initialized context
     */
    Context createContext(ApplicationBoot<Config, Context> boot, Config configuration) throws ApplicationBootInitializerException;

    /**
     * Create executor.
     *
     * @param boot          incoming boot
     * @param configuration incoming configuration
     * @param context       incoming context
     * @return initialized executor
     */
    ActionExecutor createExecutor(ApplicationBoot<Config, Context> boot, Config configuration, Context context);

    /**
     * Code to invoke only once.
     */
    void initOnce() throws ApplicationBootInitializerException;

    /**
     * Code to invoke at each start onf application.
     * @param configuration incoming configuration
     * @param context       incoming context
     */
    void init(Config configuration, Context context) throws ApplicationBootInitializerException;

    /**
     * Code to invoke only once when application is shutting down.
     */
    @Override
    void close();

    /**
     * Code to invoke at each start just after execution of method {@link #init(ApplicationConfiguration, ApplicationContext)}.
     *
     * @param configuration incoming configuration
     * @param context       incoming context
     */
    void start(Config configuration, Context context);

}
