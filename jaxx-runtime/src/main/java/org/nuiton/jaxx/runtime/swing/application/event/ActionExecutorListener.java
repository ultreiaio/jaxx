package org.nuiton.jaxx.runtime.swing.application.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;

import java.util.EventListener;

/**
 * To listen execution on action {@link ActionWorker} in {@link ActionExecutor}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public interface ActionExecutorListener extends EventListener {

    /**
     * When action has started.
     *
     * @param event the action executor event
     * @see ActionExecutor#onActionStart(ActionWorker)
     */
    void actionStart(ActionExecutorEvent event);

    /**
     * When action has failed.
     *
     * @param event the action executor event
     * @see ActionExecutor#onActionFail(ActionWorker)
     */
    void actionFail(ActionExecutorEvent event);

    /**
     * When action has cancel.
     *
     * @param event the action executor event
     * @see ActionExecutor#onActionCancel(ActionWorker)
     */
    void actionCancel(ActionExecutorEvent event);

    /**
     * When action has end.
     *
     * @param event the action executor event
     * @see ActionExecutor#onActionEnd(ActionWorker)
     */
    void actionEnd(ActionExecutorEvent event);

    /**
     * When action has done.
     *
     * @param event the action executor event
     * @see ActionExecutor#onAfterAction(ActionWorker)
     */
    void actionDone(ActionExecutorEvent event);

}
