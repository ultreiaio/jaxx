package org.nuiton.jaxx.runtime.swing.session;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.awt.Component;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SwingSessionHelper implements Closeable {

    private static final Logger log = LogManager.getLogger(SwingSessionHelper.class);

    private final SwingSession session;

    public SwingSessionHelper(File file) {
        session = new SwingSession(file, false);
    }

    public void addComponent(Component c, boolean replace) {
        session.add(c, replace);
    }

    public void removeComponent(Component c) {
        session.remove(c);
    }

    public void save() {
        try {
            session.save();
        } catch (IOException e) {
            log.error(String.format("Could not save swing session on file %s", session.getFile()), e);
        }
    }

    @Override
    public void close() {
        save();
    }

}
