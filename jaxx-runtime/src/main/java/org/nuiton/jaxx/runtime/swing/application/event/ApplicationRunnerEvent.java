package org.nuiton.jaxx.runtime.swing.application.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;

import java.util.EventObject;
import java.util.Objects;

/**
 * Event send by  {@link ApplicationRunnerListener}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ApplicationRunnerEvent extends EventObject {

    private boolean reload;
    private Exception exception;

    public ApplicationRunnerEvent(ApplicationRunner source) {
        super(Objects.requireNonNull(source));
    }

    public ApplicationRunnerEvent(ApplicationRunner source, boolean reload) {
        this(source);
        this.reload = reload;
    }

    public ApplicationRunnerEvent(ApplicationRunner source, Exception exception) {
        this(source);
        this.exception = Objects.requireNonNull(exception);
    }

    @Override
    public ApplicationRunner getSource() {
        return (ApplicationRunner) super.getSource();
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public boolean isReload() {
        return reload;
    }

    public void setReload(boolean reload) {
        this.reload = reload;
    }
}
