package org.nuiton.jaxx.runtime.application.action.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.application.action.ActionWorker;
import org.nuiton.jaxx.runtime.application.action.WizardActionWorker;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

/**
 * Created by tchemit on 27/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class WizardActionWorkerExecutorListener implements ActionExecutorListener {

    private static final Logger log = LogManager.getLogger(WizardActionWorkerExecutorListener.class);

    @Override
    public void onActionStart(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        if (source instanceof WizardActionWorker) {
            WizardActionWorker admin = (WizardActionWorker) source;
            WizardExtModel model = admin.getModel();
            model.setBusy(true);
            model.setStepState(WizardState.RUNNING);
        }
    }

    @Override
    public void onActionFail(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        Exception error = source.getError();
        if (source instanceof WizardActionWorker) {
            WizardActionWorker<?, ?> admin = (WizardActionWorker) source;
            WizardExtModel<?> model = admin.getModel();
            model.setErrorOnStepModel(error);
            model.setStepState(WizardState.FAILED);
        }
    }

    @Override
    public void onActionCancel(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        if (source instanceof WizardActionWorker) {
            WizardActionWorker<?, ?> admin = (WizardActionWorker) source;
            WizardExtModel model = admin.getModel();
            model.setStepState(WizardState.CANCELED);
        }
    }

    @Override
    public void onActionEnd(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        if (source instanceof WizardActionWorker) {
            WizardActionWorker<?, ?> admin = (WizardActionWorker) source;
            WizardState state;
            try {
                state = admin.get();
                admin.getModel().setStepState(state);
            } catch (Exception e) {
                log.error(String.format("Could not get data from worker %s", admin), e);
            }
        }
    }

    @Override
    public void onAfterAction(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        if (source instanceof WizardActionWorker) {
            WizardActionWorker<?, ?> admin = (WizardActionWorker) source;
            WizardExtModel model = admin.getModel();
            model.setBusy(false);
        }
    }

}
