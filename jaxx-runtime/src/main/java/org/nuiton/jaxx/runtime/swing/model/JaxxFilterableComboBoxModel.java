package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * ComboBoxModel which can filter the elements displayed in the popup.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.12
 */
@SuppressWarnings("unused")
public class JaxxFilterableComboBoxModel<E> extends JaxxDefaultComboBoxModel<E> {

    public static final Character DEFAULT_WILDCARD_CHARACTER = '*';
    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(JaxxFilterableComboBoxModel.class);
    protected final List<E> filteredItems = new ArrayList<>();
    protected final List<Predicate<E>> filters = new ArrayList<>();
    protected String filterText;
    protected Character wildcardCharacter = DEFAULT_WILDCARD_CHARACTER;
    /**
     * the decorator of data
     */
    protected Decorator decorator;
    private boolean adjusting;

    public JaxxFilterableComboBoxModel() {
    }

    @SafeVarargs
    public JaxxFilterableComboBoxModel(E... items) {
        this(Arrays.asList(items));
    }

    public JaxxFilterableComboBoxModel(Collection<E> v) {
        delegate = new ArrayList<>(v);
    }

    @Override
    public int getIndexOf(E anObject) {
        return filteredItems.indexOf(anObject);
    }

    @Override
    public void addAllElements(Collection<E> objects) {
        super.addAllElements(objects);
        applyFilter();
    }

    @Override
    public void removeAllElements() {
        super.removeAllElements();
        applyFilter();
    }

    @Override
    public int getSize() {
        return filteredItems.size();
    }

    @Override
    public E getElementAt(int index) {
        E result;

        if (index >= 0 && index < filteredItems.size()) {
            result = filteredItems.get(index);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public void addElement(Object anObject) {
        super.addElement(anObject);
        applyFilter();
    }

    @Override
    public void insertElementAt(Object anObject, int index) {
        super.insertElementAt(anObject, index);
        applyFilter();
    }

    @Override
    public void removeElementAt(int index) {
        super.removeElementAt(index);
        applyFilter();
    }

    @Override
    public void removeElement(Object anObject) {
        super.removeElement(anObject);
        applyFilter();
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        String oldFilterText = this.filterText;
        this.filterText = filterText;
        log.info(String.format("Set new filter text: '%s'", filterText));
        applyFilter();
//        if (!Objects.equals(oldFilterText, filterText)) {
//        }
    }

    public Character getWildcardCharacter() {
        return wildcardCharacter;
    }

    public void setWildcardCharacter(Character wildcardCharacter) {
        this.wildcardCharacter = Objects.requireNonNull(wildcardCharacter, "WildcardCharacter can not be null");
        applyFilter();

    }

    public Decorator getDecorator() {
        return decorator;
    }

    public void setDecorator(Decorator decorator) {
        this.decorator = decorator;
    }

    public void addFilter(Predicate<E> filter) {
        filters.add(filter);
        applyFilter();
    }

    public void removeFilter(Predicate<E> filter) {
        filters.remove(filter);
        applyFilter();
    }

    public void clearFilters() {
        filters.clear();
        applyFilter();
    }

    public void refreshFilteredElements() {
        applyFilter();
    }

    protected void applyFilter() {
        filteredItems.clear();

        if ((StringUtils.isEmpty(filterText)
                || wildcardCharacter != null && StringUtils.isEmpty(StringUtils.remove(filterText, wildcardCharacter)))
                && filters.isEmpty()) {
            log.info(String.format("Using all items (filterText: '%s')", filterText));
            filteredItems.addAll(delegate);

        } else {
            Pattern pattern = null;
            if (StringUtils.isNotBlank(filterText)) {
                String patternText = Pattern.quote(filterText).replace(wildcardCharacter.toString(), "\\E.*\\Q") + ".*";
                pattern = Pattern.compile(patternText, Pattern.CASE_INSENSITIVE);
            }

            boolean withoutPattern = pattern == null;
            for (E element : delegate) {
                boolean addElement = canAddElement(element);
                if (!addElement) {
                    break;
                }
                String decoratedElement = decorateElement(element);
                boolean matches = withoutPattern || pattern.matcher(decoratedElement).matches();
                if (matches) {
                    filteredItems.add(element);
                }
            }
        }

        log.info("After apply filter, nb items: " + getSize());

        if (!isAdjusting()) {
            fireContentsChanged(this, 0, getSize());
        }
    }

    public String decorateElement(E element) {
        return decorator == null ? String.valueOf(element) : decorator.decorate(element);
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    protected boolean canAddElement(E element) {
        for (Predicate<E> filter : filters) {
            if (!filter.test(element)) {
                return false;
            }
        }
        return true;
    }
}
