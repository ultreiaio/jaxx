package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 02/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ApplicationInstances {

    public static ApplicationContext<?, ?> context() {
        return ApplicationBoot.boot().getContext();
    }

    public static <C extends ApplicationContext> C context(Class<C> contextType) {
        return contextType.cast(context());
    }

    public static ApplicationConfiguration configuration() {
        return ApplicationBoot.boot().getContext().getConfig();
    }

    public static <C extends ApplicationConfiguration> C configuration(Class<C> contextType) {
        return contextType.cast(configuration());
    }

    public static ApplicationConfiguration config() {
        return context().getConfig();
    }
}
