package org.nuiton.jaxx.runtime.application.command;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.application.ApplicationConfiguration;
import org.nuiton.jaxx.runtime.application.ApplicationContext;

import java.awt.Frame;

/**
 * Action to start main ui.
 * <p>
 * Created by tchemit on 08/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class StartDefaultUI<Config extends ApplicationConfiguration, Context extends ApplicationContext<Config, Context>> extends WithMainUICommandSupport<Config, Context> {

    public StartDefaultUI() {
    }

    @Override
    protected void run(Frame ui) {

    }
}
