package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.ApplicationConfig;
import io.ultreia.java4all.config.ArgumentsParserException;
import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import io.ultreia.java4all.config.spi.ConfigActionDef;
import io.ultreia.java4all.config.spi.ConfigOptionDef;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Version;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 27/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ApplicationConfiguration {

    static Properties loadProperties(Properties sourceProperties, ApplicationConfig config) {
        Properties targetProperties = new Properties();
        for (Map.Entry<Object, Object> entry : sourceProperties.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            String newValue = config.replaceRecursiveOptions(value);
            targetProperties.setProperty(key, newValue);
        }
        return targetProperties;
    }

    static List<String> loadProperties(List<String> sourceProperties, ApplicationConfig config) {
        List<String> targetProperties = new LinkedList<>();
        for (String entry : sourceProperties) {
            String newValue = config.replaceRecursiveOptions(entry);
            targetProperties.add(newValue);
        }
        return targetProperties;
    }

    ApplicationConfig get();

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_ID} configuration option.
     *
     * @return id of application (this is used as prefix of everything in application).
     */
    default String getApplicationId() {
        return get().getOption(DefaultOptions.APPLICATION_ID.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_NAME} configuration option.
     *
     * @return name of application
     */
    default String getApplicationName() {
        return get().getOption(DefaultOptions.APPLICATION_NAME.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_VERSION} configuration option.
     *
     * @return stable version of application (remove any snapshot suffix)
     */
    default Version getApplicationVersion() {
        return get().getOptionAsVersion(DefaultOptions.APPLICATION_VERSION.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_ORGANIZATION_NAME} configuration option.
     *
     * @return organization name of application
     */
    default String getApplicationOrganizationName() {
        return get().getOption(DefaultOptions.APPLICATION_ORGANIZATION_NAME.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_ORGANIZATION_URL} configuration option.
     *
     * @return organization url of application
     */
    default URL getApplicationOrganizationUrl() {
        return get().getOptionAsURL(DefaultOptions.APPLICATION_ORGANIZATION_URL.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_INCEPTION_YEAR} configuration option.
     *
     * @return inceptionYear of application
     */
    default int getApplicationInceptionYear() {
        return get().getOptionAsInt(DefaultOptions.APPLICATION_INCEPTION_YEAR.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_LOCALE} configuration option.
     *
     * @return locale of application
     */
    default Locale getApplicationLocale() {
        return get().getOptionAsLocale(DefaultOptions.APPLICATION_LOCALE.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_LOCALE} configuration option.
     *
     * @param applicationLocale new locale of application
     */
    default void setApplicationLocale(Locale applicationLocale) {
        get().setOption(DefaultOptions.APPLICATION_LOCALE.key, applicationLocale.toString());
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_SITE_URL} configuration option.
     *
     * @return site url of application
     */
    default URL getApplicationSiteUrl() {
        return get().getOptionAsURL(DefaultOptions.APPLICATION_SITE_URL.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_LICENSE_NAME} configuration option.
     *
     * @return license name of application
     */
    default String getApplicationLicenseName() {
        return get().getOption(DefaultOptions.APPLICATION_LICENSE_NAME.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_LICENSE_URL} configuration option.
     *
     * @return license url of application
     */
    default URL getApplicationLicenseUrl() {
        return get().getOptionAsURL(DefaultOptions.APPLICATION_LICENSE_URL.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_BUILD_VERSION} configuration option.
     *
     * @return version of build
     */
    default Version getApplicationBuildVersion() {
        return get().getOptionAsVersion(DefaultOptions.APPLICATION_BUILD_VERSION.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_BUILD_DATE} configuration option.
     *
     * @return date of build
     */
    default Date getApplicationBuildDate() {
        return get().getOptionAsDate(DefaultOptions.APPLICATION_BUILD_DATE.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#APPLICATION_BUILD_NUMBER} configuration option.
     *
     * @return number of build
     */
    default String getApplicationBuildNumber() {
        return get().getOption(DefaultOptions.APPLICATION_BUILD_NUMBER.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#I18N_DIRECTORY} configuration option.
     *
     * @return i18n directory
     */
    default File getI18nDirectory() {
        return get().getOptionAsFile(DefaultOptions.I18N_DIRECTORY.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#I18N_DEFINITION_FILE} configuration option.
     *
     * @return i18n definition file
     */
    default File getI18nDefinitionFile() {
        return get().getOptionAsFile(DefaultOptions.I18N_DEFINITION_FILE.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#I18N_BUNDLE_NAME} configuration option.
     *
     * @return name of i18n bundle
     */
    default String getI18nBundleName() {
        return get().getOption(DefaultOptions.I18N_BUNDLE_NAME.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#I18N_PROVIDER_NAME} configuration option.
     *
     * @return name of i18n provider
     */
    default String getI18nProviderName() {
        return get().getOption(DefaultOptions.I18N_PROVIDER_NAME.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#I18N_PREFIX} configuration option.
     *
     * @return i18n prefix
     */
    default String getI18nPrefix() {
        return get().getOption(DefaultOptions.I18N_PREFIX.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#DATA_DIRECTORY} configuration option.
     *
     * @return data directory (user directory where to place all data of application)
     */
    default File getDataDirectory() {
        return get().getOptionAsFile(DefaultOptions.DATA_DIRECTORY.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#TMP_DIRECTORY} configuration option.
     *
     * @return temporary directory (content is delete at each launch)
     */
    default File getTmpDirectory() {
        return get().getOptionAsFile(DefaultOptions.TMP_DIRECTORY.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#RESOURCES_DIRECTORY} configuration option.
     *
     * @return resources directory (each version has his own directory)
     */
    default File getResourcesDirectory() {
        return get().getOptionAsFile(DefaultOptions.RESOURCES_DIRECTORY.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#LOG_CONFIGURATION_FILE} configuration option.
     *
     * @return log configuration file
     */
    default File getLogConfigurationFile() {
        return get().getOptionAsFile(DefaultOptions.LOG_CONFIGURATION_FILE.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#UI_SWING_SESSION_FILE} configuration option.
     *
     * @return swing session file
     */
    default File getSwingSessionFile() {
        return get().getOptionAsFile(DefaultOptions.UI_SWING_SESSION_FILE.key);
    }

    /**
     * Note: bind to {@link DefaultOptions#UI_FULL_SCREEN} configuration option.
     *
     * @return full screen state
     */
    default boolean isFullScreen() {
        return get().getOptionAsBoolean(DefaultOptions.UI_FULL_SCREEN.key);
    }

    /**
     * Change full screen state value.
     *
     * @param fullScreen new state value
     */
    default void setFullScreen(boolean fullScreen) {
        get().setOption(DefaultOptions.UI_FULL_SCREEN.key, Boolean.toString(fullScreen));
        get().saveForUser();
    }

    default String getApplicationCopyrightText() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int inceptionYear = getApplicationInceptionYear();
        String copyrightText = String.format("Version %s - %s @ %d", getApplicationVersion(), getApplicationOrganizationName(), inceptionYear);
        if (year != inceptionYear) {
            copyrightText += " - " + year;
        }
        return copyrightText;
    }

    default void printConfigurationOptions(StringBuilder builder) {
        ApplicationConfig applicationConfig = get();
        builder.append(String.format("\n\t%1$-40s → %2$s", "Config file", applicationConfig.getUserConfigFile()));
        List<ConfigOptionDef> options = getAllOptions();
        for (ConfigOptionDef option : options) {
            builder.append(String.format("\n\t%1$-40s → %2$s", option.getKey(), applicationConfig.getOption(option.getKey())));
        }
    }

    default List<ConfigOptionDef> getAllOptions() {
        List<ConfigOptionDef> options = new ArrayList<>(Arrays.asList(DefaultOptions.values()));
        ApplicationConfigProvider applicationConfigProvider = ApplicationConfigProvider.getProvider(getClass().getClassLoader(), getI18nProviderName());
        options.addAll(Arrays.asList(applicationConfigProvider.getOptions()));
        return orderedByKey(options);
    }

    default void printOptions(StringBuilder out, ConfigOptionDef... options) {
        for (ConfigOptionDef o : options) {
            out.append(String.format("\t%s (%s): %s\n", o.getKey(), o.getDefaultValue(), t(o.getDescription())));
        }
    }

    default void printActions(StringBuilder out, ConfigActionDef... actions) {
        for (ConfigActionDef a : actions) {
            out.append(String.format("\t%s (%s): %s\n", Arrays.toString(a.getAliases()), a.getAction(), t(a.getDescription())));
        }
    }

    default void initConfig(Properties p, String... args) throws ArgumentsParserException {
        ApplicationConfig applicationConfig = get();
        applicationConfig.loadDefaultOptions(DefaultOptions.values());
        for (Object k : p.keySet()) {
            String key = String.valueOf(k);
            Object value = p.get(k);
            String strValue = String.valueOf(value);
            applicationConfig.setDefaultOption(key, strValue);
        }
        applicationConfig.parse(args);
        Version version = applicationConfig.getOptionAsVersion(DefaultOptions.APPLICATION_VERSION.key);
        Objects.requireNonNull(version, String.format("Could not find default option %s.", DefaultOptions.APPLICATION_VERSION.key));
        if (version.isSnapshot()) {
            applicationConfig.setOption(DefaultOptions.APPLICATION_VERSION.key, null);
            applicationConfig.setDefaultOption(DefaultOptions.APPLICATION_VERSION.key, Version.removeSnapshot(version).getVersion());
        }
        for (DefaultOptions defaultOption : DefaultOptions.values()) {
            String defaultOptionValue = applicationConfig.getOption(defaultOption.key);
            Objects.requireNonNull(defaultOptionValue, String.format("Could not find default option %s.", defaultOption.key));
        }
    }

    default List<ConfigOptionDef> orderedByKey(List<ConfigOptionDef> options) {
        List<ConfigOptionDef> values = new ArrayList<>(options);
        values.sort(Comparator.comparing(ConfigOptionDef::getKey));
        return Collections.unmodifiableList(values);
    }

    void removeJaxxPropertyChangeListener();

    enum DefaultOptions implements ConfigOptionDef {

        APPLICATION_ID(
                String.class,
                "application.id",
                I18n.n(""),
                null
        ),

        APPLICATION_NAME(
                String.class,
                "application.name",
                I18n.n(""),
                null
        ),

        APPLICATION_VERSION(
                Version.class,
                "application.version",
                I18n.n(""),
                null
        ),

        APPLICATION_LOCALE(
                Locale.class,
                "application.locale",
                I18n.n(""),
                "fr_FR"
        ),

        APPLICATION_ORGANIZATION_NAME(
                String.class,
                "application.organization.name",
                I18n.n(""),
                null
        ),

        APPLICATION_ORGANIZATION_URL(
                URL.class,
                "application.organization.url",
                I18n.n(""),
                null
        ),

        APPLICATION_LICENSE_NAME(
                String.class,
                "application.license.name",
                I18n.n(""),
                null
        ),

        APPLICATION_LICENSE_URL(
                URL.class,
                "application.license.url",
                I18n.n(""),
                null
        ),

        APPLICATION_SITE_URL(
                URL.class,
                "application.site.url",
                I18n.n(""),
                null
        ),

        APPLICATION_INCEPTION_YEAR(
                int.class,
                "application.inceptionYear",
                I18n.n(""),
                null
        ),

        APPLICATION_BUILD_VERSION(
                Version.class,
                "application.build.version",
                I18n.n(""),
                null
        ),

        APPLICATION_BUILD_DATE(
                Date.class,
                "application.build.date",
                I18n.n(""),
                null
        ),

        APPLICATION_BUILD_NUMBER(
                String.class,
                "application.build.number",
                I18n.n(""),
                null
        ),

        DATA_DIRECTORY(
                File.class,
                "data.directory",
                I18n.n(""),
                "${user.home}/.${application.id}"
        ),

        RESOURCES_DIRECTORY(
                File.class,
                "resources.directory",
                I18n.n(""),
                "${data.directory}/resources-${application.version}"
        ),

        TMP_DIRECTORY(
                File.class,
                "tmp.directory",
                I18n.n(""),
                "${data.directory}/tmp"
        ),

        LOG_CONFIGURATION_FILE(
                File.class,
                "application.log.configuration.file",
                I18n.n(""),
                "${resources.directory}/${application.id}-log4j.properties"
        ),

        UI_SWING_SESSION_FILE(
                File.class,
                "application.ui.swingSessionFile",
                I18n.n(""),
                "${resources.directory}/${application.id}-swing-session.ui.xml"
        ),

        I18N_DIRECTORY(
                File.class,
                "i18n.directory",
                I18n.n(""),
                "${resources.directory}/i18n"
        ),

        I18N_DEFINITION_FILE(
                File.class,
                "i18n.definition.file",
                I18n.n(""),
                "${i18n.directory}/${i18n.bundle.name}-definition.properties"
        ),

        I18N_PREFIX(
                String.class,
                "i18n.prefix",
                I18n.n(""),
                "${application.id}."
        ),

        I18N_BUNDLE_NAME(
                String.class,
                "i18n.bundle.name",
                I18n.n(""),
                "${application.id}-i18n"
        ),
        I18N_PROVIDER_NAME(
                String.class,
                "i18n.provider.name",
                I18n.n(""),
                "${application.id}Config"
        ),

        UI_FULL_SCREEN(boolean.class, "ui.full.screen", I18n.n(""), "false");

        private final Class type;
        private final String key;
        private final String description;
        private String defaultValue;

        DefaultOptions(Class type, String key, String description, String defaultValue) {
            this.type = type;
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class getType() {
            return type;
        }

        @Override
        public String getDescription() {
            return I18n.t(description);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public boolean isTransient() {
            return true;
        }

        @Override
        public void setTransient(boolean _transient) {
        }

        @Override
        public boolean isFinal() {
            return true;
        }

        @Override
        public void setFinal(boolean _final) {
        }

    }
}
