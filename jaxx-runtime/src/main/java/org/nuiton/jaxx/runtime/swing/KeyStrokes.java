package org.nuiton.jaxx.runtime.swing;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.util.LinkedHashSet;
import java.util.Set;

import static javax.swing.Action.ACCELERATOR_KEY;

/**
 * Created by tchemit on 02/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class KeyStrokes {

    public static void addKeyStroke(AbstractButton component, KeyStroke actionKey) {

        String actionStr = SwingUtil.keyStrokeToStr(actionKey);
        if (component.getText() != null && !component.getText().contains(actionStr)) {
            String text = component.getText() + actionStr;
            component.setText(text);
        }
        if (component.getToolTipText() != null && !component.getToolTipText().contains(actionStr)) {
            String tip = component.getToolTipText() + actionStr;
            component.setToolTipText(tip);
        }
    }

    public static String suffixTextWithKeyStroke(String text, KeyStroke keyStroke) {
        text += SwingUtil.keyStrokeToStr(keyStroke);
        return text;
    }

    public static void addKeyStrokeFromMnemonic(AbstractButton editor) {
        int mnemonic = editor.getMnemonic();
        if (mnemonic > 0) {
            String accelerator = " (Alt + " + (char) mnemonic + ")";
            if (editor.getText() != null) {
                editor.setText(editor.getText() + accelerator);
            }
            if (editor.getToolTipText() != null) {
                editor.setToolTipText(editor.getToolTipText() + accelerator);
            }
        }
    }

    public static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject) {
        Set<JAXXObject> done = new LinkedHashSet<>();
        addKeyStrokeFromMnemonic(jaxxObject, done);
    }

    protected static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject, Set<JAXXObject> done) {

        if (done.contains(jaxxObject)) {
            return;
        }
        done.add(jaxxObject);

        jaxxObject.get$objectMap().values().stream().filter(o -> !(o instanceof JMenuItem) && o instanceof AbstractButton).forEach(
                o -> {
                    AbstractButton b = (AbstractButton) o;
                    if (b.getAction() == null || b.getAction().getValue(ACCELERATOR_KEY) == null) {
                        addKeyStrokeFromMnemonic((AbstractButton) o);
                    }
                }
        );

        jaxxObject.get$objectMap().values().stream().filter(o -> o instanceof JAXXObject).forEach(
                o -> addKeyStrokeFromMnemonic((JAXXObject) o, done)
        );
    }

    public static void addKeyStoreToText(AbstractButton editor, ApplicationAction action) {
        if (editor.getText() != null) {
            editor.setText(suffixTextWithKeyStroke(editor.getText(), action.getAcceleratorKey()));
        }
        if (editor.getToolTipText() != null) {
            editor.setToolTipText(suffixTextWithKeyStroke(editor.getToolTipText(), action.getAcceleratorKey()));
        }
    }

    public static String removeAcceleratorKey(String text) {
        int endIndex = text.indexOf('(');
        return endIndex == -1 ? text : text.substring(0, endIndex);
    }
}
