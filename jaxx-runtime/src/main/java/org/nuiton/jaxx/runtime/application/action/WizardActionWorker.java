/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.application.action;


import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtStep;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.concurrent.Callable;

/**
 * Un worker pour les opération longues d'administration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class WizardActionWorker<E extends WizardExtStep, M extends WizardExtModel<E>> extends ActionWorker<WizardState, String> {

    protected final M model;

    private WizardActionWorker(M model, String actionLabel) {
        super(actionLabel);
        this.model = model;
    }

    public static <E extends WizardExtStep, M extends WizardExtModel<E>> WizardActionWorker<E, M> newWorker(M model, String actionLabel, Callable<WizardState> target) {
        WizardActionWorker<E, M> adminActionWorker = new WizardActionWorker<>(model, actionLabel);
        adminActionWorker.setTarget(target);
        return adminActionWorker;
    }

    public M getModel() {
        return model;
    }
}
