package org.nuiton.jaxx.runtime.i18n;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;

/**
 * To build a dictionary of i18n labels used in ui based on a decorator.
 * <p>
 * Created by tchemit on 24/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
public class I18nLabelsBuilder {

    private final BeanPropertyI18nKeyProducer keyProducer;
    private final Class beanType;

    public I18nLabelsBuilder(Class beanType) {
        this.keyProducer = BeanPropertyI18nKeyProducerProvider.get().getDefaultLabelsBuilder();
        this.beanType = beanType;
    }

    public String getI18nKey(String property) {
        return keyProducer.getI18nPropertyKey(beanType, property);
    }


}
