package org.nuiton.jaxx.runtime.swing.application.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;

import java.util.Date;

public class LogListener implements ActionExecutorListener {

    private final Logger log;

    public LogListener(Logger log) {
        this.log = log;
    }

    @Override
    public void actionStart(ActionExecutorEvent event) {
        ActionWorker<?, ?> worker = event.getWorker();
        log.debug("Action [" + worker.getActionLabel() + "] was started at " + new Date(worker.getStartTime()));
    }

    @Override
    public void actionFail(ActionExecutorEvent event) {
        ActionWorker<?, ?> worker = event.getWorker();
        Exception error = worker.getError();
        log.error("Action [" + worker.getActionLabel() + "] failed with error " + error.getCause(), error);

    }

    @Override
    public void actionCancel(ActionExecutorEvent event) {
        ActionWorker<?, ?> worker = event.getWorker();
        log.debug("Action [" + worker.getActionLabel() + "] was canceled");
    }

    @Override
    public void actionEnd(ActionExecutorEvent event) {
        ActionWorker<?, ?> source = event.getWorker();
    }

    @Override
    public void actionDone(ActionExecutorEvent event) {
        ActionWorker<?, ?> worker = event.getWorker();
        long count = event.getSource().getNbActions();
        log.debug("Action [" + worker.getActionLabel() + " ] is consumed (still " + count + " tasks to treat).");
    }
}
