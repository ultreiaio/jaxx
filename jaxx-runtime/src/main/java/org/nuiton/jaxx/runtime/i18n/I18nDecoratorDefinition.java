package org.nuiton.jaxx.runtime.i18n;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;

import java.util.LinkedHashMap;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * To get all about i18n for a decorator.
 * <p>
 * To create your own, just implements this and make it available via the ServiceLoader mechanism.
 * <p>
 * The {@link #accept(Decorator)} will be used then to find the correct i18n decorator definition to keep.
 * <p>
 * There is one default implementation {@link DefaultI18nDecoratorDefinition} used as a fallback in {@link I18nDecoratorDefinitions#getDefinition(Decorator)}
 *
 * <p>
 * Created on 27/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see I18nDecoratorDefinitions
 * @since 3.0.0
 */
public interface I18nDecoratorDefinition {

    /**
     * @param decorator decorator to inspect
     * @return {@code true} if it accept to deal with with decorator.
     */
    boolean accept(Decorator decorator);

    /**
     * @param decorator decorator to inspect
     * @return the type used by I18n to decorate properties, might be different of the {@link DecoratorDefinition#type()}...
     */
    Class<?> getDecoratorType(Decorator decorator);

    /**
     * Build the properties labels for a decorator.
     *
     * @param decorator     decorator to inspect
     * @param labelsBuilder the labels builder used tyo compute labels.
     * @return the dictionary of labels (keys are property names, values are properties labels)
     */
    default LinkedHashMap<String, String> computeLabels(Decorator decorator, BeanPropertyI18nKeyProducer labelsBuilder) {
        if (!accept(decorator)) {
            throw new IllegalStateException(String.format("Not allowed to deal with this decorator: %s", decorator.definition()));
        }
        DecoratorDefinition<?, ?> definition = decorator.definition();
        Class<?> type = getDecoratorType(decorator);
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        int nbContext = definition.contextCount();
        if (nbContext > 1) {
            definition.properties().forEach(
                    propertyName -> {
                        String property = labelsBuilder.getI18nPropertyKey(type, propertyName);
                        String propertyI18n = t(property);
                        result.put(property, propertyI18n);
                    }
            );
        }
        return result;
    }
}
