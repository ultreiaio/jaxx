package org.nuiton.jaxx.runtime.application.test;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

/**
 * Created by tchemit on 13/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BootTestHelper {

    public static BootArguments getBootArgumentsAnnotation(AnnotatedElement testClass) {
        return getAnnotation(testClass, BootArguments.class);
    }

    public static <E extends Annotation> E getAnnotation(AnnotatedElement clazz, Class<E> annotationType) {

        E annotation = clazz.getAnnotation(annotationType);

        if (annotation == null && clazz instanceof Class && ((Class) clazz).getSuperclass() != null) {
            annotation = getAnnotation(((Class) clazz).getSuperclass(), annotationType);
        }
        return annotation;
    }

    public static <E extends Annotation> E[] getAnnotations(AnnotatedElement clazz, Class<E> annotationType) {

        E[] annotations = clazz.getAnnotationsByType(annotationType);

        if (annotations.length == 0 && clazz instanceof Class && ((Class) clazz).getSuperclass() != null) {
            annotations = getAnnotations(((Class) clazz).getSuperclass(), annotationType);
        }
        return annotations;
    }

    public static <E extends Annotation> boolean withAnnotation(AnnotatedElement testClass,Class<E> annotationType) {
        return BootTestHelper.getAnnotation(testClass, annotationType) != null;
    }

    private static File testsBasedir;

    private static File basedir;

    public static File getTestBasedir(Class<?> testClass) {
        return new File(getTestsBasedir(), testClass.getSimpleName());
    }

    private static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);
        }
        return basedir;
    }

    private static File getTestsBasedir() {
        if (testsBasedir == null) {
            testsBasedir = getBasedir().toPath().resolve("target").resolve("surefire-workdir").resolve("tests").resolve(System.nanoTime() + "").toFile();
        }
        return testsBasedir;
    }

}
