package org.nuiton.jaxx.runtime.i18n;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;

import java.util.LinkedHashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created on 27/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.0
 */
public class I18nDecoratorDefinitions {

    public static I18nDecoratorDefinitions INSTANCE;
    private final Set<I18nDecoratorDefinition> definitions;
    private final I18nDecoratorDefinition defaultDefinition;

    public static I18nDecoratorDefinitions get() {
        return INSTANCE == null ? INSTANCE = new I18nDecoratorDefinitions() : INSTANCE;
    }

    public I18nDecoratorDefinitions() {
        definitions = new LinkedHashSet<>();
        for (I18nDecoratorDefinition definition : ServiceLoader.load(I18nDecoratorDefinition.class)) {
            definitions.add(definition);
        }
        defaultDefinition = new DefaultI18nDecoratorDefinition();
    }

    public I18nDecoratorDefinition getDefinition(Decorator decorator) {
        return definitions.stream().filter(d -> d.accept(decorator)).findFirst().orElse(defaultDefinition);
    }
}
