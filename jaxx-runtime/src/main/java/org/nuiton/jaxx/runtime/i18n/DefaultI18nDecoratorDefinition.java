package org.nuiton.jaxx.runtime.i18n;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;

/**
 * Created on 27/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.0
 */
public class DefaultI18nDecoratorDefinition implements I18nDecoratorDefinition {
    @Override
    public boolean accept(Decorator decorator) {
        return true;
    }

    @Override
    public Class<?> getDecoratorType(Decorator decorator) {
        return decorator.definition().type();
    }
}
