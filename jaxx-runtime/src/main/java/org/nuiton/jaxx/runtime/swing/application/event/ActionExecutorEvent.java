package org.nuiton.jaxx.runtime.swing.application.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.application.ActionExecutor;
import org.nuiton.jaxx.runtime.swing.application.ActionWorker;

import java.util.EventObject;
import java.util.Objects;


/**
 * Event send by  {@link ActionExecutorListener}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ActionExecutorEvent extends EventObject {

    private final ActionWorker<?, ?> worker;

    private boolean consumed;

    public ActionExecutorEvent(ActionExecutor source, ActionWorker<?, ?> worker) {
        super(Objects.requireNonNull(source));
        this.worker = worker;
    }

    @Override
    public ActionExecutor getSource() {
        return (ActionExecutor) super.getSource();
    }

    public ActionWorker<?, ?> getWorker() {
        return worker;
    }

    public boolean isConsumed() {
        return consumed;
    }

    public void consume() {
        this.consumed = true;
    }

}
