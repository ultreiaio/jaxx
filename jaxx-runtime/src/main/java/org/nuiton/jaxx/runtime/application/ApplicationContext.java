package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.swing.session.SwingSessionHelper;

import javax.swing.SwingUtilities;
import java.awt.Frame;
import java.io.Closeable;
import java.util.List;

/**
 * Application context (only one instance should exists and it is initialized by the {@link ApplicationBoot}).
 * <p>
 * Created by tchemit on 26/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ApplicationContext<Config extends ApplicationConfiguration, Context extends ApplicationContext<Config, Context>> extends JAXXContext, Closeable {

    /**
     * Install the application into the given {@code context}.
     *
     * @param context where to install application context
     */
    void install(JAXXContext context);

    /**
     * @return a new initial context within this application context.
     */
    JAXXInitialContext toInitialContext();

    /**
     * @return application boot
     */
    ApplicationBoot<Config, Context> getBoot();

    /**
     * @return application config
     */
    Config getConfig();

    List<ApplicationContextComponent<Config, Context, ?>> components();

    List<ApplicationContextComponent<Config, Context, ?>> variables();

    /**
     * @return {@code true} if context is closed.
     * @see ApplicationBoot#isClosed()
     */
    boolean isClosed();

    default boolean isClosing() {
        return getBoot().isClosing();
    }
    void addAction(String actionLabel, Runnable action);

    void handlingError(String message, Exception e);

    void handlingError(Exception e);

    void displayInfo(String startMessage);

    @Override
    void close();

    void lock() throws InterruptedException;

    void unlock();

    void busy();

    void notBusy();

    Frame getMainUI();

    Frame newMainUI();

    void removeMainUI();

    SwingSessionHelper getSwingSessionHelper();

    default void setMainUIVisible(Frame ui, boolean replace) {
        SwingUtilities.invokeLater(() -> ui.setVisible(true));
        getSwingSessionHelper().addComponent(ui, replace);
    }

}
