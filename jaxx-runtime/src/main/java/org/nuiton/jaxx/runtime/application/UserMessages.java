package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.JOptionPanes;

import javax.swing.JOptionPane;
import java.awt.Frame;

/**
 * Created by tchemit on 21/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UserMessages {

    private static Frame mainUI;

    public static void setMainUI(Frame mainUI) {
        UserMessages.mainUI = mainUI;
    }

    public static void displayInfo(String title, String text) {
        JOptionPanes.displayInfo(mainUI, title, text);
    }

    public static void displayWarning(String title, String text) {
        JOptionPanes.displayWarning(mainUI, title, text);
    }

    public static int askUser(String title, String message, int typeMessage, Object[] options, int defaultOption) {
        return JOptionPanes.askUser(mainUI, title, message, typeMessage, options, defaultOption);
    }

    public static int askUser(String title, Object message, int typeMessage, Object[] options, int defaultOption) {
        return JOptionPanes.askUser(mainUI, title, message, typeMessage, options, defaultOption);
    }

    public static int askUser(JOptionPane pane, String title, Object[] options) {
        return JOptionPanes.askUser(mainUI, pane, title, options);
    }
}
