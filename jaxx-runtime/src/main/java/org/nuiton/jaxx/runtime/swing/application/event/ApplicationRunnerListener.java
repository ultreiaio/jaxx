package org.nuiton.jaxx.runtime.swing.application.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.application.ApplicationRunner;

import java.util.EventListener;

/**
 * To listen execution on {@link ApplicationRunner} using {@link ApplicationRunnerEvent}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */

public interface ApplicationRunnerListener extends EventListener {

    /**
     * @param event the action executor event
     * @see ApplicationRunner#initOnce()
     */
    void initOnce(ApplicationRunnerEvent event);

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onInit()
     */
    void init(ApplicationRunnerEvent event) throws Exception;

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onStart()
     */
    void start(ApplicationRunnerEvent event) throws Exception;

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onClose(boolean)
     */
    void close(ApplicationRunnerEvent event) throws Exception;

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onShutdown()
     */
    void shutdown(ApplicationRunnerEvent event) throws Exception;

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onShutdown(Exception)
     */
    void shutdownWithError(ApplicationRunnerEvent event);

    /**
     * @param event the action executor event
     * @see ApplicationRunner#onError(Exception)
     */
    void error(ApplicationRunnerEvent event);

}
