package org.nuiton.jaxx.runtime.application.command;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.jaxx.runtime.application.ApplicationConfiguration;
import org.nuiton.jaxx.runtime.application.ApplicationContext;
import org.nuiton.jaxx.runtime.application.ApplicationInstances;
import org.nuiton.jaxx.runtime.application.action.ActionWorker;

import java.util.Objects;

/**
 * Support for command line actions.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public abstract class CommandSupport<Config extends ApplicationConfiguration, Context extends ApplicationContext<Config, Context>> {

    protected final Logger log = LogManager.getLogger(getClass());

    public abstract void run() throws InterruptedException;

    public final void start() throws InterruptedException {
        try {
            run();
        } finally {
            getContext().unlock();
        }
    }

    protected final Config getConfig() {
        return getContext().getConfig();
    }

    protected final void launchAction(String title, Runnable target) throws InterruptedException {
        CommandLineActionWorker worker = new CommandLineActionWorker(title, target);
        getContext().addAction(worker.getActionLabel(), worker);
        getContext().lock();
    }

    @SuppressWarnings("unchecked")
    public Context getContext() {
        return Objects.requireNonNull((Context) ApplicationInstances.context());
    }

    public class CommandLineActionWorker extends ActionWorker<Void, String> {

        CommandLineActionWorker(String actionLabel, Runnable target) {
            super(actionLabel);
            setTarget(target);
        }

        public CommandSupport getAction() {
            return CommandSupport.this;
        }
    }
}
