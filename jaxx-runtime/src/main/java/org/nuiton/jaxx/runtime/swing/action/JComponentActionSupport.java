package org.nuiton.jaxx.runtime.swing.action;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public abstract class JComponentActionSupport<UI extends JComponent & JAXXObject> extends JAXXObjectActionSupport<UI> {

    protected JComponentActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected JComponentActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }

    protected JComponentActionSupport(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    protected JComponentActionSupport(String actionCommandKey, String label, String shortDescription, String actionIcon, char acceleratorKey) {
        super(actionCommandKey, label, shortDescription, actionIcon, acceleratorKey);
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_IN_FOCUSED_WINDOW;
    }

    @Override
    protected InputMap getInputMap(UI ui, int inputMapCondition) {
        return ui.getInputMap(inputMapCondition);
    }

    @Override
    protected ActionMap getActionMap(UI ui) {
        return ui.getActionMap();
    }

}

