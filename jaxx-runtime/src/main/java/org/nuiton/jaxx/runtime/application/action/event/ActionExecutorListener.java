package org.nuiton.jaxx.runtime.application.action.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.EventListener;

/**
 * Created by tchemit on 26/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ActionExecutorListener extends EventListener {

    /**
     * Hook when a action is about to start.
     *
     * @param event event
     */
    void onActionStart(ActionExecutorEvent event);

    /**
     * Hook when a action has failed.
     *
     * @param event event
     */
    void onActionFail(ActionExecutorEvent event);

    /**
     * Hook when a action has been canceled.
     *
     * @param event event
     */
    void onActionCancel(ActionExecutorEvent event);

    /**
     * Hook when a action has end with no failure or cancel.
     *
     * @param event event
     */
    void onActionEnd(ActionExecutorEvent event);

    /**
     * Hook after action is consumed.
     *
     * @param event event
     */
    void onAfterAction(ActionExecutorEvent event);
}
