package org.nuiton.jaxx.runtime.application;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

/**
 * To initialize some stuff in your module.
 * <p>
 * Make it usable for {@link java.util.ServiceLoader}.
 * <p>
 * Created by tchemit on 31/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ModuleInitializer extends Closeable {

    static ImmutableList<ModuleInitializer> loadInitializer() {
        List<ModuleInitializer> list = new ArrayList<>();
        for (ModuleInitializer moduleInitializer : ServiceLoader.load(ModuleInitializer.class)) {
            list.add(moduleInitializer);
        }
        return ImmutableList.copyOf((list.stream().sorted(Comparator.comparingInt(ModuleInitializer::getPriority)).collect(Collectors.toList())));
    }

    int getPriority();

    void init();

    @Override
    void close();
}
