package org.nuiton.jaxx.runtime.application.action.event;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.application.ApplicationContext;
import org.nuiton.jaxx.runtime.application.action.ActionWorker;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Date;

/**
 * Created by tchemit on 27/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DefaultActionExecutorListener implements ActionExecutorListener {

    private static final Logger log = LogManager.getLogger(DefaultActionExecutorListener.class);
    private final ApplicationContext context;

    public DefaultActionExecutorListener(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void onActionStart(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        log.info(String.format("Action [%s] was started at %s", source.getActionLabel(), new Date(source.getStartTime())));
        context.busy();
    }

    @Override
    public void onActionFail(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        Exception error = source.getError();
        log.error(String.format("Action [%s] failed with error %s", source.getActionLabel(), error.getCause()), error);
        if (context.isClosed()) {
            return;
        }
//        log.info((String.format("Action [%s] fail with error: %s.", source.getActionLabel(), error.getMessage())));
    }

    @Override
    public void onActionCancel(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        log.info(String.format("Action [%s] was canceled", source.getActionLabel()));
        if (context.isClosed()) {
            return;
        }
        log.info(String.format("Action [%s] canceled.", source.getActionLabel()));
    }

    @Override
    public void onActionEnd(ActionExecutorEvent event) {
        ActionWorker source = event.getSource();
        log.info(String.format("Action [%s] was done in %s", source.getActionLabel(), source.getTime()));
        if (context.isClosed()) {
            return;
        }
        log.info(String.format("Action [%s] end.", source.getActionLabel()));
    }

    @Override
    public void onAfterAction(ActionExecutorEvent event) {
        long count = event.getExecutor().getNbActions();
        ActionWorker source = event.getSource();
        log.info(String.format("Action [%s] is consumed (still %d tasks to treat).", source.getActionLabel(), count));
        if (context.isClosed()) {
            return;
        }
        if (count < 1) {
            context.notBusy();
        }
    }

}
