package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import java.util.Optional;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import io.ultreia.java4all.i18n.editor.ui.LocalizedItemUI;
import io.ultreia.java4all.i18n.editor.ui.ProjectUI;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GoTranslationsAction extends ProjectActionSupport {

    public static final String ACTION_NAME = GoTranslationsAction.class.getName();
    public static final KeyStroke KEY_STROKE = KeyStroke.getKeyStroke("alt pressed F2");

    public GoTranslationsAction(ProjectUI ui) {
        super(ui, ACTION_NAME, KEY_STROKE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Optional<LocalizedItemUI> focus = ui.getSelectedItem().getPanel().getFocus();
        ui.getNavigationTabbedPane().setSelectedIndex(0);
        focus.ifPresent(f -> {
            LocalizedItemUI itemUI = ui.getSelectedItem().getPanel().getLocalizedItemUI(f.getLocale());
            SwingUtilities.invokeLater(itemUI.getEditor()::requestFocusInWindow);
        });
    }
}
