package io.ultreia.java4all.i18n.editor.ui;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.i18n.editor.action.ResetAction;
import io.ultreia.java4all.i18n.editor.action.RevertAction;
import io.ultreia.java4all.i18n.editor.action.SaveAction;
import io.ultreia.java4all.i18n.editor.action.SaveAndNextAction;
import io.ultreia.java4all.i18n.editor.model.Item;
import io.ultreia.java4all.i18n.editor.model.ItemSupport;
import io.ultreia.java4all.i18n.editor.model.LocalizedItem;
import io.ultreia.java4all.i18n.editor.model.Project;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.beans.PropertyChangeListener;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ItemUI extends JPanel {

    public static final String PROPERTY_SAVED = "saved";
    /** Logger. */
    private static final Logger log = LogManager.getLogger(ItemUI.class);
    private final PropertyChangeListener modifiedChanged;
    private final PropertyChangeListener modifiedGlobalChanged;
    private final Map<Locale, LocalizedItemUI> editors;
    private final JButton resetGlobal;
    private final JButton reset;
    private final JButton save;
    private final JButton saveAndNext;
    private final JPanel emptyPanel;
    private final JPanel fillPanel;
    private final ItemComponent itemComponent;
    private Item model;
    private LocalizedItemUI focus;

    ItemUI(ItemComponent itemComponent, Project project, Function<Locale, LocalizedItemUI> generator) {
        this.itemComponent = itemComponent;

        setLayout(new BorderLayout());
        fillPanel = new JPanel();
        fillPanel.setLayout(new GridLayout(0, 1));

        emptyPanel = new JPanel();
        emptyPanel.setLayout(new BorderLayout());
        add(emptyPanel, BorderLayout.CENTER);

        JLabel label = new JLabel("< Select an item >");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setEnabled(false);
        emptyPanel.add(label, BorderLayout.CENTER);

        ImmutableMap.Builder<Locale, LocalizedItemUI> editorsBuilder = ImmutableMap.builder();

        for (Locale locale : project.getLocales()) {

            LocalizedItemUI localEditor = generator.apply(locale);
            editorsBuilder.put(locale, localEditor);
            fillPanel.add(localEditor);
        }
        editors = editorsBuilder.build();

        JPanel actions = new JPanel();
        actions.setLayout(new GridLayout(1, 0));

        resetGlobal = addAction(actions, new RevertAction(itemComponent));
        reset = addAction(actions, new ResetAction(itemComponent));
        save = addAction(actions, new SaveAction(itemComponent));
        saveAndNext = addAction(actions, new SaveAndNextAction(itemComponent));

        modifiedChanged = e1 -> modifiedChanged();
        modifiedGlobalChanged = e -> modifiedGlobalChanged();

        add(actions, BorderLayout.SOUTH);
        setMinimumSize(new Dimension(400, 30));
    }

    private JButton addAction(JPanel actions, Action action) {
        JButton result = new JButton(action);
        actions.add(result);
        result.setEnabled(false);
        result.setDisplayedMnemonicIndex(result.getText().length() - 2);
        return result;
    }

    private void modifiedGlobalChanged() {
        if (model == null) {
            return;
        }
        resetGlobal.setEnabled(!model.isModified() && model.isGlobalModified());
    }

    private void modifiedChanged() {
        if (model == null) {
            return;
        }
        boolean modified = model.isModified();
        save.setEnabled(modified);
        saveAndNext.setEnabled(modified && !itemComponent.isLast());
        reset.setEnabled(modified);
        resetGlobal.setEnabled(!modified && model.isGlobalModified());
    }

    public LocalizedItemUI getLocalizedItemUI(Locale locale) {
        return Optional.ofNullable(editors.get(locale)).orElseThrow(IllegalStateException::new);
    }

    public Optional<Item> getModel() {
        return Optional.ofNullable(model);
    }

    public void setModel(Item model) {
        if (this.model != null) {
            this.model.removePropertyChangeListener(ItemSupport.PROPERTY_GLOBAL_MODIFIED, modifiedGlobalChanged);
            this.model.removePropertyChangeListener(ItemSupport.PROPERTY_MODIFIED, modifiedChanged);
        }
        this.model = model;

        if (model == null) {
            remove(fillPanel);
            remove(emptyPanel);
            add(emptyPanel, BorderLayout.CENTER);
            SwingUtilities.invokeLater(this::revalidate);
            return;
        }
        remove(fillPanel);
        remove(emptyPanel);
        add(fillPanel, BorderLayout.CENTER);

        if (focus == null) {
            Locale locale = editors.keySet().iterator().next();
            JTextComponent editor = editors.get(locale).getEditor();
            log.info("Set focus to " + editor);
            focus(editors.get(locale));
        }

        for (LocalizedItem localizedItem : model) {
            getLocalizedItemUI(localizedItem.getLocale()).setModel(localizedItem);
        }
        this.model.addPropertyChangeListener(ItemSupport.PROPERTY_GLOBAL_MODIFIED, modifiedGlobalChanged);
        this.model.addPropertyChangeListener(ItemSupport.PROPERTY_MODIFIED, modifiedChanged);

        modifiedGlobalChanged();
        modifiedChanged();


    }

    @Override
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void focus(LocalizedItemUI focus) {
        this.focus = focus;
    }

    public Optional<LocalizedItemUI> getFocus() {
        return Optional.ofNullable(focus);
    }

}
