package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.editor.ui.ProjectUI;
import org.nuiton.jaxx.runtime.swing.JOptionPanes;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ExitAction extends ProjectActionSupport {

    public ExitAction(ProjectUI ui) {
        super(ui,
              "Quit",
              "Quit editor",
              (int) 'Q',
              "exit");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (ui.getModel().isModified()) {
            // ask user if he wants to perform an export (to not loose his modifications)
            // See https://gitlab.com/ultreiaio/i18n/-/issues/236
            int response = JOptionPanes.askUser(ui, "Project was modified", "Project was modified, if you quit, you will loose any modifications. You can do an export before quitting.", JOptionPane.WARNING_MESSAGE,
                    new Object[]{"Quit with no export", "Export then quit"},
                    1);
            switch (response) {
                case JOptionPane.CLOSED_OPTION:
                    // do nothing
                    return;
                case 0:
                    // do quit
                    ui.dispose();
                    break;
                case 1:
                    // do export then quit
                    ui.getModel().setAskToQuit(true);
                    ui.getExport().doClick();
                    break;

            }
        } else {
            // no modification, can quit right now
            ui.dispose();
        }
    }
}
