package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.HashMultimap;
import io.ultreia.java4all.i18n.runtime.I18nLanguage;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;
import io.ultreia.java4all.i18n.spi.io.I18nTranslationSetClassPathReader;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.i18n.editor.model.Item;
import io.ultreia.java4all.i18n.editor.model.LocalizedItem;
import io.ultreia.java4all.i18n.editor.model.Project;
import io.ultreia.java4all.i18n.editor.ui.ProjectUI;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.nuiton.jaxx.runtime.swing.JOptionPanes.askUser;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ExportAction extends ProjectActionSupport {

    private static final Logger log = LogManager.getLogger(ExportAction.class);

    public ExportAction(ProjectUI ui) {
        super(ui,
              "Export",
              "Export modified translation(s) and templates",
              (int) 'E',
              "export");
    }

    private static String exportProject(Project project) throws IOException, I18nResourceInitializationException {

        StringBuilder report = new StringBuilder("<html><body>");

        String filename = project.getSessionId();

        project.setLastExportDate(new Date());

        Path path = project.getI18nEditorDirectory().resolve(filename);

        log.info("Will store modifications at: " + path);

        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }

        Set<Item> modifiedTranslations = project.getModifiedTranslations();

        log.info(String.format("Found %d modified translation(s).", modifiedTranslations.size()));

        I18nLanguageProvider languageProvider = project.getLanguageProvider();
        Charset encoding = languageProvider.getConfiguration().getEncoding();

        report.append("<h2>Summary</h2>");

        if (!modifiedTranslations.isEmpty()) {

            report.append(String.format("<h3>%d translation(s) modified</h2><ul>", modifiedTranslations.size()));
            HashMultimap<Locale, Pair<String, String>> translations = HashMultimap.create();

            for (Item item : modifiedTranslations) {
                for (LocalizedItem localizedItem : item.getGlobalModifiedItems()) {
                    translations.put(localizedItem.getLocale(), Pair.of(item.getKey(), localizedItem.getLastValidValue()));
                }
            }

            Path translationsPath = path.resolve(I18nTranslationSetDefinition.PATH);
            Path i18nUserTranslationsDirectory = project.getI18nUserTranslationsDirectory();

            for (Map.Entry<Locale, Collection<Pair<String, String>>> entry : translations.asMap().entrySet()) {
                Locale locale = entry.getKey();
                I18nLanguage language = languageProvider.getLanguage(locale);
                I18nTranslationSetDefinition translationSetDefinition = language.getTranslationSetDefinition();
                Collection<Pair<String, String>> value = entry.getValue();
                Properties modifiedTranslationProperties = toProperties(value);

                Path applicationTarget = I18nTranslationSetDefinition.write(translationSetDefinition, encoding, false, translationsPath, modifiedTranslationProperties, true);

                report.append(String.format("<li>Store %d translation(s) to file: %s</li>", modifiedTranslationProperties.size(), applicationTarget.toFile().getName()));

                if (Files.exists(i18nUserTranslationsDirectory)) {
                    Properties languageTranslations = language.getTranslations();
                    languageTranslations.putAll(modifiedTranslationProperties);
                    Path write = I18nTranslationSetDefinition.write(translationSetDefinition, encoding, false, i18nUserTranslationsDirectory, languageTranslations, true);
                    report.append(String.format("<li>Merge %d translation(s) to application file: %s</li>", modifiedTranslationProperties.size(), write.toFile().getName()));
                }

                I18nTranslationSetClassPathReader reader = new I18nTranslationSetClassPathReader(languageProvider.getConfiguration().getClassLoader(), true);
                for (I18nTranslationSetDefinition i18nTranslationSetDefinition : language.getDependenciesTranslationSetDefinition()) {
                    Properties pp = reader.read(i18nTranslationSetDefinition, encoding);
                    Collection<Pair<String, String>> valueForThisBundle = value.stream().filter(p -> pp.containsKey(p.getKey())).collect(Collectors.toSet());
                    Properties properties = toProperties(valueForThisBundle);
                    if (!valueForThisBundle.isEmpty()) {
                        Path dependencyTarget = I18nTranslationSetDefinition.write(i18nTranslationSetDefinition, encoding, true, translationsPath, properties, true);
                        report.append(String.format("<li>Store %d translation(s) to file: %s</li>", valueForThisBundle.size(), dependencyTarget.toFile().getName()));
                    }
                }
            }
            report.append("</ul>");
        }
        Set<Item> modifiedTemplates = project.getModifiedTemplates();

        log.info(String.format("Found %d modified template(s).", modifiedTemplates.size()));

        if (!modifiedTemplates.isEmpty()) {

            report.append(String.format("<h3>%d templates modified</h3><ul>", modifiedTranslations.size()));

            Path templatesPath = path.resolve(I18nTemplateDefinition.PATH);

            Path i18nUserTemplatesDirectory = project.getI18nUserTemplatesDirectory();

            for (Item item : modifiedTemplates) {
                for (LocalizedItem localizedItem : item.getGlobalModifiedItems()) {
                    I18nLanguage language = languageProvider.getLanguage(localizedItem.getLocale());
                    I18nTemplateDefinition templateDefinition = language.getTemplateDefinition(item.getKey());

                    String template = localizedItem.getLastValidValue();
                    Path write = I18nTemplateDefinition.write(templateDefinition, templatesPath, encoding, false, template, true);
                    report.append(String.format("<li>Store template %s to file: %s</li>", templateDefinition.getId(), write.toFile().getName()));
                    if (Files.exists(i18nUserTemplatesDirectory)) {
                        Path write1 = I18nTemplateDefinition.write(templateDefinition, i18nUserTemplatesDirectory, encoding, false, template, true);
                        report.append(String.format("<li>Merge template %s to application file: %s</li>", templateDefinition.getId(), write1.toFile().getName()));
                    }
                }
            }
            report.append("</ul>");
        }

        Path zipFile = project.getExportFile();
        log.info("Store modifications to zip file: " + zipFile);

        report.append("<h2>Export</h2>");
        report.append(String.format("<p>Store modifications to <a href=\"file://%1$s\">%1$s</a></p>", path));
        report.append(String.format("<p>Archive created at <a href=\"file://%s\">%s</a></p>", zipFile.getParent(), zipFile));

        report.append("<h2>Note</h2>");
        report.append("<p><i>Please click on archive or directory to open it on your system.</i></p>");
        try (ZipOutputStream zipFileOut = new ZipOutputStream(Files.newOutputStream(zipFile))) {
            try (Stream<Path> paths = Files.walk(path).filter(Files::isRegularFile)) {
                for (Path p : paths.collect(Collectors.toList())) {
                    addToZip(zipFileOut, path, p, encoding);
                }
            }
        }

        return report.toString();
    }

    private static void addToZip(ZipOutputStream zipFile, Path path, Path file, Charset encoding) throws IOException {
        String filename = path.relativize(file).toString();
        log.info(String.format("Add zip entry: %s", filename));
        zipFile.putNextEntry(new ZipEntry(new String(filename.getBytes(), StandardCharsets.UTF_8)));
        List<String> lines = Files.readAllLines(file, encoding);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, encoding))) {
                for (String line : lines) {
                    writer.write(line);
                    writer.newLine();
                }
            }
            zipFile.write(out.toByteArray());
        }
    }

    private static Properties toProperties(Collection<Pair<String, String>> value) {
        SortedProperties pp = new SortedProperties();
        value.forEach(p -> pp.put(p.getLeft(), p.getRight()));
        return pp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Project project = ui.getModel();

        String exportResult;
        try {
            exportResult = exportProject(project);

        } catch (Exception ee) {
            throw new RuntimeException("Could not export", ee);
        }

        JEditorPane label = new JEditorPane("text/html; charset=UTF8", exportResult);
        label.addHyperlinkListener(SwingUtil::openLink);
        label.setEditable(false);
        JScrollPane pane = new JScrollPane(label, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        if (ui.getModel().isAskToQuit()) {
            // Just show export result then quit
            askUser(ui,
                    "Export done (before quitting)",
                    pane,
                    JOptionPane.INFORMATION_MESSAGE,
                    new Object[]{"Quit"},
                    0);
            ui.dispose();
            return;
        }

        int response = askUser(ui,
                               "Export done",
                               pane,
                               JOptionPane.INFORMATION_MESSAGE,
                               new Object[]{"Quit", "Continue editing"},
                               1);
        switch (response) {
            case JOptionPane.CLOSED_OPTION:
            case 1:
                // continue
                break;
            case 0:
                // quit
                ui.dispose();
                break;
        }
    }

}
