package io.ultreia.java4all.i18n.editor.ui;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import org.jdesktop.swingx.JXList;
import io.ultreia.java4all.i18n.editor.action.FilterKeysAction;
import io.ultreia.java4all.i18n.editor.action.ResetUserFilterAction;
import io.ultreia.java4all.i18n.editor.model.Item;
import io.ultreia.java4all.i18n.editor.model.Project;
import io.ultreia.java4all.i18n.editor.ui.ItemComponentTab.StringDefaultListModel;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ItemComponent {

    private final ItemUI panel;
    private final int mainIndex;
    private final JTabbedPane navigationPane;
    private final Function<String, Item> itemGetter;
    private final String title;
    private final JToggleButton filter;
    private final JTextField searchField;
    private ItemComponentTab currentTab;
    private Map<JXList, ItemComponentTab> tabs;
    private JComponent navigationContent;

    ItemComponent(Project model,
                  int mainIndex,
                  JTabbedPane navigationPane,
                  Function<Locale, LocalizedItemUI> uiGenerator,
                  Function<String, Item> itemGetter,
                  String title,
                  Set<String> items,
                  PropertyChangeListener modifiedChanged,
                  PropertyChangeListener globalModifiedChanged,
                  PropertyChangeListener saneChanged,
                  Map<String, String> tabs) {
        this.mainIndex = mainIndex;
        this.navigationPane = navigationPane;
        this.itemGetter = itemGetter;
        this.title = title;
        this.tabs = new LinkedHashMap<>();
        ImmutableList.Builder<ItemComponentTab> tabsUIBuilder = ImmutableList.builder();
        ImmutableList<ItemComponentTab> tabsUI;
        Set<String> allItems = new LinkedHashSet<>(items);
        boolean noTab = tabs.isEmpty();
        if (noTab) {
            ItemComponentTab tab = new ItemComponentTab(
                    0,
                    this,
                    itemGetter,
                    "",
                    items
            );
            navigationContent = tab.getScrollPane();
            tabsUIBuilder.add(tab);
            this.currentTab = tab;
            tabsUI = ImmutableList.of(this.currentTab);
            this.tabs.put(tab.getList(), tab);
        } else {
            navigationContent = new JTabbedPane();
            ((JTabbedPane) navigationContent).setTabPlacement(SwingConstants.LEFT);

            int index = 0;
            for (Map.Entry<String, String> entry : tabs.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                Predicate<String> p = e -> false;
                if (value.contains("|")) {
                    for (String s : value.split("\\|")) {
                        p = p.or(s1 -> s1.startsWith(s));
                    }
                } else {
                    p = s1 -> s1.startsWith(value);
                }
                Set<String> collect = allItems.stream().filter(p).collect(Collectors.toCollection(LinkedHashSet::new));
                allItems.removeAll(collect);
                ItemComponentTab tab = new ItemComponentTab(
                        index++,
                        this,
                        itemGetter,
                        key,
                        collect
                );
                ((JTabbedPane) navigationContent).addTab(tab.getTitle(), tab.getScrollPane());
                tabsUIBuilder.add(tab);
            }

            if (!allItems.isEmpty()) {
                ItemComponentTab tab = new ItemComponentTab(
                        index,
                        this,
                        itemGetter,
                        "others",
                        allItems
                );
                ((JTabbedPane) navigationContent).addTab(tab.getTitle(), tab.getScrollPane());
                tabsUIBuilder.add(tab);
            }
            ((JTabbedPane) navigationContent).addChangeListener(e -> {
                JTabbedPane source = (JTabbedPane) e.getSource();
                JScrollPane selectedComponent = (JScrollPane) source.getSelectedComponent();
                this.currentTab = selectedComponent == null ? null : this.tabs.get(selectedComponent.getViewport().getView());
                if (currentTab != null) {
                    currentTab.clearSelection();
                    if (isEmpty()) {
                        ItemComponent.this.panel.setModel(null);
                        SwingUtilities.invokeLater(ItemComponent.this.panel::repaint);
                    } else {
                        setSelectedIndex(0);
                    }
                }
            });

            tabsUI = tabsUIBuilder.build();
            tabsUI.forEach(e -> this.tabs.put(e.getList(), e));
            ((JTabbedPane) navigationContent).setSelectedIndex(-1);
            ((JTabbedPane) navigationContent).setSelectedIndex(0);
        }

        panel = new ItemUI(this, model, uiGenerator);

        JPanel scrollPane = new JPanel(new BorderLayout());
        navigationPane.add(scrollPane);
        updateTabTitle();
        scrollPane.add(navigationContent, BorderLayout.CENTER);

        JPanel header = new JPanel();
        scrollPane.add(header, BorderLayout.NORTH);
        header.setLayout(new BorderLayout());

        JToolBar filterActionsPane = new JToolBar();
        filterActionsPane.setFloatable(false);
        filterActionsPane.setBorderPainted(false);
        filterActionsPane.add(filter = new JToggleButton(new FilterKeysAction(this)));
//        if (!noTab) {
        filterActionsPane.add(new JButton(new ResetUserFilterAction(this)));
        searchField = new JTextField();
        searchField.addActionListener(e -> updateListFromUserFilter(searchField.getText()));
//            JPanel p = new JPanel(new BorderLayout());
        header.add(searchField, BorderLayout.CENTER);
//            header.add(p, BorderLayout.CENTER);
//        } else {
//            searchField = null;
//        }
        header.add(filterActionsPane, BorderLayout.WEST);

        int index = 0;
        for (ItemComponentTab itemComponentTab : tabsUI) {
            int finalIndex = index;
            itemComponentTab.getList().addListSelectionListener(e -> {
                if (currentTab != itemComponentTab) {
                    return;
                }
                JXList list = itemComponentTab.getList();
                Optional<LocalizedItemUI> focus = panel.getFocus();
                panel.getModel().ifPresent(itemModel -> {
                    itemModel.removePropertyChangeListener(Item.PROPERTY_MODIFIED, modifiedChanged);
                    itemModel.removePropertyChangeListener(Item.PROPERTY_GLOBAL_MODIFIED, globalModifiedChanged);
                    itemModel.removePropertyChangeListener(Item.PROPERTY_SANE, saneChanged);
                });
                String selectedKey = (String) list.getSelectedValue();
                Item itemModel = get(selectedKey);
                if (itemModel != null) {
                    itemModel.addPropertyChangeListener(Item.PROPERTY_MODIFIED, modifiedChanged);
                    itemModel.addPropertyChangeListener(Item.PROPERTY_GLOBAL_MODIFIED, globalModifiedChanged);
                    itemModel.addPropertyChangeListener(Item.PROPERTY_SANE, saneChanged);
                }
                itemComponentTab.refreshActions();

                panel.setModel(itemModel);
                SwingUtilities.invokeLater(panel::repaint);
                focus.ifPresent(f -> SwingUtilities.invokeLater(f.getEditor()::requestFocusInWindow));
            });

            panel.addPropertyChangeListener(ItemUI.PROPERTY_SAVED, e -> {
                if (navigationContent instanceof JTabbedPane) {
                    JTabbedPane navigationContent1 = (JTabbedPane) navigationContent;
                    if (finalIndex != navigationContent1.getSelectedIndex()) {
                        return;
                    }
                }
                JXList list = currentTab.getList();
                StringDefaultListModel listModel = (StringDefaultListModel) list.getModel();
                listModel.fireContentsChanged(listModel, list.getSelectedIndex(), list.getSelectedIndex());
                list.resetSortOrder();
                SwingUtilities.invokeLater(panel::repaint);
            });
            index++;
            itemComponentTab.updateTabTitle();
        }

        if (isNotEmpty()) {
            currentTab.setSelectedIndex(0);
        }
        tabsUI.get(0).refreshActions();
    }

    protected String getTitle() {
        return String.format("%s (%d/%d)", title, getTotalFilteredCount(), getTotalItemCount());
    }

    public JTextField getSearchField() {
        return searchField;
    }

    public JToggleButton getFilter() {
        return filter;
    }

    public void toggleFilter() {
        filter.doClick();
    }

    public void updateListFromUserFilter(String text) {
        for (ItemComponentTab value : tabs.values()) {
            value.updateListFromUserFilter(text);
        }
        if (currentTab != null) {
            currentTab.clearSelection();
            if (currentTab.isNotEmpty()) {
                currentTab.setSelectedIndex(0);
            }
        }
        updateTabTitle();
        SwingUtilities.invokeLater(searchField::requestFocusInWindow);
    }

    protected void updateTabTitle() {
        navigationPane.setTitleAt(mainIndex, getTitle());
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public boolean isFirst() {
        return currentTab.getSelectedIndex() == 0;
    }

    public boolean isLast() {
        return currentTab.getSelectedIndex() + 1 == getItemCount();
    }

    public ItemUI getPanel() {
        return panel;
    }

    public JXList getList() {
        return currentTab == null ? null : currentTab.getList();
    }

    void repaintList() {
        currentTab.repaintList();
    }

    public int getSelectedIndex() {
        return currentTab.getSelectedIndex();
    }

    public void setSelectedIndex(int selectedIndex) {
        currentTab.setSelectedIndex(selectedIndex);
    }

    public int getItemCount() {
        return currentTab.getList().getRowSorter().getViewRowCount();
    }

    public int getTotalFilteredCount() {
        return tabs.values().stream().map(ItemComponentTab::getList).map(JXList::getRowSorter).mapToInt(RowSorter::getViewRowCount).sum();
    }

    public int getTotalItemCount() {
        return tabs.values().stream().map(ItemComponentTab::getListModel).mapToInt(StringDefaultListModel::size).sum();
    }

    public Item get(String itemKey) {
        return itemKey == null ? null : itemGetter.apply(itemKey);
    }

    public Collection<ItemComponentTab> getTabs() {
        return tabs.values();
    }

    public ItemComponentTab getCurrentTab() {
        return currentTab;
    }

    public JComponent getNavigationContent() {
        return navigationContent;
    }

    public void updateListFromFilter(boolean add) {

        for (ItemComponentTab tab : getTabs()) {
            tab.updateListFromFilter(add);
        }
        updateTabTitle();
    }
}
