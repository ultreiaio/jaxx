package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import io.ultreia.java4all.i18n.editor.ui.ProjectUI;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 10/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ProjectActionSupport extends AbstractAction {

    protected final ProjectUI ui;

    ProjectActionSupport(ProjectUI ui, String text, String tip, int mnemonic, String icon) {
        this.ui = ui;
        putValue(MNEMONIC_KEY, mnemonic);
        putValue(SMALL_ICON, SwingUtil.createActionIcon(icon));
        String accelerator = " (Alt + " + (char) mnemonic + ")";
        putValue(SHORT_DESCRIPTION, (tip + accelerator).trim());
        putValue(NAME, (text + accelerator).trim());
    }

    ProjectActionSupport(ProjectUI ui, String name, KeyStroke keyStroke) {
        this.ui = ui;
        putValue(NAME, name);
        putValue(ACCELERATOR_KEY, keyStroke);

    }
}
