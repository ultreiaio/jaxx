package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.editor.model.Item;
import io.ultreia.java4all.i18n.editor.ui.ItemComponent;
import io.ultreia.java4all.i18n.editor.ui.ItemUI;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SaveAndNextAction extends ItemActionSupport {

    public SaveAndNextAction(ItemComponent itemComponent) {
        super(itemComponent,
              "Save and Next",
              "Save any modification on this item and go to next item",
              (int) 'X',
              "save");
    }

    @Override
    protected boolean doAction(ItemComponent itemComponent) {
        itemComponent.getPanel().getModel().ifPresent(Item::save);
        itemComponent.getPanel().firePropertyChange(ItemUI.PROPERTY_SAVED, null, true);
        int selectedIndex = itemComponent.getSelectedIndex();
        int viewRowCount = itemComponent.getItemCount();
        if (viewRowCount > selectedIndex) {
            itemComponent.setSelectedIndex(selectedIndex + 1);
        }
        return true;
    }

}
