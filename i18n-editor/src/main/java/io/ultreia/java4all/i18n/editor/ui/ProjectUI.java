package io.ultreia.java4all.i18n.editor.ui;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.editor.action.ExitAction;
import io.ultreia.java4all.i18n.editor.action.ExportAction;
import io.ultreia.java4all.i18n.editor.action.GoTemplatesAction;
import io.ultreia.java4all.i18n.editor.action.GoTranslationsAction;
import io.ultreia.java4all.i18n.editor.model.Project;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.beans.PropertyChangeListener;
import java.util.Collections;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProjectUI extends JDialog {

    private final ItemComponent translations;
    private final ItemComponent templates;
    private final JButton export;
    private final Project model;
    private final JTabbedPane navigationTabbedPane;
    private ItemComponent selectedItem;
    private final JButton exit;

    public ProjectUI(Frame owner, Project model) {
        super(owner, true);
        this.model = model;

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setTitle(String.format("I18n editor for %s v%s", model.getName(), model.getVersion()));
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setResizeWeight(0.8f);
//        splitPane.setDividerLocation(1f);
        getRootPane().setLayout(new BorderLayout());
        getRootPane().add(splitPane, BorderLayout.CENTER);
        JPanel actions = new JPanel();
        getRootPane().add(actions, BorderLayout.SOUTH);

        actions.setLayout(new GridLayout(1, 0));

        exit = new JButton(new ExitAction(this));
        exit.setDisplayedMnemonicIndex(exit.getText().length() - 2);
        actions.add(exit);
        export = new JButton(new ExportAction(this));
        export.setEnabled(false);
        export.setDisplayedMnemonicIndex(export.getText().length() - 2);
        actions.add(export);

        navigationTabbedPane = new JTabbedPane();
        navigationTabbedPane.setMinimumSize(new Dimension(550, 500));
//        navigationTabbedPane.setSize(new Dimension(550, 500));

        splitPane.setLeftComponent(navigationTabbedPane);

        JPanel editor = new JPanel();
        editor.setLayout(new BorderLayout());

        splitPane.setRightComponent(editor);

        PropertyChangeListener saneChanged = e -> saneChanged();
        PropertyChangeListener modifiedChanged = e -> modifiedChanged();
        PropertyChangeListener globalModifiedChanged = e -> globalModifiedChanged();

        translations = new ItemComponent(model,
                                         0,
                                         navigationTabbedPane,
                                         LocalizedItemUI::createTranslation,
                                         model::getTranslation,
                                         String.format("Translations (%s)", GoTranslationsAction.KEY_STROKE.toString().replace("pressed", "+")),
                                         model.getTranslationKeys(),
                                         modifiedChanged,
                                         globalModifiedChanged,
                                         saneChanged,
                                         model.getLanguageProvider().getApplicationDefinition().getKeyCategories());

        templates = new ItemComponent(model,
                                      1,
                                      navigationTabbedPane,
                                      LocalizedItemUI::createTemplate,
                                      model::getTemplate,
                                      String.format("Templates (%s)", GoTemplatesAction.KEY_STROKE.toString().replace("pressed", "+")),
                                      model.getTemplateNames(),
                                      modifiedChanged,
                                      globalModifiedChanged,
                                      saneChanged,
                                      Collections.emptyMap());

        navigationTabbedPane.getModel().addChangeListener(e -> {

            editor.removeAll();
            int selectedIndex = navigationTabbedPane.getSelectedIndex();
            switch (selectedIndex) {
                case 0: // translations
                    selectedItem = translations;
                    break;
                case 1: // templates
                    selectedItem = templates;
                    break;
            }
            editor.add(selectedItem.getPanel(), BorderLayout.CENTER);
            SwingUtilities.invokeLater(editor::repaint);

            selectedItem.getPanel().getFocus().ifPresent(i -> i.getEditor().requestFocusInWindow());
        });

        InputMap inputMap = splitPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap actionMap = splitPane.getActionMap();
        actionMap.put(GoTranslationsAction.ACTION_NAME, new GoTranslationsAction(this));
        inputMap.put(GoTranslationsAction.KEY_STROKE, GoTranslationsAction.ACTION_NAME);

        actionMap.put(GoTemplatesAction.ACTION_NAME, new GoTemplatesAction(this));
        inputMap.put(GoTemplatesAction.KEY_STROKE, GoTemplatesAction.ACTION_NAME);

        navigationTabbedPane.setSelectedIndex(1);
        navigationTabbedPane.setSelectedIndex(0);

        if (owner != null) {
            Dimension size = owner.getSize();
            Dimension thisSize = new Dimension((int) size.getWidth() - 20, (int) size.getHeight() - 20);
            setMinimumSize(thisSize);
            setSize(thisSize);
        } else {
            setMinimumSize(new Dimension(1024, 800));
            pack();
        }

        SwingUtil.center(owner, this);
        SwingUtilities.invokeLater(() -> setVisible(true));

    }

    private void globalModifiedChanged() {
        selectedItem.repaintList();
        export.setEnabled(model.isModified());
    }

    private void modifiedChanged() {
        selectedItem.repaintList();
    }

    private void saneChanged() {
        selectedItem.repaintList();
    }

    public Project getModel() {
        return model;
    }

    public void toggleFilter() {
        templates.toggleFilter();
        translations.toggleFilter();
    }

    public JTabbedPane getNavigationTabbedPane() {
        return navigationTabbedPane;
    }

    public ItemComponent getSelectedItem() {
        return selectedItem;
    }

    public JButton getExit() {
        return exit;
    }

    public JButton getExport() {
        return export;
    }
}
