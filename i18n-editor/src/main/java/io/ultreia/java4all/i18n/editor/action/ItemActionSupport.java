package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import java.util.Optional;
import javax.swing.AbstractAction;
import io.ultreia.java4all.i18n.editor.ui.ItemComponent;
import io.ultreia.java4all.i18n.editor.ui.LocalizedItemUI;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 10/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ItemActionSupport extends AbstractAction {

    private final ItemComponent itemComponent;

    protected abstract boolean doAction(ItemComponent itemComponent);

    ItemActionSupport(ItemComponent itemComponent, Object atext, Object atip, int mnemonic, String icon) {
        this.itemComponent = itemComponent;
        putValue(MNEMONIC_KEY, mnemonic);
        putValue(SMALL_ICON, SwingUtil.createActionIcon(icon));
        String accelerator = " (Alt + " + (char) mnemonic + ")";
        putValue(SHORT_DESCRIPTION, (atip + accelerator).trim());
        putValue(NAME, (atext + accelerator).trim());
    }

    @Override
    public final void actionPerformed(ActionEvent e) {
        Optional<LocalizedItemUI> focus = itemComponent.getPanel().getFocus();
        if (doAction(itemComponent)) {
            focus.ifPresent(f -> f.getEditor().requestFocusInWindow());
        }
    }
}
