package io.ultreia.java4all.i18n.editor.ui;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import org.jdesktop.swingx.JXList;
import io.ultreia.java4all.i18n.editor.action.GoFirstAction;
import io.ultreia.java4all.i18n.editor.action.GoLastAction;
import io.ultreia.java4all.i18n.editor.action.GoNextAction;
import io.ultreia.java4all.i18n.editor.action.GoPreviousAction;
import io.ultreia.java4all.i18n.editor.action.GoTemplatesAction;
import io.ultreia.java4all.i18n.editor.action.GoTranslationsAction;
import io.ultreia.java4all.i18n.editor.model.Item;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.ScrollPaneConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Set;
import java.util.function.Function;

/**
 * Created by tchemit on 20/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ItemComponentTab {

    private final int tabIndex;
    private final ItemComponent parent;
    private final Function<String, Item> itemGetter;
    private final JXList list;

    private final JButton first;
    private final JButton previous;
    private final JButton next;
    private final JButton last;
    private final JScrollPane scrollPane;
    private final StringDefaultListModel listModel;
    private String title;
    private RowFilter<ListModel, Integer> filter;

    ItemComponentTab(int tabIndex,
                     ItemComponent parent,
                     Function<String, Item> itemGetter,
                     String title,
                     Set<String> items) {
        this.tabIndex = tabIndex;
        this.parent = parent;
        this.itemGetter = itemGetter;
        this.title = title;

        listModel = new StringDefaultListModel();
        items.forEach(listModel::addElement);
        list = new JXList(listModel);

        list.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setAutoCreateRowSorter(true);

        list.setCellRenderer(new ItemListCellRenderer(itemGetter));

        list.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(GoTranslationsAction.KEY_STROKE, "none");
        list.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(GoTemplatesAction.KEY_STROKE, "none");

        scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setViewportView(list);

        JToolBar navigationActionsPane = new JToolBar();
        navigationActionsPane.setFloatable(false);
        navigationActionsPane.setBorderPainted(false);
        navigationActionsPane.add(first = new JButton(new GoFirstAction(parent)));
        navigationActionsPane.add(previous = new JButton(new GoPreviousAction(parent)));
        navigationActionsPane.add(next = new JButton(new GoNextAction(parent)));
        navigationActionsPane.add(last = new JButton(new GoLastAction(parent)));

        JPanel header = new JPanel();
        header.setLayout(new BorderLayout());
        header.add(navigationActionsPane, BorderLayout.CENTER);

        scrollPane.setColumnHeaderView(header);

        first.setFocusPainted(false);
        first.setFocusable(false);

        previous.setFocusPainted(false);
        previous.setFocusable(false);

        next.setFocusPainted(false);
        next.setFocusable(false);

        last.setFocusPainted(false);
        last.setFocusable(false);
        last.setDisplayedMnemonicIndex(last.getText().length() - 2);
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public boolean isFirst() {
        return getSelectedIndex() == 0;
    }

    public boolean isLast() {
        return getSelectedIndex() + 1 == getItemCount();
    }

    public JXList getList() {
        return list;
    }

    public int getSelectedIndex() {
        return list.getSelectedIndex();
    }

    public void setSelectedIndex(int selectedIndex) {
        list.getSelectionModel().setSelectionInterval(selectedIndex, selectedIndex);
        list.scrollRectToVisible(list.getCellBounds(selectedIndex, selectedIndex));
    }

    public void clearSelection() {
        list.getSelectionModel().clearSelection();
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public int getItemCount() {
        return list.getRowSorter().getViewRowCount();
    }

    public Item get(String itemKey) {
        return itemKey == null ? null : itemGetter.apply(itemKey);
    }

    public String getTitle() {
        return title;
    }

    public StringDefaultListModel getListModel() {
        return listModel;
    }

    public void refreshActions() {
        boolean notEmpty = isNotEmpty();
        boolean notFirst = !isFirst();
        boolean notLast = !isLast();
        previous.setEnabled(notEmpty && notFirst);
        first.setEnabled(notEmpty && notFirst);
        next.setEnabled(notEmpty && notLast);
        last.setEnabled(notEmpty && notLast);
    }

    void repaintList() {
        list.repaint(list.getCellBounds(list.getSelectedIndex(), list.getSelectedIndex()));
    }

    public void updateListFromUserFilter(String text) {
        RowFilter<ListModel, Integer> rowFilter = new RowFilter<ListModel, Integer>() {
            @Override
            public boolean include(Entry<? extends ListModel, ? extends Integer> entry) {
                if (text == null) {
                    return true;
                }
                for (int i = entry.getValueCount() - 1; i >= 0; i--) {
                    String itemKey = entry.getStringValue(i);
                    Item item = get(itemKey);
                    boolean match = itemKey.contains(text) || item.getItems().stream().anyMatch(e -> e.getLastValidValue().contains(text));
                    if (match) {
                        return true;
                    }
                }
                return false;
            }
        };
        if (filter == null) {
            list.setRowFilter(rowFilter);
        } else {
            list.setRowFilter(RowFilter.andFilter(ImmutableList.of(filter, rowFilter)));
        }
        updateTabTitle();
        refreshActions();
    }

    public void updateListFromFilter(boolean add) {
        if (add) {
            filter = new RowFilter<ListModel, Integer>() {
                @Override
                public boolean include(Entry<? extends ListModel, ? extends Integer> entry) {
                    for (int i = entry.getValueCount() - 1; i >= 0; i--) {
                        String itemKey = entry.getStringValue(i);
                        Item item = get(itemKey);
                        if (item.isGlobalModified() || !item.isSane()) {
                            return true;
                        }
                    }
                    return false;
                }
            };
            list.setRowFilter(filter);
        } else {
            list.setRowFilter(filter = null);
        }
        updateTabTitle();
        refreshActions();
    }

    public void updateTabTitle() {
        int itemCount = getItemCount();
        String title = String.format("%s (%d/%s)", this.title, itemCount, listModel.size());
        JComponent navigationContent = parent.getNavigationContent();
        if (navigationContent instanceof JTabbedPane) {
            ((JTabbedPane) navigationContent).setTitleAt(tabIndex, title);
        }
    }

    static class ItemListCellRenderer extends DefaultListCellRenderer {

        private final Function<String, Item> generator;
        private Font font;

        ItemListCellRenderer(Function<String, Item> generator) {
            this.generator = generator;
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Item item = generator.apply((String) value);
            if (item.isGlobalModified()) {
                value = value + " *";
            }
            JComponent component = (JComponent) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            Color color;
            if (font == null) {
                font = getFont();
            }
            Font font;
            if (!item.isSane()) {
                color = Color.RED;
            } else {
                color = item.isGlobalModified() ? Color.BLUE : Color.BLACK;
            }

            if (item.isGlobalModified()) {
                font = this.font.deriveFont(Font.ITALIC | Font.BOLD);
            } else if (!item.isSane()) {
                font = this.font;
            } else {
                font = this.font.deriveFont(~Font.BOLD);
            }

            component.setForeground(color);
            component.setFont(font);

            return component;
        }
    }

    static class StringDefaultListModel extends DefaultListModel<String> {
        @Override
        public void fireContentsChanged(Object source, int index0, int index1) {
            super.fireContentsChanged(source, index0, index1);
        }
    }
}
