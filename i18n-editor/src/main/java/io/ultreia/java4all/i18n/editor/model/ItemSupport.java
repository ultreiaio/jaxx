package io.ultreia.java4all.i18n.editor.model;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractBean;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ItemSupport extends AbstractBean {

    public static final String PROPERTY_MODIFIED = "modified";
    public static final String PROPERTY_GLOBAL_MODIFIED = "globalModified";
    public static final String PROPERTY_SANE = "sane";

    public abstract boolean isModified();

    public abstract boolean isSane();

    public abstract boolean isGlobalModified();

    public abstract void reset();

    public abstract void resetGlobal();

    public abstract void save();

    void fireGlobalModified(Boolean oldValue) {
        firePropertyChange(PROPERTY_GLOBAL_MODIFIED, oldValue, isGlobalModified());
    }

    void fireModified(Boolean oldValue) {
        firePropertyChange(PROPERTY_MODIFIED, oldValue, isModified());
    }

    void fireSane(Boolean oldValue) {
        firePropertyChange(PROPERTY_SANE, oldValue, isModified());
    }
}
