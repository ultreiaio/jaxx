package io.ultreia.java4all.i18n.editor.action;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.editor.ui.ItemComponent;

/**
 * Created by tchemit on 09/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GoNextAction extends ItemActionSupport {

    public GoNextAction(ItemComponent itemComponent) {
        super(itemComponent,
              "",
              "Go to next item",
              (int) 'N',
              "next");
    }


    @Override
    protected boolean doAction(ItemComponent itemComponent) {

        int selectedIndex = itemComponent.getSelectedIndex();
        if (itemComponent.getItemCount() > selectedIndex) {
            itemComponent.setSelectedIndex(selectedIndex + 1);
            return true;
        }
        return false;
    }
}
