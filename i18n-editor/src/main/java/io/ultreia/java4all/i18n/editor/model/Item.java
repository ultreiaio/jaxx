package io.ultreia.java4all.i18n.editor.model;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;

import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Item extends ItemSupport implements Comparable<Item>, Iterable<LocalizedItem> {

    private final String key;
    private final ImmutableSet<LocalizedItem> items;

    private boolean adjusting;

    public Item(String key, Set<Locale> locales, String... values) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(locales);
        Objects.requireNonNull(values);
        if (locales.size() != values.length) {
            throw new IllegalStateException("Mismatch lengths!!!!");
        }
        this.key = key;

        ImmutableSet.Builder<LocalizedItem> builder = ImmutableSet.builder();

        int i = 0;
        for (Locale locale : locales) {
            LocalizedItem item = new LocalizedItem(locale, values[i++]);
            item.addPropertyChangeListener(LocalizedItem.PROPERTY_MODIFIED, evt -> fireModified(null));
            item.addPropertyChangeListener(LocalizedItem.PROPERTY_GLOBAL_MODIFIED, evt -> fireGlobalModified(null));
            builder.add(item);
        }
        this.items = builder.build();
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean isModified() {
        return items.stream().anyMatch(LocalizedItem::isModified);
    }

    @Override
    public boolean isGlobalModified() {
        return items.stream().anyMatch(LocalizedItem::isGlobalModified);
    }

    public Set<LocalizedItem> getGlobalModifiedItems() {
        return items.stream().filter(LocalizedItem::isGlobalModified).collect(Collectors.toSet());
    }

    public ImmutableSet<LocalizedItem> getItems() {
        return items;
    }

    @Override
    public boolean isSane() {
        for (LocalizedItem item : items) {
            if (!item.isSane()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void reset() {
        boolean sane = isSane();
        boolean modified = isModified();
        adjusting = true;
        try {
            items.forEach(LocalizedItem::reset);
        } finally {
            adjusting = false;
        }
        fireModified(modified);
        fireSane(sane);
    }

    @Override
    public void resetGlobal() {
        boolean sane = isSane();
        boolean globalModified = isGlobalModified();
        adjusting = true;
        try {
            items.forEach(LocalizedItem::resetGlobal);
        } finally {
            adjusting = false;
        }
        fireGlobalModified(globalModified);
        fireSane(sane);
    }

    @Override
    public void save() {
        boolean modified = isModified();
        boolean globalModified = isGlobalModified();
        boolean sane = isSane();
        adjusting = true;
        try {
            items.stream().filter(LocalizedItem::isModified).forEach(LocalizedItem::save);
        } finally {
            adjusting = false;
        }
        fireModified(modified);
        fireGlobalModified(globalModified);
        fireSane(sane);
    }

    @Override
    public int compareTo(Item o) {
        return key.compareTo(o.getKey());
    }

    @Override
    public Iterator<LocalizedItem> iterator() {
        return items.iterator();
    }

    @Override
    void fireGlobalModified(Boolean oldValue) {
        if (!adjusting) {
            super.fireGlobalModified(oldValue);
        }
    }

    @Override
    void fireModified(Boolean oldValue) {
        if (!adjusting) {
            super.fireModified(oldValue);
        }
    }

    @Override
    void fireSane(Boolean oldValue) {
        if (!adjusting) {
            super.fireSane(oldValue);
        }
    }

    Set<? extends Locale> getModifiedLocales() {
        return items.stream().filter(LocalizedItem::isGlobalModified).map(LocalizedItem::getLocale).collect(Collectors.toSet());
    }

}
