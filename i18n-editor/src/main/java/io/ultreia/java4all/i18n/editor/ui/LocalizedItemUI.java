package io.ultreia.java4all.i18n.editor.ui;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.JXTitledPanel;
import io.ultreia.java4all.i18n.editor.model.LocalizedItem;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class LocalizedItemUI extends JXTitledPanel {

    private static Border focusBorder = new LineBorder(new Color(64, 64, 64), 3, true);
    private static Border noFocusBorder = new LineBorder(new Color(192, 192, 192), 3, true);

    private final JLabel rightDecoration;

    static LocalizedItemUI createTranslation(Locale locale) {
        return new LocalizedItemUI(locale) {
            @Override
            public JTextComponent createEditor() {
                return new JTextField();
            }
        };
    }

    static LocalizedItemUI createTemplate(Locale locale) {
        return new LocalizedItemUI(locale) {
            @Override
            public JTextComponent createEditor() {
                return new JTextArea();
            }
        };
    }

    private final Locale locale;
    private final JTextComponent editor;
    private final PropertyChangeListener modifiedChanged;
    private final PropertyChangeListener globalModifiedChanged;
    private final PropertyChangeListener lastValidaValueChanged;
    private final PropertyChangeListener saneChanged;

    private LocalizedItem model;

    private Color titleColor;
    private Font titleFont;
    private String titleText;

    private Color editorColor;
    private Font editorFont;

    public abstract JTextComponent createEditor();

    private LocalizedItemUI(Locale locale) {
        this.locale = locale;
        editor = createEditor();
        JLabel leftDecoration = new JLabel(SwingUtil.createActionIcon("i18n-" + locale.getLanguage()));
        rightDecoration = new JLabel(SwingUtil.createActionIcon("save"));
        setLeftDecoration(leftDecoration);
        JScrollPane scrollPane = new JScrollPane(editor);
        setContentContainer(scrollPane);

        editor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                valueChanged();
            }
        });
        editor.setBorder(noFocusBorder);
        globalModifiedChanged = e2 -> modifiedChanged();
        modifiedChanged = e1 -> modifiedChanged();
        lastValidaValueChanged = e -> lastValidValueChanged();
        saneChanged = e -> modifiedChanged();
        editor.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                editor.setBorder(focusBorder);
                ItemUI parent = (ItemUI) LocalizedItemUI.this.getParent().getParent();
                parent.focus(LocalizedItemUI.this);
            }

            @Override
            public void focusLost(FocusEvent e) {
                editor.setBorder(noFocusBorder);
            }
        });
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    void setModel(LocalizedItem model) {
        Objects.requireNonNull(model);
        if (this.model != null) {
            this.model.removePropertyChangeListener(LocalizedItem.PROPERTY_MODIFIED, modifiedChanged);
            this.model.removePropertyChangeListener(LocalizedItem.PROPERTY_GLOBAL_MODIFIED, globalModifiedChanged);
            this.model.removePropertyChangeListener(LocalizedItem.PROPERTY_LAST_VALID_VALUE, lastValidaValueChanged);
        }
        this.model = model;
        this.model.addPropertyChangeListener(LocalizedItem.PROPERTY_MODIFIED, modifiedChanged);
        this.model.addPropertyChangeListener(LocalizedItem.PROPERTY_GLOBAL_MODIFIED, globalModifiedChanged);
        this.model.addPropertyChangeListener(LocalizedItem.PROPERTY_LAST_VALID_VALUE, lastValidaValueChanged);
        this.model.addPropertyChangeListener(LocalizedItem.PROPERTY_SANE, saneChanged);

        modifiedChanged();
        lastValidValueChanged();

        if (Objects.equals(this, ((ItemUI) LocalizedItemUI.this.getParent().getParent()).getFocus().orElse(null))) {
            editor.setBorder(focusBorder);
            if (!editor.hasFocus()) {
                editor.requestFocusInWindow();
            }
        }
    }

    Optional<LocalizedItem> getModel() {
        return Optional.ofNullable(model);
    }

    private String getValue() {
        return editor.getText().trim();
    }

    private void valueChanged() {
        if (model == null) {
            return;
        }
        model.setValue(getValue());

        modifiedChanged();
    }

    private void lastValidValueChanged() {
        if (model == null) {
            return;
        }
        editor.setText(model.getLastValidValue());
    }

    private void modifiedChanged() {
        if (model == null) {
            return;
        }
        if (model.isModified()) {

            editorColor = Color.BLUE;
            editorFont = getFont().deriveFont(Font.ITALIC);

        } else {
            editorColor = Color.BLACK;
            editorFont = getFont();
        }

        int fontStyle = 0;

        titleColor = Color.BLACK;
        titleText = locale.getDisplayName();
        if (!model.isSane()) {
            fontStyle |= Font.BOLD;

        } else if (model.isGlobalModified()) {
            titleColor = Color.BLUE;
        }

        if (model.isModified()) {
            fontStyle |= Font.ITALIC;
            titleText += " *";

        }
        if (model.isGlobalModified()) {
            fontStyle |= Font.ITALIC | Font.BOLD;
        }
        if (fontStyle == 0) {
            titleFont = getFont();
        } else {
            titleFont = getFont().deriveFont(fontStyle);
        }

        if ((model.isModified() && !model.isValueSane())) {
            editorColor = Color.RED;
        }
        if (!model.isModified() && !model.isSane()) {
            editorColor = Color.RED;
        }
        if (!(model.isValueSane() && model.isSane())) {
            titleColor = Color.RED;
        }

        updateTitle();
    }

    private void updateTitle() {
        editor.setFont(editorFont);
        editor.setForeground(editorColor);
        setTitleFont(titleFont);
        setTitleForeground(titleColor);
        setTitle(titleText);
        setRightDecoration(model.isGlobalModified() ? rightDecoration : null);

    }

    public JTextComponent getEditor() {
        return editor;
    }

}
