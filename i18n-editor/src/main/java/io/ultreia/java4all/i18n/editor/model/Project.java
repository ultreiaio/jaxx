package io.ultreia.java4all.i18n.editor.model;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import io.ultreia.java4all.i18n.editor.action.ExitAction;
import io.ultreia.java4all.i18n.runtime.I18nLanguage;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Project {

    /**
     * Logger.
     */
    private static final Logger log = LogManager.getLogger(Project.class);

    private final I18nLanguageProvider languageProvider;
    private final SortedSet<String> translationKeys;
    private final SortedSet<String> templateNames;
    private final SortedSet<Item> translations;
    private final SortedSet<Item> templates;
    private final Path i18nDirectory;
    private final String sessionId;
    private Date lastExportDate;
    /**
     * a flag to keep in mind if user has ask to quit.
     *
     * This is used if user ask to quit, but finally do an export before quitting
     *
     * @see ExitAction
     */
    private boolean askToQuit;

    public static HashMultimap<Locale, String> getTranslationKeys(I18nLanguageProvider languageProvider) {
        HashMultimap<Locale, String> translationKeys = HashMultimap.create();
        for (Locale locale : languageProvider.getLocales()) {
            Properties properties = languageProvider.getLanguage(locale).getTranslations();
            properties.keySet().forEach(o -> {
                translationKeys.put(null, (String) o);
                translationKeys.put(locale, (String) o);
            });
        }
        return translationKeys;
    }

    public Project(I18nLanguageProvider languageProvider, Path i18nDirectory) {

        this.languageProvider = Objects.requireNonNull(languageProvider);
        this.i18nDirectory = Objects.requireNonNull(i18nDirectory);
        this.sessionId = String.format("%s-%s-%s", getName(), getVersion(), new SimpleDateFormat("dd-MM-yyy-HH-mm").format(new Date()));

        ImmutableSet.Builder<Item> translationItemsBuilder = ImmutableSet.builder();

        Set<Locale> locales = this.languageProvider.getLocales();

        Map<Locale, Properties> bundles = new LinkedHashMap<>();
        for (Locale locale : locales) {
            bundles.put(locale, this.languageProvider.getLanguage(locale).getTranslations());
        }

        HashMultimap<Locale, String> translationKeys = getTranslationKeys(this.languageProvider);
        this.translationKeys = new TreeSet<>(translationKeys.get(null));
        Iterator<String> iterator = this.translationKeys.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();

            Set<Locale> missingLocales = new LinkedHashSet<>();
            List<String> translations = new LinkedList<>();
            for (Locale locale : locales) {
                if (translationKeys.containsEntry(locale, key)) {
                    translations.add(bundles.get(locale).getProperty(key));
                } else {
                    missingLocales.add(locale);
                }
            }
            if (!missingLocales.isEmpty()) {
                log.warn(String.format("Key [%s] is missing for locale(s): %s", key, missingLocales));
                iterator.remove();
                continue;
            }

            translationItemsBuilder.add(new Item(key, locales, translations.toArray(new String[0])));
        }

        this.translations = new TreeSet<>(translationItemsBuilder.build());

        ImmutableSet.Builder<Item> templateItemsBuilder = ImmutableSet.builder();

        this.templateNames = new TreeSet<>(this.languageProvider.getApplicationDefinition().getTemplateList());

        Iterator<String> iterator1 = templateNames.iterator();
        while (iterator1.hasNext()) {
            String templateName = iterator1.next();

            Set<Locale> missingLocales = new LinkedHashSet<>();
            List<String> templateContents = new LinkedList<>();

            for (Locale locale : locales) {
                I18nLanguage language = this.languageProvider.getLanguage(locale);
                String templateContent = language.getTemplate(templateName);
                if (templateContent == null) {
                    missingLocales.add(locale);
                } else {
                    templateContents.add(templateContent);
                }
            }
            if (!missingLocales.isEmpty()) {

                log.warn(String.format("Template [%s] is missing for locale(s): %s", templateName, missingLocales));
                iterator1.remove();
                continue;
            }
            templateItemsBuilder.add(new Item(templateName, locales, templateContents.toArray(new String[0])));

        }

        this.templates = new TreeSet<>(templateItemsBuilder.build());
    }

    public Path getI18nDirectory() {
        return i18nDirectory;
    }

    public Path getI18nUserTranslationsDirectory() {
        return getI18nDirectory().resolve(I18nTranslationSetDefinition.PATH);
    }

    public Path getI18nUserTemplatesDirectory() {
        return getI18nDirectory().resolve(I18nTemplateDefinition.PATH);
    }

    public Path getI18nEditorDirectory() {
        return getI18nDirectory().resolve("i18n-editor");
    }

    public Item getTemplate(String templateName) {
        return templates.stream().filter(i -> Objects.equals(templateName, i.getKey())).findFirst().orElseThrow(IllegalStateException::new);
    }

    public Item getTranslation(String key) {
        return translations.stream().filter(i -> Objects.equals(key, i.getKey())).findFirst().orElse(null);
    }

    public Set<Locale> getLocales() {
        return languageProvider.getLocales();
    }

    public Set<String> getTranslationKeys() {
        return translationKeys;
    }

    public Set<String> getTemplateNames() {
        return templateNames;
    }

    public boolean isModified() {
        return translations.stream().anyMatch(Item::isGlobalModified) || templates.stream().anyMatch(Item::isGlobalModified);
    }

    public String getSessionId() {
        return sessionId;
    }

    public Path getExportFile() {
        return getI18nEditorDirectory().resolve(String.format("%s.zip", getSessionId()));
    }

    public String getName() {
        return languageProvider.getApplicationDefinition().getName();
    }

    public Version getVersion() {
        return Version.valueOf(languageProvider.getApplicationDefinition().getVersion());
    }

    public I18nLanguageProvider getLanguageProvider() {
        return languageProvider;
    }

    public Set<Item> getModifiedTranslations() {
        return translations.stream().filter(Item::isGlobalModified).collect(Collectors.toSet());
    }

    public Set<Item> getModifiedTemplates() {
        return templates.stream().filter(Item::isGlobalModified).collect(Collectors.toSet());
    }

    public void setLastExportDate(Date lastExportDate) {
        this.lastExportDate = lastExportDate;
    }

    public Date getLastExportDate() {
        return lastExportDate;
    }

    public void setAskToQuit(boolean askToQuit) {
        this.askToQuit = askToQuit;
    }

    public boolean isAskToQuit() {
        return askToQuit;
    }
}
