package io.ultreia.java4all.i18n.editor.model;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Locale;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LocalizedItem extends ItemSupport {

    public static final String PROPERTY_LAST_VALID_VALUE = "lastValidValue";
    private final Locale locale;
    private final String originalValue;
    private String lastValidValue;
    private String value;

    LocalizedItem(Locale locale, String originalValue) {
        this.locale = locale;
        this.originalValue = originalValue.trim();
        this.lastValidValue = originalValue.trim();
        this.value = originalValue.trim();
    }

    public void setValue(String value) {
        boolean sane = isSane();
        boolean modified = isModified();
        this.value = value;
        fireModified(modified);
        fireSane(sane);
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLastValidValue() {
        return lastValidValue;
    }

    @Override
    public boolean isGlobalModified() {
        return !Objects.equals(originalValue, lastValidValue);
    }

    @Override
    public boolean isModified() {
        return !Objects.equals(lastValidValue, value);
    }

    @Override
    public boolean isSane() {
        return StringUtils.isNotEmpty(lastValidValue) && !lastValidValue.contains("#TODO");
    }

    public boolean isValueSane() {
        return StringUtils.isNotEmpty(value) && !value.contains("#TODO");
    }

    @Override
    public void reset() {
        String value = this.value;
        boolean modified = isModified();
        this.value = lastValidValue;
        fireModified(modified);
        // trick: we compare with old value, should fire something if value changed...
        fireLastValidValue(value);
    }

    @Override
    public void resetGlobal() {
        boolean sane = isSane();
        boolean modified = isModified();
        boolean globalModified = isGlobalModified();
        String lastValidValue = this.lastValidValue;
        this.lastValidValue = originalValue;
        this.value = originalValue;
        fireModified(modified);
        fireGlobalModified(globalModified);
        fireLastValidValue(lastValidValue);
        fireSane(sane);
    }

    @Override
    public void save() {
        boolean sane = isSane();
        boolean modified = isModified();
        boolean globalModified = isGlobalModified();
        String oldLastValidValue = this.lastValidValue;
        this.lastValidValue = value;
        fireModified(modified);
        fireGlobalModified(globalModified);
        fireLastValidValue(oldLastValidValue);
        fireSane(sane);
    }

    private void fireLastValidValue(String oldValue) {
        firePropertyChange(PROPERTY_LAST_VALID_VALUE, oldValue, lastValidValue);
    }

}
