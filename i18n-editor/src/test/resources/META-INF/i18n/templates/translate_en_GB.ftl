<#--
 #%L
 JAXX :: I18n Editor
 %%
 Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 You should have received a copy of the GNU General Lesser Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
<body>
<h2>Comment traduire ObServe</h2>Vous pouvez nous aider à traduire l'application.
<hr/>
<br/>
<ul>
  <li>Récupérer le fichier <a href="${file}">observe-i18n.zip</a>. Il s'agit d'une archive comprenant les templates utilisées dans l'application ainsi que le fichier <i>csv</i> qui contient toutes les traductions de l'application.
  <li>Ouvrez le fichier <i>observe-i18n.csv</i> dans un tableur avec les options suivantes :
    <dl>
      <dt>caractère séparateur</dt>
      <dd><strong><i>|</i></strong></dd>
      <dt>encoding</dt>
      <dd><strong><i>UTF-8</i></strong></dd>
      <dt>texte encapsulé par des</dt>
      <dd><strong><i>"</i></strong></dd>
    </dl>
  </li>
  <li>Traduisez, Améliorer, ...</li>
  <li>Enfin renvoyez-le nous ce que vous avez modifié.</li>
</ul>
<br/>
<p>
  Vos modifications seront prises en compte dans une version utlérieure.
</p>
</body>
</html>
