<#--
 #%L
 JAXX :: I18n Editor
 %%
 Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 You should have received a copy of the GNU General Lesser Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
<body>
Droits :

<strong> &bull; Référentiel : </strong>
<#if canReadReferential() >
  Lecture
  <#if canWriteReferential() >
    / Ecriture
  </#if>
  <#elseif canWriteReferential()>
    Ecriture
    <#else>
      Aucun droit
</#if>

<strong> &bull; Données observateur : </strong>
<#if canReadData() >
  Lecture
  <#if canWriteData() >
    / Ecriture
  </#if>
  <#elseif canWriteData() >
    Ecriture
    <#else>
      Aucun droit
</#if>
<Strong> &bull; Version : </Strong>
    v ${version}

</body>
</html>
