<#--
 #%L
 JAXX :: I18n Editor
 %%
 Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 You should have received a copy of the GNU General Lesser Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
<body>

<h3>Loading a data sourced</h3>

<p>
    The local database does not exist (path ${localDb.absolutePath}).
</p>

You can now:

<ul>
<#if withBackup>
    <li>use last automatic backup (${backupDate})</li>
</#if>
    <li>create a new local database</li>
    <li>connect to a remote database</li>
</ul>

</body>
</html>
