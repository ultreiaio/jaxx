<#--
 #%L
 JAXX :: I18n Editor
 %%
 Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 You should have received a copy of the GNU General Lesser Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<html>
<body>
<h3>ObServe</h3>
<a href="http://www.ird.fr/informatique-scientifique/projets/observe/">Système d'Information,
  d'Observation et de Suivi des pêches thonières tropicales de surface.</a>
<hr/>
<p>
  Application de saisie des données observateurs et de consultation de telles données depuis une base <i>Obstuna</i>.
</p>
<p>
  Ce projet a été initiée en 2008 par l'unité US 007-OSIRIS de <a href="http://www.ird.fr">l'IRD</a> dans le cadre
  d'un
  <a href="http://www.ird.fr/informatique-scientifique/soutien/spirales/anciens_projets/affiche_projet.php?code=2008.11">project
    spirale</a>.</p>
<p>
  Il a été réalisé par la société <a href="http://codelutin.com">Code Lutin</a> en 2009.
</p>
<br/>
<hr/>
<p>
  Pour plus d'informations, vous pouvez visiter le <a href="http://observe.codelutin.com">site du projet</a>.
</p>

<h4>Version du locigiel</h4>
<dl>
  <dt>Version</dt>
  <dd>${buildVersion}</dd>
  <dt>Date</dt>
  <dd>${buildDate?string('dd.MM.yyyy HH:mm:ss')}</dd>
  <dt>Numéro de build</dt>
  <dd><a href="https://gitlab.nuiton.org/codelutin/observe/commit/${buildNumber}">${buildNumber}</a></dd>
</dl>

</body>
</html>
