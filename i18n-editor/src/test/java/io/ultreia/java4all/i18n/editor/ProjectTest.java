package io.ultreia.java4all.i18n.editor;

/*-
 * #%L
 * JAXX :: I18n Editor
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.boot.UserI18nBootLoader;
import org.junit.Test;
import io.ultreia.java4all.i18n.editor.model.Project;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by tchemit on 07/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProjectTest {

    @Test
    public void test() throws IOException {
        Path path = Paths.get("/tmp/" + ProjectTest.class.getName() + "/" + System.nanoTime());
        if (Files.notExists(path)) {
            Files.createDirectories(path);
        }

        I18n.init(new UserI18nBootLoader(path, new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration())), null);
        Project projectModel = new Project(I18n.getLanguageProvider(), path);

        I18nLanguageProvider definitionFile = projectModel.getLanguageProvider();
//        definitionFile.store(path.toFile());
    }
}
