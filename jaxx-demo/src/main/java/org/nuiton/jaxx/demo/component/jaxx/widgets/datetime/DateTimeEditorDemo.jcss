/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

#configPanel {
  border:{new TitledBorder(t("jaxxdemo.DateTimeEditor.configuration"))};
  layout:{new GridLayout(0,1)};
}

#labelConfigLabel {
  text:"jaxxdemo.DateTimeEditor.configuration.label";
}

#labelConfig {
  text:{t("jaxxdemo.DateTimeEditor.configuration.label.value")};
}

#formatConfigLabel {
  text:"jaxxdemo.DateTimeEditor.configuration.format";
}

#formatConfig {
  text:{t("jaxxdemo.DateTimeEditor.configuration.format.value")};
}

#enableButton {
  text: "jaxxdemo.DateTimeEditor.enable";
  selected: true;
}

#dateEditableButton {
  text: "jaxxdemo.DateTimeEditor.dateEditable";
  selected: true;
}

#timeEditableButton {
  text: "jaxxdemo.DateTimeEditor.timeEditable";
  selected: true;
}

#editor {
  bean:{model};
  propertyDate:date;
  propertyDayDate:dayDate;
  propertyTimeDate:timeDate;
  border:{new TitledBorder(t("jaxxdemo.DateTimeEditor.editor"))};
  date:{model.getDate()};
  label:{labelConfig.getText()};
  dateFormat:{formatConfig.getText()};
  enabled:{enableButton.isSelected()};
  dateEditable:{dateEditableButton.isSelected()};
  timeEditable:{timeEditableButton.isSelected()};
}

#result {
  border: {new TitledBorder(t("jaxxdemo.DateTimeEditor.result"))};
}

#resultDateLabel{
  text: "jaxxdemo.DateTimeEditor.result.date";
}

#resultDate{
  editable:false;
  text: {handler.getDate(model.getDate())};
}

#resultDayDateLabel{
  text: "jaxxdemo.DateTimeEditor.result.dayDate";
}

#resultDayDate{
  editable:false;
  text: {handler.getDayDate(model.getDayDate())};
}

#resultTimeDateLabel{
  text: "jaxxdemo.DateTimeEditor.result.timeDate";
}

#resultTimeDate{
  editable:false;
  text: {handler.getTimeDate(model.getTimeDate())};
}
