package org.nuiton.jaxx.demo;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2020 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;

/**
 * Created on 1/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class DemoApplicationContext extends DefaultApplicationContext {

    static final JAXXContextEntryDef<DemoUI> MAIN_UI =
            JAXXUtil.newContextEntryDef("MainUI", DemoUI.class);
    /**
     * The singleton instance of the main context
     */
    private static volatile DemoApplicationContext context;

    /**
     * @return <code>true</code> si le context a été initialisé via la méthode
     * {@link #init(DemoConfig)}, <code>false</code> autrement.
     */
    protected static boolean isInit() {
        return context != null;
    }

    /**
     * Permet l'initialisation du contexte applicatif et positionne
     * l'context partagée.
     * <p>
     * Note : Cette méthode ne peut être appelée qu'une seule fois.
     *
     * @param config application config
     * @return le context partagée
     * @throws IllegalStateException si un contexte applicatif a déja été positionné.
     */
    protected static DemoApplicationContext init(DemoConfig config) throws IllegalStateException {
        if (isInit()) {
            throw new IllegalStateException("there is an already application context registred.");
        }
        context = new DemoApplicationContext(config);
        return context;
    }

    /**
     * Récupération du contexte applicatif.
     *
     * @return l'context partagé du contexte.
     * @throws IllegalStateException si le contexte n'a pas été initialisé via
     *                               la méthode {@link #init(DemoConfig)}
     */
    public static DemoApplicationContext get() throws IllegalStateException {
        if (!isInit()) {
            throw new IllegalStateException("no application context registred.");
        }
        return context;
    }

    public DemoApplicationContext(DemoConfig config) {

        // share config
        setContextValue(config);

    }

    public DemoConfig getConfig() {
        return getContextValue(DemoConfig.class);
    }

    public DemoUI getMainUI() {

        return MAIN_UI.getContextValue(this);
    }

}
