<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2020 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.

  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->


<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    org.nuiton.jaxx.runtime.swing.SwingUtil
    org.nuiton.jaxx.runtime.swing.BlockingLayerUI
    org.nuiton.jaxx.runtime.swing.BlockingLayerUI2

    java.awt.Color
    java.awt.event.ActionEvent

    javax.swing.AbstractAction
    javax.swing.DefaultListModel
    javax.swing.JComponent
  </import>
  <BlockingLayerUI id='layerUI'
                   acceptAction='{new AbstractAction() {
                     private static final long serialVersionUID = 1L;
                     @Override
                     public void actionPerformed(ActionEvent e) {
                        BoxedDecoratorDemo.this.accept(e, "from icon of layer");
                     }
                     }}'/>
  <BlockingLayerUI2 id='layerUI2'
                    acceptAction='{new AbstractAction() {
                     private static final long serialVersionUID = 1L;
                     @Override
                     public void actionPerformed(ActionEvent e) {
                        BoxedDecoratorDemo.this.accept(e, "from icon of layer");
                     }
                     }}'/>
  <script><![CDATA[

void $afterCompleteSetup() {
    for (JComponent boxed : SwingUtil.getLayeredComponents(this)) {
        if (boxed == buttonD) {
            SwingUtil.setLayerUI(boxed, layerUI2);
            continue;
        }

        BlockingLayerUI ui = null;
        try {
            ui = layerUI.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        if (boxed == buttonC) {
            ui.setBlock(true);
        }
        SwingUtil.setLayerUI(boxed, ui);
    }
}

public void setLayer(boolean active) {
    for (JComponent boxed : SwingUtil.getLayeredComponents(this)) {
        if (boxed == buttonD) {
            continue;
        }
        BlockingLayerUI ui = (BlockingLayerUI) SwingUtil.getLayer(boxed).getUI();
        if (boxed == buttonC) {
            ui.setBlock(active);
        } 
        ui.setUseIcon(active);
    }
}

protected void accept(ActionEvent e, String suffix) {
    JButton source = (JButton) e.getSource();
    String clickedMessage = (String) source.getClientProperty("clickedText");
    String msg = "'" + source.getText() + "' clicked - " + suffix + " : " + clickedMessage;
    ((DefaultListModel)messages.getModel()).addElement(msg);
}]]>
  </script>
  <Table fill='both' weightx='1' constraints='BorderLayout.CENTER'>
    <row>
      <cell>
        <JCheckBox id='toggle' selected='true'
                   text='{toggle.isSelected() ? "Active layer" : "No layer"}'
                   onActionPerformed='setLayer(toggle.isSelected());'/>
      </cell>
    </row>
    <row>
      <cell weighty='0.5'>
        <JPanel id='buttonPane'>
          <JButton id='buttonA' decorator='boxed'
                   _clickedText='"button A was clicked"'
                   onActionPerformed='accept(event, "from button (no layer)")'/>
          <JButton id='buttonB' decorator='boxed'
                   onActionPerformed='accept(event, "from button (no layer)")'/>
          <JButton id='buttonC' decorator='boxed'
                   onActionPerformed='accept(event, "from button (no layer)");'/>
          <JButton id='buttonD' decorator='boxed'
                   onActionPerformed='accept(event, "from button (no layer)");'/>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell weighty='0.5'>
        <JScrollPane>
          <JList id='messages'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>
</org.nuiton.jaxx.demo.DemoPanel>
