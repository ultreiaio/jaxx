/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2020 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.tree;

import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Component;
import java.util.Locale;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class NavDemoTreeCellRenderer extends AbstractNavTreeCellRenderer<DefaultTreeModel, NavDemoTreeNode> {

    /** Logger */
    protected static final Logger log =
            LogManager.getLogger(NavDemoTreeCellRenderer.class);


    public NavDemoTreeCellRenderer(DemoDataProvider provider) {
        setDataProvider(provider);
    }

    @Override
    public DemoDataProvider getDataProvider() {
        return (DemoDataProvider) super.getDataProvider();
    }

    @Override
    protected String computeNodeText(NavDemoTreeNode node) {

        // Get node type
        Class<?> editType = node.getInternalClass();
        String id = node.getId();

        // get decorator
        @SuppressWarnings("rawtypes") Decorator decorator = DecoratorProvider.get().decorator(Locale.ENGLISH, editType);

        Object toDecorate = null;

        // People node
        if (editType.equals(People.class)) {
            toDecorate = getDataProvider().getPeople(id);

            // Movie node
        } else if (editType.equals(Movie.class)) {
            toDecorate = getDataProvider().getMovie(id);
        }

        // Get decorated value
        @SuppressWarnings("unchecked") String decorated = decorator.decorate(toDecorate);

        if (log.isDebugEnabled()) {
            log.debug("Compute text for node " +
                              node + " return " +
                              decorated);
        }

        return decorated;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {

        if (!(value instanceof NavDemoTreeNode)) {
            return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }

        // get node
        NavDemoTreeNode node = (NavDemoTreeNode) value;

        // get text for node
        String text = getNodeText(node);

        // Render node
        return super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
    }
}
