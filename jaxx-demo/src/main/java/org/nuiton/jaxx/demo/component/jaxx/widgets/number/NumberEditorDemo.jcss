/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

NumberEditor {
  bean: {model};
  autoPopup:{autoPopupButton.isSelected()};
  showPopupButton:{showPopupButton.isSelected()};
  showReset:{showResetButton.isSelected()};
}

#numberEditorConfigurationPanel {
  border:{new TitledBorder(t("jaxxdemo.numbereditor.configuration"))};
}

#showPopupButton {
  text:"jaxxdemo.numbereditor.showPopupButton";
  selected:true;
}

#showResetButton {
  text:"jaxxdemo.numbereditor.showReset";
  selected:true;
}

#autoPopupButton {
  text:"jaxxdemo.numbereditor.autoPopup";
  selected:false;
}

#integerNumberConfig {
  text:"jaxxdemo.numbereditor.integerNumberConfig";
  labelFor:{integerNumberPattern};
}

#floatNumberConfig {
  text:"jaxxdemo.numbereditor.floatNumberConfig";
  labelFor:{floatNumberPattern};
}

#doubleNumberConfig {
  text:"jaxxdemo.numbereditor.doubleNumberConfig";
  labelFor:{doubleNumberPattern};
}

#integerNumberEnabled {
  selected:true;
}

#floatNumberEnabled {
  selected:true;
}

#doubleNumberEnabled {
  selected:true;
}


#integerEditor {
  property:integerNumber;
  numberValue: {model.getIntegerNumber()};
  numberType:{Integer.class};
  numberPattern:{integerNumberPattern.getText()};
  enabled:{integerNumberEnabled.isSelected()};
}

#floatEditor {
  property:floatNumber;
  numberValue: {model.getFloatNumber()};
  numberType:{Float.class};
  numberPattern:{floatNumberPattern.getText()};
  enabled:{floatNumberEnabled.isSelected()};
}

#doubleEditor {
  property:doubleNumber;
  numberValue: {model.getDoubleNumber()};
  numberType:{Double.class};
  numberPattern:{doubleNumberPattern.getText()};
  enabled:{doubleNumberEnabled.isSelected()};
}

#resultPanel {
  border:{new TitledBorder(t("jaxxdemo.numbereditor.model"))};
}

#resultInteger{
  text:{t("jaxxdemo.numbereditor.model.integer", model.getIntegerNumber())};
}

#resultFloat{
  text:{t("jaxxdemo.numbereditor.model.float", model.getFloatNumber())};
}

#resultDouble{
  text:{t("jaxxdemo.numbereditor.model.double", model.getDoubleNumber())};
}