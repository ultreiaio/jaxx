<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2020 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.

  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    java.awt.Color
    javax.swing.BorderFactory
  </import>

  <Table id='demoPanel' anchor='north' fill='both'>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <Table anchor='west' fill='both'>
          <row>
            <cell>
              <JLabel text='Text:'/>
            </cell>
            <cell weightx='1'>
              <JTextField id='text' text='Data Binding'/>
            </cell>
          </row>

          <row>
            <cell>
              <JLabel text='Red:'/>
            </cell>
            <cell>
              <JSlider id='red' value='128' maximum='255' styleClass='color'/>
            </cell>
          </row>

          <row>
            <cell>
              <JLabel text='Green:'/>
            </cell>
            <cell>
              <JSlider id='green' value='0' maximum='255' styleClass='color'/>
            </cell>
          </row>

          <row>
            <cell>
              <JLabel text='Blue:'/>
            </cell>
            <cell>
              <JSlider id='blue' value='255' maximum='255' styleClass='color'/>
            </cell>
          </row>

          <row>
            <cell>
              <JLabel text='Size:'/>
            </cell>
            <cell>
              <JSlider id='dummySize' value='36' minimum='6' maximum='60'/>
            </cell>
          </row>

          <row>
            <cell columns='2' fill='both' weighty='1'>
              <JPanel border='{BorderFactory.createTitledBorder("Preview")}'
                      height='90'
                      layout='{new BorderLayout()}'>
                <VBox
                        background='{(Color)( backgroundCheckbox.isSelected() ? backgroundColor.getSelectedValue() : null)}'
                        margin='0'
                        horizontalAlignment='center'
                        verticalAlignment='middle'>
                  <JLabel text='{text.getText()}'
                          font-size='{dummySize.getValue()}'
                          foreground='{new Color(red.getValue(), green.getValue(), blue.getValue())}'/>
                </VBox>
              </JPanel>
            </cell>
          </row>
        </Table>
      </cell>

      <cell>
        <VBox spacing='0'
              border='{BorderFactory.createTitledBorder("Background")}'>
          <JCheckBox id='backgroundCheckbox' text='Show Background'/>
          <JRadioButton text='Red' buttonGroup='backgroundColor'
                        value='{Color.RED}' selected='true'/>
          <JRadioButton text='Orange' buttonGroup='backgroundColor'
                        value='{Color.ORANGE}'/>
          <JRadioButton text='Yellow' buttonGroup='backgroundColor'
                        value='{Color.YELLOW}'/>
          <JRadioButton text='Green' buttonGroup='backgroundColor'
                        value='{Color.GREEN}'/>
          <JRadioButton text='Cyan' buttonGroup='backgroundColor'
                        value='{Color.CYAN}'/>
          <JRadioButton text='Blue' buttonGroup='backgroundColor'
                        value='{Color.BLUE}'/>
          <JRadioButton text='Purple' buttonGroup='backgroundColor'
                        value='{new Color(160, 30, 255)}'/>
        </VBox>
      </cell>
    </row>
  </Table>
</org.nuiton.jaxx.demo.DemoPanel>
