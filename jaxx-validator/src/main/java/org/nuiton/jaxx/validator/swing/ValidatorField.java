package org.nuiton.jaxx.validator.swing;

/*-
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created on 30/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.4
 */
public class ValidatorField {
    private final String validatorId;
    private final String editorName;
    private final String[] propertyName;

    public ValidatorField(String validatorId, String editorName, String... propertyName) {
        this.validatorId = validatorId;
        this.editorName = editorName;
        this.propertyName = propertyName;
    }

    public String validatorId() {
        return validatorId;
    }

    public String editorName() {
        return editorName;
    }

    public String[] propertyName() {
        return propertyName;
    }
}
