/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.tab.TabInfoWithValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.bean.BeanValidatorMessage;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import java.awt.Color;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * The helper class for swing validation module.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
public class SwingValidatorUtil  {

    private static final Logger log = LogManager.getLogger(SwingValidatorUtil.class);

    protected static EnumMap<NuitonValidatorScope, ImageIcon> icons;

    protected static EnumMap<NuitonValidatorScope, Color> colors;

    public static EnumMap<NuitonValidatorScope, ImageIcon> getIcons() {
        if (icons == null) {
            icons = new EnumMap<>(NuitonValidatorScope.class);
            icons.put(NuitonValidatorScope.FATAL, SwingUtil.createImageIcon("fatal.png"));
            icons.put(NuitonValidatorScope.ERROR, SwingUtil.createImageIcon("error.png"));
            icons.put(NuitonValidatorScope.WARNING, SwingUtil.createImageIcon("warning.png"));
            icons.put(NuitonValidatorScope.INFO, SwingUtil.createImageIcon("info.png"));
        }
        return icons;
    }

    public static EnumMap<NuitonValidatorScope, Color> getColors() {
        if (colors == null) {
            colors = new EnumMap<>(NuitonValidatorScope.class);
            colors.put(NuitonValidatorScope.FATAL, Color.MAGENTA);
            colors.put(NuitonValidatorScope.ERROR, Color.RED);
            colors.put(NuitonValidatorScope.WARNING, Color.YELLOW);
            colors.put(NuitonValidatorScope.INFO, Color.GREEN);
        }
        return colors;
    }

    public static Color getColor(NuitonValidatorScope scope) {
        return scope == null ? null : getColors().get(scope);
    }

    public static ImageIcon getIcon(NuitonValidatorScope scope) {
        return scope == null ? null : getIcons().get(scope);
    }

    public static ImageIcon getFatalIcon() {
        return getIcons().get(NuitonValidatorScope.FATAL);
    }

    public static ImageIcon getErrorIcon() {
        return getIcons().get(NuitonValidatorScope.ERROR);
    }

    public static ImageIcon getWarningIcon() {
        return getIcons().get(NuitonValidatorScope.WARNING);
    }

    public static ImageIcon getInfoIcon() {
        return getIcons().get(NuitonValidatorScope.INFO);
    }

    /**
     * To install all the stuff for validation on a {@link JAXXValidator} ui.
     * <p>
     * This method is called after validators has been detected in the ui (via
     * the method {@link JAXXValidator#registerValidatorFields()}).
     * <p>
     * It will first find and register all validator field via the method
     * {@link JAXXValidator#registerValidatorFields()}, then for each
     * validators it will install ui for it (says connect validator to ui via layers)
     * and will reload attached bean to make visible bean validation state on ui.
     * <p>
     * This method is always invoked by a generated jaxx-validator file at the
     * end of the {@code $completeSetup} method.
     *
     * @param ui the validator ui to init.
     */
    @Deprecated
    public static void installUI(JAXXValidator ui) {
        ui.installValidationUI();
    }


    /**
     * Prepare the ui where to display the validators messages.
     *
     * @param errorTable the table where to display validators messages
     * @param render     renderer to use
     */
    public static void installUI(JTable errorTable, SwingValidatorMessageTableRenderer render) {
        errorTable.setDefaultRenderer(Object.class, render);
        errorTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));
        SwingUtil.setI18nTableHeaderRenderer(
                errorTable,
                n("validator.scope.header"),
                n("validator.scope.header.tip"),
                n("validator.field.header"),
                n("validator.field.header.tip"),
                n("validator.message.header"),
                n("validator.message.header.tip"));
        // register a single 'goto widget error' mouse listener on errorTable
        registerErrorTableMouseListener(errorTable);
        SwingUtil.fixTableColumnWidth(errorTable, 0, 25);
    }

    @Deprecated
    public static void installTabUI(JAXXValidator ui, TabInfoWithValidator tab) {
        ui.installTabUI(tab);
    }

    @Deprecated
    public static void uninstallTabUI(JAXXValidator ui, Collection<TabInfoWithValidator> tabEditors, SwingValidatorMessageTableModel errorTableModel) {
        ui.uninstallTabUI(tabEditors, errorTableModel);
    }

    /**
     * Register for a given validator table ui a validator mouse listener
     * <p>
     * Note: there is only one listener registered for a given table model, so
     * invoking this method twice or more will have no effect.
     *
     * @param table the validator table ui
     * @return the listener instantiate or found
     * @see SwingValidatorMessageTableMouseListener
     */
    public static SwingValidatorMessageTableMouseListener registerErrorTableMouseListener(JTable table) {
        SwingValidatorMessageTableMouseListener listener = getErrorTableMouseListener(table);
        if (listener != null) {
            return listener;
        }
        listener = new SwingValidatorMessageTableMouseListener();
        log.debug(listener.toString());
        table.addMouseListener(listener);
        return listener;
    }

    /**
     * @param table the validator table ui
     * @return the validator table mouse listener, or <code>null</code> if not
     * found
     * @see SwingValidatorMessageTableMouseListener
     */
    public static SwingValidatorMessageTableMouseListener getErrorTableMouseListener(JTable table) {
        if (table != null) {
            for (MouseListener listener : table.getMouseListeners()) {
                if (listener instanceof SwingValidatorMessageTableMouseListener) {
                    return (SwingValidatorMessageTableMouseListener) listener;
                }
            }
        }
        return null;
    }

    public static String getMessage(SwingValidatorMessage model) {
        String text = model.getMessage();
        if (model.getField() != null) {
            text = model.getI18nError(text);
        }
        return text;
    }

    public static String getMessage(BeanValidatorMessage<?> model) {
        String text = model.getMessage();
        if (model.getField() != null) {
            text = model.getI18nError(text);
        }
        return text;
    }

    public static String getFieldName(SwingValidatorMessage model, String value) {
        String text = null;
        JComponent editor = model.getEditor();
        if (editor != null) {
            text = (String) editor.getClientProperty("validatorLabel");
            /*if (l != null) {
            text = I18n.t(l);
            } else {
            // TODO should try the text
            }*/
        }
        if (text == null) {
            text = value;
        }
        return text;
    }

    protected SwingValidatorUtil() {
        // no instance
    }

}
