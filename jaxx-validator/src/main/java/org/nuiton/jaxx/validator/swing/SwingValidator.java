/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.validation.bean.BeanValidator;
import io.ultreia.java4all.validation.api.NuitonValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.validator.swing.ui.AbstractBeanValidatorUI;
import org.nuiton.jaxx.validator.swing.ui.IconValidationUI;

import javax.swing.JComponent;
import java.awt.Container;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * La surcharge de {@link BeanValidator} pour les ui swing
 * <p>
 * Permet d'ajouter facilement le support de la validation des champs d'un bean
 * et de le relier a une interface graphique. Utilise xwork pour la validation
 * et JXLayer pour la visualisation.
 * <p>
 * Le mieux pour son integration dans Jaxx est de faire de la generation pour
 * force la compilation du code suivant:
 *
 * <pre>
 * myValidor.getBean().get&lt;field&gt;();
 * </pre>
 * <p>
 * et ceci pour chaque field ajoute a la map fieldRepresentation. De cette facon
 * meme si le champs field est en texte on a une verification de son existance a
 * la compilation.
 * <p>
 * La representation en tag pourrait etre
 * <pre>
 * &lt;validator id="myValidator" beanClass="{Personne.class}"
 * errorList="$list"&gt;
 *   &lt;field name="name" component="$name"/&gt;
 *   &lt;field name="firstName" component="$firstName"/&gt;
 *   &lt;field name="birthDate" component="$birthDate"/&gt;
 * &lt;/validator&gt;
 * &lt;validator beanClass="{Personne.class}" autoField="true"
 * errorList="$list"&gt;
 *   &lt;fieldRepresentation name="name" component="$lastName"/&gt;
 * &lt;/validator&gt;
 * </pre>
 * <p>
 * dans le premier exemple on fait un mapping explicite des champs, mais on voit
 * que le nom du composant graphique est le meme que celui du champs. Pour
 * eviter de longue saisie, il est possible d'utiliser le flag <b>autoField</b>
 * qui pour chaque champs du ayant une methode get du bean recherche un
 * composant avec cet Id. Il est aussi possible de surcharge un champs
 * explicitement comme ici name, dans le cas ou le composant qui porterait ce
 * nom serait utilise pour autre chose.
 * <p>
 * Il faut un handler particulier pour ce composant car les attributs
 * <b>beanClass</b> et <b>autoField</b> ne sont present que dans le XML jaxx et
 * servent a la generation. Il faut aussi prendre en compte les elements
 * fieldRepresentation fils du tag validator.
 * <p>
 * Voici ce que pourrait etre le code genere par jaxx
 * <pre>
 * // declaration du bean
 * BeanValidator&lt;beanClass&gt; $myValidator;
 * // init du bean
 * protected void createMyValidator() {
 *   $myValidator = new BeanValidator&lt;beanClass&gt;();
 *   // genere seulement si autoField = true
 *   for (Method m : beanClass.getMethod()) {
 *     if (m.getName().startsWith("get")) {
 *       String fieldName = m.getName().substring(3).toLowerCase();
 *       $myValidator.setFieldRepresentation(fieldName,
 * $objectMap.get(fieldName));
 *     }
 *   }
 *   // pour chaque tag fieldRepresentation
 *   myValidator.setFieldRepresentation("name", $lastName);
 *   // si beanClass est specifie et n'est pas Object, on force l'acces au
 * champs
 *   // pour validation a la compilation
 *   $myValidator.getBean().getName();
 *   $objectMap.put("myValidator", $myValidator);
 * }
 * </pre>
 *
 * @param <B> le type de bean a valider
 * @author Tony Chemit - dev@tchemit.fr
 * @version 1.0
 */
public class SwingValidator<B> extends BeanValidator<B> {

    private static final Logger log = LogManager.getLogger(SwingValidator.class);

    private static final Class<? extends AbstractBeanValidatorUI> DEFAULT_UI_CLASS = IconValidationUI.class;
    /**
     * permet de faire le lien en un champs du bean et l'objet qui permet de
     * l'editer
     */
    protected final Map<String, JComponent> fieldRepresentation;
    /**
     * Object servant a contenir la liste des erreurs
     */
    protected SwingValidatorMessageTableModel errorTableModel;
    /**
     * ui renderer class
     */
    protected Class<? extends AbstractBeanValidatorUI> uiClass;

    public SwingValidator(NuitonValidatorProvider provider, Class<B> beanClass, String contextName, NuitonValidatorScope... filterScopes) {
        super(provider, beanClass, contextName, filterScopes);
        fieldRepresentation = new HashMap<>();
    }

    /**
     * Obtain a new {@link BeanValidator} for the given parameters.
     * <p>
     * <b>Note:</b> It will use the default provider of {@link NuitonValidator}
     *
     * @param type    type of bean to validate
     * @param context context of validation
     * @param scopes  authorized scopes (if {@code null}, will use all scopes)
     * @param <O>     type of bean to validate
     * @return the new instanced {@link BeanValidator}.
     * @see NuitonValidatorProviders#getDefaultFactoryName()
     */
    public static <O> SwingValidator<O> newValidator(Class<O> type, String context, NuitonValidatorScope... scopes) {
        Objects.requireNonNull(type, "type parameter can not be null.");
        // get the provider default name
        String providerName = NuitonValidatorProviders.getDefaultFactoryName();
        // get delegate validator provider
        NuitonValidatorProvider provider = NuitonValidatorProviders.newProvider(providerName, null);
        // get the bean validator with this provider
        return new SwingValidator<>(provider, type, context, scopes);
    }

    /**
     * To reload a bean in the validator.
     * <p>
     * This method is used to reload ui, since some editors could not exist when
     * validator is init, so some messages should not be attached to an editor.
     */
    public void reloadBean() {
        B b = getBean();
        if (log.isInfoEnabled()) {
            log.info("Will reload bean : " + b);
        }
        if (b != null) {
            setBean(null);
            setBean(b);
        }
    }

    public JComponent getFieldRepresentation(String fieldName) {
        return fieldRepresentation.get(fieldName);
    }

    public Class<? extends AbstractBeanValidatorUI> getUiClass() {
        return uiClass;
    }

    public void setUiClass(Class<? extends AbstractBeanValidatorUI> uiClass) {
        this.uiClass = uiClass;
    }

    @Override
    public void setContext(String context) {
        String oldContext = getContext();

        super.setContext(context);

        if (context == null && oldContext == null || Objects.equals(context, oldContext)) {
            // same context do nothing
            return;
        }
        if (fieldRepresentation != null) {

            // must reinstall ui
            installUIs();
        }
    }

    /**
     * Permet d'indiquer le composant graphique responsable de l'affichage d'un
     * attribut du bean
     *
     * @param fieldName the field name in the bean
     * @param c         the editor component for the field
     */
    public void setFieldRepresentation(String fieldName, JComponent c) {
        boolean fieldFound = getDelegate().getEffectiveFields().contains(fieldName);

        if (!fieldFound) {
            // no field registered in the validator
            if (log.isWarnEnabled()) {
                log.warn("the field '{}}' is not defined in validator (no rules on it)", fieldName);
            }
        } else {
            if (log.isInfoEnabled()) {
                log.info("register field'{}' with component : {}", fieldName, c.getName());
            }
            fieldRepresentation.put(fieldName, c);
        }
    }

    public void setFieldRepresentation(
            Map<String, JComponent> fieldRepresentation) {
        for (Map.Entry<String, JComponent> e : fieldRepresentation.entrySet()) {
            setFieldRepresentation(e.getKey(), e.getValue());
        }
    }

    @Override
    public SwingValidator<?> getParentValidator() {
        return (SwingValidator<?>) super.getParentValidator();
    }

    public void setParentValidator(SwingValidator<?> parentValidator) {
        super.setParentValidator(parentValidator);
    }

    /**
     * install ui on required components
     */
    public void installUIs() {
        if (fieldRepresentation == null) {
            throw new NullPointerException("fieldRepresentation is null, must init before invoking installUIs method...");
        }

        if (uiClass == null) {
            // use the default one
            uiClass = DEFAULT_UI_CLASS;
        }

        // compute reverse map of fieldRepresentation : a same editor can
        // reference more than one field
        Multimap<JComponent, String> fieldsByEditor = ArrayListMultimap.create();
        for (Entry<String, JComponent> entry : fieldRepresentation.entrySet()) {
            fieldsByEditor.put(entry.getValue(), entry.getKey());
        }

        for (JComponent editor : fieldsByEditor.keySet()) {
            Collection<String> fields = fieldsByEditor.get(editor);
            try {
                setMessageRepresentation(
                        editor,
                        fields,
                        uiClass
                );
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected void setMessageRepresentation(
            JComponent c,
            Collection<String> fieldNames,
            Class<? extends AbstractBeanValidatorUI> uiClass)
            throws InvocationTargetException,
            IllegalAccessException,
            InstantiationException,
            NoSuchMethodException {
        Objects.requireNonNull(c, "No editor");
        Container container = c.getParent();
        if (container instanceof JXLayer<?>) {
            JXLayer jx = (JXLayer<?>) container;
            Object oldUI = jx.getUI();
            if (oldUI instanceof AbstractBeanValidatorUI) {
                // supression de l'ancien layer
                removeBeanValidatorListener((AbstractBeanValidatorUI) oldUI);
            }
            Constructor<? extends AbstractBeanValidatorUI> cons = uiClass.getConstructor(Collection.class);
            AbstractBeanValidatorUI ui = cons.newInstance(fieldNames);
            addBeanValidatorListener(ui);
            jx.setUI(ui);
        }
    }

    public SwingValidatorMessageTableModel getErrorTableModel() {
        return errorTableModel;
    }

    public void setErrorTableModel(
            SwingValidatorMessageTableModel errorTableModel) {
        this.errorTableModel = errorTableModel;
        if (errorTableModel != null) {
            // register the validator in the  model table
            errorTableModel.registerValidator(this);
        }
    }
}
