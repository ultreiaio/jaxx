package org.nuiton.jaxx.validator.swing.tab;

/*-
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.TabInfo;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelListener;
import java.awt.Component;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * To build validation on tabs at runtime.
 * <p>
 * The previous approach to generate tab info is not ok since it is done at generation time and we could want to
 * add extra components in some existing. Will replace TabInfoWithValidator...
 * <p>
 * Created by tchemit on 01/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JTabbedPaneValidator {

    public static final Icon ERROR_ICON = UIManager.getIcon("action.error");
    private static final Icon FATAL_ICON = UIManager.getIcon("action.fatal");
    private static final Icon WARNING_ICON = UIManager.getIcon("action.warning");
    private static final Icon INFO_ICON = UIManager.getIcon("action.info");

    private static final Map<NuitonValidatorScope, Icon> SCOPES_ICONS =
            ImmutableMap.<NuitonValidatorScope, Icon>builder()
                    .put(NuitonValidatorScope.FATAL, FATAL_ICON)
                    .put(NuitonValidatorScope.ERROR, ERROR_ICON)
                    .put(NuitonValidatorScope.WARNING, WARNING_ICON)
                    .put(NuitonValidatorScope.INFO, INFO_ICON)
                    .build();

    private static final Logger log = LogManager.getLogger(JTabbedPaneValidator.class);
    private final TableModelListener computeTabValidStateListener;
    private final Multimap<TabInfo, String> propertiesByTab;
    private final Multimap<TabInfo, SwingValidator<?>> tabValidators;
    private final boolean fatal;
    private final boolean error;
    private final boolean warning;
    private final boolean info;

    private JTabbedPaneValidator(Multimap<TabInfo, String> propertiesByTab, Multimap<TabInfo, SwingValidator<?>> tabValidators, EnumSet<NuitonValidatorScope> scopes) {
        this.propertiesByTab = propertiesByTab;
        this.tabValidators = tabValidators;
        this.fatal = scopes.contains(NuitonValidatorScope.FATAL);
        this.error = scopes.contains(NuitonValidatorScope.ERROR);
        this.warning = scopes.contains(NuitonValidatorScope.WARNING);
        this.info = scopes.contains(NuitonValidatorScope.INFO);
        this.computeTabValidStateListener = e -> {
            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);
        };
    }

    public static Builder builder(JAXXValidator ui, String tabbedPaneId) {
        return new Builder(Objects.requireNonNull(ui), Objects.requireNonNull(tabbedPaneId));
    }

    public void install(SwingValidatorMessageTableModel errorTableModel) {
        errorTableModel.removeTableModelListener(computeTabValidStateListener);
        errorTableModel.addTableModelListener(computeTabValidStateListener);
        computeTabValidState(errorTableModel);
    }

    public void uninstall(SwingValidatorMessageTableModel errorTableModel) {
        errorTableModel.removeTableModelListener(computeTabValidStateListener);
    }

    public void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {
        int rowCount = errorTableModel.getRowCount();
//        Multimap<NuitonValidatorScope, String> propertiesByScope = ArrayListMultimap.create();
//        for (int i = 0; i < rowCount; i++) {
//            SwingValidatorMessage row = errorTableModel.getRow(i);
//            propertiesByScope.put(row.getScope(), row.getField());
//        }

        for (Map.Entry<TabInfo, Collection<String>> entry : propertiesByTab.asMap().entrySet()) {
            TabInfo tab = entry.getKey();
            Collection<String> tabProperties = entry.getValue();
            Collection<SwingValidator<?>> tabValidator = tabValidators.get(tab);
            Multimap<NuitonValidatorScope, String> propertiesByScope = ArrayListMultimap.create();
            for (int i = 0; i < rowCount; i++) {
                SwingValidatorMessage row = errorTableModel.getRow(i);
                SwingValidator<?> validator = (SwingValidator<?>) row.getValidator();
                if (tabValidator.isEmpty() || tabValidator.contains(validator)) {
                    propertiesByScope.put(row.getScope(), row.getField());
                }
            }
            NuitonValidatorScope scope = null;
            if (fatal && propertiesByScope.get(NuitonValidatorScope.FATAL).removeAll(tabProperties)) {
                scope = NuitonValidatorScope.FATAL;
            } else if (error && propertiesByScope.get(NuitonValidatorScope.ERROR).removeAll(tabProperties)) {
                scope = NuitonValidatorScope.ERROR;
            } else if (warning && propertiesByScope.get(NuitonValidatorScope.WARNING).removeAll(tabProperties)) {
                scope = NuitonValidatorScope.WARNING;
            } else if (info && propertiesByScope.get(NuitonValidatorScope.INFO).removeAll(tabProperties)) {
                scope = NuitonValidatorScope.INFO;
            }
            changeTabStates(tab, scope);
        }
    }

    public Multimap<TabInfo, String> getPropertiesByTab() {
        return propertiesByTab;
    }

    public Multimap<TabInfo, SwingValidator<?>> getTabValidators() {
        return tabValidators;
    }

    private void changeTabStates(TabInfo propertyName, NuitonValidatorScope scope) {
        propertyName.setIcon(SCOPES_ICONS.get(scope));
    }

    public static class Builder {

        private final JAXXValidator ui;
        private final String tabbedPaneId;
        private final EnumSet<NuitonValidatorScope> scopes;
        private Map<String, JAXXValidator> extraTabs;

        Builder(JAXXValidator ui, String tabbedPaneId) {
            this.ui = ui;
            this.tabbedPaneId = tabbedPaneId;
            this.scopes = EnumSet.noneOf(NuitonValidatorScope.class);
        }

        public Builder addScope(NuitonValidatorScope... scope) {
            scopes.addAll(Arrays.asList(scope));
            return this;
        }

        public Builder addExtraTab(Map<String, JAXXValidator> extraTabs) {
            this.extraTabs = Objects.requireNonNull(extraTabs);
            return this;
        }

        public JTabbedPaneValidator build() {

            JAXXObject jui = (JAXXObject) ui;
            JTabbedPane tabbedPane = (JTabbedPane) Objects.requireNonNull(jui.getObjectById(tabbedPaneId));
            List<TabInfo> tabs = jui.get$objectMap().values().stream().filter(o -> o instanceof TabInfo && tabbedPaneId.equals(((TabInfo) o).getContainerId())).map(o -> (TabInfo) o).collect(Collectors.toList());
            log.info(String.format("Detected %d form TabbedPane: %s", tabs.size(), tabbedPaneId));

            Multimap<TabInfo, SwingValidator<?>> tabValidators = ArrayListMultimap.create();

            ImmutableMap.Builder<String, Component> tabComponentsBuilder = ImmutableMap.builder();
            ImmutableMap.Builder<String, TabInfo> tabsBuilder = ImmutableMap.builder();
            for (TabInfo tab : tabs) {
                String tabId = tab.getId();
                Component tabComponent = tabbedPane.getComponentAt(tab.getTabIndex());
                tabComponentsBuilder.put(tabId, tabComponent);
                tabsBuilder.put(tabId, tab);
            }
            ImmutableMap<String, TabInfo> tabMap = tabsBuilder.build();
            ImmutableMap<String, Component> tabComponents = tabComponentsBuilder.build();

            Multimap<TabInfo, String> propertiesByTab = ArrayListMultimap.create();

            for (String validatorId : ui.getValidatorIds()) {
                SwingValidator<?> validator = ui.getValidator(validatorId);
                Set<String> effectiveFields = validator.getEffectiveFields();
                for (String effectiveField : effectiveFields) {
                    JComponent editor = validator.getFieldRepresentation(effectiveField);
                    if (editor != null) {
                        for (Map.Entry<String, Component> entry : tabComponents.entrySet()) {

                            if (SwingUtilities.isDescendingFrom(editor, entry.getValue())) {
                                log.info(String.format("Found in tab: %s - editor: %s", entry.getKey(), effectiveField));
                                propertiesByTab.put(tabMap.get(entry.getKey()), effectiveField);
                            }
                        }
                    }
                }
            }
            if (extraTabs != null) {
                for (Map.Entry<String, JAXXValidator> entry : extraTabs.entrySet()) {
                    String tabId = entry.getKey();
                    TabInfo tabInfo = tabMap.get(tabId);
                    JAXXValidator extraUi = entry.getValue();
                    for (String validatorId : extraUi.getValidatorIds()) {
                        SwingValidator<?> validator = extraUi.getValidator(validatorId);
                        tabValidators.put(tabInfo, validator);
                        Set<String> effectiveFields = validator.getEffectiveFields();
                        for (String effectiveField : effectiveFields) {
                            log.info(String.format("Found in tab: %s - editor: %s", entry.getKey(), effectiveField));
                            JComponent editor = validator.getFieldRepresentation(effectiveField);
                            if (editor != null) {
                                propertiesByTab.put(tabInfo, effectiveField);
                            }
                        }
                    }
                }
            }

            return new JTabbedPaneValidator(propertiesByTab, tabValidators, scopes);
        }
    }
}
