package org.nuiton.jaxx.validator.swing.tab;

/*-
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.awt.Containers;
import org.nuiton.jaxx.runtime.swing.TabInfo;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.Component;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by tchemit on 04/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TabInfoWithValidator extends TabInfo {

    private static final Icon FATAL_ICON = UIManager.getIcon("action.fatal");
    private static final Icon ERROR_ICON = UIManager.getIcon("action.error");
    private static final Icon WARNING_ICON = UIManager.getIcon("action.warning");
    private static final Icon INFO_ICON = UIManager.getIcon("action.info");

    private final Map<String, Component> components;
    private final Map<String, Component> validatorComponents;
    private final Set<String> validatorComponentIds;
    private SwingValidator<?> validator;
    private String validatorId;
    private JTabbedPane parent;

    private boolean withFatal;
    private boolean withError;
    private boolean withWarning;
    private boolean withInfo;

    public TabInfoWithValidator(String id) {
        super(id);
        this.components = new LinkedHashMap<>();
        this.validatorComponents = new LinkedHashMap<>();
        this.validatorComponentIds = new LinkedHashSet<>();
    }

    public static void onTableModelChanged(TabInfoWithValidator component, SwingValidatorMessageTableModel errorTableModel) {
        int rowCount = errorTableModel.getRowCount();
        Set<NuitonValidatorScope> scopes = new LinkedHashSet<>();

        Set<String> validatorComponentIds = component.getValidatorComponentIds();
        for (int i = 0; i < rowCount; i++) {
            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (validatorComponentIds.contains(row.getField())) {
                scopes.add(row.getScope());
            }
        }
        component.setWithFatal(scopes.contains(NuitonValidatorScope.FATAL));
        component.setWithError(scopes.contains(NuitonValidatorScope.ERROR));
        component.setWithWarning(scopes.contains(NuitonValidatorScope.WARNING));
        component.setWithInfo(scopes.contains(NuitonValidatorScope.INFO));
        if (component.isWithFatal()) {
            component.setIcon(FATAL_ICON);
        } else if (component.isWithError()) {
            component.setIcon(ERROR_ICON);
        } else if (component.isWithWarning()) {
            component.setIcon(WARNING_ICON);
        } else if (component.isWithInfo()) {
            component.setIcon(INFO_ICON);
        } else {
            component.setIcon(null);
        }
    }

    public boolean isWithFatal() {
        return withFatal;
    }

    public void setWithFatal(boolean withFatal) {
        this.withFatal = withFatal;
    }

    public boolean isWithError() {
        return withError;
    }

    public void setWithError(boolean withError) {
        this.withError = withError;
    }

    public boolean isWithWarning() {
        return withWarning;
    }

    public void setWithWarning(boolean withWarning) {
        this.withWarning = withWarning;
    }

    public boolean isWithInfo() {
        return withInfo;
    }

    public void setWithInfo(boolean withInfo) {
        this.withInfo = withInfo;
    }

    public SwingValidator getValidator() {
        return validator;
    }

    public String getValidatorId() {
        return validatorId;
    }

    public void setValidatorId(String validatorId) {
        this.validatorId = validatorId;
    }

    public Map<String, Component> getComponents() {
        return components;
    }

    public Map<String, Component> getValidatorComponents() {
        return validatorComponents;
    }

    public Set<String> getValidatorComponentIds() {
        return validatorComponentIds;
    }

    public JTabbedPane getParent() {
        return parent;
    }

    public void init(JAXXObject ui, JTabbedPane parent) {
        this.parent = parent;
        setTabComponent((JComponent) Objects.requireNonNull(parent.getComponentAt(getTabIndex())));
        this.validator = (SwingValidator<?>) Objects.requireNonNull(Objects.requireNonNull(ui).getObjectById(getValidatorId()));
        Set<String> effectiveFields = validator.getEffectiveFields();
        components.clear();
        validatorComponents.clear();
        validatorComponentIds.clear();
        for (Component component : Containers.findComponents(getTabComponent(), c -> true)) {
            String name = component.getName();
            components.put(name, component);
            if (effectiveFields.contains(name)) {
                validatorComponents.put(name, component);
                validatorComponentIds.add(name);
            }
        }
    }

    public static class TabTableModelListener implements TableModelListener {

        private final TabInfoWithValidator component;

        public TabTableModelListener(TabInfoWithValidator component) {
            this.component = component;
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            SwingValidatorMessageTableModel errorTableModel = (SwingValidatorMessageTableModel) e.getSource();
            onTableModelChanged(component, errorTableModel);
        }

        public TabInfoWithValidator getComponent() {
            return component;
        }
    }

}
