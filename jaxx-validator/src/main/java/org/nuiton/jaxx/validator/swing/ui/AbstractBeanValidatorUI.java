/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing.ui;

import com.google.common.collect.Ordering;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.bean.BeanValidatorListener;
import io.ultreia.java4all.validation.bean.BeanValidator;
import io.ultreia.java4all.validation.bean.BeanValidatorEvent;

import javax.swing.JComponent;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Abstract renderer
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class AbstractBeanValidatorUI extends AbstractLayerUI<JComponent> implements BeanValidatorListener {

    /** Logger */
    private static final Logger log = LogManager.getLogger(AbstractBeanValidatorUI.class);

    private static final long serialVersionUID = 1L;

    /**
     * Actual scope to display in the layer.
     *
     * This field will be recomputed each time a new event arrived on this
     * field.
     */
    protected NuitonValidatorScope scope;

    /** Field name in validator. */
    protected final Set<String> fields;

    protected AbstractBeanValidatorUI(String field) {
        this(Collections.singleton(field));
    }

    protected AbstractBeanValidatorUI(Collection<String> fields) {
        this(new HashSet<>(fields));
    }

    protected AbstractBeanValidatorUI(HashSet<String> fields) {
        this.fields = fields;
        if (log.isDebugEnabled()) {
            log.debug("install " + this + "<fields:" + this.fields + ">");
        }
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    @Override
    public void onFieldChanged(BeanValidatorEvent<?> event) {
        if (fields.contains(event.getField())) {

            scope = getHighestScope(event);
            if (log.isDebugEnabled()) {
                log.debug("set new scope : " + scope + " to field " + fields);
            }
            // ask to repaint the layer
            setDirty(true);
        }
    }

    protected NuitonValidatorScope getHighestScope(BeanValidatorEvent event) {
        BeanValidator<?> source = event.getSource();
        Set<NuitonValidatorScope> scopes = new HashSet<>();
        for (String field : fields) {
            NuitonValidatorScope scope = source.getHighestScope(field);
            if (scope != null) {
                scopes.add(scope);
            }
        }
        return scopes.isEmpty() ? null : Ordering.natural().max(scopes);
    }


}
