package io.ultreia.java4all.jaxx.widgets.combobox.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.nuiton.jaxx.runtime.swing.session.State;

/**
 * All states to persist on a {@link FilterableComboBox}.
 * <p>
 * Created at 30/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.26
 * <p>
 */
public class FilterableComboBoxState implements State {

    protected int index = 0;
    protected boolean reverseSort = false;
    protected boolean onlySearchOnFirstProperty = false;

    public FilterableComboBoxState() {
    }

    public FilterableComboBoxState(int index, boolean reverseSort, boolean onlySearchOnFirstProperty) {
        this.index = index;
        this.reverseSort = reverseSort;
        this.onlySearchOnFirstProperty = onlySearchOnFirstProperty;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    public boolean isOnlySearchOnFirstProperty() {
        return onlySearchOnFirstProperty;
    }

    public void setOnlySearchOnFirstProperty(boolean onlySearchOnFirstProperty) {
        this.onlySearchOnFirstProperty = onlySearchOnFirstProperty;
    }

    protected FilterableComboBox<?> checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof FilterableComboBox)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (FilterableComboBox<?>) o;
    }

    @Override
    public State getState(Object o) {
        FilterableComboBox<?> combo = checkComponent(o);
        return new FilterableComboBoxState(combo.getModel().getIndex(), combo.getModel().getReverseSort(),combo.getModel().getOnlySearchOnFirstProperty());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof FilterableComboBoxState)) {
            throw new IllegalArgumentException("invalid state");
        }
        FilterableComboBox<?> ui = checkComponent(o);
        FilterableComboBoxState typedState = (FilterableComboBoxState) state;
        ui.getModel().setIndex(typedState.getIndex());
        ui.getModel().setReverseSort(typedState.isReverseSort());
        ui.getModel().setOnlySearchOnFirstProperty(typedState.isOnlySearchOnFirstProperty());
    }


}

