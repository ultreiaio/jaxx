package io.ultreia.java4all.jaxx.widgets.choice;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.ItemEvent;
import java.util.Objects;

/**
 * Created on 02/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
class BooleanEditorHandler extends BeanUIHandlerSupport<BooleanEditor> {
    private static final Logger log = LogManager.getLogger(BooleanEditorHandler.class);

    @Override
    protected String getProperty(BooleanEditor ui) {
        return ui.getProperty();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init BooleanEditor", ui.getName()));
        if (property == null || property.isEmpty()) {
            ui.setProperty(ui.getName());
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        bean.addPropertyChangeListener(property, e -> {
            Boolean oldValue = ui.getBooleanValue();
            Boolean newValue = (Boolean) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                log.debug(String.format("%s - [%s] get new value from bean: %s", ui.getName(), property, newValue));
                ui.setBooleanValue(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        ui.addItemListener(event -> {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Boolean newValue = ((BooleanEditor) event.getSource()).getBooleanValue();
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }
}
