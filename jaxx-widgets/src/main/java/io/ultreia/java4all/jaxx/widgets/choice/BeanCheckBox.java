package io.ultreia.java4all.jaxx.widgets.choice;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;

import javax.swing.JCheckBox;

/**
 * Created on 01/30/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class BeanCheckBox extends JCheckBox implements BeanScopeAware {
    private static final Logger log = LogManager.getLogger(BeanCheckBox.class);
    private final BeanCheckBoxHandler handler;
    private String property;
    private JavaBean bean;

    public BeanCheckBox() {
        this.handler = new BeanCheckBoxHandler();
    }

    public void init() {
        handler.init(this);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        handler.checkNotInit();
        log.debug(String.format("%s - set property: %s", getName(), property));
        this.property = property;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    @Override
    public void setBean(Object bean) {
        handler.checkNotInit();
        log.debug(String.format("%s - set bean: %s", getName(), bean));
        this.bean = (JavaBean) bean;
    }

}
