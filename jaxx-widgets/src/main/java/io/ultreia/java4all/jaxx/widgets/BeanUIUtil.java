package io.ultreia.java4all.jaxx.widgets;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.i18n.I18nDecoratorDefinition;
import org.nuiton.jaxx.runtime.i18n.I18nDecoratorDefinitions;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.beans.Introspector;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Class with useful methods used in bean uis.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.2
 */
public class BeanUIUtil {

    public static final String DEFAULT_POPUP_LABEL = n("bean.popup.label");

    public static final String DEFAULT_SELECTED_TOOLTIP = n("bean.sort.on");

    public static final String DEFAULT_NOT_SELECTED_TOOLTIP = n("bean.sort.off");

    public static abstract class PopupHandler implements Runnable {

        public static final Logger log = LogManager.getLogger(PopupHandler.class);

        public abstract JPopupMenu getPopup();

        public abstract JComponent getInvoker();

        @Override
        public void run() {

            updatePopup();

            Dimension dim = getPopup().getPreferredSize();

            JComponent invoker = getInvoker();
            getPopup().show(
                    invoker,
                    (int) (invoker.getPreferredSize().getWidth() - dim.getWidth()),
                    invoker.getHeight()
            );
        }

        /**
         * Toggle the popup visible state.
         */
        public void togglePopup() {
            boolean newValue = !getPopup().isVisible();

            if (log.isTraceEnabled()) {
                log.trace(newValue);
            }

            if (!newValue) {
                if (getPopup() != null) {
                    getPopup().setVisible(false);
                }
                return;
            }
            SwingUtilities.invokeLater(this);
        }

        protected void updatePopup() {
            getPopup().pack();
        }

        public void preparePopup(String selectedTip,
                                 String notSelectedTip,
                                 String i18nPrefix,
                                 String title,
                                 ButtonGroup indexes,
                                 JSeparator popupSeparator,
                                 JLabel popupLabel,
                                 AbstractButton sortUp,
                                 AbstractButton sortDown,
                                 Decorator decorator) {
            if (selectedTip == null) {
                // use default selected tip text
                selectedTip = DEFAULT_SELECTED_TOOLTIP;
            }
            if (notSelectedTip == null) {
                // use default selected tip text
                notSelectedTip = DEFAULT_NOT_SELECTED_TOOLTIP;
            }
            JPopupMenu popup = getPopup();

            //Container container = ui.getIndexesContainer();
            I18nDecoratorDefinition i18nDecoratorDefinition = I18nDecoratorDefinitions.get().getDefinition(decorator);
            BeanPropertyI18nKeyProducer labelsBuilder = BeanPropertyI18nKeyProducerProvider.get().getDefaultLabelsBuilder();

            int nbContext = decorator.definition().contextCount();
            if (nbContext > 1) {
                LinkedHashMap<String, String> labels = i18nDecoratorDefinition.computeLabels(decorator, labelsBuilder);

                int i = 0;
                for (Map.Entry<String, String> entry : labels.entrySet()) {
                    String property = entry.getKey();
                    String propertyI18n = entry.getValue();
                    JRadioButtonMenuItem button = new JRadioButtonMenuItem(propertyI18n);
                    button.setName(property);
                    button.putClientProperty(JAXXButtonGroup.BUTTON8GROUP_CLIENT_PROPERTY, indexes);
                    button.putClientProperty(JAXXButtonGroup.VALUE_CLIENT_PROPERTY, i++);
                    popup.add(button);
                    if (selectedTip != null) {
                        button.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t(selectedTip, propertyI18n));
                    }
                    if (notSelectedTip != null) {
                        button.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t(notSelectedTip, propertyI18n));
                    }
                    button.setSelected(false);
                    indexes.add(button);
                }
            }
            if (title == null) {
                // use default popup title
                title = DEFAULT_POPUP_LABEL;

                Class<?> type = i18nDecoratorDefinition.getDecoratorType(decorator);
                String beanI18nKey;
                if (type == null) {
                    beanI18nKey = n("bean.unknown.type");
                } else {
                    beanI18nKey = i18nPrefix + Introspector.decapitalize(type.getSimpleName());
                }
                String beanI18n = t(beanI18nKey);
                title = t(title, beanI18n);
            } else {
                title = t(title);
            }

            sortDown.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.down.tip"));
            sortDown.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.down.toSelect.tip"));

            sortUp.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.up.tip"));
            sortUp.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.up.toSelect.tip"));

            if (nbContext < 2) {
                getPopup().remove(popupSeparator);
                getPopup().remove(popupLabel);
            }
            popupLabel.setText(title);
            getPopup().setLabel(title);
            getPopup().invalidate();
        }
    }

    public static void invokeMethod(Method mut, Object source, Object... params) {

        if (mut != null) {
            try {
                mut.invoke(source, params);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }
}
