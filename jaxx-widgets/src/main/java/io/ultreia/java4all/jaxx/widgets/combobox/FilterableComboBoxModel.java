package io.ultreia.java4all.jaxx.widgets.combobox;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.decoration.DecoratorRenderer;
import io.ultreia.java4all.lang.Setters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.jaxx.widgets.BeanUIUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created on 23/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class FilterableComboBoxModel<O extends JavaBean> extends AbstractJavaBean {

    public static final String PROPERTY_BEAN = "bean";
    public static final String PROPERTY_DATA = "data";
    public static final String PROPERTY_INDEX = "index";
    public static final String PROPERTY_REVERSE_SORT = "reverseSort";
    public static final String PROPERTY_SELECTED_ITEM = "selectedItem";
    public static final String PROPERTY_ONLY_SEARCH_ON_SELECTED_PROPERTY = "onlySearchOnSelectedProperty";
    private static final Logger log = LogManager.getLogger(FilterableComboBoxModel.class);
    private final FilterableComboBoxConfig<O> config;
    /**
     * selectedItem property
     */
    O selectedItem;
    /**
     * flag to reverse the sort
     */
    private Boolean reverseSort = false;
    /**
     * Flag to only search on selected decorator property.
     */
    private Boolean onlySearchOnFirstProperty = false;

    /**
     * bean property
     */
    private Object bean;
    /**
     * data of the combo-box
     */
    private List<O> data;
    /**
     * filtered data of the combo-box (when editing)
     */
    private final List<O> filteredData;
    /**
     * sort index property
     */
    private int index = 0;
    /**
     * Mutator method on the property of boxed bean in the ui
     */
    private Method mutator;

    public FilterableComboBoxModel(FilterableComboBoxConfig<O> config) {
        this.config = config;
        this.filteredData = new ArrayList<>();
    }

    public Boolean getReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(Boolean reverseSort) {
        Boolean oldValue = getReverseSort();
        this.reverseSort = Objects.equals(true, reverseSort);
        firePropertyChange(PROPERTY_REVERSE_SORT, oldValue, this.reverseSort);
    }

    public Boolean getOnlySearchOnFirstProperty() {
        return onlySearchOnFirstProperty;
    }

    public void setOnlySearchOnFirstProperty(Boolean onlySearchOnFirstProperty) {
        Boolean oldValue = this.onlySearchOnFirstProperty;
        this.onlySearchOnFirstProperty = onlySearchOnFirstProperty;
        firePropertyChange(PROPERTY_ONLY_SEARCH_ON_SELECTED_PROPERTY, oldValue, onlySearchOnFirstProperty);
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Object oldValue = getBean();
        this.bean = bean;
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    public List<O> getData() {
        return data;
    }

    public void setData(List<O> data) {
        List<O> oldValue = getData();
        this.data = data;
        if (data != null && config.isBeanDecoratorAware()) {
            Decorator decorator = config.getDecorator();
            for (O datum : data) {
                ((Decorated) datum).registerDecorator(decorator);
            }
        }
        firePropertyChange(PROPERTY_DATA, oldValue, data);
    }

    public O getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(O selectedItem) {
        O oldValue = getSelectedItem();
        this.selectedItem = selectedItem;
        if (config.isBeanDecoratorAware() && selectedItem instanceof Decorated) {
            Decorated item = (Decorated) selectedItem;
            if (item.decorator().isEmpty()) {
                item.registerDecorator(config.getDecorator());
            }
        }
        if (!Objects.equals(oldValue, selectedItem)) {
            firePropertyChange(PROPERTY_SELECTED_ITEM, oldValue, selectedItem);
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        int oldValue = getIndex();
        this.index = index;
        firePropertyChange(PROPERTY_INDEX, oldValue, index);
    }

    public List<O> getFilteredData() {
        return filteredData;
    }

    public List<O> updateFilteredData(String filterText) {
        filteredData.clear();
        for (O originalItem : data) {
            if (acceptFilter(filterText, originalItem)) {
                filteredData.add(originalItem);
            }
        }
        return filteredData;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private boolean acceptFilter(String filterText, O item) {
        if (filterText.isEmpty()) {
            return true;
        }
        String text;
        if (getOnlySearchOnFirstProperty()) {
            Decorator decorator = ((Decorated) item).decorator().orElseThrow();
            DecoratorDefinition definition = decorator.definition();
            text = definition.decorateContext(decorator.getLocale(), (DecoratorRenderer) definition.sorter(), item, getIndex());
        } else {
            text = item.toString();
        }
        return text.toLowerCase().contains(filterText.toLowerCase());
    }
    public FilterableComboBoxConfig<O> getConfig() {
        return config;
    }

    /**
     * @return {@code true} if there is no data in comboBox, {@code false} otherwise.
     */
    public boolean isEmpty() {
        return getData() == null || getData().isEmpty();
    }

    /**
     * Add the given items into the comboBox.
     * <p>
     * <strong>Note:</strong> The item will be inserted at his correct following the selected ordering.
     *
     * @param items items to add in comboBox.
     */
    public void addItems(Iterable<O> items) {
        List<O> data = getData();
        boolean wasEmpty = data.isEmpty();
        boolean beanDecoratorAware = config.isBeanDecoratorAware();
        Decorator decorator = config.getDecorator();
        for (O item : items) {
            data.add(item);
            if (beanDecoratorAware) {
                ((Decorated) item).registerDecorator(decorator);
            }
        }
        firePropertyChange(PROPERTY_DATA, null, getData());
        fireEmpty(wasEmpty);
    }

    /**
     * Remove the given items from the comboBox model.
     * <p>
     * <strong>Note:</strong> If this item was selected, then selection will be cleared.
     *
     * @param items items to remove from the comboBox model
     */
    public void removeItems(Iterable<O> items) {
        List<O> data = getData();
        boolean needUpdate = false;
        for (O item : items) {
            boolean remove = data.remove(item);
            if (remove) {
                // item was found in data
                Object selectedItem = getSelectedItem();
                if (item == selectedItem) {
                    // item was selected item, reset selected item then
                    setSelectedItem(null);
                }
                needUpdate = true;
            }
        }
        if (needUpdate) {
            firePropertyChange(PROPERTY_DATA, null, getData());
            fireEmpty(false);
        }
    }

    /**
     * Add the given item into the comboBox.
     * <p>
     * <strong>Note:</strong> The item will be inserted at his correct following the selected ordering.
     *
     * @param item item to add in comboBox.
     */
    public void addItem(O item) {
        addItems(Collections.singleton(item));
    }

    /**
     * Remove the given item from the comboBox model.
     * <p>
     * <strong>Note:</strong> If this item was selected, then selection will be cleared.
     *
     * @param item the item to remove from the comboBox model
     */
    public void removeItem(O item) {
        removeItems(Collections.singleton(item));
    }

    void fireEmpty(boolean wasEmpty) {
        firePropertyChange(JaxxComboBox.PROPERTY_EMPTY, wasEmpty, isEmpty());
    }

    void mutateBeanProperty(O newValue) {
        Object bean = getBean();
        if (mutator == null && bean != null && config.getProperty() != null) {
            mutator = Setters.getMutator(bean, config.getProperty());
        }
        log.info(String.format("%s → set new value: %s", config.getProperty(), newValue));
        BeanUIUtil.invokeMethod(mutator, bean, newValue);
    }

}
