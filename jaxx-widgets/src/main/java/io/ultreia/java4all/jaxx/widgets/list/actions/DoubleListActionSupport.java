package io.ultreia.java4all.jaxx.widgets.list.actions;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.action.JComponentActionSupport;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;

import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 * Created by tchemit on 16/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public abstract class DoubleListActionSupport<O> extends JComponentActionSupport<DoubleList<O>> {

    DoubleListActionSupport(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(label, shortDescription, actionIcon, acceleratorKey);
    }

    @Override
    protected int getInputMapCondition() {
        return JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
    }

}
