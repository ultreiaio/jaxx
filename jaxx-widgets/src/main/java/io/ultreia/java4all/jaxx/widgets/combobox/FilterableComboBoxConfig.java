package io.ultreia.java4all.jaxx.widgets.combobox;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.jaxx.runtime.bean.BeanTypeAware;

import java.util.Objects;

/**
 * Created on 25/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class FilterableComboBoxConfig <O extends JavaBean> extends AbstractJavaBean implements BeanTypeAware<O> {

    public static final String PROPERTY_BEAN_TYPE = "beanType";
    public static final String PROPERTY_PROPERTY = "property";
    public static final String PROPERTY_I18N_PREFIX = "i18nPrefix";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_NOT_SELECTED_TOOL_TIP_TEXT = "notSelectedToolTipText";
    public static final String PROPERTY_POPUP_TITLE_TEXT = "popupTitleText";
    public static final String PROPERTY_SELECTED_TOOL_TIP_TEXT = "selectedToolTipText";
    public static final String PROPERTY_SHOW_DECORATOR = "showDecorator";
    public static final String PROPERTY_SHOW_RESET = "showReset";
    public static final String PROPERTY_SORTABLE = "sortable";
    public static final String PROPERTY_SHOW_ONLY_SEARCH_ON_SELECTED_PROPERTY = "showOnlySearchOnSelectedProperty";

    /**
     * show reset property
     */
    private boolean showReset = false;
    /**
     * show decorator property
     */
    private boolean showDecorator = true;
    /**
     * editable combo property
     */
    private boolean editable = true;
    /**
     * sortable combo property
     */
    private boolean sortable = true;
    /**
     * To be able to search only on the selected decorator property.
     * <p>
     * See <a href="https://gitlab.com/ultreiaio/jaxx/-/issues/851">issue 851</a>
     */
    private boolean showOnlySearchOnFirstProperty = true;
    /**
     * Bean type
     */
    private Class<O> beanType;
    /**
     * Is bean type decorator aware
     *
     * @see Decorated
     */
    private boolean beanDecoratorAware;
    /**
     * Bean property linked state
     */
    private String property;
    /**
     *
     */
    private String selectedToolTipText;
    /**
     *
     */
    private String notSelectedToolTipText;
    /**
     *
     */
    private String popupTitleText;
    /**
     *
     */
    private String i18nPrefix;
    /**
     *
     */
    private Decorator decorator;

    public boolean isShowReset() {
        return showReset;
    }

    public void setShowReset(boolean showReset) {
        boolean oldValue = isShowReset();
        this.showReset = showReset;
        firePropertyChange(PROPERTY_SHOW_RESET, oldValue, showReset);
    }

    public boolean isShowDecorator() {
        return showDecorator;
    }

    public void setShowDecorator(boolean showDecorator) {
        boolean oldValue = isShowDecorator();
        this.showDecorator = showDecorator;
        firePropertyChange(PROPERTY_SHOW_DECORATOR, oldValue, showDecorator);
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        boolean oldValue = isSortable();
        this.sortable = sortable;
        firePropertyChange(PROPERTY_SORTABLE, oldValue, sortable);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        boolean oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public boolean isShowOnlySearchOnFirstProperty() {
        return showOnlySearchOnFirstProperty;
    }

    public void setShowOnlySearchOnFirstProperty(boolean showOnlySearchOnFirstProperty) {
        boolean oldValue = this.showOnlySearchOnFirstProperty;
        this.showOnlySearchOnFirstProperty = showOnlySearchOnFirstProperty;
        firePropertyChange(PROPERTY_SHOW_ONLY_SEARCH_ON_SELECTED_PROPERTY, oldValue, showOnlySearchOnFirstProperty);
    }

    @Override
    public Class<O> getBeanType() {
        return beanType;
    }

    @Override
    public void setBeanType(Class<O> beanType) {
        Class<O> oldValue = getBeanType();
        this.beanType = Objects.requireNonNull(beanType);
        beanDecoratorAware = Decorated.class.isAssignableFrom(beanType);
        firePropertyChange(PROPERTY_BEAN_TYPE, oldValue, beanType);
    }

    public boolean isBeanDecoratorAware() {
        return beanDecoratorAware;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        String oldValue = getProperty();
        this.property = property;
        firePropertyChange(PROPERTY_PROPERTY, oldValue, property);
    }

    public String getSelectedToolTipText() {
        return selectedToolTipText;
    }

    public void setSelectedToolTipText(String selectedToolTipText) {
        String oldValue = getSelectedToolTipText();
        this.selectedToolTipText = selectedToolTipText;
        firePropertyChange(PROPERTY_SELECTED_TOOL_TIP_TEXT, oldValue, selectedToolTipText);
    }

    public String getNotSelectedToolTipText() {
        return notSelectedToolTipText;
    }

    public void setNotSelectedToolTipText(String notSelectedToolTipText) {
        String oldValue = getNotSelectedToolTipText();
        this.notSelectedToolTipText = notSelectedToolTipText;
        firePropertyChange(PROPERTY_NOT_SELECTED_TOOL_TIP_TEXT, oldValue, notSelectedToolTipText);
    }

    public String getPopupTitleText() {
        return popupTitleText;
    }

    public void setPopupTitleText(String popupTitleText) {
        String oldValue = getPopupTitleText();
        this.popupTitleText = popupTitleText;
        firePropertyChange(PROPERTY_POPUP_TITLE_TEXT, oldValue, popupTitleText);
    }

    public String getI18nPrefix() {
        return i18nPrefix;
    }

    public void setI18nPrefix(String i18nPrefix) {
        String oldValue = getI18nPrefix();
        this.i18nPrefix = i18nPrefix;
        firePropertyChange(PROPERTY_I18N_PREFIX, oldValue, i18nPrefix);
    }

    public Decorator getDecorator() {
        return decorator;
    }

    public void setDecorator(Decorator decorator) {
        this.decorator = decorator;
    }
}

