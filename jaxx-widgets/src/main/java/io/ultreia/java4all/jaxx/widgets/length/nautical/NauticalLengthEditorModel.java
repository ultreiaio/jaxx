package io.ultreia.java4all.jaxx.widgets.length.nautical;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.lang.Numbers;

/**
 * Created on 21/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.15
 */
@GenerateJavaBeanDefinition
public class NauticalLengthEditorModel extends AbstractJavaBean {

   public static final String PROPERTY_FORMAT = "format";
    static final String PROPERTY_STORAGE_NAUTICAL_LENGTH = "storageNauticalLength";
    static final String PROPERTY_NAUTICAL_LENGTH = "nauticalLength";

    private NauticalLengthEditorConfig config;

    /**
     * Format used to display value.
     */
    private NauticalLengthFormat format;
    /**
     * NauticalLength to display (using the format in {@link #format}.
     */
    private Float nauticalLength;
    /**
     * NauticalLength to send back to bean (using the format in {@link NauticalLengthEditorConfig#getStorageFormat()}).
     */
    private Float storageNauticalLength;

    /**
     * Bean where to push back value.
     */
    private Object bean;

    public NauticalLengthEditorConfig getConfig() {
        return config;
    }

    public void setConfig(NauticalLengthEditorConfig config) {
        this.config = config;
    }

    public String getLabel() {
        return config.getLabel() + " (" + format.getLabel() + ")";
    }

    public int getDigits() {
        return getConfig().getDigits();
    }

    public NauticalLengthFormat getFormat() {
        return format;
    }

    public void setFormat(NauticalLengthFormat format) {
        Object oldValue = getFormat();
        this.format = format;
        firePropertyChange(PROPERTY_FORMAT, oldValue, format);
        if (config == null) {
            return;
        }
        // reload storage nauticalLength (will update then nauticalLength)
        adjusting = true;
        try {
            setStorageNauticalLength(storageNauticalLength);
        } finally {
            adjusting = false;
        }
    }

    public Float getNauticalLength() {
        return nauticalLength;
    }

    private boolean adjusting;

    public void setNauticalLength(Float nauticalLength) {
        Float oldNauticalLength = getNauticalLength();
        this.nauticalLength = round(nauticalLength);
        firePropertyChange(PROPERTY_NAUTICAL_LENGTH, oldNauticalLength, this.nauticalLength);
        if (!adjusting) {
            Object oldStorageNauticalLength = getStorageNauticalLength();
            storageNauticalLength = round(format.convert(config.getStorageFormat(), this.nauticalLength));
            firePropertyChange(PROPERTY_STORAGE_NAUTICAL_LENGTH, oldStorageNauticalLength, storageNauticalLength);
        }
    }

    public Float getStorageNauticalLength() {
        return storageNauticalLength;
    }

    void setStorageNauticalLength(Float storageNauticalLength) {
        this.storageNauticalLength = round(storageNauticalLength);
        if (config != null) {
            Float oldNauticalLength = getNauticalLength();
            nauticalLength = round(config.getStorageFormat().convert(format, this.storageNauticalLength));
            firePropertyChange(PROPERTY_NAUTICAL_LENGTH, oldNauticalLength, nauticalLength);
        }
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    Float round(Float value) {
        return Numbers.roundNDigits(value, getConfig().getDigits());
    }

    public String getEditorKey() {
        return getEditorKey(getBean(), getConfig().getProperty());
    }

    public static String getEditorKey(Object bean, String property) {
        String prefix = bean == null ? "null" : bean.getClass().getName();
        return prefix + "-" + property;
    }
}
