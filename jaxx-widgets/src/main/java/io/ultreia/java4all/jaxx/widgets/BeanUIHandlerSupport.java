package io.ultreia.java4all.jaxx.widgets;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;
import org.nuiton.jaxx.runtime.init.UIInitializerSupport;

import javax.swing.JComponent;
import java.util.Objects;

/**
 * Created on 02/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public abstract class BeanUIHandlerSupport<U extends JComponent & BeanScopeAware> {

    protected U ui;
    private boolean init;

    protected abstract String getProperty(U ui);

    protected abstract void prepareInit(String property);

    protected abstract void prepareBindFromBean(String property, JavaBean bean);

    protected abstract void prepareBindToBean(String property, JavaBean bean);

    public void beforeInit(U ui) {
        this.ui = ui;
    }

    public void init(U ui) {
        checkNotInit();
        this.ui = Objects.requireNonNull(ui);
        try {
            prepareInit(getProperty(ui));
            String property = getProperty(ui);
            Object bean = ui.getBean();
            if (bean instanceof JavaBean && property != null) {
                boolean bindingFromBean = UIInitializerSupport.isBindingFromBean(ui);
                if (bindingFromBean) {
                    prepareBindFromBean(property, (JavaBean) bean);
                }
                boolean bindingToBean = UIInitializerSupport.isBindingToBean(ui);
                if (bindingToBean) {
                    prepareBindToBean(property, (JavaBean) bean);
                }
            }
        } finally {
            init = true;
        }
    }

    public void checkNotInit() {
        if (init) {
            throw new IllegalStateException("can not init the handler twice");
        }
    }

    public void checkInit() {
        if (!init) {
            throw new IllegalStateException("Widget should be init to come here!");
        }
    }
}
