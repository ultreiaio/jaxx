package io.ultreia.java4all.jaxx.widgets.combobox;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import java.awt.Color;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class FilterableComboBoxEditor<T extends JavaBean> extends BasicComboBoxEditor {
    private static final Logger log = LogManager.getLogger(FilterableComboBoxEditor.class);

    private final FilterableComboBoxModel<T> model;
    private final JComboBox<T> comboBox;

    private String filterText = "";
    private Object selected;
    private boolean editing;
    private boolean filterMatch;

    static class CustomTextField extends JTextField {
        private final Color defaultForeground;

        public CustomTextField() {
            UIDefaults defaults = new UIDefaults();
            defaults.put("TextField.contentMargins", new Insets(2, 6, 2, 6));
            putClientProperty("Nimbus.Overrides", defaults);
            this.defaultForeground = UIManager.getColor("ComboBox.foreground");
            setFocusTraversalKeysEnabled(false);
        }

        public void resetForegroundColor() {
            setForeground(defaultForeground);
        }

        // workaround for 4530952
        @Override
        public void setText(String s) {
            if (Objects.equals(getText(), s)) {
                return;
            }
            super.setText(s);
        }

        @Override
        public void setBorder(Border b) {
            if (!(b instanceof UIResource)) {
                super.setBorder(b);
            }
        }

        public void updateForegroundColor(boolean filterMatch) {
            //red color when no match
            setForeground(filterMatch ? defaultForeground : Color.RED);
        }

        public void updateText(boolean editing, String text, Object anObject) {
            if (editing) {
                log.info(String.format("updateFilterLabelText: search: %s vs %s", text, getText()));
                setText(text);
            } else {
                setText(anObject == null ? null : anObject.toString());
            }
        }
    }

    public FilterableComboBoxEditor(FilterableComboBoxModel<T> model, JComboBox<T> comboBox) {
        this.model = Objects.requireNonNull(model);
        this.comboBox = Objects.requireNonNull(comboBox);
        this.editor = new CustomTextField();
        EditorHandler editorHandler = new EditorHandler();
        editor.addFocusListener(editorHandler);
        editor.addKeyListener(editorHandler);
        editor.addMouseListener(editorHandler);
        comboBox.addPopupMenuListener(editorHandler);
        comboBox.setEditor(this);
        comboBox.setEditable(true);
    }

    public String getFilterText() {
        return filterText;
    }

    public void updateComboBoxModel(List<T> filteredItems, Supplier<T> selectedValue) {
        DefaultComboBoxModel<T> model = (DefaultComboBoxModel<T>) comboBox.getModel();
        model.removeAllElements();
        model.addAll(filteredItems);
        if (selectedValue != null) {
            model.setSelectedItem(selectedValue.get());
        }
    }

    @Override
    public CustomTextField getEditorComponent() {
        return (CustomTextField) super.getEditorComponent();
    }

    @Override
    public Object getItem() {
        return selected;
    }

    @Override
    public void setItem(Object anObject) {
        log.info(String.format("FilterableComboBoxEditor set item (editing %s with text: %s): %s", editing, filterText, anObject));
        getEditorComponent().updateText(editing, filterText, anObject);
        this.selected = anObject;
    }

    @Override
    protected JTextField createEditorComponent() {
        return null;
    }

    private void resetFilterComponent() {
        if (!editing) {
            return;
        }
        getEditorComponent().resetForegroundColor();
        filterText = "";
        editing = false;
        filterMatch = false;

        //restore original order
        updateComboBoxModel(model.getData(), model::getSelectedItem);
    }

    private void updateSelectedItemFromComboBox() {
        @SuppressWarnings("unchecked") T selectedItem = (T) comboBox.getSelectedItem();
        model.setSelectedItem(selectedItem);
    }

    private void applyFilter() {
        log.info("Apply filter with: " + filterText);
        List<T> filteredItems = model.updateFilteredData(filterText);
        filterMatch = !filteredItems.isEmpty();
        getEditorComponent().updateForegroundColor(filterMatch);
        updateComboBoxModel(filteredItems, () -> filterMatch ? filteredItems.get(0) : null);
    }

    private void cancelEditing() {
        T selectedItem = model.getSelectedItem();
        log.info(String.format("Cancel editing, restore model value: %s", selectedItem));
        resetFilterComponent();
        comboBox.setSelectedItem(selectedItem);
        setItem(selectedItem);
    }

    private class EditorHandler implements KeyListener, FocusListener, PopupMenuListener, MouseListener {

        /**
         * Internal state to know if popup was canceled.
         */
        private boolean wasCanceled;

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            wasCanceled = false;
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            if (wasCanceled) {
                return;
            }
            if (editing && !filterMatch) {
                return;
            }
            Object selectedItem = comboBox.getSelectedItem();
            T modelSelectedItem = model.getSelectedItem();
            if (!Objects.equals(modelSelectedItem, selectedItem)) {
                log.info("popupMenuWillBecomeInvisible::need to set new selected item: " + selectedItem);
                updateSelectedItemFromComboBox();
                resetFilterComponent();
            }
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            wasCanceled = true;
            cancelEditing();
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            resetFilterComponent();
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (e.isControlDown()) {
                return;
            }
            switch (keyCode) {
                case KeyEvent.VK_ENTER:
                    if (!comboBox.isPopupVisible()) {
                        comboBox.showPopup();
                        e.consume();
                        return;
                    }
                    updateSelectedItemFromComboBox();
                    resetFilterComponent();
                    e.consume();
                    comboBox.hidePopup();
                    return;
                case KeyEvent.VK_TAB:
                    if (filterMatch || !editing) {
                        updateSelectedItemFromComboBox();
                    }
                    resetFilterComponent();
                    if (comboBox.isPopupVisible()) {
                        comboBox.hidePopup();
                    }
                    if (e.isShiftDown()) {
                        focusPreviousComponent();
                    } else {
                        focusNextComponent();
                    }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            char keyChar = e.getKeyChar();
            if (!Character.isDefined(keyChar)) {
                return;
            }
            if (e.isControlDown()) {
                return;
            }
            int keyCode = e.getKeyCode();
            switch (keyCode) {
                case KeyEvent.VK_DELETE:
                case KeyEvent.VK_ESCAPE:
                case KeyEvent.VK_ENTER:
                case KeyEvent.VK_TAB:
                    return;
                case KeyEvent.VK_BACK_SPACE:
                    removeCharAtEnd();
                    break;
                default:
                    addChar(keyChar);
                    e.consume();
            }
            if (editing) {
                if (!comboBox.isPopupVisible()) {
                    comboBox.showPopup();
                }
                applyFilter();
            }
        }

        private void focusPreviousComponent() {
            JXTable table = (JXTable) comboBox.getClientProperty("Table");
            if (table != null) {
                table.getActionForKeyStroke(KeyStroke.getKeyStroke("shift pressed TAB")).actionPerformed(null);
            } else {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        }

        private void focusNextComponent() {
            JXTable table = (JXTable) comboBox.getClientProperty("Table");
            if (table != null) {
                table.getActionForKeyStroke(KeyStroke.getKeyStroke("pressed TAB")).actionPerformed(null);
            } else {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        }

        public void addChar(char c) {
            filterText += c;
            editing = true;
        }

        public void removeCharAtEnd() {
            if (filterText.length() > 0) {
                filterText = filterText.substring(0, filterText.length() - 1);
                editing = true;
            }
        }


        @Override
        public void mouseClicked(MouseEvent e) {
            if (comboBox.isEnabled() && !comboBox.isPopupVisible()) {
                comboBox.showPopup();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
