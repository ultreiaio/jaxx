package io.ultreia.java4all.jaxx.widgets.combobox.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.combobox.JaxxComboBox;
import org.nuiton.jaxx.runtime.swing.session.State;

/**
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.20
 */
@SuppressWarnings("unused")
public class JaxxComboBoxState implements State {

    protected int index = 0;

    protected boolean reverseSort = false;

    public JaxxComboBoxState() {
    }

    public JaxxComboBoxState(int index, boolean reverseSort) {
        this.index = index;
        this.reverseSort = reverseSort;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    protected JaxxComboBox<?> checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JaxxComboBox)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JaxxComboBox<?>) o;
    }

    @Override
    public State getState(Object o) {
        JaxxComboBox<?> combo = checkComponent(o);
        return new JaxxComboBoxState(combo.getModel().getIndex(), combo.getModel().getReverseSort());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof JaxxComboBoxState)) {
            throw new IllegalArgumentException("invalid state");
        }
        JaxxComboBox<?> combo = checkComponent(o);
        JaxxComboBoxState typedState = (JaxxComboBoxState) state;
        combo.getModel().setIndex(typedState.getIndex());
        combo.getModel().setReverseSort(typedState.isReverseSort());
    }


}
