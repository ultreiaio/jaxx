package io.ultreia.java4all.jaxx.widgets.length.nautical;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Created on 21/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.15
 */
public class NauticalLengthEditorConfig  {
    /**
     * Default number of digits (to round values).
     *
     * @see NauticalLengthEditorModel#getDigits()
     */
    public static final int DEFAULT_DIGITS = 4;
    /**
     * Format used in storage (always use this format to send back to bean).
     * <p>
     * This format will never changed.
     */
    private final NauticalLengthFormat storageFormat;
    /**
     * Label to display.
     */
    private final String label;
    /**
     * Property of the bean where to push back value.
     */
    private String property;
    /**
     * Number of digits to use to round values.
     */
    private int digits = DEFAULT_DIGITS;

    public NauticalLengthEditorConfig(NauticalLengthFormat storageFormat, String label, String property) {
        this.storageFormat = Objects.requireNonNull(storageFormat);
        this.label = label;
        this.property = property;
    }

    public NauticalLengthFormat getStorageFormat() {
        return storageFormat;
    }

    String getLabel() {
        return label;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public int getDigits() {
        return digits;
    }

    public void setDigits(int digits) {
        this.digits = digits;
    }
}
