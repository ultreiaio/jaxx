package io.ultreia.java4all.jaxx.widgets.list.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.list.ListHeader;
import org.nuiton.jaxx.runtime.swing.session.State;

/**
 * All states to persist on a {@link ListHeader}.
 * <p>
 * Created at 30/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.26
 */
public class ListHeaderState implements State {

    protected int index = 0;

    protected boolean reverseSort = false;

    public ListHeaderState() {
    }

    public ListHeaderState(int index, boolean reverseSort) {
        this.index = index;
        this.reverseSort = reverseSort;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    protected ListHeader<?> checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof ListHeader)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (ListHeader<?>) o;
    }

    @Override
    public State getState(Object o) {
        ListHeader<?> combo = checkComponent(o);
        return new ListHeaderState(combo.getIndex(), combo.getReverseSort());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof ListHeaderState)) {
            throw new IllegalArgumentException("invalid state");
        }
        ListHeader<?> ui = checkComponent(o);
        ListHeaderState typedState = (ListHeaderState) state;
        ui.setIndex(typedState.getIndex());
        ui.setReverseSort(typedState.isReverseSort());
    }

}
