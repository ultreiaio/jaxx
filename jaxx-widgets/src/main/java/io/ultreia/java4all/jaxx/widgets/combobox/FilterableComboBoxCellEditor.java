package io.ultreia.java4all.jaxx.widgets.combobox;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created on 23/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class FilterableComboBoxCellEditor extends AbstractCellEditor implements TableCellEditor, AncestorListener, PropertyChangeListener {

    private final FilterableComboBox<? super JavaBean> component;

    private Object value;

    @SuppressWarnings({"rawtypes", "unchecked"})
    public FilterableComboBoxCellEditor(FilterableComboBox<?> editor) {
        this.component = (FilterableComboBox<? super JavaBean>) editor;
        JComboBox comboBox = editor.getCombobox();
        component.setOpaque(false);
        ((JComponent) comboBox.getEditor().getEditorComponent()).addAncestorListener(this);
    }

    @Override
    public Object getCellEditorValue() {
        return value;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.value = value;
        JComboBox<? extends JavaBean> comboBox = component.getCombobox();
        comboBox.putClientProperty("Table", table);
        component.setSelectedItem((JavaBean) value);
        listen();
        return component;
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        SwingUtilities.invokeLater(() -> component.getCombobox().requestFocusInWindow());
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        value = evt.getNewValue();
        stopCellEditing();
    }

    @Override
    public boolean stopCellEditing() {
        unListen();
        return super.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        unListen();
        super.cancelCellEditing();
    }

    public FilterableComboBox<?> getComponent() {
        return component;
    }

    private void listen() {
        unListen();
        component.getModel().addPropertyChangeListener(FilterableComboBoxModel.PROPERTY_SELECTED_ITEM, this);
    }

    private void unListen() {
        component.getModel().removePropertyChangeListener(FilterableComboBoxModel.PROPERTY_SELECTED_ITEM, this);
    }

}
