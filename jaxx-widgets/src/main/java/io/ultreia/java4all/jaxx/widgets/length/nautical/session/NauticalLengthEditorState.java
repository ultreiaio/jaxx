package io.ultreia.java4all.jaxx.widgets.length.nautical.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditor;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat;
import org.nuiton.jaxx.runtime.swing.session.State;

/**
 * All states to persist on a {@link NauticalLengthEditor}.
 * <p>
 * Created at 30/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.26
 */
public class NauticalLengthEditorState implements State {

    protected NauticalLengthFormat format;

    public NauticalLengthEditorState() {
    }

    public NauticalLengthEditorState(NauticalLengthFormat format) {
        this.format = format;
    }

    public NauticalLengthFormat getFormat() {
        return format;
    }

    public void setFormat(NauticalLengthFormat format) {
        this.format = format;
    }

    protected NauticalLengthEditor checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof NauticalLengthEditor)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (NauticalLengthEditor) o;
    }

    @Override
    public State getState(Object o) {
        NauticalLengthEditor ui = checkComponent(o);
        return new NauticalLengthEditorState(ui.getModel().getFormat());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof NauticalLengthEditorState)) {
            throw new IllegalArgumentException("invalid state");
        }
        NauticalLengthEditor ui = checkComponent(o);
        NauticalLengthEditorState typedState = (NauticalLengthEditorState) state;
        ui.getModel().setFormat(typedState.getFormat());
    }
}
