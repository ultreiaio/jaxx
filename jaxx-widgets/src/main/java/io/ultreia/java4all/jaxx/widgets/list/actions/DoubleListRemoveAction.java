package io.ultreia.java4all.jaxx.widgets.list.actions;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.SwingUtil;
import io.ultreia.java4all.jaxx.widgets.list.DoubleList;

import java.awt.event.ActionEvent;
import java.util.List;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created by tchemit on 16/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class DoubleListRemoveAction<O> extends DoubleListActionSupport<O> {

    public DoubleListRemoveAction() {
        super("", t("beandoublelist.button.remove"), "doublelist-unselect", SwingUtil.findKeyStroke("Doublelist.remove", "ctrl pressed L"));
        setCheckMenuItemIsArmed(false);
    }

    @Override
    protected void doActionPerformed(ActionEvent e, DoubleList<O> ui) {
        List<O> selectedValues = ui.getSelectedList().getSelectedValuesList();
        ui.getHandler().removeFromSelected(selectedValues);
        ui.getSelectedList().clearSelection();
    }
}
