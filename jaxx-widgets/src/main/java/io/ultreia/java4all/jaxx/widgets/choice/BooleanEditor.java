/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.jaxx.widgets.choice;


import io.ultreia.java4all.bean.JavaBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;

import javax.swing.JComboBox;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Un éditeur de {@link Boolean} avec trois valeurs possibles :
 * <ul>
 * <li>{@code Null}</li>
 * <li>{@code false}</li>
 * <li>{@code true}</li>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class BooleanEditor extends JComboBox<BooleanEditor.ValueEntry> implements BeanScopeAware {
    private static final Logger log = LogManager.getLogger(BooleanEditor.class);

    private static final long serialVersionUID = 1L;
    private final BooleanEditorHandler handler;
    private String property;
    private JavaBean bean;

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {

        private final List<ValueEntry> entries = new LinkedList<>();
        private ValueEntry nullEntry;
        private ValueEntry falseEntry;
        private ValueEntry trueEntry;

        public Builder addNullEntry(String label) {
            if (nullEntry != null) {
                throw new IllegalStateException("Null entry already added");
            }
            nullEntry = new ValueEntry(Value.NULL, label);
            entries.add(nullEntry);
            return this;
        }

        public Builder addFalseEntry(String label) {
            if (falseEntry != null) {
                throw new IllegalStateException("False entry already added");
            }
            falseEntry = new ValueEntry(Value.FALSE, label);
            entries.add(falseEntry);
            return this;
        }

        public Builder addTrueEntry(String label) {
            if (trueEntry != null) {
                throw new IllegalStateException("True entry already added");
            }
            trueEntry = new ValueEntry(Value.TRUE, label);
            entries.add(trueEntry);
            return this;
        }

        public BooleanEditor build() {
            return new BooleanEditor(entries.toArray(new ValueEntry[0]));
        }
    }

    public BooleanEditor() {
        this(null, null, null);
    }

    public BooleanEditor(String nullLibelle, String falseLibelle, String trueLibelle) {
        this(new ValueEntry[]{
                new ValueEntry(Value.NULL, nullLibelle),
                new ValueEntry(Value.FALSE, falseLibelle),
                new ValueEntry(Value.TRUE, trueLibelle)});
    }

    public BooleanEditor(ValueEntry[] entries) {
        super(entries);
        this.handler = new BooleanEditorHandler();
    }

    public void init() {
        handler.init(this);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        handler.checkNotInit();
        log.debug(String.format("%s - set property: %s", getName(), property));
        this.property = property;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    @Override
    public void setBean(Object bean) {
        handler.checkNotInit();
        log.debug(String.format("%s - set bean: %s", getName(), bean));
        this.bean = (JavaBean) bean;
    }

    public Boolean getBooleanValue() {
        Object o = getSelectedItem();
        if (o == null) {
            return null;
        }
        ValueEntry v = (ValueEntry) o;
        return v.getValue().getBooleanValue();
    }

    public void setBooleanValue(Boolean b) {
        Value v = Value.valueOf(b);
        ValueEntry e = dataModel.getElementAt(v.ordinal());
        setSelectedItem(e);
    }

    public enum Value {
        NULL(n("boolean.null"), null),
        FALSE(n("boolean.false"), false),
        TRUE(n("boolean.true"), true);

        private final String defaultLibelle;

        private final Boolean booleanValue;

        Value(String defaultLibelle, Boolean booleanValue) {
            this.defaultLibelle = defaultLibelle;
            this.booleanValue = booleanValue;
        }

        public static Value valueOf(Boolean value) {
            Value result = null;
            for (Value v : values()) {
                if (v.getBooleanValue() == value) {
                    result = v;
                    break;
                }
            }
            return result;
        }

        public Boolean getBooleanValue() {
            return booleanValue;
        }

        public String getDefaultLibelle() {
            return defaultLibelle;
        }
    }

    protected static class ValueEntry {

        protected final Value value;
        protected final String text;

        ValueEntry(Value value, String text) {
            this.value = Objects.requireNonNull(value);
            this.text = t(text == null ? value.getDefaultLibelle() : text);
        }

        public Value getValue() {
            return value;
        }

        public String getText() {
            return text;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ValueEntry)) return false;
            ValueEntry that = (ValueEntry) o;
            return value == that.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return text;
        }
    }

}
