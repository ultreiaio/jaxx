package io.ultreia.java4all.jaxx.widgets.list.session;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.list.DoubleList;
import org.nuiton.jaxx.runtime.swing.session.State;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class DoubleListState implements State {

    protected int index = 0;

    protected Boolean reverseSort = false;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    @Override
    public State getState(Object o) {
        DoubleList<?> list = checkComponent(o);
        DoubleListState state = new DoubleListState();
        state.setIndex(list.getIndex());
        state.setReverseSort(list.isReverseSort());
        return state;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof DoubleListState)) {
            throw new IllegalArgumentException("invalid state");
        }

        DoubleList<?> list = checkComponent(o);
        DoubleListState beanDoubleListState = (DoubleListState) state;
        list.setIndex(beanDoubleListState.getIndex());
        list.setReverseSort(beanDoubleListState.isReverseSort());

    }

    protected DoubleList<?> checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof DoubleList)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (DoubleList<?>) o;
    }

}
