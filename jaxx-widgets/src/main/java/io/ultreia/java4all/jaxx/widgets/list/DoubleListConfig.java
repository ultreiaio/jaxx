package io.ultreia.java4all.jaxx.widgets.list;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.Decorated;

/**
 * Put here all immutable options used to init the filtreable double list.
 * <p>
 * Created on 11/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class DoubleListConfig<O> {

    /**
     * Optional property where to bind the selected value changes in optional bean.
     */
    protected String property;

    /**
     * Bean type.
     */
    protected Class<O> beanType;

    /**
     * to be able to select a same entry more than one time property.
     */
    protected boolean useMultiSelect;
    /**
     * Is bean type decorator aware
     *
     * @see Decorated
     */
    private boolean beanDecoratorAware;
    /**
     * I18n prefix use to display build i18n messages.
     */
    protected String i18nPrefix;

    /**
     * {@code true} to see labels above lists.
     */
    protected boolean showListLabel;

    /**
     * Optional widget label.
     */
    protected String label;
    /**
     * Optional universe list label.
     */
    protected String universeLabel;

    /**
     * Optional selected list label.
     */
    protected String selectedLabel;

    /**
     * To auto sort selected list, when it changes.
     */
    protected boolean autoSortSelectedList;

    public Class<O> getBeanType() {
        return beanType;
    }

    public void setBeanType(Class<O> beanType) {
        this.beanType = beanType;
        beanDecoratorAware = Decorated.class.isAssignableFrom(beanType);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public boolean isUseMultiSelect() {
        return useMultiSelect;
    }

    public void setUseMultiSelect(boolean useMultiSelect) {
        this.useMultiSelect = useMultiSelect;
    }

    public String getI18nPrefix() {
        return i18nPrefix;
    }

    public void setI18nPrefix(String i18nPrefix) {
        this.i18nPrefix = i18nPrefix;
    }

    public boolean isShowListLabel() {
        return showListLabel;
    }

    public void setShowListLabel(boolean showListLabel) {
        this.showListLabel = showListLabel;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUniverseLabel() {
        return universeLabel;
    }

    public void setUniverseLabel(String universeLabel) {
        this.universeLabel = universeLabel;
    }

    public String getSelectedLabel() {
        return selectedLabel;
    }

    public void setSelectedLabel(String selectedLabel) {
        this.selectedLabel = selectedLabel;
    }

    public boolean isAutoSortSelectedList() {
        return autoSortSelectedList;
    }

    public void setAutoSortSelectedList(boolean autoSortSelectedList) {
        this.autoSortSelectedList = autoSortSelectedList;
    }

    public boolean isBeanDecoratorAware() {
        return beanDecoratorAware;
    }
}
