package io.ultreia.java4all.jaxx.widgets.combobox;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.HtmlHighlighter;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Component;
import java.util.function.Supplier;

public class FilterableComboBoxRenderer extends DefaultListCellRenderer {
    private static final Color defaultBackground = UIManager.getColor("List.background");
    private static final Color defaultForeground = UIManager.getColor("List.foreground");
    private static final Color alternateBackgroundColor = UIManager.getColor("Table.alternateRowColor");
    private final int decoratorContextCount;
    private final String decoratorSeparator;
    private final Supplier<Boolean> onlySearchOnSelectedPropertySupplier;
    private final Supplier<String> highlightTextSupplier;

    public FilterableComboBoxRenderer(int decoratorContextCount, String decoratorSeparator, Supplier<Boolean> onlySearchOnSelectedPropertySupplier, Supplier<String> highlightTextSupplier) {
        this.decoratorContextCount = decoratorContextCount;
        this.decoratorSeparator = decoratorSeparator;
        this.onlySearchOnSelectedPropertySupplier = onlySearchOnSelectedPropertySupplier;
        this.highlightTextSupplier = highlightTextSupplier;
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value == null) {
            return this;
        }
        Boolean onlySearchOnSelectedProperty = onlySearchOnSelectedPropertySupplier.get();
        String text = value.toString();
        String textToHighlight = highlightTextSupplier.get();
        if (onlySearchOnSelectedProperty != null && onlySearchOnSelectedProperty && decoratorContextCount > 1) {
            int selectedPropertyIndex = text.indexOf(decoratorSeparator);
            String selectedProperty = text.substring(0, selectedPropertyIndex);
            text = HtmlHighlighter.highlightText(selectedProperty, textToHighlight, text.substring(selectedPropertyIndex));
        } else {
            text = HtmlHighlighter.highlightText(text, textToHighlight, null);
        }
        this.setText(text);
        if (!isSelected) {
            this.setBackground(index % 2 == 0 ? defaultBackground : alternateBackgroundColor);
        }
        this.setForeground(defaultForeground);
        return this;
    }

}
