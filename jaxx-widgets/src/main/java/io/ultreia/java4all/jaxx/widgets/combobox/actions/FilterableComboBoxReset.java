package io.ultreia.java4all.jaxx.widgets.combobox.actions;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.combobox.FilterableComboBox;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import java.awt.event.ActionEvent;

import static io.ultreia.java4all.i18n.I18n.n;

/**
 * Created on 23/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class FilterableComboBoxReset<O extends JavaBean> extends FilterableComboBoxActionSupport<O> {

    public FilterableComboBoxReset() {
        super(null, n("bean.action.reset.tip"), "combobox-reset", SwingUtil.findKeyStroke("bean.reset", "ctrl pressed D"));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, FilterableComboBox<O> ui) {
        ui.reset();
    }
}
