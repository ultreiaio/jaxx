package io.ultreia.java4all.jaxx.widgets.choice;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.function.Predicate;

/**
 * Created on 14/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class BooleanTableCellRenderer extends JCheckBox implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    private static final Predicate<?> DEFAULT_PREDICATE = input -> (input != null && (Boolean) input);
    protected final TableCellRenderer defaultDelegate;
    @SuppressWarnings("rawtypes")
    private final Predicate predicate;

    public BooleanTableCellRenderer(TableCellRenderer delegate, Predicate<?> predicate) {
        this(delegate, null, predicate);
    }

    public BooleanTableCellRenderer(TableCellRenderer delegate) {
        //super(new BorderLayout());
        this(delegate, DEFAULT_PREDICATE);

    }

    public BooleanTableCellRenderer(TableCellRenderer delegate, Icon icon, Predicate<?> predicate) {
        this.defaultDelegate = delegate;
        this.predicate = predicate;
        setOpaque(true);
        setBorderPainted(true);
        setHorizontalAlignment(JLabel.CENTER);
        setIcon(icon);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        ((JComponent) defaultDelegate).setBackground(null);
        JComponent render = (JComponent) defaultDelegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(render.getForeground());
            setBackground(render.getBackground());
            if (row % 2 == 0) {
                setBackground(Color.WHITE);
            }
        }

        @SuppressWarnings("unchecked") boolean selectCheckBox = predicate.test(value);
        setSelected(selectCheckBox);

        setBorder(render.getBorder());
        return this;
    }
}
