/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

#model {
  format:{(NauticalLengthFormat) formatBG.getSelectedValue()};
}

JRadioButton {
    text:{NauticalLengthFormat.%%.getLabel()};
    toolTipText:{NauticalLengthFormat.%%.getDescription()};
    value:{NauticalLengthFormat.%%};
    selected:{NauticalLengthFormat.%% == model.getFormat()};
    buttonGroup:formatBG;
    focusable:false;
}

#editor {
  property:{NauticalLengthEditorModel.PROPERTY_NAUTICAL_LENGTH};
  numberValue:{model.getNauticalLength()};
  numberPattern:{SwingUtil.DECIMAL4_PATTERN};
  numberType:{Float.class};
  useSign:false;
}
