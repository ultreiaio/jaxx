package io.ultreia.java4all.jaxx.widgets.length.nautical;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

/**
 * Created on 21/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.15
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = "label", pattern = "enum.@CLASS_NAME@.@NAME@"),
        @TranslateEnumeration(name = "description", pattern = "enum.@CLASS_NAME@.@NAME@.description")})
public enum NauticalLengthFormat {
    M {
        @Override
        public Float toM(Float value) {
            return value;
        }

        @Override
        public Float toKM(Float value) {
            return meterToKiloMeter(value);
        }

        @Override
        public Float toNM(Float value) {
            return meterToKiloMeter(kiloMeterToMile(value));
        }

        @Override
        public Float toFTM(Float value) {
            return meterToBrass(value);
        }
    },
    KM {
        @Override
        public Float toM(Float value) {
            return kiloMeterToMeter(value);
        }

        @Override
        public Float toKM(Float value) {
            return value;
        }

        @Override
        public Float toNM(Float value) {
            return kiloMeterToMile(value);
        }

        @Override
        public Float toFTM(Float value) {
            return meterToBrass(kiloMeterToMeter(value));
        }
    },
    NM {
        @Override
        public Float toM(Float value) {
            return kiloMeterToMeter(mileToKiloMeter(value));
        }

        @Override
        public Float toKM(Float value) {
            return mileToKiloMeter(value);
        }

        @Override
        public Float toNM(Float value) {
            return value;
        }

        @Override
        public Float toFTM(Float value) {
            return meterToBrass(kiloMeterToMeter(mileToKiloMeter(value)));
        }
    },
    FTM {
        @Override
        public Float toM(Float value) {
            return brassToMeter(value);
        }

        @Override
        public Float toKM(Float value) {
            return meterToKiloMeter(brassToMeter(value));
        }

        @Override
        public Float toNM(Float value) {
            return kiloMeterToMile(meterToKiloMeter(brassToMeter(value)));
        }

        @Override
        public Float toFTM(Float value) {
            return value;
        }
    };

    /**
     * Convert the given {@code value} from this format to the given {@code targetFormat}.
     *
     * @param targetFormat target format
     * @param value        value to convert
     * @return converted value
     */
    public final Float convert(NauticalLengthFormat targetFormat, Float value) {
        if (value == null) {
            return null;
        }
        switch (targetFormat) {
            case M:
                return toM(value);
            case KM:
                return toKM(value);
            case NM:
                return toNM(value);
            case FTM:
                return toFTM(value);
            default:
                throw new IllegalStateException();
        }
    }

    public abstract Float toM(Float value);

    public abstract Float toKM(Float value);

    public abstract Float toNM(Float value);

    public abstract Float toFTM(Float value);

    public String getLabel() {
        return NauticalLengthFormatI18n.getLabel(this);
    }

    public String getDescription() {
        return NauticalLengthFormatI18n.getDescription(this);
    }

    /**
     * One brass = ratio meter
     */
    public static final float BRASS_TO_METER_RATIO = 1.8288f;
    /**
     * One kiloMeter = ratio meter
     */
    public static final float KILOMETER_TO_METER_RATIO = 1000f;
    /**
     * One mile = ratio kiloMeter
     */
    public static final float MILE_TO_KILOMETER_RATIO = 1.852f;

    public static float brassToMeter(Float value) {
        return value * BRASS_TO_METER_RATIO;
    }

    public static float meterToBrass(Float value) {
        return value / BRASS_TO_METER_RATIO;
    }

    public static float kiloMeterToMeter(Float value) {
        return value * KILOMETER_TO_METER_RATIO;
    }

    public static float meterToKiloMeter(Float value) {
        return value / KILOMETER_TO_METER_RATIO;
    }

    public static float mileToKiloMeter(Float value) {
        return value * MILE_TO_KILOMETER_RATIO;
    }

    public static float kiloMeterToMile(Float value) {
        return value / MILE_TO_KILOMETER_RATIO;
    }
}

