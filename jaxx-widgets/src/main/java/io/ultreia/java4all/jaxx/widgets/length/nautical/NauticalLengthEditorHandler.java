package io.ultreia.java4all.jaxx.widgets.length.nautical;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.number.NumberEditorModel;

import javax.swing.JLabel;
import java.util.Objects;

/**
 * Created on 21/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.15
 */
public class NauticalLengthEditorHandler extends BeanUIHandlerSupport<NauticalLengthEditor> implements UIHandler<NauticalLengthEditor> {
    private static final Logger log = LogManager.getLogger(NauticalLengthEditorHandler.class);

    @Override
    public void beforeInit(NauticalLengthEditor ui) {
        super.beforeInit(ui);
        NauticalLengthEditorModel model = new NauticalLengthEditorModel();
        model.setFormat(NauticalLengthFormat.M);
        ui.setContextValue(model);
    }

    @Override
    protected String getProperty(NauticalLengthEditor ui) {
        NauticalLengthEditorModel model = ui.getModel();
        if (model == null) {
            return null;
        }
        NauticalLengthEditorConfig config = model.getConfig();
        if (config == null) {
            return null;
        }
        return config.getProperty();
    }

    public void init(NauticalLengthEditor ui, JLabel label) {
        Objects.requireNonNull(label, "No label, can't init.");
        init(ui);

        NauticalLengthEditorModel model = ui.getModel();
        model.addPropertyChangeListener(NauticalLengthEditorModel.PROPERTY_FORMAT, evt -> label.setText(model.getLabel()));
        label.setText(model.getLabel());
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init NauticalLengthEditor", ui.getName()));
        NauticalLengthEditorModel model = ui.getModel();
        NauticalLengthEditorConfig config = model.getConfig();
        Objects.requireNonNull(config, "No config in editor, can't init.");

        if (property == null || property.isEmpty()) {
            config.setProperty(ui.getName());
        }
        ui.getEditor().init();
        ui.getEditor().getModel().addPropertyChangeListener(NumberEditorModel.PROPERTY_NUMBER_VALUE, evt -> model.setNauticalLength((Float) evt.getNewValue()));
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        NauticalLengthEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, e -> {
            Float oldValue = model.getStorageNauticalLength();
            Float newValue = (Float) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                log.debug(String.format("%s - [%s] get new value from bean: %s", ui.getName(), property, newValue));
                ui.setStorageNauticalLength(newValue);
            }
        });

    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        NauticalLengthEditorModel model = ui.getModel();
        model.addPropertyChangeListener(NauticalLengthEditorModel.PROPERTY_STORAGE_NAUTICAL_LENGTH, evt -> {
            Float newValue = (Float) evt.getNewValue();
            if (!Objects.equals(newValue, bean.get(property))) {
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }

}

