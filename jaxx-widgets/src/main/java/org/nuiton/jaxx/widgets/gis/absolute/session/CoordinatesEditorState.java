package org.nuiton.jaxx.widgets.gis.absolute.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.session.State;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;

/**
 * All states to persist on a {@link CoordinatesEditor}.
 * <p>
 * Created at 30/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.26
 */
public class CoordinatesEditorState implements State {

    protected CoordinateFormat format;

    public CoordinatesEditorState() {
    }

    public CoordinatesEditorState(CoordinateFormat format) {
        this.format = format;
    }

    public CoordinateFormat getFormat() {
        return format;
    }

    public void setFormat(CoordinateFormat format) {
        this.format = format;
    }

    protected CoordinatesEditor checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof CoordinatesEditor)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (CoordinatesEditor) o;
    }

    @Override
    public State getState(Object o) {
        CoordinatesEditor ui = checkComponent(o);
        return new CoordinatesEditorState(ui.getModel().getFormat());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof CoordinatesEditorState)) {
            throw new IllegalArgumentException("invalid state");
        }
        CoordinatesEditor ui = checkComponent(o);
        CoordinatesEditorState typedState = (CoordinatesEditorState) state;
        ui.getModel().setFormat(typedState.getFormat());
    }
}
