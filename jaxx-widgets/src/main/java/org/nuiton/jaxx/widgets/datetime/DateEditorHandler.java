package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.KeyStroke;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created on 9/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateEditorHandler extends BeanUIHandlerSupport<DateEditor> implements UIHandler<DateEditor> {

    private static final Logger log = LogManager.getLogger(DateEditorHandler.class);

    @Override
    public void beforeInit(DateEditor ui) {
        super.beforeInit(ui);
        DateEditorModel model = new DateEditorModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(DateEditor ui) {
        ExtendedBasicDatePickerUI extendedUi = new ExtendedBasicDatePickerUI();
        extendedUi.setShowPopupButton(true);
        ui.getDateEditor().setUI(extendedUi);
        JXDatePicker dateEditor = ui.getDateEditor();

        InputMap pickerInputMap = dateEditor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pickerInputMap.put(KeyStroke.getKeyStroke("pressed DOWN"), "TOGGLE_POPUP");

        dateEditor.getEditor().setFocusLostBehavior(JFormattedTextField.COMMIT);
    }

    @Override
    protected String getProperty(DateEditor ui) {
        return ui.getModel().getProperty();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init DateEditor", ui.getName()));
        if (property == null || property.isEmpty()) {
            ui.setProperty(ui.getName());
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        DateEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, e -> {
            Date oldValue = model.getDate();
            Date newValue = (Date) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                ui.setDate(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        DateEditorModel model = ui.getModel();
        Predicate<DateEditorModel> predicate = model.canUpdateBeanValuePredicate();
        model.addPropertyChangeListener(DateEditorModel.PROPERTY_DATE, evt -> {
            if (predicate.test(model)) {
                Object newValue = evt.getNewValue();
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }

}
