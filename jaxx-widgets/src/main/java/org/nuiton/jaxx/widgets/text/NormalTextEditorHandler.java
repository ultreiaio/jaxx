package org.nuiton.jaxx.widgets.text;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.JTextComponentDocumentListener;
import org.nuiton.jaxx.widgets.text.actions.NormalTextEditorReset;

import javax.swing.JTextField;
import java.util.Objects;

/**
 * Created by tchemit on 11/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class NormalTextEditorHandler extends BeanUIHandlerSupport<NormalTextEditor> implements UIHandler<NormalTextEditor> {
    private static final Logger log = LogManager.getLogger(NormalTextEditorHandler.class);

    @Override
    public void afterInit(NormalTextEditor ui) {
        ui.addPropertyChangeListener(NormalTextEditor.PROPERTY_RESET_TIP, evt -> {
            String newValue = (String) evt.getNewValue();
            NormalTextEditorReset action = (NormalTextEditorReset) ui.getReset().getAction();
            action.setTooltipText(newValue);
            action.rebuildTexts(true);
        });
        new JTextComponentDocumentListener<>(ui, ui.getTextEditor()) {

            @Override
            protected void doUpdate(NormalTextEditor ui, JTextField editor) {
                ui.firePropertyChange("text", "", ui.getText());
            }
        }.install();
    }

    @Override
    protected String getProperty(NormalTextEditor ui) {
        return ui.getProperty();
    }

    @Override
    protected void prepareInit(String property) {
        if (property == null || property.isEmpty()) {
            ui.setProperty(ui.getName());
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        bean.addPropertyChangeListener(property, e -> {
            String oldValue = ui.getText();
            String newValue = (String) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                log.debug(String.format("%s - [%s] get new value from bean: %s", ui.getName(), property, newValue));
                ui.setText(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        ui.addPropertyChangeListener("text", e -> {
            String oldValue = (String) e.getOldValue();
            String newValue = (String) e.getNewValue();
            if (!Objects.equals(oldValue, newValue) && !Objects.equals(bean.get(property), newValue)) {
                log.debug(String.format("%s - [%s] get new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }

}
