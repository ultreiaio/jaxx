package org.nuiton.jaxx.widgets.temperature;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Numbers;
import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TemperatureEditorModel extends AbstractSerializableBean {

    public static final String PROPERTY_FORMAT = "format";
    static final String PROPERTY_STORAGE_TEMPERATURE = "storageTemperature";
    static final String PROPERTY_TEMPERATURE = "temperature";
    private static final long serialVersionUID = -3883079504284754230L;

    private TemperatureEditorConfig config;

    /**
     * Format used to display value.
     */
    private TemperatureFormat format;
    /**
     * Temperature to display (using the format in {@link #format}.
     */
    private Float temperature;
    /**
     * Temperature to send back to bean (using the format in {@link TemperatureEditorConfig#getStorageFormat()}).
     */
    private Float storageTemperature;

    /**
     * Bean where to push back value.
     */
    private Object bean;

    public TemperatureEditorConfig getConfig() {
        return config;
    }

    public void setConfig(TemperatureEditorConfig config) {
        this.config = config;
    }

    public String getLabel() {
        return config.getLabel() + " (" + format.getLabel() + ")";
    }

    public int getDigits() {
        return getConfig().getDigits();
    }

    public TemperatureFormat getFormat() {
        return format;
    }

    public void setFormat(TemperatureFormat format) {
        Object oldValue = getFormat();
        this.format = format;
        firePropertyChange(PROPERTY_FORMAT, oldValue, format);
        if (config == null) {
            return;
        }
        // reload storage temperature (will update then temperature)
        adjusting = true;
        try {
            setStorageTemperature(storageTemperature);
        } finally {
            adjusting = false;
        }
    }

    public Float getTemperature() {
        return temperature;
    }

    private boolean adjusting;

    public void setTemperature(Float temperature) {
        Float oldTemperature = getTemperature();
        this.temperature = round(temperature);
        firePropertyChange(PROPERTY_TEMPERATURE, oldTemperature, this.temperature);
        if (!adjusting) {
            Object oldStorageTemperature = getStorageTemperature();
            storageTemperature = round(format.convert(this.temperature, config.getStorageFormat()));
            firePropertyChange(PROPERTY_STORAGE_TEMPERATURE, oldStorageTemperature, storageTemperature);
        }
    }

    private Float getStorageTemperature() {
        return storageTemperature;
    }

    void setStorageTemperature(Float storageTemperature) {
        this.storageTemperature = round(storageTemperature);
        if (config != null) {
            Float oldTemperature = getTemperature();
            temperature = round(config.getStorageFormat().convert(this.storageTemperature, format));
            firePropertyChange(PROPERTY_TEMPERATURE, oldTemperature, temperature);
        }
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    Float round(Float value) {
        return Numbers.roundNDigits(value, getConfig().getDigits());
    }

    public String getEditorKey() {
        return getEditorKey(getBean(), getConfig().getProperty());
    }

    public static String getEditorKey(Object bean, String property) {
        String prefix = bean == null ? "null" : bean.getClass().getName();
        return prefix + "-" + property;
    }
}
