package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.widgets.ModelToBean;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Predicate;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class TimeEditorModel extends AbstractSerializableBean implements ModelToBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_TIME = "time";

    public static final String PROPERTY_VALUE_IS_ADJUSTING = "valueIsAdjusting";

    /**
     * Optional bean where to push back dates.
     */
    protected Object bean;

    /**
     * Optional bean property where to push back the time date.
     */
    protected String propertyTime;

    /**
     * Time
     */
    protected Date time = new Date();

    /**
     * State to be able to custom the model. will be pass to {@code false} by the {@code DateTimeEditorHandler#init(DateTimeEditor)}.
     */
    protected final boolean fillState = true;

    /**
     * To stop propagate events when we are doing some modifications on the model.
     */
    protected boolean valueIsAdjusting;

    protected final Calendar calendar = new GregorianCalendar();

    public String getPropertyTime() {
        return propertyTime;
    }

    public void setPropertyTime(String propertyTime) {
        Preconditions.checkState(fillState, "cant change *propertyTimeDate* property once the fillState is off.");
        this.propertyTime = propertyTime;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Preconditions.checkState(fillState, "cant change *bean* property once the fillState is off.");
        this.bean = bean;
    }

    public boolean isValueIsAdjusting() {
        return valueIsAdjusting;
    }

    public void setValueIsAdjusting(boolean valueIsAdjusting) {
        boolean oldValue = isValueIsAdjusting();
        this.valueIsAdjusting = valueIsAdjusting;
        fireValueIsAdjusting(oldValue);
    }

    public void setHour(Integer hour) {
        if (!isValueIsAdjusting()) {
            Integer minute = time == null ? null : getMinute(time);
            setTime(hour, minute);
        }
    }

    public void setMinute(Integer minute) {
        if (!isValueIsAdjusting()) {
            Integer hour = time == null ? null : getHour(time);
            setTime(hour, minute);
        }
    }

    public void setTimeInMinutes(Integer time) {
        if (!isValueIsAdjusting()) {
            if (time == null) {
                setTime(null, null);
            } else {
                int hour = time / 60;
                int minute = time % 60;
                setTime(hour, minute);
            }
        }
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        Integer hour = null;
        Integer minute = null;
        if (time != null) {
            hour = getHour(time);
            minute = getMinute(time);
        }
        setTime(hour, minute);
    }

    protected Integer getHour(Date date) {

        Integer hour = null;
        if (date != null) {
            calendar.setTime(date);
            hour = calendar.get(Calendar.HOUR_OF_DAY);
        }
        return hour;

    }

    protected Integer getMinute(Date date) {

        Integer hour = null;
        if (date != null) {
            calendar.setTime(date);
            hour = calendar.get(Calendar.MINUTE);
        }
        return hour;

    }

    protected void setTime(Integer hour, Integer minute) {
        Date oldTime = getTime();
        setValueIsAdjusting(true);
        try {
            if (hour == null || minute == null) {
                this.time = null;
            } else {
                calendar.setTime(new Date(0));
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                this.time = calendar.getTime();
            }
        } finally {
            setValueIsAdjusting(false);
            fireTime(oldTime);
        }
    }

    protected void fireTime(Date oldTime) {
        firePropertyChange(PROPERTY_TIME, oldTime, getTime());
    }

    protected void fireValueIsAdjusting(boolean oldValue) {
        firePropertyChange(PROPERTY_VALUE_IS_ADJUSTING, oldValue, isValueIsAdjusting());
    }

    protected Predicate<TimeEditorModel> canUpdateBeanValuePredicate() {
        return input -> !isValueIsAdjusting();
    }
}
