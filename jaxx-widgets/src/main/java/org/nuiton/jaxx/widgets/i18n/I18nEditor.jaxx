<!--
  #%L
  JAXX :: Widgets
  %%
  Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->


<JPanel implements='PropertyChangeListener, ActionListener' id='content' layout='{new BorderLayout()}'>

  <import>

    org.nuiton.jaxx.runtime.swing.SwingUtil

    java.awt.BorderLayout
    java.awt.event.ItemEvent
    java.awt.event.ActionEvent
    java.awt.event.ActionListener
    javax.swing.border.TitledBorder

    java.beans.PropertyChangeListener
    java.beans.PropertyChangeEvent
    java.util.Locale

    org.nuiton.jaxx.runtime.swing.renderer.LocaleListCellRenderer

    static io.ultreia.java4all.i18n.I18n.n
  </import>

  <String id='selectedToolTipText' javaBean='null'/>
  <String id='notSelectedToolTipText' javaBean='null'/>

  <Border id='popupBorder'
          javaBean='new TitledBorder(t("i18neditor.popup.title"))'/>

  <Boolean id='showText' javaBean='Boolean.TRUE'/>
  <Boolean id='showIcon' javaBean='Boolean.TRUE'/>
  <Boolean id='showPopupText' javaBean='Boolean.TRUE'/>
  <Boolean id='showPopupIcon' javaBean='Boolean.TRUE'/>
  <Boolean id='popupVisible' javaBean='Boolean.FALSE'/>

  <java.util.List id='locales' javaBean='null' genericType='Locale'/>

  <Locale id='selectedLocale' javaBean='Locale.getDefault()'/>

  <LocaleListCellRenderer id='renderer'
                          showIcon='{isShowIcon()}'
                          showText='{isShowText()}'
                          javaBean='new LocaleListCellRenderer(showIcon , showText)'/>

  <!-- popup to change sorted property-->
  <JPopupMenu id='popup'
              border='{getPopupBorder()}'
              onPopupMenuWillBecomeInvisible='button.setSelected(false)'
              onPopupMenuCanceled='button.setSelected(false)'>
    <JLabel id='popupLabel' enabled='false' text='i18neditor.empty.locales'/>
  </JPopupMenu>

  <JToggleButton
          id='button'
          text='{SwingUtil.getStringValue(renderer.getText(getSelectedLocale()))}'
          toolTipText='{getTip(getSelectedLocale())}'
          icon='{renderer.getIcon(getSelectedLocale())}'
          constraints='BorderLayout.CENTER'
          selected='{popup.isVisible()}'
          focusable='true'
          focusPainted='false'
          onItemStateChanged='if (event.getStateChange()  == ItemEvent.SELECTED) { setPopupVisible(true); } else { popupVisible = false; }'/>

  <ButtonGroup id='indexes'
               onStateChanged='log.info(indexes.getSelectedValue())'/>

  <script><![CDATA[
public static final String DEFAULT_SELECTED_TOOLTIP = n("i18neditor.selected");
public static final String DEFAULT_NOT_SELECTED_TOOLTIP = n("i18neditor.unselected");

public static final String LOCALES_PROPERTY = "locales";
public static final String SELECTED_LOCALE_PROPERTY = "selectedLocale";
public static final String SHOW_ICON_PROPERTY = "showIcon";
public static final String SHOW_TEXT_PROPERTY = "showText";
public static final String SHOW_POPUP_ICON_PROPERTY = "showPopupIcon";
public static final String SHOW_POPUP_TEXT_PROPERTY = "showPopupText";
public static final String POPUP_BORDER_PROPERTY = "popupBorder";
public static final String POPUP_VISIBLE_PROPERTY = "popupVisible";

@Override
public void propertyChange(PropertyChangeEvent evt) {
    handler.propertyChange(evt);
}

@Override
public void actionPerformed(ActionEvent event) {
    handler.actionPerformed(event);
}

public void loadI18nBundles() {
    handler.loadI18nBundles();
}

protected void rebuildPopup() {
    handler.rebuildPopup();
}

protected String getTip(Locale l) {
    return handler.getTip(l);
}

protected String getSelectedTip(Locale l) {
    return handler.getSelectedTip(l);
}

protected String getNotSelectedTip(Locale l) {
    return handler.getNotSelectedTip(l);
}
]]>
  </script>
</JPanel>
