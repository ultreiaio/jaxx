package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.runtime.util.DateUtil;
import org.nuiton.jaxx.widgets.ModelToBean;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created on 9/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateEditorModel extends AbstractSerializableBean implements ModelToBean {

    public static final String PROPERTY_DATE = "date";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_VALUE_IS_ADJUSTING = "valueIsAdjusting";
    private static final long serialVersionUID = 1L;
    /**
     * State to be able to custom the model. will be pass to {@code false} by the {@code DateTimeEditorHandler#init(DateTimeEditor)}.
     */
    protected final boolean fillState = true;
    protected final Calendar calendar = new GregorianCalendar();
    /**
     * Optional bean where to push back dates.
     */
    protected Object bean;
    /**
     * Optional bean property where to push back the day date.
     */
    protected String property;
    /**
     * Is date editable ?
     */
    protected boolean editable = true;
    /**
     * Full date (date + time)
     */
    protected Date date;
    /**
     * To stop propagate events when we are doing some modifications on the model.
     */
    protected boolean valueIsAdjusting;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        Preconditions.checkState(fillState, "cant change *property* property once the fillState is off.");
        this.property = property;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Preconditions.checkState(fillState, "cant change *bean* property once the fillState is off.");
        this.bean = bean;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    public boolean isValueIsAdjusting() {
        return valueIsAdjusting;
    }

    public void setValueIsAdjusting(boolean valueIsAdjusting) {
        boolean oldValue = isValueIsAdjusting();
        this.valueIsAdjusting = valueIsAdjusting;
        fireValueIsAdjusting(oldValue);
    }

    void reset() {

        Date oldDate = date;

        setValueIsAdjusting(true);
        try {
            this.date = isEditable() ? null : getDate();
            fireDate(oldDate);
        } finally {
            setValueIsAdjusting(false);
        }
        // need to fires again (outside of adjusting mode) to send to model bean null value (see canUpdateBeanValuePredicate method)
        fireDate(oldDate);
    }

    public Date getDate() {
        return date == null ? null : DateUtil.getDay(date);
    }

    public void setDate(Date date) {
        Date oldValue = getDate();
        if (Objects.equals(date, oldValue)) {
            return;
        }
        this.date = date == null ? null : DateUtil.getDay(date);

        fireDate(oldValue);
    }


    protected void fireDate(Date oldDate) {
        firePropertyChange(PROPERTY_DATE, oldDate, getDate());
    }

    protected void fireValueIsAdjusting(boolean oldValue) {
        firePropertyChange(PROPERTY_VALUE_IS_ADJUSTING, oldValue, isValueIsAdjusting());
    }

    protected Predicate<DateEditorModel> canUpdateBeanValuePredicate() {
        return input -> !isValueIsAdjusting();
    }
}
