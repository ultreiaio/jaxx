package org.nuiton.jaxx.widgets.temperature.session;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.session.State;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureFormat;

/**
 * All states to persist on a {@link TemperatureEditor}.
 * <p>
 * Created at 30/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.26
 */
public class TemperatureEditorState implements State {

    protected TemperatureFormat format;

    public TemperatureEditorState() {
    }

    public TemperatureEditorState(TemperatureFormat format) {
        this.format = format;
    }

    public TemperatureFormat getFormat() {
        return format;
    }

    public void setFormat(TemperatureFormat format) {
        this.format = format;
    }

    protected TemperatureEditor checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof TemperatureEditor)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (TemperatureEditor) o;
    }

    @Override
    public State getState(Object o) {
        TemperatureEditor ui = checkComponent(o);
        return new TemperatureEditorState(ui.getModel().getFormat());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof TemperatureEditorState)) {
            throw new IllegalArgumentException("invalid state");
        }
        TemperatureEditor ui = checkComponent(o);
        TemperatureEditorState typedState = (TemperatureEditorState) state;
        ui.getModel().setFormat(typedState.getFormat());
    }
}
