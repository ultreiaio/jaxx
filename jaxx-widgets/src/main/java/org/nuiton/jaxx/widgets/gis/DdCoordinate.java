package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.function.BiConsumer;
import java.util.regex.Pattern;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DdCoordinate extends AbstractSerializableBean implements Coordinate {

    public static final String COORDINATE_STRING_PATTERN = "%s%s.%s";
    public static final Pattern COORDINATE_PATTERN = Pattern.compile("(.*)\\.(.*)");
    public static final String PROPERTY_SIGN = "sign";
    public static final String PROPERTY_DEGREE = "degree";
    public static final String PROPERTY_DECIMAL = "decimal";
    private static final long serialVersionUID = 1L;
    protected boolean sign;

    protected Integer degree;

    protected Integer decimal;

    public static DdCoordinate empty() {
        return new DdCoordinate();
    }

    /**
     * Methode statique de fabrique de position a partir d'un autre {@link DdCoordinate}.
     * <p>
     * Note : Si la valeur vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static DdCoordinate valueOf(DdCoordinate decimal) {
        DdCoordinate r = empty();
        if (decimal != null) {
            r.setSign(decimal.isSign());
            r.setDegree(decimal.getDegree());
            r.setDecimal(decimal.getDecimal());
        }
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * decimal.
     * <p>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static DdCoordinate valueOf(Float decimal) {
        DdCoordinate r = new DdCoordinate();
        r.fromDecimal(decimal);
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * degre décimale.
     *
     * @param sign le signe
     * @param d    la valeur des degres
     * @param dc   la valeur des décimales de minutes
     * @return une nouvelle instance de position convertie
     */
    public static DdCoordinate valueOf(boolean sign, Integer d, Integer dc) {
        DdCoordinate r = new DdCoordinate();
        r.setSign(sign);
        r.setDegree(d);
        r.setDecimal(dc);
        return r;
    }

    @Override
    public CoordinateFormat format() {
        return CoordinateFormat.dd;
    }

    /**
     * @return {@code true} si aucune composante n'est renseignée, {@code false} autrement.
     */
    @Override
    public boolean isNull() {
        return degree == null && decimal == null;
    }

    /**
     * Mets a jour les composants de la position a partir d'une valeur decimal.
     * <p>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimalValue la valeur decimale a convertir (qui peut etre nulle).
     */
    @Override
    public void fromDecimal(Float decimalValue) {
        Integer d = null;
        Integer dc = null;
        boolean si = false;
        if (decimalValue != null) {
            si = decimalValue < 0;

            float absDecimal = Math.abs(decimalValue);

            d = (int) (Math.round(absDecimal + 0.5) - 1);
            dc = Math.round(1000 * (absDecimal - d));
        }

        degree = d;
        decimal = dc;
        sign = si;

        if (decimal != null) {
            removeTrailingZero();
        }
    }

    @Override
    public Float toDecimal() {
        if (isNull()) {
            return null;
        }
        int d = getNotNullDegree();
        int dc = getNotNullDecimal();
        Float result = (float) d;
        result += dc / 1000f;
        if (sign) {
            result *= -1;
        }
        result = CoordinateHelper.roundToFourDecimals(result);
        return result;
    }

    @Override
    public void addTrailingZero() {

        if (degree == null) {
            degree = 0;
        }
        if (decimal == null) {
            decimal = 0;
        }
    }

    @Override
    public void removeTrailingZero() {
        if (degree != null && degree == 0) {
            degree = null;
        }
        if (decimal != null && decimal == 0) {
            decimal = null;
        }
    }

    @Override
    public void validateLatitude(BiConsumer<String, Object> messageConsumer) {
        Integer degree = getDegree();
        if (degree != null) {
            if (degree > 90) {
                messageConsumer.accept(I18n.n("jaxx.validation.coordinate.degree.latitude.outOfBound"), degree);
            }
        }
    }

    @Override
    public void validateLongitude(BiConsumer<String, Object> messageConsumer) {
        Integer degree = getDegree();
        if (degree != null) {
            if (degree > 180) {
                messageConsumer.accept(I18n.n("jaxx.validation.coordinate.degree.longitude.outOfBound"), degree);
            }
        }
    }

    public boolean isSign() {
        return sign;
    }

    public void setSign(boolean sign) {
        Object oldValue = isSign();
        this.sign = sign;
        firePropertyChange(PROPERTY_SIGN, oldValue, sign);
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        Object oldValue = getDegree();
        this.degree = degree;
        firePropertyChange(PROPERTY_DEGREE, oldValue, degree);
    }

    public Integer getDecimal() {
        return decimal;
    }

    public void setDecimal(Integer decimal) {
        Object oldValue = getDecimal();
        this.decimal = decimal;
        firePropertyChange(PROPERTY_DECIMAL, oldValue, decimal);
    }

    public boolean isDegreeNull() {
        return degree == null || degree == 0;
    }

    public boolean isDecimalNull() {
        return decimal == null || decimal == 0;
    }

    public Integer getSignedDegree() {
        Integer result = null;
        if (!isDegreeNull()) {
            result = degree;
            if (isSign()) {
                result *= -1;
            }
        }
        return result;
    }

    public int getNotNullDegree() {
        return isDegreeNull() ? 0 : degree;
    }

    public int getNotNullDecimal() {
        return isDecimalNull() ? 0 : decimal;
    }

    public boolean isLatitudeDegreeValid() {
        return isDegreeValid(false);
    }

    public boolean isLongitudeDegreeValid() {
        return isDegreeValid(true);
    }

    public boolean isDecimalValid() {
        return isDecimalNull() || (0 <= decimal && decimal < 1000);
    }

    @Override
    public String toString() {
        return "DdCoordinateComponent{" +
                "sign=" + sign +
                ", degree=" + degree +
                ", decimal=" + decimal +
                '}';
    }

    public void reset() {
        decimal = null;
        degree = null;
    }

    protected boolean isDegreeValid(boolean longitude) {
        boolean result = true;
        if (!isDegreeNull()) {
            int bound = longitude ? 180 : 90;
            if (bound == degree) {

                // can not have decimal
                result = isDecimalNull();
            } else {
                result = 0 <= degree && degree < bound;
            }
        }
        return result;
    }

}
