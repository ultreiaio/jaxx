<!--
  #%L
  JAXX :: Widgets
  %%
  Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<JPanel layout='{new BorderLayout()}'
        onMouseExited='setPopupVisible(false);popup.setVisible(false);'
        implements="org.nuiton.jaxx.runtime.bean.BeanScopeAware">

  <import>
    java.awt.Color
    java.awt.BorderLayout
    java.awt.GridLayout
    java.awt.Dimension

    javax.swing.BorderFactory

  </import>

  <NumberEditorModel id="model" initializer="getContextValue(NumberEditorModel.class)"/>

  <!-- autoPopup property -->
  <Boolean id='autoPopup' javaBean='false'/>

  <!-- showPopupButton property -->
  <Boolean id='showPopupButton' javaBean='false'/>

  <!-- show reset property -->
  <Boolean id='showReset' javaBean='false'/>

  <!-- internal state -->
  <Boolean id='popupVisible' javaBean='false'/>

  <script><![CDATA[
@Override
public Object getBean() { return model.getBean(); }

@Override
public void setBean(Object bean) { model.setBean(bean); }

// Config delegate methods
public void setProperty(String property) { model.getConfig().setProperty(property); }
public void setUseSign(boolean useSign) { model.getConfig().setUseSign(useSign); }
public void setNumberType(Class<?> numberType) { model.getConfig().setNumberType(numberType); }
public void setSelectAllTextOnError(boolean selectAllTextOnError) { model.getConfig().setSelectAllTextOnError(selectAllTextOnError); }

// Model delegate methods
public void setNumberValue(Number numberValue) { model.setNumberValue(numberValue); }
public void setNumberPattern(String numberPattern) { model.setNumberPattern(numberPattern); }

public void init() { handler.init(this); }

public void reset() { handler.reset(); }

void showPopup() {
   if ( popupVisible || autoPopup ) {
       if (!popupVisible) {
           setPopupVisible(true);
       } else if (!getPopup().isVisible()) {
           handler.setPopupVisible(true);
       }
   }
}

@Override
public void setToolTipText(String toolTipText) {
    super.setToolTipText(toolTipText);
    textField.setToolTipText(toolTipText);
}

]]>
  </script>

  <!-- popup digital number editor -->
  <JPopupMenu id='popup'
              onPopupMenuWillBecomeVisible='showPopUpButton.setSelected(true)'
              onPopupMenuWillBecomeInvisible='showPopUpButton.setSelected(false)'
              onPopupMenuCanceled='showPopUpButton.setSelected(false)'>
    <JPanel id='popupPanel' layout='{new GridLayout(4,4)}'>

      <JButton id='number7Button' styleClass='digit' onActionPerformed="handler.addChar('7')"/>
      <JButton id='number8Button' styleClass='digit' onActionPerformed="handler.addChar('8')"/>
      <JButton id='number9Button' styleClass='digit' onActionPerformed="handler.addChar('9')"/>
      <JButton id='clearAllButton' styleClass='clear' onActionPerformed='handler.reset()'/>

      <JButton id='number4Button' styleClass='digit' onActionPerformed="handler.addChar('4')"/>
      <JButton id='number5Button' styleClass='digit' onActionPerformed="handler.addChar('5')"/>
      <JButton id='number6Button' styleClass='digit' onActionPerformed="handler.addChar('6')"/>
      <JButton id='clearOneButton' styleClass='clear' onActionPerformed="handler.removeChar()"/>

      <JButton id='number1Button' styleClass='digit' onActionPerformed="handler.addChar('1')"/>
      <JButton id='number2Button' styleClass='digit' onActionPerformed="handler.addChar('2')"/>
      <JButton id='number3Button' styleClass='digit' onActionPerformed="handler.addChar('3')"/>

      <JButton enabled="false"/>

      <JButton id='number0' styleClass='digit' onActionPerformed="handler.addChar('0')"/>
      <JButton id='toggleSignButton' styleClass='operator' onActionPerformed='handler.toggleSign()'/>
      <JButton id='dotButton' styleClass='operator' onActionPerformed="handler.addChar('.')"/>
      <JButton id='validateButton' onActionPerformed="handler.validate()"/>
    </JPanel>
  </JPopupMenu>

  <JToolBar id='leftToolbar' constraints='BorderLayout.WEST' styleClass="enabled">
    <JButton id='reset'/>
  </JToolBar>

  <JTextField id='textField' constraints='BorderLayout.CENTER' styleClass="enabled"
              onKeyReleased='handler.setTextValue(event, textField.getText())'/>

  <JToolBar id='rightToolbar' constraints='BorderLayout.EAST'>
    <JToggleButton id='showPopUpButton' styleClass="enabled" onActionPerformed='handler.setPopupVisible(!popup.isVisible())'/>
  </JToolBar>

</JPanel>
