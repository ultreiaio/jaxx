/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


#categoryLabelPanel {
    background:{Color.WHITE};
}

#descriptionPane {
    columnHeaderView:{new JLabel(t("config.description"), SwingUtil.getUIManagerActionIcon("info"), 10)};
}

#description {
    rows:3;
    editable:false;
    focusable:false;
    font-size:10;
    text:{t("config.no.option.selected")};
}

#tablePane {
    columnHeaderView:{table.getTableHeader()};
    verticalScrollBarPolicy:{ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS};
    horizontalScrollBarPolicy:{ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER};
}

#table {
    rowSelectionAllowed:false;
    autoCreateRowSorter:true;
    autoResizeMode:{JXTable.AUTO_RESIZE_ALL_COLUMNS};
    rowHeight:25;
    selectionModel:{selectionModel};
}

#reset{
    text:"config.action.reset";
    toolTipText:"config.action.reset.tip";
    actionIcon:"config-reset";
    enabled:{getCategoryModel().isModified()};
}

#save{
    text:"config.action.save";
    toolTipText:"config.action.save.tip";
    actionIcon:"config-save";
    enabled:{getCategoryModel().isModified() && getCategoryModel().isValid()};
}

#tableToolBar {
    floatable: false;
    opaque: false;
    borderPainted: false;
}

#tablePopup {
    label: "config.table.actions";
}

#copyCellValue{
    text:"config.action.copyCellValue";
    toolTipText:"config.action.copyCellValue.tip";
    actionIcon:"config-copy";
}

#resetOptionValue{
    text:"config.action.resetOptionValue";
    toolTipText:"config.action.resetOptionValue.tip";
    actionIcon:"config-reset-option";
}
