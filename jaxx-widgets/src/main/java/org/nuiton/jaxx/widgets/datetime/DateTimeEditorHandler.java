package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.KeyStroke;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created on 9/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateTimeEditorHandler extends BeanUIHandlerSupport<DateTimeEditor> implements UIHandler<DateTimeEditor> {

    private static final Logger log = LogManager.getLogger(DateTimeEditorHandler.class);
    protected final Calendar calendarMinute = new GregorianCalendar();
    protected final Calendar calendarHour = new GregorianCalendar();

    @Override
    public void beforeInit(DateTimeEditor ui) {
        super.beforeInit(ui);
        DateTimeEditorModel model = new DateTimeEditorModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(DateTimeEditor ui) {
        ui.getMinuteEditor().setEditor(new JSpinner.DateEditor(ui.getMinuteEditor(), "mm"));
        ui.getHourEditor().setEditor(new JSpinner.DateEditor(ui.getHourEditor(), "HH"));
        ExtendedBasicDatePickerUI extendedUi = new ExtendedBasicDatePickerUI();
        extendedUi.setShowPopupButton(true);
        JXDatePicker dateEditor = ui.getDayDateEditor();
        dateEditor.setUI(extendedUi);
        InputMap pickerInputMap = dateEditor.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pickerInputMap.put(KeyStroke.getKeyStroke("pressed DOWN"), "TOGGLE_POPUP");
        dateEditor.getEditor().setFocusLostBehavior(JFormattedTextField.COMMIT);
    }

    @Override
    protected String getProperty(DateTimeEditor ui) {
        return ui.getModel().getPropertyDate();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init DateTimeEditor", ui.getName()));
        if (property == null || property.isEmpty()) {
            ui.setPropertyDate(ui.getName());
        }
        DateTimeEditorModel model = ui.getModel();
        model.addPropertyChangeListener(DateTimeEditorModel.PROPERTY_DATE, evt -> {
            if (model.isDateEditable()) {
                model.setTimeEditable(evt.getNewValue() != null);
            }
        });
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        DateTimeEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, e -> {
            Date oldValue = model.getDate();
            Date newValue = (Date) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                ui.setDate(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        DateTimeEditorModel model = ui.getModel();
        Predicate<DateTimeEditorModel> predicate = model.canUpdateBeanValuePredicate();
        model.addPropertyChangeListener(DateTimeEditorModel.PROPERTY_DATE, evt -> {
            if (predicate.test(model)) {
                Object newValue = evt.getNewValue();
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
        String propertyDayDate = model.getPropertyDayDate();
        if (propertyDayDate != null) {
            model.addPropertyChangeListener(DateTimeEditorModel.PROPERTY_DAY_DATE, evt -> {
                if (predicate.test(model)) {
                    bean.set(propertyDayDate, evt.getNewValue());
                }
            });
        }
        String propertyTimeDate = model.getPropertyTimeDate();
        if (propertyTimeDate != null) {
            model.addPropertyChangeListener(DateTimeEditorModel.PROPERTY_TIME_DATE, evt -> {
                if (predicate.test(model)) {
                    bean.set(propertyTimeDate, evt.getNewValue());
                }
            });
        }
    }

    public Date getMinuteModelValue(Date incomingDate) {
        if (incomingDate == null) {
            if (ui.getModel().getDate() == null) {
                incomingDate = new Date(0);
            } else {
                incomingDate = new Date();
            }
        }
        calendarMinute.setTime(incomingDate);
        calendarMinute.set(Calendar.HOUR_OF_DAY, 0);
        incomingDate = calendarMinute.getTime();
        return incomingDate;
    }

    public Date getHourModelValue(Date incomingDate) {
        boolean dateIsNull = incomingDate == null && ui.getModel().getDate() == null;
        if (incomingDate == null) {
            if (dateIsNull) {
                incomingDate = new Date(0);
            } else {
                incomingDate = new Date();
            }
        }
        calendarHour.setTime(incomingDate);
        calendarHour.set(Calendar.MINUTE, 0);
        if (dateIsNull) {
            calendarHour.set(Calendar.HOUR_OF_DAY, 0);
        }
        incomingDate = calendarHour.getTime();
        return incomingDate;
    }

    public void setHours(Date hourDate) {
        DateTimeEditorModel model = ui.getModel();
        Date oldTimeDate = model.getTimeDate();
        boolean oldTimeNull = oldTimeDate == null;

        calendarHour.setTime(hourDate);
        int newHour = calendarHour.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarHour.get(Calendar.MINUTE);

        int oldHour = oldTimeNull ? 0 : model.getHour(oldTimeDate);
        int oldMinute = oldTimeNull ? 0 : model.getMinute(oldTimeDate);

        if (oldHour == newHour && oldMinute == newMinute) {

            // do nothing, same data
            log.debug(String.format("Do not update time model , stay on same time = %d:%d", oldHour, oldMinute));
            return;
        }

        // by default stay on same hour

        // by default, use the new minute data

        log.debug(String.format("hh:mm (old from dateModel)   = %d:%d", oldHour, oldMinute));
        log.debug(String.format("hh:mm (new from hourModel) = %d:%d", newHour, newMinute));
        if (!oldTimeNull) {
            Integer dayAdjust = null;

            if (newHour == 0 && oldHour == 23) {
                // add a day
                dayAdjust = 1;
            } else if (newHour == 23 && oldHour == 0) {
                // decrease a day
                dayAdjust = -1;
            }

            if (dayAdjust != null) {
                Date oldDayDate = model.getDayDate();
                calendarHour.setTime(oldDayDate);
                calendarHour.add(Calendar.DAY_OF_YEAR, dayAdjust);

                log.debug(String.format("Update day to %d", calendarHour.get(Calendar.DAY_OF_YEAR)));
                Date newDayDate = calendarHour.getTime();
                model.setDayDate(newDayDate);
            }
        }
        // change time
        model.setTimeInMinutes(newHour * 60 + oldMinute);
    }

    public void setMinutes(Date minuteDate) {
        DateTimeEditorModel model = ui.getModel();
        Date oldTimeDate = model.getTimeDate();
        boolean oldTimeNull = oldTimeDate == null;

        calendarMinute.setTime(minuteDate);
        int newHour = calendarMinute.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarMinute.get(Calendar.MINUTE);

        int oldHour = oldTimeNull ? 0 : model.getHour(oldTimeDate);
        int oldMinute = oldTimeNull ? 0 : model.getMinute(oldTimeDate);

        if (oldHour == newHour && oldMinute == newMinute) {

            // do nothing, same data
            log.debug(String.format("Do not update time model , stay on same time = %d:%d", oldHour, oldMinute));
            return;
        }

        // by default stay on same hour
        int hour = oldHour == -1 ? 0 : oldHour;

        // by default, use the new minute data
        log.debug(String.format("hh:mm (old from dateModel)   = %d:%d", oldHour, oldMinute));
        log.debug(String.format("hh:mm (new from minuteModel) = %d:%d", newHour, newMinute));

        if (!oldTimeNull) {
            Integer dayAdjust = null;
            if (newMinute == 0) {
                // minute pass to zero (check if a new hour is required)
                if (newHour == 1) {
                    if (oldHour == 23) {
                        // on next day
                        dayAdjust = 1;
                    }
                    hour = (oldHour + 1) % 24;
                }
            } else if (newMinute == 59) {
                // minute pass to 59 (check if a new hour is required)
                if (newHour == 23) {
                    if (oldHour == 0) {
                        dayAdjust = -1;
                    }
                    // decrease hour
                    hour = (oldHour - 1) % 24;
                }
            }

            if (dayAdjust != null) {
                Date oldDayDate = model.getDayDate();
                calendarHour.setTime(oldDayDate);
                calendarHour.add(Calendar.DAY_OF_YEAR, dayAdjust);
                log.debug(String.format("Update day to %d", calendarHour.get(Calendar.DAY_OF_YEAR)));
                Date newDayDate = calendarHour.getTime();
                model.setDayDate(newDayDate);
            }
        }
        // date has changed
        log.debug(String.format("Update time model to hh:mm = %d:%d", hour, newMinute));
        model.setTimeInMinutes(hour * 60 + newMinute);
    }

}
