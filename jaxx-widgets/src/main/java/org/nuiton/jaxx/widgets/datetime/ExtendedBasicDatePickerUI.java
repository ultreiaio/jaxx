/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.datetime;

import io.ultreia.java4all.i18n.I18n;
import org.jdesktop.swingx.plaf.basic.BasicDatePickerUI;
import org.nuiton.jaxx.runtime.swing.KeyStrokes;

import javax.swing.JButton;
import javax.swing.KeyStroke;
import java.beans.PropertyChangeSupport;

/**
 * @author Sylvain Lletellier
 */
public class ExtendedBasicDatePickerUI extends BasicDatePickerUI {

    public static final String PROPERTY_SHOW_POPUP_BUTTON = "showPopupButton";
    protected final PropertyChangeSupport p;
    protected JButton popupButton;
    protected boolean showPopupButton;

    public ExtendedBasicDatePickerUI() {
        p = new PropertyChangeSupport(this);
        p.addPropertyChangeListener(PROPERTY_SHOW_POPUP_BUTTON, evt -> {

            // dont do this on init
            if (datePicker != null) {
                installComponents();
                installListeners();
            }
        });
    }

    public boolean isShowPopupButton() {
        return showPopupButton;
    }

    public void setShowPopupButton(boolean showPopupButton) {
        boolean oldValue = isShowPopupButton();
        this.showPopupButton = showPopupButton;
        p.firePropertyChange("showPopupButton", oldValue, showPopupButton);
    }

    @Override
    protected JButton createPopupButton() {
        if (isShowPopupButton()) {
            popupButton = super.createPopupButton();
            String tip = KeyStrokes.suffixTextWithKeyStroke(I18n.t("DateTimeEditor.action.show.popup.tip"), KeyStroke.getKeyStroke("pressed DOWN"));
            popupButton.setToolTipText(tip);
            return popupButton;
        }
        return null;
    }

}
