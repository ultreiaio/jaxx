/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.status;

import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JProgressBar;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Dimension;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6.0
 */
public class StatusMessagePanelHandler implements UIHandler<StatusMessagePanel> {

    protected StatusMessagePanel ui;
    protected Color statusForeground;
    protected String statusReferenceContent;
    protected Timer timer;

    public void init() {
        if (ui.isShowBusy()) {
            Dimension dim = new Dimension(30, 15);
            JProgressBar busyWidget = ui.getBusyWidget();
            busyWidget.setMaximumSize(dim);
            busyWidget.setMinimumSize(dim);
            ui.addPropertyChangeListener("busy", evt -> {
                Boolean newValue = (Boolean) evt.getNewValue();
                busyWidget.setEnabled(newValue);
                busyWidget.setIndeterminate(newValue);
            });
        }
    }

    public void clearStatus() {
        stopStatusFader(ui);
        ui.getStatusLabel().setText(StatusMessagePanel.EMPTY_STATUS);
    }

    public void setStatus(String status) {
        if (status != null) {
            stopStatusFader(ui);
            ui.getStatusLabel().setText(status);
        }
        if (!ui.isBusy() && ui.isShowing()) {
            startStatusFader(ui);
        }
    }

    protected void fadeStatus(StatusMessagePanel ui) {
        for (int i = 0; i < 8; i++) {
            if (!statusReferenceContent.equals(ui.getStatusLabel().getText())) {
                return;
            }
            Color currentForeground = ui.getStatusLabel().getForeground();
            Color newColor = new Color(currentForeground.getRed(), currentForeground.getGreen(), currentForeground.getBlue(), currentForeground.getAlpha() - 25);
            ui.getStatusLabel().setForeground(newColor);
            ui.getStatusLabel().repaint();
        }
    }

    protected void startStatusFader(StatusMessagePanel ui) {
        if (!ui.isVisible() && !ui.isShowing()) {
            return;
        }
        statusReferenceContent = ui.getStatusLabel().getText();
        int millisecondsPerMinute = 5000;
        if (timer == null) {
            timer = new Timer(millisecondsPerMinute, ui);
            timer.setRepeats(false);
            timer.setInitialDelay((int) ((long) millisecondsPerMinute - System.currentTimeMillis() % (long) millisecondsPerMinute) + 500);
        }
        timer.start();
    }

    protected void stopStatusFader(StatusMessagePanel ui) {
        if (timer != null) {
            timer.stop();
            ui.getStatusLabel().setForeground(statusForeground);
        }
    }

    @Override
    public void beforeInit(StatusMessagePanel ui) {
        this.ui = ui;
    }
}
