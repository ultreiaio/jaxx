package org.nuiton.jaxx.widgets.gis;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.function.BiConsumer;

/**
 * Created at 21/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1.3
 */
public interface Coordinate {

    CoordinateFormat format();

    boolean isNull();

    Float toDecimal();

    void fromDecimal(Float decimalValue);

    void addTrailingZero();

    void removeTrailingZero();

    void validateLatitude(BiConsumer<String, Object> messageConsumer);

    void validateLongitude(BiConsumer<String, Object> messageConsumer);
}
