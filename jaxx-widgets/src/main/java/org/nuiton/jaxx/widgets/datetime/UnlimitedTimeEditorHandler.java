package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class UnlimitedTimeEditorHandler extends BeanUIHandlerSupport<UnlimitedTimeEditor> implements UIHandler<UnlimitedTimeEditor> {

    private static final Logger log = LogManager.getLogger(UnlimitedTimeEditorHandler.class);

    @Override
    protected String getProperty(UnlimitedTimeEditor ui) {
        return ui.getModel().getPropertyTime();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init UnlimitedTimeEditor", ui.getName()));
        if (property == null || property.isEmpty()) {
            ui.setPropertyTime(ui.getName());
        }
        ui.setEnabled(true);
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        UnlimitedTimeEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, event -> {
            Integer oldValue = model.getTime();
            Integer newValue = (Integer) event.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                ui.setTime(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        UnlimitedTimeEditorModel model = ui.getModel();
        Predicate<UnlimitedTimeEditorModel> predicate = model.canUpdateBeanValuePredicate();
        model.addPropertyChangeListener(UnlimitedTimeEditorModel.PROPERTY_TIME, event -> {
            if (predicate.test(model)) {
                Integer newValue = (Integer) event.getNewValue();
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }

    public void reset() {
        ui.getHourEditor().setValue(0);
        ui.getMinuteEditor().setValue(0);
        ui.getModel().setTime(null);
    }

}
