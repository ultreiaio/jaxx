package org.nuiton.jaxx.widgets;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 * Created on 10/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public abstract class JTextComponentDocumentListener<U extends JAXXObject, T extends JTextComponent> implements DocumentListener {

    private final U ui;
    private final T editor;

    public JTextComponentDocumentListener(U ui, T editor) {
        this.ui = ui;
        this.editor = editor;
    }

    public final void install() {
        editor.getDocument().addDocumentListener(this);
    }

    protected abstract void doUpdate(U ui, T editor);

    @Override
    public final void insertUpdate(DocumentEvent e) {
        doUpdate(ui, editor);
    }

    @Override
    public final void removeUpdate(DocumentEvent e) {
        doUpdate(ui, editor);
    }

    @Override
    public final void changedUpdate(DocumentEvent e) {
        doUpdate(ui, editor);
    }
}
