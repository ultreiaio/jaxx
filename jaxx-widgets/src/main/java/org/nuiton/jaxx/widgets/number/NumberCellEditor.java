package org.nuiton.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;

/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberCellEditor<E extends Number> extends AbstractCellEditor implements TableCellEditor, AncestorListener {

    private static final long serialVersionUID = 1L;

    protected final NumberEditor numberEditor;

    public static TableCellEditor newFloatColumnEditor() {
        return newFloatColumnEditor(false);
    }

    public static TableCellEditor newFloatColumnEditor(boolean useSign) {

        NumberCellEditor<Float> editor = new NumberCellEditor<>(Float.class, useSign);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern(SwingUtil.DECIMAL4_PATTERN);
        return editor;

    }

    public static TableCellEditor newIntegerColumnEditor() {
        return newIntegerColumnEditor(false);
    }

    public static TableCellEditor newIntegerColumnEditor(boolean useSign) {
        NumberCellEditor<Integer> editor = new NumberCellEditor<>(Integer.class, useSign);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern(SwingUtil.INT_3_DIGITS_PATTERN);
        return editor;

    }

    public static TableCellEditor newInteger4ColumnEditor(boolean useSign) {
        NumberCellEditor<Integer> editor = new NumberCellEditor<>(Integer.class, useSign);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern(SwingUtil.INT_4_DIGITS_PATTERN);
        return editor;

    }

    public NumberCellEditor(Class<E> type, boolean useSign) {
        numberEditor = new NumberEditor();
        numberEditor.getTextField().setHorizontalAlignment(SwingConstants.RIGHT);
        numberEditor.getTextField().setBorder(null);
        numberEditor.getTextField().addAncestorListener(this);
        numberEditor.setNumberType(type);
        numberEditor.setUseSign(useSign);
        numberEditor.init();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {

        Number number = (Number) value;
        numberEditor.setNumberValue(number);
        return numberEditor;
    }

    public NumberEditor getNumberEditor() {
        return numberEditor;
    }

    @SuppressWarnings("unchecked")
    @Override
    public E getCellEditorValue() {
        NumberEditorModel model = numberEditor.getModel();
        return (E) model.getNumberValue();
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        //FIXME See what does it means?
        SwingUtilities.invokeLater(() -> {
            numberEditor.getTextField().requestFocusInWindow();
            numberEditor.getTextField().selectAll();
        });
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
    }

    @Override
    public boolean stopCellEditing() {
        boolean result = super.stopCellEditing();
        // Reset previous data to avoid keeping it on other cell edition
        if (result) {
            numberEditor.setNumberValue(null);
//            // Use empty string, otherwise there is a NPE in NumberEditorHandler
//            numberEditor.setModelText("");
            // force binding, I do not know why the textfield text is not emptied
            // if we do not force it
            numberEditor.applyDataBinding(NumberEditor.BINDING_TEXT_FIELD_TEXT);
        }
        return result;
    }

}
