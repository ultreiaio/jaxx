<!--
  #%L
  JAXX :: Widgets
  %%
  Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<JPanel layout='{new BorderLayout()}'>

  <import>
    java.awt.Color
    javax.swing.DefaultListSelectionModel
    javax.swing.ScrollPaneConstants
    org.nuiton.jaxx.widgets.config.model.CategoryModel
    org.nuiton.jaxx.widgets.config.model.ConfigTableModel
    org.nuiton.jaxx.widgets.config.model.ConfigUIModel
    org.jdesktop.swingx.JXTable
  </import>

  <!-- le modele de l'ui -->
  <ConfigUIModel id='model'
                 initializer='getContextValue(ConfigUIModel.class)'/>

  <CategoryModel id='categoryModel'
                 javaBean='getContextValue(CategoryModel.class)'/>

  <ConfigTableModel id='tableModel' constructorParams='categoryModel'
                    onTableChanged='handler.updateDescriptionText()'/>

  <ListSelectionModel id='selectionModel'
                      javaBean='new DefaultListSelectionModel()'
                      onValueChanged='if (!event.getValueIsAdjusting()) {handler.updateDescriptionText(); }'/>

  <!-- categorie label -->
  <JPanel id="categoryLabelPanel" constraints='BorderLayout.NORTH'>
    <JLabel id='categoryLabel'/>
  </JPanel>

  <!-- table of options -->
  <JScrollPane id='tablePane' constraints='BorderLayout.CENTER'>
    <JXTable id="table" constructorParams='tableModel' onMousePressed='handler.openTablePopupMenu(event, tablePopup)'/>
  </JScrollPane>

  <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.SOUTH'>

    <!-- description of selected option in table -->
    <JScrollPane id="descriptionPane" constraints='BorderLayout.CENTER'>
      <JTextArea id='description'/>
    </JScrollPane>

    <!-- actions of the category -->
    <JPanel layout='{new GridLayout(1,0)}'
            constraints='BorderLayout.SOUTH'>
      <JButton id='reset' onActionPerformed='model.reset()'/>
      <JButton id='save' onActionPerformed='model.saveModified()'/>
    </JPanel>
  </JPanel>

  <JPopupMenu id='tablePopup'>
    <JMenuItem id='copyCellValue' onActionPerformed='handler.copyCellValue()'/>
    <JMenuItem id='resetOptionValue' onActionPerformed='handler.resetOptionValue()'/>
  </JPopupMenu>
</JPanel>
