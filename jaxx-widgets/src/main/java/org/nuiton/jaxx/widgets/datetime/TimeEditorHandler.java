package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JSpinner;
import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class TimeEditorHandler extends BeanUIHandlerSupport<TimeEditor> implements UIHandler<TimeEditor> {

    private static final Logger log = LogManager.getLogger(TimeEditorHandler.class);
    protected final Calendar calendarMinute = new GregorianCalendar();
    protected final Calendar calendarHour = new GregorianCalendar();

    @Override
    public void beforeInit(TimeEditor ui) {
        super.beforeInit(ui);
        TimeEditorModel model = new TimeEditorModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(TimeEditor ui) {
        ui.getMinuteEditor().setEditor(new JSpinner.DateEditor(ui.getMinuteEditor(), "mm"));
        ui.getHourEditor().setEditor(new JSpinner.DateEditor(ui.getHourEditor(), "HH"));
        // Listen on model time property to custom the foreground text of hour and minute editors
        // See https://gitlab.com/ultreiaio/jaxx/-/issues/844
        ui.getModel().addPropertyChangeListener(TimeEditorModel.PROPERTY_TIME, evt -> onTimeChanged(evt.getNewValue()));
        onTimeChanged(null);
    }

    private void onTimeChanged(Object newValue) {
        Color foreground;
        if (newValue == null) {
            foreground = Color.GRAY;
        } else {
            foreground = Color.BLACK;
        }
        changeJSpinnerForegroundColor(ui.getHourEditor(), foreground);
        changeJSpinnerForegroundColor(ui.getMinuteEditor(), foreground);
    }

    static void changeJSpinnerForegroundColor(JSpinner component, Color foreground) {
        JSpinner.DateEditor editor = (JSpinner.DateEditor) component.getEditor();
        editor.getTextField().setForeground(foreground);
        editor.getTextField().setSelectedTextColor(Color.WHITE);
    }

    @Override
    protected String getProperty(TimeEditor ui) {
        return ui.getModel().getPropertyTime();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init TimeEditor", ui.getName()));
        if (property == null || property.isEmpty()) {
            ui.setPropertyTime(ui.getName());
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        TimeEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, e -> {
            Date oldValue = model.getTime();
            Date newValue = (Date) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                ui.setTime(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        TimeEditorModel model = ui.getModel();
        Predicate<TimeEditorModel> predicate = model.canUpdateBeanValuePredicate();
        model.addPropertyChangeListener(TimeEditorModel.PROPERTY_TIME, event -> {
            if (predicate.test(model)) {
                Date newValue = (Date) event.getNewValue();
                log.debug(String.format("%s - [%s] set new value to bean: %s", ui.getName(), property, newValue));
                bean.set(property, newValue);
            }
        });
    }

    public Date getMinuteModelValue(Date incomingDate) {
        if (incomingDate == null) {
            calendarMinute.setTimeInMillis(0);
        } else {
            calendarMinute.setTime(incomingDate);
        }
        calendarMinute.set(Calendar.HOUR_OF_DAY, 0);
        incomingDate = calendarMinute.getTime();
        return incomingDate;
    }

    public Date getHourModelValue(Date incomingDate) {
        if (incomingDate == null) {
            calendarMinute.setTimeInMillis(0);
            // if not set we will have one hour ?
            calendarHour.set(Calendar.HOUR_OF_DAY, 0);
        } else {
            calendarHour.setTime(incomingDate);
        }
        calendarHour.set(Calendar.MINUTE, 0);
        incomingDate = calendarHour.getTime();
        return incomingDate;
    }

    public void reset() {
        ui.getHourEditor().setValue(new Date(0));
        ui.getMinuteEditor().setValue(new Date(0));
        ui.getModel().setTime(null);
    }

    public void setHours(Date hourDate) {
        TimeEditorModel model = ui.getModel();
        Date oldTime = model.getTime();
        boolean oldTimeNull = oldTime == null;

        calendarHour.setTime(hourDate);
        int newHour = calendarHour.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarHour.get(Calendar.MINUTE);

        int oldHour = oldTimeNull ? 0 : model.getHour(oldTime);
        int oldMinute = oldTimeNull ? 0 : model.getMinute(oldTime);

        if (oldHour == newHour && oldMinute == newMinute) {
            // do nothing, same data
            log.debug(String.format("Do not update time model , stay on same time = %d:%d", oldHour, oldMinute));
            return;
        }

        // by default stay on same hour
        // by default, use the new minute data
        log.debug(String.format("hh:mm (old from dateModel)   = %d:%d", oldHour, oldMinute));
        log.debug(String.format("hh:mm (new from hourModel) = %d:%d", newHour, newMinute));
        // change time
        model.setTimeInMinutes(newHour * 60 + oldMinute);
    }

    public void setMinutes(Date minuteDate) {
        TimeEditorModel model = ui.getModel();
        Date oldTime = model.getTime();
        boolean oldTimeNull = oldTime == null;

        calendarMinute.setTime(minuteDate);
        int newHour = calendarMinute.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarMinute.get(Calendar.MINUTE);

        int oldHour = oldTimeNull ? 0 : model.getHour(oldTime);
        int oldMinute = oldTimeNull ? 0 : model.getMinute(oldTime);

        if (oldHour == newHour && oldMinute == newMinute) {
            // do nothing, same data
            log.debug(String.format("Do not update time model , stay on same time = %d:%d", oldHour, oldMinute));
            return;
        }

        // by default stay on same hour
        int hour = oldHour == -1 ? 0 : oldHour;

        // by default, use the new minute data
        log.debug(String.format("hh:mm (old from dateModel)   = %d:%d", oldHour, oldMinute));
        log.debug(String.format("hh:mm (new from minuteModel) = %d:%d", newHour, newMinute));

        if (newMinute == 0) {
            // minute pass to zero (check if a new hour is required)
            if (newHour == 1) {
                hour = (oldHour + 1) % 24;
            }
        } else if (newMinute == 59) {
            // minute pass to 59 (check if a new hour is required)
            if (newHour == 23) {
                // decrease hour
                hour = (oldHour - 1) % 24;
            }
        }
        // time has changed
        log.debug(String.format("Update time model to hh:mm = %d:%d", hour, newMinute));
        model.setTimeInMinutes(hour * 60 + newMinute);
    }

}
