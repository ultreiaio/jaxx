package org.nuiton.jaxx.widgets.temperature;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = "label", pattern = "enum.@CLASS_NAME@.@NAME@"),
        @TranslateEnumeration(name = "description", pattern = "enum.@CLASS_NAME@.@NAME@.description")})
public enum TemperatureFormat {
    C {
        @Override
        public Float convert(Float value, TemperatureFormat targetFormat) {
            return targetFormat.equals(this) ? value : TemperatureHelper.convertToFahrenheit(value);
        }
    },
    F {
        @Override
        public Float convert(Float value, TemperatureFormat targetFormat) {
            return targetFormat.equals(this) ? value : TemperatureHelper.convertToCelsius(value);
        }
    };

    /**
     * Convert the given {@code value} from this format to the given {@code targetFormat}
     * @param value value to convert
     * @param targetFormat target format
     * @return converted value
     */
    public abstract Float convert(Float value, TemperatureFormat targetFormat);

    public String getLabel() {
        return TemperatureFormatI18n.getLabel(this);
    }

    public String getDescription() {
        return TemperatureFormatI18n.getDescription(this);
    }

}
