package org.nuiton.jaxx.widgets.text;

/*-
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.JTextComponentDocumentListener;
import org.nuiton.jaxx.widgets.text.actions.NormalTextEditorReset;

import javax.swing.JPasswordField;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 01/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class PasswordEditorHandler extends BeanUIHandlerSupport<PasswordEditor> implements UIHandler<PasswordEditor> {
    private static final Logger log = LogManager.getLogger(PasswordEditorHandler.class);

    @Override
    public void afterInit(PasswordEditor ui) {
        ui.addPropertyChangeListener(PasswordEditor.PROPERTY_RESET_TIP, evt -> {
            String newValue = (String) evt.getNewValue();
            NormalTextEditorReset action = (NormalTextEditorReset) ui.getReset().getAction();
            action.setTooltipText(newValue);
            action.rebuildTexts(true);
        });
        new JTextComponentDocumentListener<>(ui, ui.getTextEditor()) {

            @Override
            protected void doUpdate(PasswordEditor ui, JPasswordField editor) {
                ui.firePropertyChange("password", new char[0], ui.getPassword());
            }
        }.install();
    }

    @Override
    protected String getProperty(PasswordEditor ui) {
        return ui.getProperty();
    }

    @Override
    protected void prepareInit(String property) {
        if (property == null || property.isEmpty()) {
            ui.setProperty(ui.getName());
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        bean.addPropertyChangeListener(property, e -> {
            char[] oldValue = ui.getPassword();
            char[] newValue = (char[]) e.getNewValue();
            if (!Arrays.equals(oldValue, newValue)) {
                log.debug(String.format("%s - [%s] get new value from bean: %s", ui.getName(), property, Arrays.toString(newValue)));
                ui.setPassword(newValue);
            }
        });
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        ui.addPropertyChangeListener("password", e -> {
            char[] oldValue = (char[]) e.getOldValue();
            char[] newValue = (char[]) e.getNewValue();
            if (!Arrays.equals(oldValue, newValue) && !Objects.equals(bean.get(property), newValue)) {
                log.debug(String.format("%s - [%s] get new value to bean: %s", ui.getName(), property, Arrays.toString(newValue)));
                bean.set(property, newValue);
            }
        });
    }

}
