package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.jaxx.widgets.BeanUIHandlerSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;

import java.util.Objects;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinatesEditorHandler extends BeanUIHandlerSupport<CoordinatesEditor> implements UIHandler<CoordinatesEditor> {
    private static final Logger log = LogManager.getLogger(CoordinatesEditorHandler.class);

    @Override
    public void beforeInit(CoordinatesEditor ui) {
        super.beforeInit(ui);
        CoordinatesEditorModel model = new CoordinatesEditorModel();
        model.setFormat(CoordinateFormat.dd);
        model.setQuadrant(0);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(CoordinatesEditor ui) {
        ui.getLatitudeDd().init(false);
        ui.getLatitudeDms().init(false);
        ui.getLatitudeDmd().init(false);
        ui.getLongitudeDd().init(true);
        ui.getLongitudeDms().init(true);
        ui.getLongitudeDmd().init(true);
        SwingUtil.changeFontSize(ui.getQuadrant1(), 10f);
        SwingUtil.changeFontSize(ui.getQuadrant2(), 10f);
        SwingUtil.changeFontSize(ui.getQuadrant3(), 10f);
        SwingUtil.changeFontSize(ui.getQuadrant4(), 10f);
        SwingUtil.changeFontSize(ui.getDdFormat(), 10f);
        SwingUtil.changeFontSize(ui.getDmdFormat(), 10f);
        SwingUtil.changeFontSize(ui.getDmsFormat(), 10f);
    }

    @Override
    protected String getProperty(CoordinatesEditor ui) {
        return ui.getModel().getPropertyLatitude();
    }

    @Override
    protected void prepareInit(String property) {
        log.debug(String.format("%s - init CoordinatesEditor", ui.getName()));
        if (property == null) {
            String name = ui.getName();
            if (name.equals("coordinate")) {
                ui.setPropertyLatitude("latitude");
                ui.setPropertyLongitude("longitude");
                ui.setPropertyQuadrant("quadrant");
            } else {
                ui.setPropertyLatitude(name + "Latitude");
                ui.setPropertyLongitude(name + "Longitude");
                ui.setPropertyQuadrant(name + "Quadrant");
            }
        }
    }

    @Override
    protected void prepareBindFromBean(String property, JavaBean bean) {
        CoordinatesEditorModel model = ui.getModel();
        bean.addPropertyChangeListener(property, e -> {
            Float oldValue = model.getLatitude();
            Float newValue = (Float) e.getNewValue();
            if (!Objects.equals(oldValue, newValue)) {
                ui.setLatitude(newValue);
            }
        });

        String propertyLongitude = model.getPropertyLongitude();
        if (propertyLongitude != null) {
            bean.addPropertyChangeListener(propertyLongitude, e -> {
                Float oldValue = model.getLongitude();
                Float newValue = (Float) e.getNewValue();
                if (!Objects.equals(oldValue, newValue)) {
                    ui.setLongitude(newValue);
                }
            });
        }
        String propertyQuadrant = model.getPropertyQuadrant();
        if (propertyQuadrant != null) {
            bean.addPropertyChangeListener(propertyQuadrant, e -> {
                Integer oldValue = model.getQuadrant();
                Integer newValue = (Integer) e.getNewValue();
                if (!Objects.equals(oldValue, newValue)) {
                    ui.setQuadrant(newValue);
                }
            });
        }
    }

    @Override
    protected void prepareBindToBean(String property, JavaBean bean) {
        CoordinatesEditorModel model = ui.getModel();
        model.addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_LATITUDE,
                                        evt -> bean.set(property, evt.getNewValue()));

        String propertyLongitude = model.getPropertyLongitude();
        if (propertyLongitude != null) {
            model.addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_LONGITUDE,
                                            evt -> bean.set(propertyLongitude, evt.getNewValue()));
        }
        String propertyQuadrant = model.getPropertyQuadrant();
        if (propertyQuadrant != null) {
            model.addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_QUADRANT,
                                            evt -> bean.set(propertyQuadrant, evt.getNewValue()));
        }
    }

    public void resetEditor() {
        resetModel();
        resetQuadrant();
    }

    public void resetModel() {
        ui.getLongitudeDd().reset();
        ui.getLongitudeDms().reset();
        ui.getLongitudeDmd().reset();
        ui.getLatitudeDd().reset();
        ui.getLatitudeDms().reset();
        ui.getLatitudeDmd().reset();
    }

    public void resetQuadrant() {

        JAXXButtonGroup quadrantBG = ui.getQuadrantBG();

        // I don't know why but to reset the buttons, we need to remove them from the button group, 
        quadrantBG.remove(ui.getQuadrant1());
        quadrantBG.remove(ui.getQuadrant2());
        quadrantBG.remove(ui.getQuadrant3());
        quadrantBG.remove(ui.getQuadrant4());

        ui.getQuadrant1().setSelected(false);
        ui.getQuadrant2().setSelected(false);
        ui.getQuadrant3().setSelected(false);
        ui.getQuadrant4().setSelected(false);

        quadrantBG.add(ui.getQuadrant1());
        quadrantBG.add(ui.getQuadrant2());
        quadrantBG.add(ui.getQuadrant3());
        quadrantBG.add(ui.getQuadrant4());

        // reset in model at the end
        ui.setQuadrant(null);

        // reset button group after all
        ui.getQuadrantBG().setSelectedValue(null);

    }

    public boolean isQuadrantSelected(Integer value, int requiredValue) {
        return value != null && value == requiredValue;
    }

//    void onFocusGained() {
//        switch (ui.getModel().getFormat()) {
//
//            case dd:
//                ui.getLatitudeDd().requestFocusInWindow();
//                break;
//            case dms:
//                ui.getLatitudeDms().requestFocusInWindow();
//                break;
//            case dmd:
//                ui.getLatitudeDmd().requestFocusInWindow();
//                break;
//        }
//    }

}
