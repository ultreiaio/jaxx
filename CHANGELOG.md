# jaxx changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2025-01-07 16:12.

## Version [3.1.6](https://gitlab.com/ultreiaio/jaxx/-/milestones/244)

**Closed at 2025-01-07.**


### Issues
  * [[Evolution 856]](https://gitlab.com/ultreiaio/jaxx/-/issues/856) **Improve NauticalLength editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 857]](https://gitlab.com/ultreiaio/jaxx/-/issues/857) **Improve Temperature editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.5](https://gitlab.com/ultreiaio/jaxx/-/milestones/243)

**Closed at 2024-09-18.**


### Issues
  * [[Evolution 855]](https://gitlab.com/ultreiaio/jaxx/-/issues/855) **Remove xwork technology in validation at last!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.4](https://gitlab.com/ultreiaio/jaxx/-/milestones/240)

**Closed at 2024-09-09.**


### Issues
  * [[Evolution 854]](https://gitlab.com/ultreiaio/jaxx/-/issues/854) **Cherry-picks all improvments from milestone 3.0.26** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.3](https://gitlab.com/ultreiaio/jaxx/-/milestones/239)

**Closed at 2024-02-22.**


### Issues
No issue.

## Version [3.1.2](https://gitlab.com/ultreiaio/jaxx/-/milestones/238)

**Closed at 2024-02-13.**


### Issues
  * [[Evolution 848]](https://gitlab.com/ultreiaio/jaxx/-/issues/848) **Introduce I18n editor (coming from I18n project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1.1](https://gitlab.com/ultreiaio/jaxx/-/milestones/237)

**Closed at 2024-02-13.**


### Issues
No issue.

## Version [3.1.0](https://gitlab.com/ultreiaio/jaxx/-/milestones/236)
Move to validation 2.0.0

**Closed at 2024-02-05.**


### Issues
  * [[Evolution 845]](https://gitlab.com/ultreiaio/jaxx/-/issues/845) **Move to validation 2.0.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 846]](https://gitlab.com/ultreiaio/jaxx/-/issues/846) **Remove the SwingListValidator API and SwingValidatorMessageListModel API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 847]](https://gitlab.com/ultreiaio/jaxx/-/issues/847) **Add a new module jaxx-widgets-validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.26](https://gitlab.com/ultreiaio/jaxx/-/milestones/241)

**Closed at 2024-08-31.**


### Issues
  * [[Evolution 849]](https://gitlab.com/ultreiaio/jaxx/-/issues/849) **Add more SwingSession states for some widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 851]](https://gitlab.com/ultreiaio/jaxx/-/issues/851) **Add option in FilterableComboBox to be able to search on on the first decorator property** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 850]](https://gitlab.com/ultreiaio/jaxx/-/issues/850) **In ListHeader, the sort property is not well displayed in popup when using setIndex method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 852]](https://gitlab.com/ultreiaio/jaxx/-/issues/852) **Do not alter any model when changing CoordinatesEditor format** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.24](https://gitlab.com/ultreiaio/jaxx/-/milestones/234)

**Closed at 2023-07-26.**


### Issues
  * [[Evolution 838]](https://gitlab.com/ultreiaio/jaxx/-/issues/838) **Add more static factory constructor to NumberCellEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 839]](https://gitlab.com/ultreiaio/jaxx/-/issues/839) **Two many fires in DoubleList when using method addToSelect** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.23](https://gitlab.com/ultreiaio/jaxx/-/milestones/233)

**Closed at 2023-07-06.**


### Issues
  * [[Anomalie 837]](https://gitlab.com/ultreiaio/jaxx/-/issues/837) **In coordinate editor, setEnabled has no action on all his sub sub buttons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.22](https://gitlab.com/ultreiaio/jaxx/-/milestones/232)

**Closed at 2023-02-22.**


### Issues
No issue.

## Version [3.0.21](https://gitlab.com/ultreiaio/jaxx/-/milestones/231)

**Closed at 2022-12-11.**


### Issues
  * [[Evolution 836]](https://gitlab.com/ultreiaio/jaxx/-/issues/836) **Improve BooleanEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.20](https://gitlab.com/ultreiaio/jaxx/-/milestones/230)

**Closed at 2022-10-20.**


### Issues
  * [[Evolution 835]](https://gitlab.com/ultreiaio/jaxx/-/issues/835) **Open JTabbedPaneValidator API for complex cases we can&#39;t manage by builder** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.19](https://gitlab.com/ultreiaio/jaxx/-/milestones/229)

**Closed at 2022-10-18.**


### Issues
  * [[Anomalie 834]](https://gitlab.com/ultreiaio/jaxx/-/issues/834) **Fix sort issues in ListHeader and DoubleList** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.18](https://gitlab.com/ultreiaio/jaxx/-/milestones/228)

**Closed at 2022-10-17.**


### Issues
  * [[Evolution 833]](https://gitlab.com/ultreiaio/jaxx/-/issues/833) **Improve DateTimeEditor layout** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.16](https://gitlab.com/ultreiaio/jaxx/-/milestones/226)

**Closed at 2022-06-25.**


### Issues
  * [[Evolution 831]](https://gitlab.com/ultreiaio/jaxx/-/issues/831) **Review i18n on NauticalLength editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.15](https://gitlab.com/ultreiaio/jaxx/-/milestones/225)

**Closed at 2022-06-24.**


### Issues
  * [[Evolution 830]](https://gitlab.com/ultreiaio/jaxx/-/issues/830) **Introduce Nautical Length editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.14](https://gitlab.com/ultreiaio/jaxx/-/milestones/224)

**Closed at 2022-05-27.**


### Issues
  * [[Evolution 828]](https://gitlab.com/ultreiaio/jaxx/-/issues/828) **Improve JTabbedPaneValidator to use extra tabs validators to display correct icons on tabs (property names are no sufficient...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 829]](https://gitlab.com/ultreiaio/jaxx/-/issues/829) **Review usage of decorator in list widgets (can&#39;t change any state in incoming data)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.13](https://gitlab.com/ultreiaio/jaxx/-/milestones/223)

**Closed at 2022-05-18.**


### Issues
  * [[Evolution 827]](https://gitlab.com/ultreiaio/jaxx/-/issues/827) **DoubleList decorator popup label is wrong** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.12](https://gitlab.com/ultreiaio/jaxx/-/milestones/222)

**Closed at 2022-03-09.**


### Issues
  * [[Evolution 826]](https://gitlab.com/ultreiaio/jaxx/-/issues/826) **Little clean in status widget (should be rethink poor code)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 825]](https://gitlab.com/ultreiaio/jaxx/-/issues/825) **Time and DateTime editor does not modify model if time is null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.11](https://gitlab.com/ultreiaio/jaxx/-/milestones/221)

**Closed at 2022-02-03.**


### Issues
  * [[Evolution 821]](https://gitlab.com/ultreiaio/jaxx/-/issues/821) **Improve Menu selectFirst and selectLast methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 822]](https://gitlab.com/ultreiaio/jaxx/-/issues/822) **Improve generated actions (now use a constructor instead of nasty newInstance on class)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 823]](https://gitlab.com/ultreiaio/jaxx/-/issues/823) **Clean jaxx-widgets code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 824]](https://gitlab.com/ultreiaio/jaxx/-/issues/824) **Register actions after all components are on (avoid side-effect on menu actions)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.10](https://gitlab.com/ultreiaio/jaxx/-/milestones/220)

**Closed at 2022-02-01.**


### Issues
  * [[Evolution 820]](https://gitlab.com/ultreiaio/jaxx/-/issues/820) **Improve JAXXObject initialize method with a real method out of generated code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.9](https://gitlab.com/ultreiaio/jaxx/-/milestones/219)

**Closed at 2022-01-30.**


### Issues
  * [[Evolution 819]](https://gitlab.com/ultreiaio/jaxx/-/issues/819) **Improve UIHandler (add more hooks from generated jaxx java files)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.8](https://gitlab.com/ultreiaio/jaxx/-/milestones/218)

**Closed at 2021-12-10.**


### Issues
No issue.

## Version [3.0.8](https://gitlab.com/ultreiaio/jaxx/-/milestones/218)

**Closed at 2021-12-10.**


### Issues
No issue.

## Version [3.0.7](https://gitlab.com/ultreiaio/jaxx/-/milestones/217)

**Closed at 2021-11-14.**


### Issues
  * [[Evolution 817]](https://gitlab.com/ultreiaio/jaxx/-/issues/817) **Be able to override Table component (it will add new children at the end using the row count with attribute forceOverride)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 818]](https://gitlab.com/ultreiaio/jaxx/-/issues/818) **Exclude some swing properties from validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.7](https://gitlab.com/ultreiaio/jaxx/-/milestones/217)

**Closed at 2021-11-14.**


### Issues
  * [[Evolution 817]](https://gitlab.com/ultreiaio/jaxx/-/issues/817) **Be able to override Table component (it will add new children at the end using the row count with attribute forceOverride)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 818]](https://gitlab.com/ultreiaio/jaxx/-/issues/818) **Exclude some swing properties from validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.6](https://gitlab.com/ultreiaio/jaxx/-/milestones/216)

**Closed at 2021-11-13.**


### Issues
  * [[Evolution 815]](https://gitlab.com/ultreiaio/jaxx/-/issues/815) **Replace nuiton-utils and nuiton-version by java-util** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 816]](https://gitlab.com/ultreiaio/jaxx/-/issues/816) **Remove old select widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.6](https://gitlab.com/ultreiaio/jaxx/-/milestones/216)

**Closed at 2021-11-13.**


### Issues
  * [[Evolution 815]](https://gitlab.com/ultreiaio/jaxx/-/issues/815) **Replace nuiton-utils and nuiton-version by java-util** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 816]](https://gitlab.com/ultreiaio/jaxx/-/issues/816) **Remove old select widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.5](https://gitlab.com/ultreiaio/jaxx/-/milestones/215)

**Closed at 2021-11-10.**


### Issues
  * [[Evolution 813]](https://gitlab.com/ultreiaio/jaxx/-/issues/813) **Be able to have enumeration with natural code in his parameters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 814]](https://gitlab.com/ultreiaio/jaxx/-/issues/814) **Clean ClassDescriptorResolver API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.5](https://gitlab.com/ultreiaio/jaxx/-/milestones/215)

**Closed at 2021-11-10.**


### Issues
  * [[Evolution 813]](https://gitlab.com/ultreiaio/jaxx/-/issues/813) **Be able to have enumeration with natural code in his parameters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 814]](https://gitlab.com/ultreiaio/jaxx/-/issues/814) **Clean ClassDescriptorResolver API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.4](https://gitlab.com/ultreiaio/jaxx/-/milestones/214)

**Closed at 2021-11-03.**


### Issues
  * [[Evolution 808]](https://gitlab.com/ultreiaio/jaxx/-/issues/808) **Remove java help from project** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 809]](https://gitlab.com/ultreiaio/jaxx/-/issues/809) **Group maven jaxx-runtime modules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 810]](https://gitlab.com/ultreiaio/jaxx/-/issues/810) **remove Unified API from jaxx-validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 811]](https://gitlab.com/ultreiaio/jaxx/-/issues/811) **Improve Validator runtime API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 812]](https://gitlab.com/ultreiaio/jaxx/-/issues/812) **Remove spi init API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.4](https://gitlab.com/ultreiaio/jaxx/-/milestones/214)

**Closed at 2021-11-03.**


### Issues
  * [[Evolution 808]](https://gitlab.com/ultreiaio/jaxx/-/issues/808) **Remove java help from project** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 809]](https://gitlab.com/ultreiaio/jaxx/-/issues/809) **Group maven jaxx-runtime modules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 810]](https://gitlab.com/ultreiaio/jaxx/-/issues/810) **remove Unified API from jaxx-validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 811]](https://gitlab.com/ultreiaio/jaxx/-/issues/811) **Improve Validator runtime API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 812]](https://gitlab.com/ultreiaio/jaxx/-/issues/812) **Remove spi init API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.3](https://gitlab.com/ultreiaio/jaxx/-/milestones/213)

**Closed at 2021-10-27.**


### Issues
  * [[Anomalie 807]](https://gitlab.com/ultreiaio/jaxx/-/issues/807) **Still some issues on new FilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.3](https://gitlab.com/ultreiaio/jaxx/-/milestones/213)

**Closed at 2021-10-27.**


### Issues
  * [[Anomalie 807]](https://gitlab.com/ultreiaio/jaxx/-/issues/807) **Still some issues on new FilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.2](https://gitlab.com/ultreiaio/jaxx/-/milestones/212)

**Closed at 2021-10-26.**


### Issues
  * [[Anomalie 806]](https://gitlab.com/ultreiaio/jaxx/-/issues/806) **Fix some little issues on new FilteralbeComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.2](https://gitlab.com/ultreiaio/jaxx/-/milestones/212)

**Closed at 2021-10-26.**


### Issues
  * [[Anomalie 806]](https://gitlab.com/ultreiaio/jaxx/-/issues/806) **Fix some little issues on new FilteralbeComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.1](https://gitlab.com/ultreiaio/jaxx/-/milestones/211)

**Closed at 2021-10-25.**


### Issues
  * [[Evolution 804]](https://gitlab.com/ultreiaio/jaxx/-/issues/804) **Introduce FilterableComboBox ng ng** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 805]](https://gitlab.com/ultreiaio/jaxx/-/issues/805) **fix NPE in DoubleList in selected is null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.1](https://gitlab.com/ultreiaio/jaxx/-/milestones/211)

**Closed at 2021-10-25.**


### Issues
  * [[Evolution 804]](https://gitlab.com/ultreiaio/jaxx/-/issues/804) **Introduce FilterableComboBox ng ng** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 805]](https://gitlab.com/ultreiaio/jaxx/-/issues/805) **fix NPE in DoubleList in selected is null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0](https://gitlab.com/ultreiaio/jaxx/-/milestones/210)

**Closed at 2021-10-22.**


### Issues
  * [[Evolution 474]](https://gitlab.com/ultreiaio/jaxx/-/issues/474) **Use JLayer instead of JXLayer (but requires to up jdk level to 1.7)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 675]](https://gitlab.com/ultreiaio/jaxx/-/issues/675) **Be able to use default method in interfaces (Java 8)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 676]](https://gitlab.com/ultreiaio/jaxx/-/issues/676) **Use default method on UIHandler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 677]](https://gitlab.com/ultreiaio/jaxx/-/issues/677) **Instantiate handler with ui in construct if possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 678]](https://gitlab.com/ultreiaio/jaxx/-/issues/678) **Constructor parameters are not ok in org.nuiton.jaxx.compiler.reflect.resolvers.ClassDescriptorResolverFromJavaFile** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 680]](https://gitlab.com/ultreiaio/jaxx/-/issues/680) **Change commonCss to URI and make possible to use css from classpath** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 683]](https://gitlab.com/ultreiaio/jaxx/-/issues/683) **Introduce TabInfoWithValidator to have more interaction betwwen tabs and validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 684]](https://gitlab.com/ultreiaio/jaxx/-/issues/684) **Improve convention i18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 685]](https://gitlab.com/ultreiaio/jaxx/-/issues/685) **Be able to apply multiple css class via styleClass attribute** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 687]](https://gitlab.com/ultreiaio/jaxx/-/issues/687) **Be able to resolve generic type from java file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 803]](https://gitlab.com/ultreiaio/jaxx/-/issues/803) **Make one unique modules with main editors widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 686]](https://gitlab.com/ultreiaio/jaxx/-/issues/686) **Be able to use specialized validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0](https://gitlab.com/ultreiaio/jaxx/-/milestones/210)

**Closed at 2021-10-22.**


### Issues
  * [[Evolution 474]](https://gitlab.com/ultreiaio/jaxx/-/issues/474) **Use JLayer instead of JXLayer (but requires to up jdk level to 1.7)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 675]](https://gitlab.com/ultreiaio/jaxx/-/issues/675) **Be able to use default method in interfaces (Java 8)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 676]](https://gitlab.com/ultreiaio/jaxx/-/issues/676) **Use default method on UIHandler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 677]](https://gitlab.com/ultreiaio/jaxx/-/issues/677) **Instantiate handler with ui in construct if possible** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 678]](https://gitlab.com/ultreiaio/jaxx/-/issues/678) **Constructor parameters are not ok in org.nuiton.jaxx.compiler.reflect.resolvers.ClassDescriptorResolverFromJavaFile** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 680]](https://gitlab.com/ultreiaio/jaxx/-/issues/680) **Change commonCss to URI and make possible to use css from classpath** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 683]](https://gitlab.com/ultreiaio/jaxx/-/issues/683) **Introduce TabInfoWithValidator to have more interaction betwwen tabs and validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 684]](https://gitlab.com/ultreiaio/jaxx/-/issues/684) **Improve convention i18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 685]](https://gitlab.com/ultreiaio/jaxx/-/issues/685) **Be able to apply multiple css class via styleClass attribute** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 687]](https://gitlab.com/ultreiaio/jaxx/-/issues/687) **Be able to resolve generic type from java file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 803]](https://gitlab.com/ultreiaio/jaxx/-/issues/803) **Make one unique modules with main editors widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 686]](https://gitlab.com/ultreiaio/jaxx/-/issues/686) **Be able to use specialized validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-12](https://gitlab.com/ultreiaio/jaxx/-/milestones/209)

**Closed at 2021-10-18.**


### Issues
  * [[Evolution 801]](https://gitlab.com/ultreiaio/jaxx/-/issues/801) **Add setFileHidingEnabled for all JFileChooser mode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 802]](https://gitlab.com/ultreiaio/jaxx/-/issues/802) **Fix decorator issues in ListHEader and DoubleList** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-12](https://gitlab.com/ultreiaio/jaxx/-/milestones/209)

**Closed at 2021-10-18.**


### Issues
  * [[Evolution 801]](https://gitlab.com/ultreiaio/jaxx/-/issues/801) **Add setFileHidingEnabled for all JFileChooser mode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 802]](https://gitlab.com/ultreiaio/jaxx/-/issues/802) **Fix decorator issues in ListHEader and DoubleList** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-11](https://gitlab.com/ultreiaio/jaxx/-/milestones/208)

**Closed at 2021-10-07.**


### Issues
  * [[Evolution 798]](https://gitlab.com/ultreiaio/jaxx/-/issues/798) **Improve DoubleList design** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 799]](https://gitlab.com/ultreiaio/jaxx/-/issues/799) **Improve GIS editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 800]](https://gitlab.com/ultreiaio/jaxx/-/issues/800) **Avoid to generate twice i18n invocation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-11](https://gitlab.com/ultreiaio/jaxx/-/milestones/208)

**Closed at 2021-10-07.**


### Issues
  * [[Evolution 798]](https://gitlab.com/ultreiaio/jaxx/-/issues/798) **Improve DoubleList design** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 799]](https://gitlab.com/ultreiaio/jaxx/-/issues/799) **Improve GIS editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 800]](https://gitlab.com/ultreiaio/jaxx/-/issues/800) **Avoid to generate twice i18n invocation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-10](https://gitlab.com/ultreiaio/jaxx/-/milestones/207)

**Closed at 2021-09-23.**


### Issues
  * [[Anomalie 797]](https://gitlab.com/ultreiaio/jaxx/-/issues/797) **Fix TimeEditor reset** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-10](https://gitlab.com/ultreiaio/jaxx/-/milestones/207)

**Closed at 2021-09-23.**


### Issues
  * [[Anomalie 797]](https://gitlab.com/ultreiaio/jaxx/-/issues/797) **Fix TimeEditor reset** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-9](https://gitlab.com/ultreiaio/jaxx/-/milestones/206)

**Closed at 2021-09-19.**


### Issues
No issue.

## Version [3.0.0-RC-9](https://gitlab.com/ultreiaio/jaxx/-/milestones/206)

**Closed at 2021-09-19.**


### Issues
No issue.

## Version [3.0.0-RC-8](https://gitlab.com/ultreiaio/jaxx/-/milestones/205)

**Closed at 2021-09-18.**


### Issues
  * [[Evolution 795]](https://gitlab.com/ultreiaio/jaxx/-/issues/795) **Introduce JaxxComboBoxCellEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 794]](https://gitlab.com/ultreiaio/jaxx/-/issues/794) **Fix JaxxComboboxEditor for cell** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-8](https://gitlab.com/ultreiaio/jaxx/-/milestones/205)

**Closed at 2021-09-18.**


### Issues
  * [[Evolution 795]](https://gitlab.com/ultreiaio/jaxx/-/issues/795) **Introduce JaxxComboBoxCellEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 794]](https://gitlab.com/ultreiaio/jaxx/-/issues/794) **Fix JaxxComboboxEditor for cell** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-7](https://gitlab.com/ultreiaio/jaxx/-/milestones/204)

**Closed at 2021-08-30.**


### Issues
No issue.

## Version [3.0.0-RC-7](https://gitlab.com/ultreiaio/jaxx/-/milestones/204)

**Closed at 2021-08-30.**


### Issues
No issue.

## Version [3.0.0-RC-6](https://gitlab.com/ultreiaio/jaxx/-/milestones/203)

**Closed at 2021-07-27.**


### Issues
No issue.

## Version [3.0.0-RC-6](https://gitlab.com/ultreiaio/jaxx/-/milestones/203)

**Closed at 2021-07-27.**


### Issues
No issue.

## Version [3.0.0-RC-5](https://gitlab.com/ultreiaio/jaxx/-/milestones/202)

**Closed at 2021-06-20.**


### Issues
  * [[Evolution 791]](https://gitlab.com/ultreiaio/jaxx/-/issues/791) **Add showReset on TimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-5](https://gitlab.com/ultreiaio/jaxx/-/milestones/202)

**Closed at 2021-06-20.**


### Issues
  * [[Evolution 791]](https://gitlab.com/ultreiaio/jaxx/-/issues/791) **Add showReset on TimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-4](https://gitlab.com/ultreiaio/jaxx/-/milestones/201)

**Closed at 2021-03-21.**


### Issues
  * [[Anomalie 790]](https://gitlab.com/ultreiaio/jaxx/-/issues/790) **Fix bad state restore (JTabbedPaneState.selectedIndex) if this tab is not enabled** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-4](https://gitlab.com/ultreiaio/jaxx/-/milestones/201)

**Closed at 2021-03-21.**


### Issues
  * [[Anomalie 790]](https://gitlab.com/ultreiaio/jaxx/-/issues/790) **Fix bad state restore (JTabbedPaneState.selectedIndex) if this tab is not enabled** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-3](https://gitlab.com/ultreiaio/jaxx/-/milestones/200)

**Closed at 2021-03-11.**


### Issues
  * [[Evolution 709]](https://gitlab.com/ultreiaio/jaxx/-/issues/709) **Migrate to Java &gt; 9** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 607]](https://gitlab.com/ultreiaio/jaxx/-/issues/607) **Fix javadoc for java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-3](https://gitlab.com/ultreiaio/jaxx/-/milestones/200)

**Closed at 2021-03-11.**


### Issues
  * [[Evolution 709]](https://gitlab.com/ultreiaio/jaxx/-/issues/709) **Migrate to Java &gt; 9** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 607]](https://gitlab.com/ultreiaio/jaxx/-/issues/607) **Fix javadoc for java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-2](https://gitlab.com/ultreiaio/jaxx/-/milestones/199)

**Closed at 2021-03-03.**


### Issues
  * [[Evolution 787]](https://gitlab.com/ultreiaio/jaxx/-/issues/787) **Introduce PasswordEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-2](https://gitlab.com/ultreiaio/jaxx/-/milestones/199)

**Closed at 2021-03-03.**


### Issues
  * [[Evolution 787]](https://gitlab.com/ultreiaio/jaxx/-/issues/787) **Introduce PasswordEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-1](https://gitlab.com/ultreiaio/jaxx/-/milestones/198)

**Closed at 2021-01-31.**


### Issues
  * [[Evolution 784]](https://gitlab.com/ultreiaio/jaxx/-/issues/784) **Move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 785]](https://gitlab.com/ultreiaio/jaxx/-/issues/785) **Replace nuiton-validator by java4all validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 786]](https://gitlab.com/ultreiaio/jaxx/-/issues/786) **Add new widgets validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0.0-RC-1](https://gitlab.com/ultreiaio/jaxx/-/milestones/198)

**Closed at 2021-01-31.**


### Issues
  * [[Evolution 784]](https://gitlab.com/ultreiaio/jaxx/-/issues/784) **Move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 785]](https://gitlab.com/ultreiaio/jaxx/-/issues/785) **Replace nuiton-validator by java4all validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 786]](https://gitlab.com/ultreiaio/jaxx/-/issues/786) **Add new widgets validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-92](https://gitlab.com/ultreiaio/jaxx/-/milestones/197)

**Closed at 2021-01-17.**


### Issues
  * [[Evolution 783]](https://gitlab.com/ultreiaio/jaxx/-/issues/783) **Introduce UnlimitedTimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-92](https://gitlab.com/ultreiaio/jaxx/-/milestones/197)

**Closed at 2021-01-17.**


### Issues
  * [[Evolution 783]](https://gitlab.com/ultreiaio/jaxx/-/issues/783) **Introduce UnlimitedTimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-91](https://gitlab.com/ultreiaio/jaxx/-/milestones/196)

**Closed at 2021-01-15.**


### Issues
  * [[Evolution 782]](https://gitlab.com/ultreiaio/jaxx/-/issues/782) **Introduce jaxx-widgets-list to replace jaxx-widgets-select** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-91](https://gitlab.com/ultreiaio/jaxx/-/milestones/196)

**Closed at 2021-01-15.**


### Issues
  * [[Evolution 782]](https://gitlab.com/ultreiaio/jaxx/-/issues/782) **Introduce jaxx-widgets-list to replace jaxx-widgets-select** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-90](https://gitlab.com/ultreiaio/jaxx/-/milestones/195)

**Closed at 2021-01-13.**


### Issues
  * [[Evolution 781]](https://gitlab.com/ultreiaio/jaxx/-/issues/781) **Fix focus craziness in JaxxComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-90](https://gitlab.com/ultreiaio/jaxx/-/milestones/195)

**Closed at 2021-01-13.**


### Issues
  * [[Evolution 781]](https://gitlab.com/ultreiaio/jaxx/-/issues/781) **Fix focus craziness in JaxxComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-89](https://gitlab.com/ultreiaio/jaxx/-/milestones/194)

**Closed at 2021-01-06.**


### Issues
  * [[Evolution 779]](https://gitlab.com/ultreiaio/jaxx/-/issues/779) **Try to remove reflection hack** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 780]](https://gitlab.com/ultreiaio/jaxx/-/issues/780) **At last find out how to block JaxxComboEditor annoying key event** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 778]](https://gitlab.com/ultreiaio/jaxx/-/issues/778) **Text editors does not send back events when text is null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-89](https://gitlab.com/ultreiaio/jaxx/-/milestones/194)

**Closed at 2021-01-06.**


### Issues
  * [[Evolution 779]](https://gitlab.com/ultreiaio/jaxx/-/issues/779) **Try to remove reflection hack** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 780]](https://gitlab.com/ultreiaio/jaxx/-/issues/780) **At last find out how to block JaxxComboEditor annoying key event** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 778]](https://gitlab.com/ultreiaio/jaxx/-/issues/778) **Text editors does not send back events when text is null** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-88](https://gitlab.com/ultreiaio/jaxx/-/milestones/193)

**Closed at 2021-01-05.**


### Issues
  * [[Anomalie 776]](https://gitlab.com/ultreiaio/jaxx/-/issues/776) **Number editor popup button is enabled, even if editor is not** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 777]](https://gitlab.com/ultreiaio/jaxx/-/issues/777) **Bad caret position in number editor if use bad characters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-88](https://gitlab.com/ultreiaio/jaxx/-/milestones/193)

**Closed at 2021-01-05.**


### Issues
  * [[Anomalie 776]](https://gitlab.com/ultreiaio/jaxx/-/issues/776) **Number editor popup button is enabled, even if editor is not** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 777]](https://gitlab.com/ultreiaio/jaxx/-/issues/777) **Bad caret position in number editor if use bad characters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-87](https://gitlab.com/ultreiaio/jaxx/-/milestones/192)

**Closed at 2020-12-24.**


### Issues
  * [[Anomalie 775]](https://gitlab.com/ultreiaio/jaxx/-/issues/775) **Auto-completion is no more working :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-87](https://gitlab.com/ultreiaio/jaxx/-/milestones/192)

**Closed at 2020-12-24.**


### Issues
  * [[Anomalie 775]](https://gitlab.com/ultreiaio/jaxx/-/issues/775) **Auto-completion is no more working :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-86](https://gitlab.com/ultreiaio/jaxx/-/milestones/191)

**Closed at 2020-12-22.**


### Issues
No issue.

## Version [3.0-alpha-86](https://gitlab.com/ultreiaio/jaxx/-/milestones/191)

**Closed at 2020-12-22.**


### Issues
No issue.

## Version [3.0-alpha-85](https://gitlab.com/ultreiaio/jaxx/-/milestones/190)

**Closed at 2020-12-17.**


### Issues
  * [[Anomalie 771]](https://gitlab.com/ultreiaio/jaxx/-/issues/771) **When getting subcomponents make sure to get them only once** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 772]](https://gitlab.com/ultreiaio/jaxx/-/issues/772) **Improve action api about update lables (was a bit disymetric)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-85](https://gitlab.com/ultreiaio/jaxx/-/milestones/190)

**Closed at 2020-12-17.**


### Issues
  * [[Anomalie 771]](https://gitlab.com/ultreiaio/jaxx/-/issues/771) **When getting subcomponents make sure to get them only once** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 772]](https://gitlab.com/ultreiaio/jaxx/-/issues/772) **Improve action api about update lables (was a bit disymetric)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-84](https://gitlab.com/ultreiaio/jaxx/-/milestones/189)

**Closed at 2020-12-14.**


### Issues
  * [[Evolution 768]](https://gitlab.com/ultreiaio/jaxx/-/issues/768) **Improve new Init API (can get exact components or subComponents now)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 769]](https://gitlab.com/ultreiaio/jaxx/-/issues/769) **Replace BeanDateEditor by DateEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 770]](https://gitlab.com/ultreiaio/jaxx/-/issues/770) **Improve Init API (subComponent were not well kept)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-84](https://gitlab.com/ultreiaio/jaxx/-/milestones/189)

**Closed at 2020-12-14.**


### Issues
  * [[Evolution 768]](https://gitlab.com/ultreiaio/jaxx/-/issues/768) **Improve new Init API (can get exact components or subComponents now)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 769]](https://gitlab.com/ultreiaio/jaxx/-/issues/769) **Replace BeanDateEditor by DateEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 770]](https://gitlab.com/ultreiaio/jaxx/-/issues/770) **Improve Init API (subComponent were not well kept)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-83](https://gitlab.com/ultreiaio/jaxx/-/milestones/188)

**Closed at 2020-12-11.**


### Issues
  * [[Evolution 767]](https://gitlab.com/ultreiaio/jaxx/-/issues/767) **Introduce a new way to init JAXX objects (and depreciates the previous one, never used totally)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-83](https://gitlab.com/ultreiaio/jaxx/-/milestones/188)

**Closed at 2020-12-11.**


### Issues
  * [[Evolution 767]](https://gitlab.com/ultreiaio/jaxx/-/issues/767) **Introduce a new way to init JAXX objects (and depreciates the previous one, never used totally)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-82](https://gitlab.com/ultreiaio/jaxx/-/milestones/187)

**Closed at 2020-11-23.**


### Issues
  * [[Evolution 766]](https://gitlab.com/ultreiaio/jaxx/-/issues/766) **Introduce MenuAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 765]](https://gitlab.com/ultreiaio/jaxx/-/issues/765) **In BlockingLayerUI be able to click and more on button registred by actionCommand** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-82](https://gitlab.com/ultreiaio/jaxx/-/milestones/187)

**Closed at 2020-11-23.**


### Issues
  * [[Evolution 766]](https://gitlab.com/ultreiaio/jaxx/-/issues/766) **Introduce MenuAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 765]](https://gitlab.com/ultreiaio/jaxx/-/issues/765) **In BlockingLayerUI be able to click and more on button registred by actionCommand** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-81](https://gitlab.com/ultreiaio/jaxx/-/milestones/186)

**Closed at 2020-10-09.**


### Issues
  * [[Evolution 763]](https://gitlab.com/ultreiaio/jaxx/-/issues/763) **Make configurable generate i18n helper and apply i18n helper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-81](https://gitlab.com/ultreiaio/jaxx/-/milestones/186)

**Closed at 2020-10-09.**


### Issues
  * [[Evolution 763]](https://gitlab.com/ultreiaio/jaxx/-/issues/763) **Make configurable generate i18n helper and apply i18n helper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-80](https://gitlab.com/ultreiaio/jaxx/-/milestones/185)

**Closed at 2020-10-08.**


### Issues
  * [[Evolution 764]](https://gitlab.com/ultreiaio/jaxx/-/issues/764) **Introduce I18n keys files (and generate and apply finalizer)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-80](https://gitlab.com/ultreiaio/jaxx/-/milestones/185)

**Closed at 2020-10-08.**


### Issues
  * [[Evolution 764]](https://gitlab.com/ultreiaio/jaxx/-/issues/764) **Introduce I18n keys files (and generate and apply finalizer)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-79](https://gitlab.com/ultreiaio/jaxx/-/milestones/184)

**Closed at 2020-09-03.**


### Issues
  * [[Evolution 756]](https://gitlab.com/ultreiaio/jaxx/-/issues/756) **Introduce jaxx-widgets-choice** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 757]](https://gitlab.com/ultreiaio/jaxx/-/issues/757) **Add common widgets init methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 758]](https://gitlab.com/ultreiaio/jaxx/-/issues/758) **Introduce BeanEnumEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 759]](https://gitlab.com/ultreiaio/jaxx/-/issues/759) **Introduce BeanDateEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 760]](https://gitlab.com/ultreiaio/jaxx/-/issues/760) **Fix focusable on DateTimeEditor (format should not be focusable)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 761]](https://gitlab.com/ultreiaio/jaxx/-/issues/761) **Uniformize jaxx widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 762]](https://gitlab.com/ultreiaio/jaxx/-/issues/762) **Fix some focus errors while selecting all text on coordinate editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-79](https://gitlab.com/ultreiaio/jaxx/-/milestones/184)

**Closed at 2020-09-03.**


### Issues
  * [[Evolution 756]](https://gitlab.com/ultreiaio/jaxx/-/issues/756) **Introduce jaxx-widgets-choice** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 757]](https://gitlab.com/ultreiaio/jaxx/-/issues/757) **Add common widgets init methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 758]](https://gitlab.com/ultreiaio/jaxx/-/issues/758) **Introduce BeanEnumEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 759]](https://gitlab.com/ultreiaio/jaxx/-/issues/759) **Introduce BeanDateEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 760]](https://gitlab.com/ultreiaio/jaxx/-/issues/760) **Fix focusable on DateTimeEditor (format should not be focusable)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 761]](https://gitlab.com/ultreiaio/jaxx/-/issues/761) **Uniformize jaxx widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 762]](https://gitlab.com/ultreiaio/jaxx/-/issues/762) **Fix some focus errors while selecting all text on coordinate editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-78](https://gitlab.com/ultreiaio/jaxx/-/milestones/183)

**Closed at 2020-08-30.**


### Issues
  * [[Evolution 755]](https://gitlab.com/ultreiaio/jaxx/-/issues/755) **Remove usage of SwingUtil.setText** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-78](https://gitlab.com/ultreiaio/jaxx/-/milestones/183)

**Closed at 2020-08-30.**


### Issues
  * [[Evolution 755]](https://gitlab.com/ultreiaio/jaxx/-/issues/755) **Remove usage of SwingUtil.setText** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-77](https://gitlab.com/ultreiaio/jaxx/-/milestones/182)

**Closed at 2020-07-31.**


### Issues
  * [[Evolution 754]](https://gitlab.com/ultreiaio/jaxx/-/issues/754) **Improve ActionExecutor with introduction of an Event API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-77](https://gitlab.com/ultreiaio/jaxx/-/milestones/182)

**Closed at 2020-07-31.**


### Issues
  * [[Evolution 754]](https://gitlab.com/ultreiaio/jaxx/-/issues/754) **Improve ActionExecutor with introduction of an Event API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-76](https://gitlab.com/ultreiaio/jaxx/-/milestones/181)

**Closed at 2020-07-08.**


### Issues
  * [[Evolution 752]](https://gitlab.com/ultreiaio/jaxx/-/issues/752) **Be able to load common css from his parent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 753]](https://gitlab.com/ultreiaio/jaxx/-/issues/753) **Be able to instanciate object in inheritated jaxx objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-76](https://gitlab.com/ultreiaio/jaxx/-/milestones/181)

**Closed at 2020-07-08.**


### Issues
  * [[Evolution 752]](https://gitlab.com/ultreiaio/jaxx/-/issues/752) **Be able to load common css from his parent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 753]](https://gitlab.com/ultreiaio/jaxx/-/issues/753) **Be able to instanciate object in inheritated jaxx objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-75](https://gitlab.com/ultreiaio/jaxx/-/milestones/180)

**Closed at 2020-07-06.**


### Issues
  * [[Anomalie 751]](https://gitlab.com/ultreiaio/jaxx/-/issues/751) **Hum avoid a NPE in NumberEditor when reset from accelerator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-75](https://gitlab.com/ultreiaio/jaxx/-/milestones/180)

**Closed at 2020-07-06.**


### Issues
  * [[Anomalie 751]](https://gitlab.com/ultreiaio/jaxx/-/issues/751) **Hum avoid a NPE in NumberEditor when reset from accelerator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-74](https://gitlab.com/ultreiaio/jaxx/-/milestones/179)

**Closed at 2020-07-03.**


### Issues
No issue.

## Version [3.0-alpha-74](https://gitlab.com/ultreiaio/jaxx/-/milestones/179)

**Closed at 2020-07-03.**


### Issues
No issue.

## Version [3.0-alpha-73](https://gitlab.com/ultreiaio/jaxx/-/milestones/178)

**Closed at 2020-07-03.**


### Issues
  * [[Evolution 749]](https://gitlab.com/ultreiaio/jaxx/-/issues/749) **Ajouter raccourci clavier Ctrl+D pour réinitialiser un éditeur de nombre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 750]](https://gitlab.com/ultreiaio/jaxx/-/issues/750) **Ajouter raccourci clavier Ctrl+D pour réinitialiser un éditeur de texte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 747]](https://gitlab.com/ultreiaio/jaxx/-/issues/747) **Le reset des combo box ne fonctionne pas bien (alors que l&#39;action associée Ctrl+D est ok)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 748]](https://gitlab.com/ultreiaio/jaxx/-/issues/748) **le reset reste actif sur les éditeurs de nombres même si ceci ne sont pas actif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-73](https://gitlab.com/ultreiaio/jaxx/-/milestones/178)

**Closed at 2020-07-03.**


### Issues
  * [[Evolution 749]](https://gitlab.com/ultreiaio/jaxx/-/issues/749) **Ajouter raccourci clavier Ctrl+D pour réinitialiser un éditeur de nombre** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 750]](https://gitlab.com/ultreiaio/jaxx/-/issues/750) **Ajouter raccourci clavier Ctrl+D pour réinitialiser un éditeur de texte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 747]](https://gitlab.com/ultreiaio/jaxx/-/issues/747) **Le reset des combo box ne fonctionne pas bien (alors que l&#39;action associée Ctrl+D est ok)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 748]](https://gitlab.com/ultreiaio/jaxx/-/issues/748) **le reset reste actif sur les éditeurs de nombres même si ceci ne sont pas actif** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-72](https://gitlab.com/ultreiaio/jaxx/-/milestones/177)

**Closed at 2020-06-27.**


### Issues
  * [[Anomalie 745]](https://gitlab.com/ultreiaio/jaxx/-/issues/745) **Reset action has disappeared in DateTime editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 746]](https://gitlab.com/ultreiaio/jaxx/-/issues/746) **When setting selected item in JaxxComboBox, le&#39;ts set decorator inside item if necessary** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-72](https://gitlab.com/ultreiaio/jaxx/-/milestones/177)

**Closed at 2020-06-27.**


### Issues
  * [[Anomalie 745]](https://gitlab.com/ultreiaio/jaxx/-/issues/745) **Reset action has disappeared in DateTime editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 746]](https://gitlab.com/ultreiaio/jaxx/-/issues/746) **When setting selected item in JaxxComboBox, le&#39;ts set decorator inside item if necessary** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-71](https://gitlab.com/ultreiaio/jaxx/-/milestones/176)

**Closed at 2020-06-26.**


### Issues
  * [[Evolution 743]](https://gitlab.com/ultreiaio/jaxx/-/issues/743) **Introduce BeanDecoratorAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 744]](https://gitlab.com/ultreiaio/jaxx/-/issues/744) **Fix new JaxxComboBox using BeanDecoratorAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-71](https://gitlab.com/ultreiaio/jaxx/-/milestones/176)

**Closed at 2020-06-26.**


### Issues
  * [[Evolution 743]](https://gitlab.com/ultreiaio/jaxx/-/issues/743) **Introduce BeanDecoratorAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 744]](https://gitlab.com/ultreiaio/jaxx/-/issues/744) **Fix new JaxxComboBox using BeanDecoratorAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-70](https://gitlab.com/ultreiaio/jaxx/-/milestones/175)

**Closed at 2020-06-24.**


### Issues
  * [[Evolution 742]](https://gitlab.com/ultreiaio/jaxx/-/issues/742) **Let&#39;s introduce a new hack to not enabled actions in JMenuBar unless the popup was displayed** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 741]](https://gitlab.com/ultreiaio/jaxx/-/issues/741) **NumberEditor accept any decimal separator :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-70](https://gitlab.com/ultreiaio/jaxx/-/milestones/175)

**Closed at 2020-06-24.**


### Issues
  * [[Evolution 742]](https://gitlab.com/ultreiaio/jaxx/-/issues/742) **Let&#39;s introduce a new hack to not enabled actions in JMenuBar unless the popup was displayed** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 741]](https://gitlab.com/ultreiaio/jaxx/-/issues/741) **NumberEditor accept any decimal separator :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-69](https://gitlab.com/ultreiaio/jaxx/-/milestones/174)

**Closed at 2020-06-22.**


### Issues
  * [[Evolution 740]](https://gitlab.com/ultreiaio/jaxx/-/issues/740) **Introduce a new combo box widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 739]](https://gitlab.com/ultreiaio/jaxx/-/issues/739) **Remove jnlp demo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-69](https://gitlab.com/ultreiaio/jaxx/-/milestones/174)

**Closed at 2020-06-22.**


### Issues
  * [[Evolution 740]](https://gitlab.com/ultreiaio/jaxx/-/issues/740) **Introduce a new combo box widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 739]](https://gitlab.com/ultreiaio/jaxx/-/issues/739) **Remove jnlp demo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-68](https://gitlab.com/ultreiaio/jaxx/-/milestones/173)

**Closed at *In progress*.**


### Issues
  * [[Evolution 738]](https://gitlab.com/ultreiaio/jaxx/-/issues/738) **Add auto sort selected list on double list widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-68](https://gitlab.com/ultreiaio/jaxx/-/milestones/173)

**Closed at *In progress*.**


### Issues
  * [[Evolution 738]](https://gitlab.com/ultreiaio/jaxx/-/issues/738) **Add auto sort selected list on double list widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-67](https://gitlab.com/ultreiaio/jaxx/-/milestones/172)

**Closed at 2020-04-16.**


### Issues
  * [[Anomalie 737]](https://gitlab.com/ultreiaio/jaxx/-/issues/737) **If action is overridden then accelerator is displayed twice** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-67](https://gitlab.com/ultreiaio/jaxx/-/milestones/172)

**Closed at 2020-04-16.**


### Issues
  * [[Anomalie 737]](https://gitlab.com/ultreiaio/jaxx/-/issues/737) **If action is overridden then accelerator is displayed twice** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-66](https://gitlab.com/ultreiaio/jaxx/-/milestones/171)

**Closed at 2020-04-06.**


### Issues
  * [[Evolution 736]](https://gitlab.com/ultreiaio/jaxx/-/issues/736) **Improve TabInfo integration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-66](https://gitlab.com/ultreiaio/jaxx/-/milestones/171)

**Closed at 2020-04-06.**


### Issues
  * [[Evolution 736]](https://gitlab.com/ultreiaio/jaxx/-/issues/736) **Improve TabInfo integration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-65](https://gitlab.com/ultreiaio/jaxx/-/milestones/170)

**Closed at 2020-03-06.**


### Issues
  * [[Evolution 735]](https://gitlab.com/ultreiaio/jaxx/-/issues/735) **Downgrade to java8, but make it usable for later jdk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-65](https://gitlab.com/ultreiaio/jaxx/-/milestones/170)

**Closed at 2020-03-06.**


### Issues
  * [[Evolution 735]](https://gitlab.com/ultreiaio/jaxx/-/issues/735) **Downgrade to java8, but make it usable for later jdk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-64](https://gitlab.com/ultreiaio/jaxx/-/milestones/169)

**Closed at 2020-02-18.**


### Issues
  * [[Evolution 734]](https://gitlab.com/ultreiaio/jaxx/-/issues/734) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-64](https://gitlab.com/ultreiaio/jaxx/-/milestones/169)

**Closed at 2020-02-18.**


### Issues
  * [[Evolution 734]](https://gitlab.com/ultreiaio/jaxx/-/issues/734) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-63](https://gitlab.com/ultreiaio/jaxx/-/milestones/168)

**Closed at 2020-01-16.**


### Issues
  * [[Anomalie 733]](https://gitlab.com/ultreiaio/jaxx/-/issues/733) **Bad DoubleList i18n property generation + missing label on FilterableDoubleList** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-63](https://gitlab.com/ultreiaio/jaxx/-/milestones/168)

**Closed at 2020-01-16.**


### Issues
  * [[Anomalie 733]](https://gitlab.com/ultreiaio/jaxx/-/issues/733) **Bad DoubleList i18n property generation + missing label on FilterableDoubleList** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-62](https://gitlab.com/ultreiaio/jaxx/-/milestones/167)

**Closed at 2020-01-01.**


### Issues
  * [[Anomalie 732]](https://gitlab.com/ultreiaio/jaxx/-/issues/732) **If a double list widget is disabled, it is still possible to change selection using mouse click...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-62](https://gitlab.com/ultreiaio/jaxx/-/milestones/167)

**Closed at 2020-01-01.**


### Issues
  * [[Anomalie 732]](https://gitlab.com/ultreiaio/jaxx/-/issues/732) **If a double list widget is disabled, it is still possible to change selection using mouse click...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-61](https://gitlab.com/ultreiaio/jaxx/-/milestones/166)

**Closed at 2019-12-18.**


### Issues
  * [[Anomalie 731]](https://gitlab.com/ultreiaio/jaxx/-/issues/731) **Add missing focus management on some widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-61](https://gitlab.com/ultreiaio/jaxx/-/milestones/166)

**Closed at 2019-12-18.**


### Issues
  * [[Anomalie 731]](https://gitlab.com/ultreiaio/jaxx/-/issues/731) **Add missing focus management on some widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-60](https://gitlab.com/ultreiaio/jaxx/-/milestones/165)

**Closed at 2019-12-03.**


### Issues
  * [[Evolution 730]](https://gitlab.com/ultreiaio/jaxx/-/issues/730) **Improve Jaxx Action API and apply it to widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 728]](https://gitlab.com/ultreiaio/jaxx/-/issues/728) **Fix some focusable issues on select widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 729]](https://gitlab.com/ultreiaio/jaxx/-/issues/729) **Can&#39;t properly reset time editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-60](https://gitlab.com/ultreiaio/jaxx/-/milestones/165)

**Closed at 2019-12-03.**


### Issues
  * [[Evolution 730]](https://gitlab.com/ultreiaio/jaxx/-/issues/730) **Improve Jaxx Action API and apply it to widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 728]](https://gitlab.com/ultreiaio/jaxx/-/issues/728) **Fix some focusable issues on select widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 729]](https://gitlab.com/ultreiaio/jaxx/-/issues/729) **Can&#39;t properly reset time editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-59](https://gitlab.com/ultreiaio/jaxx/-/milestones/164)

**Closed at 2019-11-27.**


### Issues
  * [[Evolution 727]](https://gitlab.com/ultreiaio/jaxx/-/issues/727) **Do not inheritate enabled property in swing components** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-59](https://gitlab.com/ultreiaio/jaxx/-/milestones/164)

**Closed at 2019-11-27.**


### Issues
  * [[Evolution 727]](https://gitlab.com/ultreiaio/jaxx/-/issues/727) **Do not inheritate enabled property in swing components** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-58](https://gitlab.com/ultreiaio/jaxx/-/milestones/163)

**Closed at 2019-10-18.**


### Issues
  * [[Evolution 726]](https://gitlab.com/ultreiaio/jaxx/-/issues/726) **Improve JaxxObjectActionSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-58](https://gitlab.com/ultreiaio/jaxx/-/milestones/163)

**Closed at 2019-10-18.**


### Issues
  * [[Evolution 726]](https://gitlab.com/ultreiaio/jaxx/-/issues/726) **Improve JaxxObjectActionSupport** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-57](https://gitlab.com/ultreiaio/jaxx/-/milestones/162)

**Closed at 2019-09-26.**


### Issues
  * [[Evolution 725]](https://gitlab.com/ultreiaio/jaxx/-/issues/725) **Improve action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-57](https://gitlab.com/ultreiaio/jaxx/-/milestones/162)

**Closed at 2019-09-26.**


### Issues
  * [[Evolution 725]](https://gitlab.com/ultreiaio/jaxx/-/issues/725) **Improve action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-56](https://gitlab.com/ultreiaio/jaxx/-/milestones/161)

**Closed at 2019-08-22.**


### Issues
  * [[Evolution 720]](https://gitlab.com/ultreiaio/jaxx/-/issues/720) **Add a way to use css id selector inside his rules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 721]](https://gitlab.com/ultreiaio/jaxx/-/issues/721) **Introduce a new Action api (ApplicationAction ng!)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 723]](https://gitlab.com/ultreiaio/jaxx/-/issues/723) **Detects actions while parsing jaxx files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 724]](https://gitlab.com/ultreiaio/jaxx/-/issues/724) **Add addToContainer attribute on jaxx tags to be able to add to current container an overridden object** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-56](https://gitlab.com/ultreiaio/jaxx/-/milestones/161)

**Closed at 2019-08-22.**


### Issues
  * [[Evolution 720]](https://gitlab.com/ultreiaio/jaxx/-/issues/720) **Add a way to use css id selector inside his rules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 721]](https://gitlab.com/ultreiaio/jaxx/-/issues/721) **Introduce a new Action api (ApplicationAction ng!)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 723]](https://gitlab.com/ultreiaio/jaxx/-/issues/723) **Detects actions while parsing jaxx files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 724]](https://gitlab.com/ultreiaio/jaxx/-/issues/724) **Add addToContainer attribute on jaxx tags to be able to add to current container an overridden object** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-55](https://gitlab.com/ultreiaio/jaxx/-/milestones/160)

**Closed at 2019-08-11.**


### Issues
  * [[Evolution 719]](https://gitlab.com/ultreiaio/jaxx/-/issues/719) **Remove JaxxObject#firePropertyChange method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-55](https://gitlab.com/ultreiaio/jaxx/-/milestones/160)

**Closed at 2019-08-11.**


### Issues
  * [[Evolution 719]](https://gitlab.com/ultreiaio/jaxx/-/issues/719) **Remove JaxxObject#firePropertyChange method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-54](https://gitlab.com/ultreiaio/jaxx/-/milestones/159)

**Closed at 2019-07-31.**


### Issues
  * [[Evolution 718]](https://gitlab.com/ultreiaio/jaxx/-/issues/718) **Permits to use multiple synonyms :)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-54](https://gitlab.com/ultreiaio/jaxx/-/milestones/159)

**Closed at 2019-07-31.**


### Issues
  * [[Evolution 718]](https://gitlab.com/ultreiaio/jaxx/-/issues/718) **Permits to use multiple synonyms :)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-53](https://gitlab.com/ultreiaio/jaxx/-/milestones/158)

**Closed at 2019-06-28.**


### Issues
  * [[Evolution 717]](https://gitlab.com/ultreiaio/jaxx/-/issues/717) **Permits to use synonyms classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-53](https://gitlab.com/ultreiaio/jaxx/-/milestones/158)

**Closed at 2019-06-28.**


### Issues
  * [[Evolution 717]](https://gitlab.com/ultreiaio/jaxx/-/issues/717) **Permits to use synonyms classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-52](https://gitlab.com/ultreiaio/jaxx/-/milestones/157)

**Closed at *In progress*.**


### Issues
  * [[Evolution 716]](https://gitlab.com/ultreiaio/jaxx/-/issues/716) **Improve JTabbedPaneValidator to be able to register more validators inside it.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-52](https://gitlab.com/ultreiaio/jaxx/-/milestones/157)

**Closed at *In progress*.**


### Issues
  * [[Evolution 716]](https://gitlab.com/ultreiaio/jaxx/-/issues/716) **Improve JTabbedPaneValidator to be able to register more validators inside it.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-51](https://gitlab.com/ultreiaio/jaxx/-/milestones/156)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 715]](https://gitlab.com/ultreiaio/jaxx/-/issues/715) **Fix UrlEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-51](https://gitlab.com/ultreiaio/jaxx/-/milestones/156)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 715]](https://gitlab.com/ultreiaio/jaxx/-/issues/715) **Fix UrlEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-50](https://gitlab.com/ultreiaio/jaxx/-/milestones/155)

**Closed at 2019-05-21.**


### Issues
  * [[Evolution 713]](https://gitlab.com/ultreiaio/jaxx/-/issues/713) **Add a call back when a hidor show his target** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 714]](https://gitlab.com/ultreiaio/jaxx/-/issues/714) **Fix once for all the TAB accelerator problem in BeanFiltrableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-50](https://gitlab.com/ultreiaio/jaxx/-/milestones/155)

**Closed at 2019-05-21.**


### Issues
  * [[Evolution 713]](https://gitlab.com/ultreiaio/jaxx/-/issues/713) **Add a call back when a hidor show his target** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 714]](https://gitlab.com/ultreiaio/jaxx/-/issues/714) **Fix once for all the TAB accelerator problem in BeanFiltrableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-49](https://gitlab.com/ultreiaio/jaxx/-/milestones/154)

**Closed at 2019-02-03.**


### Issues
  * [[Evolution 710]](https://gitlab.com/ultreiaio/jaxx/-/issues/710) **Introduce BeanScope and unify Jaxx widgets to support it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 711]](https://gitlab.com/ultreiaio/jaxx/-/issues/711) **Fix BigTextEditor info label tip method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 712]](https://gitlab.com/ultreiaio/jaxx/-/issues/712) **Revert Use Ctrl+Tab instead of Tab to select and focus next widget on BeanFilterableComboBox and apply correct fix** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-49](https://gitlab.com/ultreiaio/jaxx/-/milestones/154)

**Closed at 2019-02-03.**


### Issues
  * [[Evolution 710]](https://gitlab.com/ultreiaio/jaxx/-/issues/710) **Introduce BeanScope and unify Jaxx widgets to support it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 711]](https://gitlab.com/ultreiaio/jaxx/-/issues/711) **Fix BigTextEditor info label tip method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 712]](https://gitlab.com/ultreiaio/jaxx/-/issues/712) **Revert Use Ctrl+Tab instead of Tab to select and focus next widget on BeanFilterableComboBox and apply correct fix** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-48](https://gitlab.com/ultreiaio/jaxx/-/milestones/153)

**Closed at 2019-01-22.**


### Issues
  * [[Evolution 708]](https://gitlab.com/ultreiaio/jaxx/-/issues/708) **Use Ctrl+Tab instead of Tab to select and focus next widget on BeanFilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-48](https://gitlab.com/ultreiaio/jaxx/-/milestones/153)

**Closed at 2019-01-22.**


### Issues
  * [[Evolution 708]](https://gitlab.com/ultreiaio/jaxx/-/issues/708) **Use Ctrl+Tab instead of Tab to select and focus next widget on BeanFilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-46](https://gitlab.com/ultreiaio/jaxx/-/milestones/152)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-46](https://gitlab.com/ultreiaio/jaxx/-/milestones/152)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-44](https://gitlab.com/ultreiaio/jaxx/-/milestones/151)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-44](https://gitlab.com/ultreiaio/jaxx/-/milestones/151)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-43](https://gitlab.com/ultreiaio/jaxx/-/milestones/150)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-43](https://gitlab.com/ultreiaio/jaxx/-/milestones/150)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-42](https://gitlab.com/ultreiaio/jaxx/-/milestones/149)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-42](https://gitlab.com/ultreiaio/jaxx/-/milestones/149)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-41](https://gitlab.com/ultreiaio/jaxx/-/milestones/148)

**Closed at *In progress*.**


### Issues
  * [[Evolution 707]](https://gitlab.com/ultreiaio/jaxx/-/issues/707) **Use new I18n api to detect keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-41](https://gitlab.com/ultreiaio/jaxx/-/milestones/148)

**Closed at *In progress*.**


### Issues
  * [[Evolution 707]](https://gitlab.com/ultreiaio/jaxx/-/issues/707) **Use new I18n api to detect keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-40](https://gitlab.com/ultreiaio/jaxx/-/milestones/147)

**Closed at *In progress*.**


### Issues
  * [[Evolution 706]](https://gitlab.com/ultreiaio/jaxx/-/issues/706) **Be able t use TAB and shift+TAB on BeanFiltreableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-40](https://gitlab.com/ultreiaio/jaxx/-/milestones/147)

**Closed at *In progress*.**


### Issues
  * [[Evolution 706]](https://gitlab.com/ultreiaio/jaxx/-/issues/706) **Be able t use TAB and shift+TAB on BeanFiltreableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-39](https://gitlab.com/ultreiaio/jaxx/-/milestones/146)

**Closed at *In progress*.**


### Issues
  * [[Evolution 705]](https://gitlab.com/ultreiaio/jaxx/-/issues/705) **Use i18n-api BeanPropertyI18nKeyProducer (instead of weak I18nLabelsBuilder) + review how to generated i18n property keys (Major break)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-39](https://gitlab.com/ultreiaio/jaxx/-/milestones/146)

**Closed at *In progress*.**


### Issues
  * [[Evolution 705]](https://gitlab.com/ultreiaio/jaxx/-/issues/705) **Use i18n-api BeanPropertyI18nKeyProducer (instead of weak I18nLabelsBuilder) + review how to generated i18n property keys (Major break)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-38](https://gitlab.com/ultreiaio/jaxx/-/milestones/145)

**Closed at 2018-10-06.**


### Issues
  * [[Evolution 704]](https://gitlab.com/ultreiaio/jaxx/-/issues/704) **Add global reset button on Coordinate editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-38](https://gitlab.com/ultreiaio/jaxx/-/milestones/145)

**Closed at 2018-10-06.**


### Issues
  * [[Evolution 704]](https://gitlab.com/ultreiaio/jaxx/-/issues/704) **Add global reset button on Coordinate editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-37](https://gitlab.com/ultreiaio/jaxx/-/milestones/144)

**Closed at *In progress*.**


### Issues
  * [[Evolution 701]](https://gitlab.com/ultreiaio/jaxx/-/issues/701) **Add containerId on TabInfo to be able to get back his container** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 702]](https://gitlab.com/ultreiaio/jaxx/-/issues/702) **Introduce JVetoableTabbedPane** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 703]](https://gitlab.com/ultreiaio/jaxx/-/issues/703) **Add a new way of managing tab validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-37](https://gitlab.com/ultreiaio/jaxx/-/milestones/144)

**Closed at *In progress*.**


### Issues
  * [[Evolution 701]](https://gitlab.com/ultreiaio/jaxx/-/issues/701) **Add containerId on TabInfo to be able to get back his container** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 702]](https://gitlab.com/ultreiaio/jaxx/-/issues/702) **Introduce JVetoableTabbedPane** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 703]](https://gitlab.com/ultreiaio/jaxx/-/issues/703) **Add a new way of managing tab validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-36](https://gitlab.com/ultreiaio/jaxx/-/milestones/143)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 700]](https://gitlab.com/ultreiaio/jaxx/-/issues/700) **Do not apply data bindings more than once at init time** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-36](https://gitlab.com/ultreiaio/jaxx/-/milestones/143)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 700]](https://gitlab.com/ultreiaio/jaxx/-/issues/700) **Do not apply data bindings more than once at init time** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-35](https://gitlab.com/ultreiaio/jaxx/-/milestones/142)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-35](https://gitlab.com/ultreiaio/jaxx/-/milestones/142)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-34](https://gitlab.com/ultreiaio/jaxx/-/milestones/141)

**Closed at 2018-08-02.**


### Issues
  * [[Anomalie 696]](https://gitlab.com/ultreiaio/jaxx/-/issues/696) **Make jaxx works again on windows :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 697]](https://gitlab.com/ultreiaio/jaxx/-/issues/697) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-34](https://gitlab.com/ultreiaio/jaxx/-/milestones/141)

**Closed at 2018-08-02.**


### Issues
  * [[Anomalie 696]](https://gitlab.com/ultreiaio/jaxx/-/issues/696) **Make jaxx works again on windows :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 697]](https://gitlab.com/ultreiaio/jaxx/-/issues/697) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-33](https://gitlab.com/ultreiaio/jaxx/-/milestones/140)

**Closed at 2018-07-19.**


### Issues
  * [[Evolution 694]](https://gitlab.com/ultreiaio/jaxx/-/issues/694) **Add accelerators on BeanFilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 695]](https://gitlab.com/ultreiaio/jaxx/-/issues/695) **Fix accelerators on widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-33](https://gitlab.com/ultreiaio/jaxx/-/milestones/140)

**Closed at 2018-07-19.**


### Issues
  * [[Evolution 694]](https://gitlab.com/ultreiaio/jaxx/-/issues/694) **Add accelerators on BeanFilterableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 695]](https://gitlab.com/ultreiaio/jaxx/-/issues/695) **Fix accelerators on widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-32](https://gitlab.com/ultreiaio/jaxx/-/milestones/139)

**Closed at *In progress*.**


### Issues
  * [[Evolution 693]](https://gitlab.com/ultreiaio/jaxx/-/issues/693) **Improve BeanFiltreableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-32](https://gitlab.com/ultreiaio/jaxx/-/milestones/139)

**Closed at *In progress*.**


### Issues
  * [[Evolution 693]](https://gitlab.com/ultreiaio/jaxx/-/issues/693) **Improve BeanFiltreableComboBox** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-31](https://gitlab.com/ultreiaio/jaxx/-/milestones/138)

**Closed at *In progress*.**


### Issues
  * [[Evolution 692]](https://gitlab.com/ultreiaio/jaxx/-/issues/692) **Add reset on DateTimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-31](https://gitlab.com/ultreiaio/jaxx/-/milestones/138)

**Closed at *In progress*.**


### Issues
  * [[Evolution 692]](https://gitlab.com/ultreiaio/jaxx/-/issues/692) **Add reset on DateTimeEditor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-30](https://gitlab.com/ultreiaio/jaxx/-/milestones/137)

**Closed at *In progress*.**


### Issues
  * [[Evolution 689]](https://gitlab.com/ultreiaio/jaxx/-/issues/689) **Replace nuiton-config by java4all config** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 688]](https://gitlab.com/ultreiaio/jaxx/-/issues/688) **Can&#39;t reload twice application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 690]](https://gitlab.com/ultreiaio/jaxx/-/issues/690) **Make compiler works even if validator is not in class-path (but in sources of module)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-30](https://gitlab.com/ultreiaio/jaxx/-/milestones/137)

**Closed at *In progress*.**


### Issues
  * [[Evolution 689]](https://gitlab.com/ultreiaio/jaxx/-/issues/689) **Replace nuiton-config by java4all config** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 688]](https://gitlab.com/ultreiaio/jaxx/-/issues/688) **Can&#39;t reload twice application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 690]](https://gitlab.com/ultreiaio/jaxx/-/issues/690) **Make compiler works even if validator is not in class-path (but in sources of module)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-29](https://gitlab.com/ultreiaio/jaxx/-/milestones/136)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-29](https://gitlab.com/ultreiaio/jaxx/-/milestones/136)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-28](https://gitlab.com/ultreiaio/jaxx/-/milestones/135)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-28](https://gitlab.com/ultreiaio/jaxx/-/milestones/135)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-27](https://gitlab.com/ultreiaio/jaxx/-/milestones/133)

**Closed at *In progress*.**


### Issues
  * [[Evolution 682]](https://gitlab.com/ultreiaio/jaxx/-/issues/682) **Can use classpath css in jaxx file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-27](https://gitlab.com/ultreiaio/jaxx/-/milestones/133)

**Closed at *In progress*.**


### Issues
  * [[Evolution 682]](https://gitlab.com/ultreiaio/jaxx/-/issues/682) **Can use classpath css in jaxx file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-26](https://gitlab.com/ultreiaio/jaxx/-/milestones/132)

**Closed at *In progress*.**


### Issues
  * [[Evolution 679]](https://gitlab.com/ultreiaio/jaxx/-/issues/679) **Be able to configure key stroke of an ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-26](https://gitlab.com/ultreiaio/jaxx/-/milestones/132)

**Closed at *In progress*.**


### Issues
  * [[Evolution 679]](https://gitlab.com/ultreiaio/jaxx/-/issues/679) **Be able to configure key stroke of an ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-25](https://gitlab.com/ultreiaio/jaxx/-/milestones/131)

**Closed at *In progress*.**


### Issues
  * [[Evolution 671]](https://gitlab.com/ultreiaio/jaxx/-/issues/671) **Improve FilterTreeModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 672]](https://gitlab.com/ultreiaio/jaxx/-/issues/672) **Add in JTrees efficient enumeration to walk on a Tree** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 673]](https://gitlab.com/ultreiaio/jaxx/-/issues/673) **Move JTrees to tree package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 674]](https://gitlab.com/ultreiaio/jaxx/-/issues/674) **Introduce IterableTreeNode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-25](https://gitlab.com/ultreiaio/jaxx/-/milestones/131)

**Closed at *In progress*.**


### Issues
  * [[Evolution 671]](https://gitlab.com/ultreiaio/jaxx/-/issues/671) **Improve FilterTreeModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 672]](https://gitlab.com/ultreiaio/jaxx/-/issues/672) **Add in JTrees efficient enumeration to walk on a Tree** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 673]](https://gitlab.com/ultreiaio/jaxx/-/issues/673) **Move JTrees to tree package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 674]](https://gitlab.com/ultreiaio/jaxx/-/issues/674) **Introduce IterableTreeNode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-24](https://gitlab.com/ultreiaio/jaxx/-/milestones/130)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-24](https://gitlab.com/ultreiaio/jaxx/-/milestones/130)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-23](https://gitlab.com/ultreiaio/jaxx/-/milestones/129)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-23](https://gitlab.com/ultreiaio/jaxx/-/milestones/129)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-22](https://gitlab.com/ultreiaio/jaxx/-/milestones/128)

**Closed at *In progress*.**


### Issues
  * [[Evolution 670]](https://gitlab.com/ultreiaio/jaxx/-/issues/670) **Add a simple way to load ui resources in from a module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-22](https://gitlab.com/ultreiaio/jaxx/-/milestones/128)

**Closed at *In progress*.**


### Issues
  * [[Evolution 670]](https://gitlab.com/ultreiaio/jaxx/-/issues/670) **Add a simple way to load ui resources in from a module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-21](https://gitlab.com/ultreiaio/jaxx/-/milestones/127)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-21](https://gitlab.com/ultreiaio/jaxx/-/milestones/127)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-20](https://gitlab.com/ultreiaio/jaxx/-/milestones/126)

**Closed at *In progress*.**


### Issues
  * [[Evolution 669]](https://gitlab.com/ultreiaio/jaxx/-/issues/669) **Improve generation of handler is ui is abstract but not handler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-20](https://gitlab.com/ultreiaio/jaxx/-/milestones/126)

**Closed at *In progress*.**


### Issues
  * [[Evolution 669]](https://gitlab.com/ultreiaio/jaxx/-/issues/669) **Improve generation of handler is ui is abstract but not handler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-19](https://gitlab.com/ultreiaio/jaxx/-/milestones/125)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 668]](https://gitlab.com/ultreiaio/jaxx/-/issues/668) **Fix ApplicationAction.isEnabled() method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-19](https://gitlab.com/ultreiaio/jaxx/-/milestones/125)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 668]](https://gitlab.com/ultreiaio/jaxx/-/issues/668) **Fix ApplicationAction.isEnabled() method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-18](https://gitlab.com/ultreiaio/jaxx/-/milestones/124)

**Closed at *In progress*.**


### Issues
  * [[Evolution 659]](https://gitlab.com/ultreiaio/jaxx/-/issues/659) **Add yet another Action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 660]](https://gitlab.com/ultreiaio/jaxx/-/issues/660) **Add new text widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 662]](https://gitlab.com/ultreiaio/jaxx/-/issues/662) **Improve BeanComboBox widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 664]](https://gitlab.com/ultreiaio/jaxx/-/issues/664) **Add yet another init API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 665]](https://gitlab.com/ultreiaio/jaxx/-/issues/665) **Let&#39;s add possible data binding on javaBean attribute content** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 666]](https://gitlab.com/ultreiaio/jaxx/-/issues/666) **Be able to use ApplicationAction in JCheckBoxMenuItem also** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 667]](https://gitlab.com/ultreiaio/jaxx/-/issues/667) **Reuse component icon if exists in ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 663]](https://gitlab.com/ultreiaio/jaxx/-/issues/663) **Temperature widget requires a Serializable bean, but should not** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 661]](https://gitlab.com/ultreiaio/jaxx/-/issues/661) **Remove module jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-18](https://gitlab.com/ultreiaio/jaxx/-/milestones/124)

**Closed at *In progress*.**


### Issues
  * [[Evolution 659]](https://gitlab.com/ultreiaio/jaxx/-/issues/659) **Add yet another Action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 660]](https://gitlab.com/ultreiaio/jaxx/-/issues/660) **Add new text widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 662]](https://gitlab.com/ultreiaio/jaxx/-/issues/662) **Improve BeanComboBox widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 664]](https://gitlab.com/ultreiaio/jaxx/-/issues/664) **Add yet another init API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 665]](https://gitlab.com/ultreiaio/jaxx/-/issues/665) **Let&#39;s add possible data binding on javaBean attribute content** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 666]](https://gitlab.com/ultreiaio/jaxx/-/issues/666) **Be able to use ApplicationAction in JCheckBoxMenuItem also** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 667]](https://gitlab.com/ultreiaio/jaxx/-/issues/667) **Reuse component icon if exists in ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 663]](https://gitlab.com/ultreiaio/jaxx/-/issues/663) **Temperature widget requires a Serializable bean, but should not** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 661]](https://gitlab.com/ultreiaio/jaxx/-/issues/661) **Remove module jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-17](https://gitlab.com/ultreiaio/jaxx/-/milestones/123)

**Closed at *In progress*.**


### Issues
  * [[Evolution 657]](https://gitlab.com/ultreiaio/jaxx/-/issues/657) **Use new i18n project APi to generate jaxx getter while generating jaxx files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 658]](https://gitlab.com/ultreiaio/jaxx/-/issues/658) **Detect any I18n api call inside JAXX files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-17](https://gitlab.com/ultreiaio/jaxx/-/milestones/123)

**Closed at *In progress*.**


### Issues
  * [[Evolution 657]](https://gitlab.com/ultreiaio/jaxx/-/issues/657) **Use new i18n project APi to generate jaxx getter while generating jaxx files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 658]](https://gitlab.com/ultreiaio/jaxx/-/issues/658) **Detect any I18n api call inside JAXX files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-16](https://gitlab.com/ultreiaio/jaxx/-/milestones/122)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 655]](https://gitlab.com/ultreiaio/jaxx/-/issues/655) **Fix getHandler method generation for abstract ui** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-16](https://gitlab.com/ultreiaio/jaxx/-/milestones/122)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 655]](https://gitlab.com/ultreiaio/jaxx/-/issues/655) **Fix getHandler method generation for abstract ui** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-15](https://gitlab.com/ultreiaio/jaxx/-/milestones/121)

**Closed at *In progress*.**


### Issues
  * [[Evolution 653]](https://gitlab.com/ultreiaio/jaxx/-/issues/653) **Improve generation of getHandler method for abstract classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 654]](https://gitlab.com/ultreiaio/jaxx/-/issues/654) **FilterableDoubleList should not accept only serializable objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-15](https://gitlab.com/ultreiaio/jaxx/-/milestones/121)

**Closed at *In progress*.**


### Issues
  * [[Evolution 653]](https://gitlab.com/ultreiaio/jaxx/-/issues/653) **Improve generation of getHandler method for abstract classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 654]](https://gitlab.com/ultreiaio/jaxx/-/issues/654) **FilterableDoubleList should not accept only serializable objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-14](https://gitlab.com/ultreiaio/jaxx/-/milestones/120)

**Closed at *In progress*.**


### Issues
  * [[Evolution 651]](https://gitlab.com/ultreiaio/jaxx/-/issues/651) **Introduce jaxx-runtime-spi module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 652]](https://gitlab.com/ultreiaio/jaxx/-/issues/652) **Improve TabInfo component** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-14](https://gitlab.com/ultreiaio/jaxx/-/milestones/120)

**Closed at *In progress*.**


### Issues
  * [[Evolution 651]](https://gitlab.com/ultreiaio/jaxx/-/issues/651) **Introduce jaxx-runtime-spi module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 652]](https://gitlab.com/ultreiaio/jaxx/-/issues/652) **Improve TabInfo component** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-13](https://gitlab.com/ultreiaio/jaxx/-/milestones/119)

**Closed at *In progress*.**


### Issues
  * [[Evolution 649]](https://gitlab.com/ultreiaio/jaxx/-/issues/649) **Be able to detect validator fields using inheritance of jaxx classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 650]](https://gitlab.com/ultreiaio/jaxx/-/issues/650) **Introduce UIInitializer API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-13](https://gitlab.com/ultreiaio/jaxx/-/milestones/119)

**Closed at *In progress*.**


### Issues
  * [[Evolution 649]](https://gitlab.com/ultreiaio/jaxx/-/issues/649) **Be able to detect validator fields using inheritance of jaxx classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 650]](https://gitlab.com/ultreiaio/jaxx/-/issues/650) **Introduce UIInitializer API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-12](https://gitlab.com/ultreiaio/jaxx/-/milestones/118)

**Closed at *In progress*.**


### Issues
  * [[Evolution 647]](https://gitlab.com/ultreiaio/jaxx/-/issues/647) **Introduce jaxx-widgets-temperature module with a temperature editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 646]](https://gitlab.com/ultreiaio/jaxx/-/issues/646) **When using computeI18n, label should always be tried before toolTipText** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 648]](https://gitlab.com/ultreiaio/jaxx/-/issues/648) **In select widgets, can&#39;t change any longer the sort type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-12](https://gitlab.com/ultreiaio/jaxx/-/milestones/118)

**Closed at *In progress*.**


### Issues
  * [[Evolution 647]](https://gitlab.com/ultreiaio/jaxx/-/issues/647) **Introduce jaxx-widgets-temperature module with a temperature editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 646]](https://gitlab.com/ultreiaio/jaxx/-/issues/646) **When using computeI18n, label should always be tried before toolTipText** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 648]](https://gitlab.com/ultreiaio/jaxx/-/issues/648) **In select widgets, can&#39;t change any longer the sort type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-11](https://gitlab.com/ultreiaio/jaxx/-/milestones/117)

**Closed at 2017-08-24.**


### Issues
  * [[Evolution 644]](https://gitlab.com/ultreiaio/jaxx/-/issues/644) **Generate i18n keys in mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 645]](https://gitlab.com/ultreiaio/jaxx/-/issues/645) **Add a way to compute i18n labels while using decorator in widgets (select)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-11](https://gitlab.com/ultreiaio/jaxx/-/milestones/117)

**Closed at 2017-08-24.**


### Issues
  * [[Evolution 644]](https://gitlab.com/ultreiaio/jaxx/-/issues/644) **Generate i18n keys in mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 645]](https://gitlab.com/ultreiaio/jaxx/-/issues/645) **Add a way to compute i18n labels while using decorator in widgets (select)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-10](https://gitlab.com/ultreiaio/jaxx/-/milestones/116)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 643]](https://gitlab.com/ultreiaio/jaxx/-/issues/643) **Do NOT invoke twice the $initialize method when inheritates from a JaxxObject...** (Thanks to ) (Reported by Tony CHEMIT)

## Version [3.0-alpha-10](https://gitlab.com/ultreiaio/jaxx/-/milestones/116)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 643]](https://gitlab.com/ultreiaio/jaxx/-/issues/643) **Do NOT invoke twice the $initialize method when inheritates from a JaxxObject...** (Thanks to ) (Reported by Tony CHEMIT)

## Version [3.0-alpha-9](https://gitlab.com/ultreiaio/jaxx/-/milestones/115)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 641]](https://gitlab.com/ultreiaio/jaxx/-/issues/641) **Bad ui handler init in Config widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 642]](https://gitlab.com/ultreiaio/jaxx/-/issues/642) **Update some libraries** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-9](https://gitlab.com/ultreiaio/jaxx/-/milestones/115)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 641]](https://gitlab.com/ultreiaio/jaxx/-/issues/641) **Bad ui handler init in Config widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 642]](https://gitlab.com/ultreiaio/jaxx/-/issues/642) **Update some libraries** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-8](https://gitlab.com/ultreiaio/jaxx/-/milestones/114)

**Closed at *In progress*.**


### Issues
  * [[Evolution 640]](https://gitlab.com/ultreiaio/jaxx/-/issues/640) **Remove usage of nuiton-utils** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 639]](https://gitlab.com/ultreiaio/jaxx/-/issues/639) **Fix a spanish translation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-8](https://gitlab.com/ultreiaio/jaxx/-/milestones/114)

**Closed at *In progress*.**


### Issues
  * [[Evolution 640]](https://gitlab.com/ultreiaio/jaxx/-/issues/640) **Remove usage of nuiton-utils** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 639]](https://gitlab.com/ultreiaio/jaxx/-/issues/639) **Fix a spanish translation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-7](https://gitlab.com/ultreiaio/jaxx/-/milestones/113)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 636]](https://gitlab.com/ultreiaio/jaxx/-/issues/636) **Config ui is not well initialized** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 637]](https://gitlab.com/ultreiaio/jaxx/-/issues/637) **Use i18n 4.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 638]](https://gitlab.com/ultreiaio/jaxx/-/issues/638) **Use ultreia.io stack** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-7](https://gitlab.com/ultreiaio/jaxx/-/milestones/113)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 636]](https://gitlab.com/ultreiaio/jaxx/-/issues/636) **Config ui is not well initialized** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 637]](https://gitlab.com/ultreiaio/jaxx/-/issues/637) **Use i18n 4.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 638]](https://gitlab.com/ultreiaio/jaxx/-/issues/638) **Use ultreia.io stack** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-4](https://gitlab.com/ultreiaio/jaxx/-/milestones/109)
&#10;&#10;*(from redmine: created on 2017-01-18)*

**Closed at *In progress*.**


### Issues
  * [[Evolution 632]](https://gitlab.com/ultreiaio/jaxx/-/issues/632) **use pom 10.5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 631]](https://gitlab.com/ultreiaio/jaxx/-/issues/631) **Error in DMD gis editor, minutes are not ok** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-4](https://gitlab.com/ultreiaio/jaxx/-/milestones/109)
&#10;&#10;*(from redmine: created on 2017-01-18)*

**Closed at *In progress*.**


### Issues
  * [[Evolution 632]](https://gitlab.com/ultreiaio/jaxx/-/issues/632) **use pom 10.5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 631]](https://gitlab.com/ultreiaio/jaxx/-/issues/631) **Error in DMD gis editor, minutes are not ok** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-3](https://gitlab.com/ultreiaio/jaxx/-/milestones/107)
&#10;&#10;*(from redmine: created on 2017-01-01)*

**Closed at 2017-01-01.**


### Issues
  * [[Evolution 541]](https://gitlab.com/ultreiaio/jaxx/-/issues/541) **Extract a minimal runtime module used in generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 618]](https://gitlab.com/ultreiaio/jaxx/-/issues/618) **Introduce jaxx-runtime-swing-session module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 619]](https://gitlab.com/ultreiaio/jaxx/-/issues/619) **Move some api from jaxx-runtime to jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 620]](https://gitlab.com/ultreiaio/jaxx/-/issues/620) **Introduce jaxx-runtime-swing-application module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 621]](https://gitlab.com/ultreiaio/jaxx/-/issues/621) **Introduce jaxx-runtime-swing-nav module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 622]](https://gitlab.com/ultreiaio/jaxx/-/issues/622) **Introduce jaxx-runtime-swing-wizard module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 623]](https://gitlab.com/ultreiaio/jaxx/-/issues/623) **Introduce jaxx-runtime-swing-help module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 624]](https://gitlab.com/ultreiaio/jaxx/-/issues/624) **Review widgets to use UIHandler and other Jaxx best pratices** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 625]](https://gitlab.com/ultreiaio/jaxx/-/issues/625) **Generate generic handler if necessary** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 617]](https://gitlab.com/ultreiaio/jaxx/-/issues/617) **Can&#39;t open links with file ou http protocol in method SwingUtil.openLink** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-3](https://gitlab.com/ultreiaio/jaxx/-/milestones/107)
&#10;&#10;*(from redmine: created on 2017-01-01)*

**Closed at 2017-01-01.**


### Issues
  * [[Evolution 541]](https://gitlab.com/ultreiaio/jaxx/-/issues/541) **Extract a minimal runtime module used in generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 618]](https://gitlab.com/ultreiaio/jaxx/-/issues/618) **Introduce jaxx-runtime-swing-session module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 619]](https://gitlab.com/ultreiaio/jaxx/-/issues/619) **Move some api from jaxx-runtime to jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 620]](https://gitlab.com/ultreiaio/jaxx/-/issues/620) **Introduce jaxx-runtime-swing-application module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 621]](https://gitlab.com/ultreiaio/jaxx/-/issues/621) **Introduce jaxx-runtime-swing-nav module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 622]](https://gitlab.com/ultreiaio/jaxx/-/issues/622) **Introduce jaxx-runtime-swing-wizard module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 623]](https://gitlab.com/ultreiaio/jaxx/-/issues/623) **Introduce jaxx-runtime-swing-help module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 624]](https://gitlab.com/ultreiaio/jaxx/-/issues/624) **Review widgets to use UIHandler and other Jaxx best pratices** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 625]](https://gitlab.com/ultreiaio/jaxx/-/issues/625) **Generate generic handler if necessary** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 617]](https://gitlab.com/ultreiaio/jaxx/-/issues/617) **Can&#39;t open links with file ou http protocol in method SwingUtil.openLink** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-2](https://gitlab.com/ultreiaio/jaxx/-/milestones/106)
&#10;&#10;*(from redmine: created on 2016-12-31)*

**Closed at 2016-12-31.**


### Issues
  * [[Evolution 82]](https://gitlab.com/ultreiaio/jaxx/-/issues/82) **Add a breadcrumb widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 608]](https://gitlab.com/ultreiaio/jaxx/-/issues/608) **Remove deprecated API and remove jaxx-widgets module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 609]](https://gitlab.com/ultreiaio/jaxx/-/issues/609) **Review default configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 610]](https://gitlab.com/ultreiaio/jaxx/-/issues/610) **Introduce jaxx-widgets-file module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 611]](https://gitlab.com/ultreiaio/jaxx/-/issues/611) **Introduce jaxx-widgets-status** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 612]](https://gitlab.com/ultreiaio/jaxx/-/issues/612) **Remove jaxx-application modules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 613]](https://gitlab.com/ultreiaio/jaxx/-/issues/613) **Introduce jaxx-widgets-font module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 614]](https://gitlab.com/ultreiaio/jaxx/-/issues/614) **Introduce jaxx-widgets-i18n module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 615]](https://gitlab.com/ultreiaio/jaxx/-/issues/615) **Introduce jaxx-widgets-error module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 616]](https://gitlab.com/ultreiaio/jaxx/-/issues/616) **Introduce jaxx-widgets-hidor module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 239]](https://gitlab.com/ultreiaio/jaxx/-/issues/239) **Some tests are failing with jdk 7 VM Server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 466]](https://gitlab.com/ultreiaio/jaxx/-/issues/466) **Review the demo application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 502]](https://gitlab.com/ultreiaio/jaxx/-/issues/502) **Reimplements the action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-2](https://gitlab.com/ultreiaio/jaxx/-/milestones/106)
&#10;&#10;*(from redmine: created on 2016-12-31)*

**Closed at 2016-12-31.**


### Issues
  * [[Evolution 82]](https://gitlab.com/ultreiaio/jaxx/-/issues/82) **Add a breadcrumb widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 608]](https://gitlab.com/ultreiaio/jaxx/-/issues/608) **Remove deprecated API and remove jaxx-widgets module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 609]](https://gitlab.com/ultreiaio/jaxx/-/issues/609) **Review default configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 610]](https://gitlab.com/ultreiaio/jaxx/-/issues/610) **Introduce jaxx-widgets-file module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 611]](https://gitlab.com/ultreiaio/jaxx/-/issues/611) **Introduce jaxx-widgets-status** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 612]](https://gitlab.com/ultreiaio/jaxx/-/issues/612) **Remove jaxx-application modules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 613]](https://gitlab.com/ultreiaio/jaxx/-/issues/613) **Introduce jaxx-widgets-font module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 614]](https://gitlab.com/ultreiaio/jaxx/-/issues/614) **Introduce jaxx-widgets-i18n module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 615]](https://gitlab.com/ultreiaio/jaxx/-/issues/615) **Introduce jaxx-widgets-error module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 616]](https://gitlab.com/ultreiaio/jaxx/-/issues/616) **Introduce jaxx-widgets-hidor module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 239]](https://gitlab.com/ultreiaio/jaxx/-/issues/239) **Some tests are failing with jdk 7 VM Server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 466]](https://gitlab.com/ultreiaio/jaxx/-/issues/466) **Review the demo application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 502]](https://gitlab.com/ultreiaio/jaxx/-/issues/502) **Reimplements the action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-1](https://gitlab.com/ultreiaio/jaxx/-/milestones/87)
&#10;&#10;*(from redmine: created on 2015-04-05)*

**Closed at 2016-12-30.**


### Issues
  * [[Evolution 545]](https://gitlab.com/ultreiaio/jaxx/-/issues/545) **Migrates package to *org.nuiton.jaxx*** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 606]](https://gitlab.com/ultreiaio/jaxx/-/issues/606) **Improve initialization of generated UI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 133]](https://gitlab.com/ultreiaio/jaxx/-/issues/133) **Cannot generate class extend JaxxObject** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 566]](https://gitlab.com/ultreiaio/jaxx/-/issues/566) **Make webstart compatible with jdk 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 338]](https://gitlab.com/ultreiaio/jaxx/-/issues/338) **Update jdk level to 1.8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-1](https://gitlab.com/ultreiaio/jaxx/-/milestones/87)
&#10;&#10;*(from redmine: created on 2015-04-05)*

**Closed at 2016-12-30.**


### Issues
  * [[Evolution 545]](https://gitlab.com/ultreiaio/jaxx/-/issues/545) **Migrates package to *org.nuiton.jaxx*** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 606]](https://gitlab.com/ultreiaio/jaxx/-/issues/606) **Improve initialization of generated UI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 133]](https://gitlab.com/ultreiaio/jaxx/-/issues/133) **Cannot generate class extend JaxxObject** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 566]](https://gitlab.com/ultreiaio/jaxx/-/issues/566) **Make webstart compatible with jdk 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 338]](https://gitlab.com/ultreiaio/jaxx/-/issues/338) **Update jdk level to 1.8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.45](https://gitlab.com/ultreiaio/jaxx/-/milestones/134)

**Closed at *In progress*.**


### Issues
  * [[Evolution 698]](https://gitlab.com/ultreiaio/jaxx/-/issues/698) **Add accelerator on add-remove actions on DoubleList widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 699]](https://gitlab.com/ultreiaio/jaxx/-/issues/699) **Convertors should not be focusable in temperature widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.45](https://gitlab.com/ultreiaio/jaxx/-/milestones/134)

**Closed at *In progress*.**


### Issues
  * [[Evolution 698]](https://gitlab.com/ultreiaio/jaxx/-/issues/698) **Add accelerator on add-remove actions on DoubleList widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 699]](https://gitlab.com/ultreiaio/jaxx/-/issues/699) **Convertors should not be focusable in temperature widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.43](https://gitlab.com/ultreiaio/jaxx/-/milestones/1)

**Closed at 2017-04-19.**


### Issues
  * [[Evolution 1]](https://gitlab.com/ultreiaio/jaxx/-/issues/1) **[TableFilter] Enable to specify a custom header renderer** (Thanks to Kevin Morin) (Reported by Kevin Morin)
  * [[Anomalie 2]](https://gitlab.com/ultreiaio/jaxx/-/issues/2) **[SwingSession] the window state is not restored if the window is always in full screen** (Thanks to Kevin Morin) (Reported by Kevin Morin)

## Version [2.43](https://gitlab.com/ultreiaio/jaxx/-/milestones/1)

**Closed at 2017-04-19.**


### Issues
  * [[Evolution 1]](https://gitlab.com/ultreiaio/jaxx/-/issues/1) **[TableFilter] Enable to specify a custom header renderer** (Thanks to Kevin Morin) (Reported by Kevin Morin)
  * [[Anomalie 2]](https://gitlab.com/ultreiaio/jaxx/-/issues/2) **[SwingSession] the window state is not restored if the window is always in full screen** (Thanks to Kevin Morin) (Reported by Kevin Morin)

