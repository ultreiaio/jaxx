package io.ultreia.java4all.jaxx.widgets.validation;

/*-
 * #%L
 * JAXX :: Widgets - Validation
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorModel;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditorModel;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * All we need to validate some widgets.
 * <p>
 * Created at 29/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1.0
 */
public interface JaxxWidgetsValidationContext extends NuitonValidationContext {

    Map<String, CoordinatesEditor> getCoordinatesEditors();

    Map<String, TemperatureEditorModel> getTemperatureEditorModels();

    Map<String, NauticalLengthEditorModel> getNauticalLengthEditorModels();

    default CoordinatesEditor getCoordinatesEditor(String name) {
        return Optional.ofNullable(getCoordinatesEditors()).map(m -> m.get(name)).orElse(null);
    }

    default void setCoordinatesEditor(String name, CoordinatesEditor editor) {
        Objects.requireNonNull(getCoordinatesEditors()).put(name, editor);
    }

    default TemperatureEditorModel getTemperatureEditorModel(String name) {
        return Optional.ofNullable(getTemperatureEditorModels()).map(m -> m.get(name)).orElse(null);
    }

    default void setTemperatureEditorModel(String name, TemperatureEditorModel editor) {
        Map<String, TemperatureEditorModel> models = Objects.requireNonNull(getTemperatureEditorModels());
        if (models.containsKey(name)) {
            throw new IllegalArgumentException(String.format("There is already a TemperatureEditorModel with name '%s'", name));
        }
        models.put(name, editor);
    }

    default NauticalLengthEditorModel getNauticalLengthEditorModel(String name) {
        return Optional.ofNullable(getNauticalLengthEditorModels()).map(m -> m.get(name)).orElse(null);
    }

    default void setNauticalLengthEditorModel(String name, NauticalLengthEditorModel editor) {
        Map<String, NauticalLengthEditorModel> models = Objects.requireNonNull(getNauticalLengthEditorModels());
        if (models.containsKey(name)) {
            throw new IllegalArgumentException(String.format("There is already a NauticalLengthEditorModel with name '%s'", name));
        }
        models.put(name, editor);
    }
}
