package io.ultreia.java4all.jaxx.widgets.validation.validator;

/*-
 * #%L
 * JAXX :: Widgets - Validation
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.jaxx.widgets.validation.JaxxWidgetsValidationContext;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;
import org.nuiton.jaxx.widgets.gis.Coordinate;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;

import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Created on 01/29/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1.0
 */
public class CoordinateLatitudeDtoFieldValidator<O> extends SkipableFieldValidatorSupport<O, Float> {

    /**
     * Editor name to get the coordinate editor in the validation context, if found then use his coordinate format.
     */
    private final String editorName;
    /**
     * Default coordinate format, if another is found in the validation context, then use it.
     */
    private final CoordinateFormat defaultCoordinateFormat;

    public CoordinateLatitudeDtoFieldValidator(String fieldName, Function<O, Float> fieldFunction, String editorName, CoordinateFormat defaultCoordinateFormat) {
        super(Objects.requireNonNull(fieldName), Objects.requireNonNull(fieldFunction), (o, c) -> fieldFunction.apply(o) != null);
        this.editorName = Objects.requireNonNull(editorName);
        this.defaultCoordinateFormat = Objects.requireNonNull(defaultCoordinateFormat);
    }

    public static CoordinateFormat getCoordinateFormat(CoordinateFormat defaultCoordinateFormat, String editorName, NuitonValidationContext validationContext) {
        CoordinateFormat coordinateFormat = defaultCoordinateFormat;
        if (validationContext instanceof JaxxWidgetsValidationContext) {
            CoordinatesEditor coordinatesEditor = ((JaxxWidgetsValidationContext) validationContext).getCoordinatesEditor(editorName);
            if (coordinatesEditor != null) {
                coordinateFormat = coordinatesEditor.getModel().getFormat();
            }
        }
        return coordinateFormat;
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        CoordinateFormat coordinateFormat = getCoordinateFormat(defaultCoordinateFormat, editorName, validationContext);
        Float field = getField(object);
        Coordinate coordinate = coordinateFormat.fromDecimal(field);
        if (coordinate.isNull()) {
            return;
        }
        BiConsumer<String, Object> messageConsumer = (s, v) -> addMessage(validationContext, messagesCollector, s);
        coordinate.validateLatitude(messageConsumer);
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends SkipableFieldValidatorSupport.GeneratorSupport {
        public Generator() {
            super(CoordinateLatitudeDtoFieldValidator.class, true, true, false, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key,
                                               FieldValidatorDefinition validatorDefinition,
                                               Class<? extends NuitonValidationContext> validationContextType,
                                               ImportManager importManager,
                                               List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(ExpressionResolver.escapeString(validatorDefinition.getParameter("editorName")));
            result.add(String.format("%s.%s", importManager.addImport(CoordinateFormat.class), validatorDefinition.getParameter("defaultCoordinateFormat").toLowerCase()));
        }
    }
}
