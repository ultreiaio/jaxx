package io.ultreia.java4all.jaxx.widgets.validation.validator;

/*-
 * #%L
 * JAXX :: Widgets - Validation
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorConfig;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorModel;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthFormat;
import io.ultreia.java4all.jaxx.widgets.validation.JaxxWidgetsValidationContext;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created on 01/29/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1.0
 */
public class NauticalLengthBoundFieldValidator<O> extends FieldValidatorSupport<O, Float> {
    /**
     * Default nauticalLength format (min and max are based on this format).
     */
    private final NauticalLengthFormat defaultNauticalLengthFormat;
    /**
     * Min nauticalLength.
     */
    private final Float min;
    /**
     * Max nauticalLength.
     */
    private final Float max;

    public NauticalLengthBoundFieldValidator(String fieldName, Function<O, Float> fieldFunction, NauticalLengthFormat defaultNauticalLengthFormat, Float min, Float max) {
        super(Objects.requireNonNull(fieldName), fieldFunction);
        this.defaultNauticalLengthFormat = Objects.requireNonNull(defaultNauticalLengthFormat);
        this.min = Objects.requireNonNull(min);
        this.max = Objects.requireNonNull(max);
        if (min >= max) {
            throw new IllegalStateException(String.format("Parameter 'min' (%s) is greater than 'max' (%s)", min, max));
        }
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Float nauticalLength = getField(object);
        if (nauticalLength == null) {
            return;
        }
        Optional<NauticalLengthEditorModel> optionalNauticalLengthEditorModel = getOptionalNauticalLengthEditorModel(object, validationContext);
        NauticalLengthFormat nauticalLengthFormat = optionalNauticalLengthEditorModel.map(NauticalLengthEditorModel::getFormat).orElse(defaultNauticalLengthFormat);
        int digits = optionalNauticalLengthEditorModel.map(NauticalLengthEditorModel::getDigits).orElse(NauticalLengthEditorConfig.DEFAULT_DIGITS);
        float min;
        float max;
        if (nauticalLengthFormat.equals(defaultNauticalLengthFormat)) {
            min = this.min;
            max = this.max;
        } else {
            min = defaultNauticalLengthFormat.convert(nauticalLengthFormat, this.min);
            max = defaultNauticalLengthFormat.convert(nauticalLengthFormat, this.max);
            nauticalLength = defaultNauticalLengthFormat.convert(nauticalLengthFormat, nauticalLength);
        }
        min = Numbers.roundNDigits(min, digits);
        max = Numbers.roundNDigits(max, digits);
        nauticalLength = Numbers.roundNDigits(nauticalLength, digits);
        if (nauticalLength < min || nauticalLength > max) {
            addMessage(validationContext, messagesCollector, I18n.n("jaxx.validation.nauticalLength.nauticalLength.bound"), min, max, nauticalLengthFormat.getLabel(), nauticalLength);
        }
    }

    protected Optional<NauticalLengthEditorModel> getOptionalNauticalLengthEditorModel(O object, NuitonValidationContext validationContext) {
        if (validationContext instanceof JaxxWidgetsValidationContext) {
            String editorKey = NauticalLengthEditorModel.getEditorKey(object, getFieldName());
            return Optional.ofNullable(((JaxxWidgetsValidationContext) validationContext).getNauticalLengthEditorModel(editorKey));
        }
        return Optional.empty();
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(NauticalLengthBoundFieldValidator.class, true, true, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(String.format("%s.%s", importManager.addImport(NauticalLengthFormat.class), validatorDefinition.getParameter("defaultNauticalLengthFormat").toUpperCase()));
            result.add(escapeFloat(validatorDefinition.getParameter("min")));
            result.add(escapeFloat(validatorDefinition.getParameter("max")));
        }
    }

}


