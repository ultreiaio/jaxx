package io.ultreia.java4all.jaxx.widgets.validation.validator;

/*-
 * #%L
 * JAXX :: Widgets - Validation
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.jaxx.widgets.length.nautical.NauticalLengthEditorModel;
import io.ultreia.java4all.jaxx.widgets.validation.JaxxWidgetsValidationContext;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditorConfig;
import org.nuiton.jaxx.widgets.temperature.TemperatureEditorModel;
import org.nuiton.jaxx.widgets.temperature.TemperatureFormat;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created on 01/29/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1.0
 */
public class TemperatureBoundFieldValidator<O> extends FieldValidatorSupport<O, Float> {

    /**
     * Default temperature format (min and max are based on this format).
     */
    private final TemperatureFormat defaultTemperatureFormat;
    /**
     * Min temperature.
     */
    private final Float min;
    /**
     * Max temperature.
     */
    private final Float max;

    public TemperatureBoundFieldValidator(String fieldName, Function<O, Float> fieldFunction, TemperatureFormat defaultTemperatureFormat, Float min, Float max) {
        super(Objects.requireNonNull(fieldName), fieldFunction);
        this.defaultTemperatureFormat = Objects.requireNonNull(defaultTemperatureFormat);
        this.min = Objects.requireNonNull(min);
        this.max = Objects.requireNonNull(max);
        if (min >= max) {
            throw new IllegalStateException(String.format("Parameter 'min' (%s) is greater than 'max' (%s)", min, max));
        }
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Float temperature = getField(object);
        if (temperature == null) {
            return;
        }
        Optional<TemperatureEditorModel> optionalTemperatureEditorModel = getTemperatureEditorModel(object, validationContext);
        TemperatureFormat temperatureFormat = optionalTemperatureEditorModel.map(TemperatureEditorModel::getFormat).orElse(defaultTemperatureFormat);
        int digits = optionalTemperatureEditorModel.map(TemperatureEditorModel::getDigits).orElse(TemperatureEditorConfig.DEFAULT_DIGITS);
        float min;
        float max;
        if (temperatureFormat.equals(defaultTemperatureFormat)) {
            min = this.min;
            max = this.max;
        } else {
            min = defaultTemperatureFormat.convert(this.min, temperatureFormat);
            max = defaultTemperatureFormat.convert(this.max, temperatureFormat);
            temperature = defaultTemperatureFormat.convert(temperature, temperatureFormat);
        }
        min = Numbers.roundNDigits(min, digits);
        max = Numbers.roundNDigits(max, digits);
        temperature = Numbers.roundNDigits(temperature, digits);
        if (temperature < min || temperature > max) {
            addMessage(validationContext, messagesCollector, I18n.n("jaxx.validation.temperature.temperature.bound"), min, max, temperatureFormat.getLabel(), temperature);
        }
    }

    protected Optional<TemperatureEditorModel> getTemperatureEditorModel(O object, NuitonValidationContext validationContext) {
        if (validationContext instanceof JaxxWidgetsValidationContext) {
            String editorKey = NauticalLengthEditorModel.getEditorKey(object, getFieldName());
            return Optional.ofNullable(((JaxxWidgetsValidationContext) validationContext).getTemperatureEditorModel(editorKey));
        }
        return Optional.empty();
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(TemperatureBoundFieldValidator.class, true, true, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(String.format("%s.%s", importManager.addImport(TemperatureFormat.class), validatorDefinition.getParameter("defaultTemperatureFormat").toUpperCase()));
            result.add(escapeFloat(validatorDefinition.getParameter("min")));
            result.add(escapeFloat(validatorDefinition.getParameter("max")));
        }
    }

}

