package org.nuiton.jaxx.compiler.binding;

/*-
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.parser.JavaParser;
import org.nuiton.jaxx.compiler.java.parser.JavaParserConstants;
import org.nuiton.jaxx.compiler.java.parser.JavaParserTreeConstants;
import org.nuiton.jaxx.compiler.java.parser.SimpleNode;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.compiler.tags.TagManager;

import java.beans.Introspector;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class TypeParser {

    private static final Logger log = LogManager.getLogger(TypeParser.class);
    private final JAXXCompiler compiler;

    public TypeParser(JAXXCompiler compiler) {
        this.compiler = compiler;
    }


    public ClassDescriptor parseType(String source) {
        JavaParser p = new JavaParser(new StringReader(source + ";"));
//        if (!p.Line()) {
//            throw new IllegalStateException("Can't find a type on `" + source + "`");
//        }
        p.Statement();
        SimpleNode node = p.popNode();
        scanNode(node);
        return node.getJavaType();
    }

    /**
     * Examines a node to identify any dependencies it contains.
     *
     * @param node node to scan
     * @throws CompilerException ?
     */
    private void scanNode(SimpleNode node) throws CompilerException {

        log.trace(node.getText());
        int count = node.jjtGetNumChildren();
        for (int i = 0; i < count; i++) {
            scanNode(node.getChild(i));
        }
        // determine node type
        ClassDescriptor type = null;
        if (node.jjtGetNumChildren() == 1) {
            type = node.getChild(0).getJavaType();
        }
        switch (node.getId()) {
            case JavaParserTreeConstants.JJTCLASSORINTERFACETYPE:
                type = TagManager.resolveClass(node.getText().trim(), compiler);
                break;
            case JavaParserTreeConstants.JJTPRIMARYEXPRESSION:
                type = determineExpressionType(node);
//                log.debug("result of determineExpressionType for " + node.getText() + " = " + type);
                break;
            case JavaParserTreeConstants.JJTLITERAL:
                type = determineLiteralType(node);
                break;
            case JavaParserTreeConstants.JJTALLOCATIONEXPRESSION:
                type = node.getChild(0).getJavaType();
                break;
            case JavaParserTreeConstants.JJTCASTEXPRESSION:
                type = TagManager.resolveClass(node.getChild(0).getText(), compiler);
                break;
            default:
                log.trace(String.format("On node type: %d - `%s`", node.getId(), node));
        }
        node.setJavaType(type);
    }

    /**
     * Adds type information to nodes where possible, and as a side effect adds event listeners to nodes which
     * can be tracked.
     *
     * @param expression the node to scan
     * @return the class descriptor of the return type or null
     */
    private ClassDescriptor determineExpressionType(SimpleNode expression) {
        assert expression.getId() == JavaParserTreeConstants.JJTPRIMARYEXPRESSION;
        SimpleNode prefix = expression.getChild(0);
        if (log.isDebugEnabled()) {
            log.debug("for expression " + expression.getText() + " - prefix " + prefix + " - nb childrens of prefix: " + prefix.jjtGetNumChildren() + ", nb childrens of expression : " + expression.jjtGetNumChildren());
        }

        if (prefix.jjtGetNumChildren() == 1) {
            int type = prefix.getChild(0).getId();
            if (type == JavaParserTreeConstants.JJTLITERAL || type == JavaParserTreeConstants.JJTEXPRESSION) {
                prefix.setJavaType(prefix.getChild(0).getJavaType());
            } else if (type == JavaParserTreeConstants.JJTNAME && expression.jjtGetNumChildren() == 1) {
                // name with no arguments after it
                ClassDescriptor classDescriptor = scanCompoundSymbol(prefix.getText().trim(), compiler.getRootObject().getObjectClass(), false);
                log.trace("scanCompoundSymbol result for node " + prefix.getText().trim() + " = " + classDescriptor);
                prefix.setJavaType(classDescriptor);
            } else if (type == JavaParserTreeConstants.JJTNAME && expression.jjtGetNumChildren() == 2) {
                ClassDescriptor classDescriptor = scanMethodInvocation(prefix.getText().trim(), compiler.getRootObject().getObjectClass(), expression);
                log.trace("scanMethodInvocation result for node " + prefix.getText().trim() + " = " + classDescriptor);
                prefix.setJavaType(classDescriptor);
                return classDescriptor;
            }
        }

        if (expression.jjtGetNumChildren() == 1) {
            return prefix.getJavaType();
        }

        ClassDescriptor contextClass = prefix.getJavaType();
        if (contextClass == null) {
            contextClass = compiler.getRootObject().getObjectClass();
        }
        String lastNode = prefix.getText().trim();

        for (int i = 1; i < expression.jjtGetNumChildren(); i++) {
            SimpleNode suffix = expression.getChild(i);
            if (suffix.jjtGetNumChildren() == 1 && suffix.getChild(0).getId() == JavaParserTreeConstants.JJTARGUMENTS) {
                if (suffix.getChild(0).jjtGetNumChildren() == 0) {
                    // at the moment only no-argument methods are trackable
                    contextClass = scanCompoundSymbol(lastNode, contextClass, true);
                    if (log.isTraceEnabled()) {
                        log.trace("scanCompoundSymbol result for node " + lastNode + " = " + contextClass);
                    }
                    if (contextClass == null) {
                        return null;
                    }
                    int dotPos = lastNode.lastIndexOf(".");
                    String code = dotPos == -1 ? "" : lastNode.substring(0, dotPos);
                    for (int j = i - 2; j >= 0; j--) {
                        code = expression.getChild(j).getText() + code;
                    }
                    if (code.length() == 0) {
                        code = compiler.getRootObject().getJavaCode();
                    }
                    String methodName = lastNode.substring(dotPos + 1).trim();
                    if (log.isTraceEnabled()) {
                        log.trace("try to find type for method " + methodName + ", code : " + code);
                    }
                    try {
                        MethodDescriptor method = contextClass.getMethodDescriptor(methodName);
                        log.debug("Will trackMemberIfPossible from method " + method.getName() + " with objectCode = " + code);
                        log.trace("method found = " + method);
                        return getMethodReturnType(contextClass, method);
                    } catch (NoSuchMethodException e) {
                        if (log.isDebugEnabled()) {
                            log.info("Could not find method " + methodName + ", code : " + code + " on : " + contextClass);
                            if (log.isDebugEnabled()) {
                                for (MethodDescriptor descriptor : contextClass.getMethodDescriptors()) {
                                    log.debug(" - " + Modifier.toString(descriptor.getModifiers()) + " " + descriptor.getName() + "(...) : " + descriptor.getReturnType());
                                }
                            }
                        }
                        // happens for methods defined in the current JAXX file via scripts
                        String propertyName = null;
                        if (methodName.startsWith("is")) {
                            propertyName = Introspector.decapitalize(methodName.substring("is".length()));
                        } else if (methodName.startsWith("get")) {
                            propertyName = Introspector.decapitalize(methodName.substring("get".length()));
                        }
                        if (propertyName != null) {
                            //TC-20091026 use the getScriptMethod from compiler
                            MethodDescriptor newMethod = compiler.getScriptMethod(methodName);
                            if (newMethod != null) {
                                //TC-20091202 must suffix dependency by property, otherwise can not have two bindings
                                // on the same parent...
                                String bindingId = compiler.getRootObject().getId() + "." + propertyName;
                                log.debug("detect a dependency [" + bindingId + "] from a script method " + newMethod.getName() + ", will try to add a listener in method is part of javaBean ...");
                                contextClass = newMethod.getReturnType();
                            }
                        }
                    }
                }
            }
            lastNode = suffix.getText().trim();
            if (lastNode.startsWith(".")) {
                lastNode = lastNode.substring(1);
            }
        }

        return null;
    }


    private static ClassDescriptor determineLiteralType(SimpleNode node) {
        assert node.getId() == JavaParserTreeConstants.JJTLITERAL;
        if (node.jjtGetNumChildren() == 1) {
            int childId = node.getChild(0).getId();
            if (childId == JavaParserTreeConstants.JJTBOOLEANLITERAL) {
                return ClassDescriptorHelper.getClassDescriptor(boolean.class);
            }
            if (childId == JavaParserTreeConstants.JJTNULLLITERAL) {
                return ClassDescriptorHelper.getClassDescriptor(DataSource.NULL.class);
            }
            throw new RuntimeException("Expected BooleanLiteral or NullLiteral, found " + JavaParserTreeConstants.jjtNodeName[childId]);
        }
        int nodeId = node.firstToken.kind;
        switch (nodeId) {
            case JavaParserConstants.INTEGER_LITERAL:
                if (node.firstToken.image.toLowerCase().endsWith("l")) {
                    return ClassDescriptorHelper.getClassDescriptor(long.class);
                }
                return ClassDescriptorHelper.getClassDescriptor(int.class);
            case JavaParserConstants.CHARACTER_LITERAL:
                return ClassDescriptorHelper.getClassDescriptor(char.class);
            case JavaParserConstants.FLOATING_POINT_LITERAL:
                if (node.firstToken.image.toLowerCase().endsWith("f")) {
                    return ClassDescriptorHelper.getClassDescriptor(float.class);
                }
                return ClassDescriptorHelper.getClassDescriptor(double.class);
            case JavaParserConstants.STRING_LITERAL:
                return ClassDescriptorHelper.getClassDescriptor(String.class);
            default:
                throw new RuntimeException("Expected literal token, found " + JavaParserConstants.tokenImage[nodeId]);
        }
    }


    /**
     * Scans through a compound symbol (foo.bar.baz) to identify and track all trackable pieces of it.
     *
     * @param symbol       symbol to scan
     * @param contextClass current class context
     * @param isMethod     flag to search a method
     * @return the type of the symbol (or null if it could not be determined).
     */
    private ClassDescriptor scanCompoundSymbol(String symbol, ClassDescriptor contextClass, boolean isMethod) {
        String[] tokens = symbol.split("\\s*\\.\\s*");
        if (log.isDebugEnabled()) {
            log.debug("for symbol " + symbol + ", contextClass " + contextClass + ", isMethod " + isMethod);
            log.debug("tokens " + Arrays.toString(tokens));
        }
        StringBuilder currentSymbol = new StringBuilder();
        StringBuilder tokensSeenSoFar = new StringBuilder();
        // if this ends up false, it means we weren't able to figure out
        boolean accepted;
        // which object the method is being invoked on
        boolean recognizeClassNames = true;
//        for (int j = 0; j < tokens.length - (isMethod ? 1 : 0); j++) {
        for (int j = 0; j < tokens.length - (isMethod ? 1 : 0); j++) {
            accepted = false;
            if (tokensSeenSoFar.length() > 0) {
                tokensSeenSoFar.append('.');
            }
            tokensSeenSoFar.append(tokens[j]);
            if (currentSymbol.length() > 0) {
                currentSymbol.append('.');
            }
            currentSymbol.append(tokens[j]);
            if (log.isTraceEnabled()) {
                log.trace("try to find type for " + currentSymbol);
            }
            if (currentSymbol.indexOf(".") == -1) {
                String memberName = currentSymbol.toString();

                try {
                    FieldDescriptor field = contextClass.getFieldDescriptor(memberName);
                    try {
                        contextClass = field.getType();
                    } catch (Exception e) {
                        log.warn("could not find type for field " + field);
                        throw new NoSuchFieldException(e.getMessage());
                    }
                    currentSymbol.setLength(0);
                    accepted = true;
                    recognizeClassNames = false;
                } catch (NoSuchFieldException e) {

                    CompiledObject object = compiler.getCompiledObject(memberName);
                    if (object != null) {
                        if (log.isTraceEnabled()) {
                            log.trace("detected an object " + object);
                        }
                        contextClass = object.getObjectClass();
                        currentSymbol.setLength(0);
                        accepted = true;
                        recognizeClassNames = false;
                    } else if (j == 0 || j == 1 && tokens[0].equals(compiler.getRootObject().getId())) {
                        // still in root context
                        FieldDescriptor newField = compiler.getScriptField(memberName);
                        if (newField != null) {
                            contextClass = newField.getType();
                            assert contextClass != null : "script field '" + memberName + "' is defined, but has type null";
                            currentSymbol.setLength(0);
                            accepted = true;
                            recognizeClassNames = false;
                        }
                    }
                }
            }
            if (currentSymbol.length() > 0 && recognizeClassNames) {
                if (log.isDebugEnabled()) {
                    log.debug("Try to recognizeClassNames for symbol " + currentSymbol);
                }
                contextClass = TagManager.resolveClass(currentSymbol.toString(), compiler);
                if (contextClass != null) {
                    currentSymbol.setLength(0);
                    accepted = true;
                    recognizeClassNames = false;
                    // TODO: for now we don't handle statics
//                    return null;
                }
            }
            if (!accepted) {
                if (log.isDebugEnabled()) {
                    log.debug("symbol " + symbol + " was not accepted.");
                }
                return null;
            }
        }

        return contextClass;
    }

    /**
     * Scans through a compound symbol (foo.bar.baz) to identify and track all trackable pieces of it.
     *
     * @param symbol       symbol to scan
     * @param contextClass current class context
     * @param expression
     * @return the type of the symbol (or null if it could not be determined).
     */
    private ClassDescriptor scanMethodInvocation(String symbol, ClassDescriptor contextClass, SimpleNode expression) {
        String[] tokens = symbol.split("\\s*\\.\\s*");
        if (log.isDebugEnabled()) {
            log.debug("for method invocation " + symbol + ", contextClass " + contextClass);
            log.debug("tokens " + Arrays.toString(tokens));
        }
        StringBuilder currentSymbol = new StringBuilder();
        StringBuilder tokensSeenSoFar = new StringBuilder();
        // if this ends up false, it means we weren't able to figure out
        boolean accepted;
        // which object the method is being invoked on
        boolean recognizeClassNames = true;
//        for (int j = 0; j < tokens.length - (isMethod ? 1 : 0); j++) {
        for (int j = 0; j < tokens.length; j++) {
            accepted = false;

            if (tokensSeenSoFar.length() > 0) {
                tokensSeenSoFar.append('.');
            }
            tokensSeenSoFar.append(tokens[j]);
            if (currentSymbol.length() > 0) {
                currentSymbol.append('.');
            }
            currentSymbol.append(tokens[j]);
            if (log.isTraceEnabled()) {
                log.trace("try to find type for " + currentSymbol);
            }
            if (currentSymbol.indexOf(".") == -1) {
                String memberName = currentSymbol.toString();
                CompiledObject object = compiler.getCompiledObject(memberName);
                if (object != null) {
                    if (log.isTraceEnabled()) {
                        log.trace("detected an object " + object);
                    }
                    contextClass = object.getObjectClass();
                    currentSymbol.setLength(0);
                    accepted = true;
                    recognizeClassNames = false;
                } else {
                    try {
                        FieldDescriptor field = contextClass.getFieldDescriptor(memberName);
                        try {
                            contextClass = field.getType();
                        } catch (Exception e) {
                            log.warn("could not find type for field " + field);
                            throw new NoSuchFieldException(e.getMessage());
                        }

                        currentSymbol.setLength(0);
                        accepted = true;
                        recognizeClassNames = false;
                    } catch (NoSuchFieldException e) {
                        if (j == 0 || j == 1 && tokens[0].equals(compiler.getRootObject().getId())) {
                            // still in root context
                            FieldDescriptor newField = compiler.getScriptField(memberName);
                            if (newField != null) {
                                contextClass = newField.getType();
                                assert contextClass != null : "script field '" + memberName + "' is defined, but has type null";
                                currentSymbol.setLength(0);
                                accepted = true;
                                recognizeClassNames = false;
                            }
                        }
                    }
                }
            }
            if (currentSymbol.length() > 0)
                if (recognizeClassNames) {
                    if (log.isDebugEnabled()) {
                        log.debug("Try to recognizeClassNames for symbol " + currentSymbol);
                    }
                    contextClass = TagManager.resolveClass(currentSymbol.toString(), compiler);
                    if (contextClass != null) {
                        currentSymbol.setLength(0);
                        accepted = true;
                        recognizeClassNames = false;
                        // TODO: for now we don't handle statics
//                    return null;
                    }
                } else {
                    // try to find a method
                    String methodName = currentSymbol.toString();
                    for (MethodDescriptor methodDescriptor : contextClass.getMethodDescriptors()) {
                        if (methodName.equals(methodDescriptor.getName())) {
                            ClassDescriptor[] parameterTypes = methodDescriptor.getParameterTypes();
                            int numChildren = expression.jjtGetNumChildren();
                            if (parameterTypes.length != numChildren - 1) {
                                continue;
                            }
                            ClassDescriptor[] actual = new ClassDescriptor[numChildren - 1];
                            for (int i = 1; i < numChildren; i++) {
                                actual[i-1] = expression.getChild(i).getJavaType();
                            }
                            if (Arrays.equals(parameterTypes, actual)) {
                                return getMethodReturnType(contextClass, methodDescriptor);
                            }
                        }
                    }

                }

            if (!accepted) {
                if (log.isDebugEnabled()) {
                    log.debug("symbol " + symbol + " was not accepted.");
                }
                return null;
            }
        }

        return contextClass;
    }

    /**
     * Given a method from a given context class, try to obtain his method
     * return type.
     * <p>
     * Sometimes, the return type is unknown (generics can not be bind for
     * example). As a fallback, we try if the context class is exactly the
     * root context class of the compiler, replace it by the script method with
     * same name on which we can have more chance to obtain a return type...
     *
     * @param contextClass the context class of the method
     * @param method       the method
     * @return the method return type
     * @since 2.4.2
     */
    private ClassDescriptor getMethodReturnType(ClassDescriptor contextClass,
                                                MethodDescriptor method) {
        ClassDescriptor returnType = method.getReturnType();
        if (returnType == null && contextClass.equals(compiler.getRootObject().getObjectClass())) {

            // special case to deal with generics (we need to
            // have the concrete type)...
            method = compiler.getScriptMethod(method.getName());
            if (method != null) {
                returnType = method.getReturnType();
            }
        }
        return returnType;
    }
}
