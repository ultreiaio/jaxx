/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.validator;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.beans.JAXXBeanInfo;
import org.nuiton.jaxx.compiler.beans.JAXXPropertyDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.jaxx.validator.swing.ui.AbstractBeanValidatorUI;
import org.w3c.dom.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class BeanValidatorHandler extends DefaultObjectHandler {

    public static final String TAG = "BeanValidator";

    public static final String BEAN_ATTRIBUTE = "bean";

    public static final String BEAN_CLASS_ATTRIBUTE = "beanClass";


    public static final String ERROR_TABLE_MODEL_ATTRIBUTE = "errorTableModel";


    public static final String ERROR_TABLE_ATTRIBUTE = "errorTable";


    public static final String ERROR_TABLE_MODEL_DEFAULT = "errorTableModel";


    public static final String ERROR_TABLE_DEFAULT = "errorTable";

    public static final String AUTO_FIELD_ATTRIBUTE = "autoField";

    public static final String UI_CLASS_ATTRIBUTE = "uiClass";

    public static final String STRICT_MODE_ATTRIBUTE = "strictMode";

    public static final String CONTEXT_ATTRIBUTE = "context";

    public static final String PARENT_VALIDATOR_ATTRIBUTE = "parentValidator";
    protected static final Map<JAXXCompiler, List<CompiledBeanValidator>> validators = new HashMap<>();
    protected static final Map<JAXXCompiler, List<String>> validatedComponents = new HashMap<>();
    static final Logger log = LogManager.getLogger(BeanValidatorHandler.class);

    /**
     * The compiled objet representing a BeanValidator to be generated in
     * JAXXObject
     *
     * @author Tony Chemit - dev@tchemit.fr
     */
    @SuppressWarnings("unused")
    public static class CompiledBeanValidator extends CompiledObject {

        /**
         * Map of field to add into validator.
         * <p>
         * Keys are editors, Values are bean properties.
         */
        protected final Multimap<String, String> fields;

        /**
         * Map of field to exclude.
         * <p>
         * Keys are bean properties, Values are editors.
         */
        protected final Map<String, String> excludeFields;

        /**
         * Will never add validator field on this.
         */
        protected static final Set<String> EXCLUDE_SWING_PROPERTY_NAMES = Set.of("enabled", "opaque", "visible");

        protected String bean;

        protected String beanClass;

        protected String context;

        protected String uiClass;


        protected Boolean autoField;

        protected Boolean strictMode;

        protected JAXXBeanInfo beanDescriptor;

        protected String errorTableModel;

        protected String errorTable;

        protected String parentValidator;

        public CompiledBeanValidator(String id, JAXXCompiler compiler) {
            //TC-20090524 Use the real class descriptor, not the one by default,
            //TC-20090524 otherwise can not override the validator class while generation
            //super(id, objectClass, compiler);
            super(id, compiler.getEngine().getValidatorFactoryClass(), compiler);
            fields = ArrayListMultimap.create();
            excludeFields = new TreeMap<>();
        }

        public Multimap<String, String> getFields() {
            return fields;
        }

        public boolean containsFieldEditor(String editorName) {
            return fields.containsKey(editorName);
        }

        public boolean containsFieldPropertyName(String propertyName) {
            return fields.containsValue(propertyName);
        }

        public Set<String> getFieldEditors() {
            return fields.keySet();
        }

        public boolean containsExcludeFieldEditor(String editorName) {
            return EXCLUDE_SWING_PROPERTY_NAMES.contains(editorName)  || excludeFields.containsValue(editorName);
        }

        protected boolean containsExcludeFieldPropertyName(String editorName) {
            return EXCLUDE_SWING_PROPERTY_NAMES.contains(editorName) || excludeFields.containsKey(editorName);
        }

        public Set<String> getExcludeFieldPropertyNames() {
            return excludeFields.keySet();
        }

        protected void removeFieldPropertyName(String propertyName) {
            //must find the editor for this property
            for (String editor : fields.keySet()) {
                if (fields.containsEntry(editor, propertyName)) {
                    fields.remove(editor, propertyName);
                    break;
                }
            }
        }

        public void addField(String propertyName, String editor) {
            fields.put(editor, propertyName);
        }

        public void addExcludeField(String propertyName, String editor) {
            excludeFields.put(propertyName, editor);
        }

        public Collection<String> getFieldPropertyNames(String editor) {
            return fields.get(editor);
        }

        @Override
        public void addProperty(String property, String value) {
            if (BEAN_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    bean = value.trim();
                }
                return;
            }
            if (CONTEXT_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    context = value.trim();
                }
                return;
            }
            if (BEAN_CLASS_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    beanClass = value.trim();
                }
                return;
            }
            if (ERROR_TABLE_MODEL_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorTableModel = value.trim();
                }
                return;
            }
            if (ERROR_TABLE_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorTable = value.trim();
                }
                return;
            }
            if (UI_CLASS_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    uiClass = value.trim();
                }
                return;
            }
            if (AUTO_FIELD_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    autoField = (Boolean) TypeManager.convertFromString(value.trim(), Boolean.class);
                }
                return;
            }
            if (STRICT_MODE_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    strictMode = (Boolean) TypeManager.convertFromString(value.trim(), Boolean.class);
                }
                return;
            }
            if (PARENT_VALIDATOR_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    parentValidator = value.trim();
                }
                return;
            }
            throw new CompilerException(String.format("property %s is not allowed on object %s", property, this));
        }

        public String getBean() {
            return bean;
        }

        public boolean getAutoField() {
            return autoField != null && autoField;
        }

        public boolean getStrictMode() {
            return strictMode != null && strictMode;
        }

        public String getUiClass() {
            return uiClass;
        }

        public String getBeanClass() {
            return beanClass;
        }

        public String getContext() {
            return context;
        }

        public String getParentValidator() {
            return parentValidator;
        }

        public JAXXBeanInfo getBeanDescriptor(JAXXCompiler compiler) {
            if (beanDescriptor == null && foundBean()) {
                try {
                    ClassDescriptor beanClassDescriptor = ClassDescriptorHelper.getClassDescriptor(beanClass);
                    beanDescriptor = DefaultObjectHandler.getJAXXBeanInfo(beanClassDescriptor);
                } catch (Exception e) {
                    compiler.reportError(String.format("could not load class %s", beanClass));
                }
            }
            return beanDescriptor;
        }

        @Override
        public void addChild(CompiledObject child, String constraints, JAXXCompiler compiler) throws CompilerException {
            // do nothing
            compiler.reportError(String.format("can not add CompiledObject in the tag '%s (only field tags)", TAG));
        }

        public boolean foundBean() {
            return !(beanClass == null || beanClass.isEmpty());
        }

        protected boolean addUiClass(BeanValidatorHandler handler, JAXXCompiler compiler) {
            boolean withError = false;
            if (uiClass == null &&
                    compiler.getConfiguration().getDefaultErrorUI() != null) {
                uiClass = compiler.getConfiguration().getDefaultErrorUI().getName();
            }
            if (uiClass != null) {
                try {
                    ClassDescriptor uiClazz =
                            ClassDescriptorHelper.getClassDescriptor(uiClass);
                    if (!ClassDescriptorHelper.getClassDescriptor(AbstractBeanValidatorUI.class).isAssignableFrom(uiClazz)) {
                        compiler.reportError(String.format("attribute 'ui'  :'%s' is not assignable from class %s", uiClass, AbstractBeanValidatorUI.class)
                        );
                        withError = true;
                    } else {
                        String prefix = compiler.getImportedType(uiClazz.getName());
                        String code = handler.getSetPropertyCode(getJavaCode(), UI_CLASS_ATTRIBUTE, prefix + ".class", compiler);
                        appendAdditionCode(code);
                    }
                } catch (ClassNotFoundException e) {
                    compiler.reportError(String.format("class not found '%s'", uiClass));
                    withError = true;
                }
            }
            return withError;
        }


        protected boolean addErrorTableModel(Element tag, BeanValidatorHandler handler, JAXXCompiler compiler) {
            if (errorTableModel == null) {
                // try with the default "errors"
                if (!compiler.checkReference(tag, ERROR_TABLE_MODEL_DEFAULT, false, ERROR_TABLE_MODEL_ATTRIBUTE)) {
                    return false;
                }
                errorTableModel = ERROR_TABLE_MODEL_DEFAULT;
            } else {
                if (errorTableModel.startsWith("{") && errorTableModel.endsWith("}")) {// this is a script, no check here
                    errorTableModel = errorTableModel.substring(1, errorTableModel.length() - 1).trim();
                } else if (!compiler.checkReference(tag, errorTableModel, true, ERROR_TABLE_MODEL_ATTRIBUTE)) {
                    // errorListModel is not defined
                    return true;
                }
            }
            String code = handler.getSetPropertyCode(getJavaCode(), ERROR_TABLE_MODEL_ATTRIBUTE, errorTableModel, compiler);
            appendAdditionCode(code);
            return false;
        }

        protected boolean addParentValidator(Element tag, BeanValidatorHandler handler, JAXXCompiler compiler) {
            if (parentValidator != null) {
                String initializer;
                if (parentValidator.startsWith("{") && parentValidator.endsWith("}")) {
                    // todo : should be able to bind
                    initializer = parentValidator.substring(1, parentValidator.length() - 1);
                } else {
                    // the attribute refers an existing widget
                    if (!compiler.checkReference(tag, parentValidator, true, PARENT_VALIDATOR_ATTRIBUTE)) {
                        // parentValidator is not defined
                        return true;
                    }
                    initializer = parentValidator;
                }
                String code = handler.getSetPropertyCode(getJavaCode(), PARENT_VALIDATOR_ATTRIBUTE, initializer, compiler);
                appendAdditionCode(code);
            }
            return false;
        }

        protected boolean addErrorTable(Element tag, JAXXCompiler compiler) {
            if (errorTable == null) {
                // try with the default "errorList"
                if (!compiler.checkReference(tag, ERROR_TABLE_DEFAULT, false, ERROR_TABLE_ATTRIBUTE)) {
                    return false;
                }
                errorTable = ERROR_TABLE_DEFAULT;
            } else {
                if (!compiler.checkReference(tag, errorTable, true, ERROR_TABLE_ATTRIBUTE)) {
                    return true;
                }
            }
            String prefix = compiler.getImportedType(SwingValidatorUtil.class);
            String code = String.format("%s.registerErrorTableMouseListener(%s);", prefix, errorTable);
            appendAdditionCode(code);
            return false;
        }

        protected boolean addBean(Element tag, BeanValidatorHandler handler, JAXXCompiler compiler) {
            if (beanClass == null || beanClass.isEmpty()) {
                // try to guest beanClass from bean attribute
                if (bean != null && !bean.isEmpty()) {
                    beanClass = compiler.getSymbolTable().getClassTagIds().get(bean);
                    if (beanClass == null) {
                        compiler.reportError(String.format("could not find class of the bean '%s', and no beanClass was setted", bean));
                        return true;
                    }
                }
            }
            if (beanClass == null) {
                compiler.reportError(String.format("tag '%s' requires a 'beanClass' attribute, and could not guest it from 'bean' attribute (no bean attribute setted...)", tag));
                return true;
            }
            JAXXBeanInfo beanInfo = getBeanDescriptor(compiler);
            if (beanInfo == null) {
                compiler.reportError(tag, String.format("could not find descriptor of class %s", beanClass));
                return true;
            }
            String beanInitializer = null;
            if (bean != null) {
                if (bean.startsWith("{") && bean.endsWith("}")) {
                    String labelBinding = compiler.processDataBindings(bean);
                    if (labelBinding != null) {
                        compiler.getBindingHelper().registerDataBinding(getId() + ".bean", labelBinding, getId() + ".setBean(" + labelBinding + ");"
                        );
                    }
                    bean = null;
                } else {
                    if (!compiler.checkReference(tag, bean, true, BEAN_ATTRIBUTE)) {
                        // could not find bean in compiled object
                        return true;
                    }
                    if (isBeanUsedByValidator(compiler, bean)) {
                        compiler.reportError(String.format("the bean '%s' is already used in another the validator, can not used it in  '%s'", bean, tag)
                        );
                        return true;
                    }
                    beanInitializer = bean;
                }
            }
            if (beanInitializer != null) {
                String code = handler.getSetPropertyCode(getJavaCode(), BEAN_ATTRIBUTE, compiler.checkJavaCode(beanInitializer), compiler);
                appendAdditionCode(code);
            }
            String beanClassName = beanInfo.getJAXXBeanDescriptor().getClassDescriptor().getName();
            String type = compiler.getImportedType(beanClassName);
            // contextName must be in constructor to able to init validator with his correct contextName
            String constructorParams = type + ".class, " + TypeManager.getJavaCode(context);
            String validatorFactoryFqn = compiler.getConfiguration().getValidatorFactoryFQN();
            String prefix = compiler.getImportedType(validatorFactoryFqn);
            setInitializer(prefix + ".newValidator(" + constructorParams + ")");
            // add generic type to validator
            setGenericTypes(beanClassName);
            if (getAutoField()) {
                registerAutoFieldBean(tag, compiler, beanInfo);
            }
            if (getBeanDescriptor(compiler) != null) {
                // register the validator in compiler
                registerValidator(compiler, this);
            }
            return false;
        }

        private void registerValidator(JAXXCompiler compiler, CompiledBeanValidator compiledBeanValidator) {
            List<CompiledBeanValidator> validators = BeanValidatorHandler.validators.computeIfAbsent(compiler, k -> new ArrayList<>());
            validators.add(compiledBeanValidator);
            List<String> ids = validatedComponents.computeIfAbsent(compiler, k -> new ArrayList<>());
            ids.addAll(compiledBeanValidator.getFieldEditors());
        }

        protected void registerAutoFieldBean(Element tag, JAXXCompiler compiler, JAXXBeanInfo beanInfo) {
            for (JAXXPropertyDescriptor beanProperty :
                    beanInfo.getJAXXPropertyDescriptors()) {
                String descriptionName = beanProperty.getName();
                log.debug(String.format("try to bind on bean %s property %s", beanInfo.getJAXXBeanDescriptor().getName(), descriptionName));
                if (beanProperty.getWriteMethodDescriptor() == null) {
                    // read-only property
                    continue;
                }
                if (containsFieldPropertyName(descriptionName)) {
                    // already defined in field
                    continue;
                }
                if (containsExcludeFieldPropertyName(descriptionName)) {
                    // exclude field
                    continue;
                }
                if (!compiler.checkReference(tag, descriptionName, getStrictMode(), null)) {
                    // no editor component found
                    continue;
                }
                // ok add the field mapping
                registerField(descriptionName, descriptionName, compiler);
            }
            for (String key : getExcludeFieldPropertyNames()) {
                if (containsFieldPropertyName(key)) {
                    compiler.reportWarning(String.format("field '%s' can not be used and excluded at same time ! (field is skipped) for validator %s", key, this)
                    );
                    removeFieldPropertyName(key);
                }
            }
        }

        public void registerField(String id, String component, JAXXCompiler compiler) {
            if (containsFieldPropertyName(id)) {
                compiler.reportError(String.format("duplicate field '%s' for validator %s", id, this));
            } else {
                log.debug(String.format("add field <%s:%s>", id, component));
                addField(id, component);
            }
        }

        public void registerExcludeField(String id, String component, JAXXCompiler compiler) {
            if (containsExcludeFieldPropertyName(id)) {
                compiler.reportError(String.format("duplicate field '%s' for validator %s", id, this));
            } else {
                log.debug(String.format("add excludeField <%s:%s>", id, component));
                addExcludeField(id, component);
            }
        }

        public boolean checkBeanProperty(JAXXCompiler compiler, String propertyName) {
            for (JAXXPropertyDescriptor beanProperty :
                    getBeanDescriptor(compiler).getJAXXPropertyDescriptors()) {
                if (beanProperty.getName().equals(propertyName)) {
                    if (beanProperty.getWriteMethodDescriptor() == null) {
                        // read-only property
                        compiler.reportError(String.format("could not bind the readonly property '%s' on bean [%s] ", propertyName, getBean()));
                        return false;
                    }
                    return true;
                }
            }
            compiler.reportError(String.format("could not find the property '%s' on bean [%s] ", propertyName, getBean()));
            return false;
        }
    }

    /**
     * Test if a given bean is attached to a validator.
     *
     * @param compiler current compiler to use
     * @param beanId   the bean to test
     * @return <code>true</code> if the given bean is attached to a validator,
     * <code>false</code> otherwise
     */
    public static boolean isBeanUsedByValidator(JAXXCompiler compiler, String beanId) {
        List<CompiledBeanValidator> beanValidatorList = validators.get(compiler);
        if (beanValidatorList != null) {
            for (CompiledBeanValidator validator : beanValidatorList) {
                if (beanId.equals(validator.getBean())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param compiler compiler to use
     * @return <code>true</code> if some validators were detected,
     * <code>false</code> otherwise
     */
    public static boolean hasValidator(JAXXCompiler compiler) {
        List<CompiledBeanValidator> beanValidatorList = validators.get(compiler);
        return beanValidatorList != null && !beanValidatorList.isEmpty();
    }

    /**
     * Test if a given CompiledObject is attached to a validator.
     *
     * @param compiler    compiler to use
     * @param componentId the compiled object to test
     * @return <code>true</code> if the given compiled object is attached to
     * a validator, <code>false</code> otherwise
     */
    public static boolean isComponentUsedByValidator(JAXXCompiler compiler, String componentId) {
        List<String> ids = validatedComponents.get(compiler);
        return ids != null && ids.contains(componentId);
    }

    public static List<CompiledBeanValidator> getValidators(JAXXCompiler compiler) {
        return validators.get(compiler);
    }

    public BeanValidatorHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, SwingValidator.class);
    }

    @Override
    protected CompiledObject createCompiledObject(String id, JAXXCompiler compiler) {
        return new CompiledBeanValidator(id, compiler);
    }

    @Override
    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        log.debug(tag);
        if (!tag.getLocalName().equals(FieldValidatorHandler.TAG)) {
            compiler.reportError(String.format("tag '%s' may only contain %s as children, but found : %s", tag.getParentNode().getLocalName(), FieldValidatorHandler.TAG, tag.getLocalName()));
        } else {
            compiler.compileFirstPass(tag);
        }
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileSecondPass(tag, compiler);
        CompiledBeanValidator info = (CompiledBeanValidator) objectMap.get(tag);
        boolean error = info.addErrorTableModel(tag, this, compiler);
        if (!error) {
            error = info.addErrorTable(tag, compiler);
        }
        if (!error) {
            error = info.addUiClass(this, compiler);
        }
        if (!error) {
            error = info.addBean(tag, this, compiler);
        }
        if (!error) {
            error = info.addParentValidator(tag, this, compiler);
        }
        if (error) {
            log.warn(String.format("error were detected in second compile pass of CompiledObject [%s]", info));
        }
        // close the compiled object
        compiler.closeComponent(info);
    }

    @Override
    protected void setDefaults(CompiledObject object, Element tag, JAXXCompiler compiler) {
        // open the compiled object
        compiler.openInvisibleComponent(object);
    }

    @Override
    public void setAttribute(CompiledObject object, String propertyName, String stringValue, boolean inline, JAXXCompiler compiler) {
        log.debug(String.format("%s : %s for %s", propertyName, stringValue, object));
        // delegate to the compiled object with is stateful (but not the tag handler)
        object.addProperty(propertyName, stringValue);
    }
}
