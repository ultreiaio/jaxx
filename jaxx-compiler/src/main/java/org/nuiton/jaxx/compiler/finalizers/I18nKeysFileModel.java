package org.nuiton.jaxx.compiler.finalizers;

/*-
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.CompilerException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * FIXME Move this in I18n project
 * Created on 08/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class I18nKeysFileModel {
    private static final Logger log = LogManager.getLogger(I18nKeysFileModel.class);
    private static final String METHOD_PATTERN =
            "    public static String %1$s(Class<?> type) {\n" +
                    "        return I18n.t(BeanPropertyI18nKeyProducerProvider.get().getDefaultLabelsBuilder().getI18nPropertyKey(type, \"%2$s\"));\n" +
                    "    }\n";
    private static final String FILE_PATTERN =
            "package %1$s;\n\n" +
                    "import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;\n" +
                    "import io.ultreia.java4all.i18n.I18n;\n\n" +
                    "import javax.annotation.Generated;\n\n" +
                    "@Generated(value = \"org.nuiton.jaxx.plugin.GenerateMojo\", date = \"%2$s\")\n" +
                    "public class %3$s {\n\n" +
                    "%4$s" +
                    "}";
    private final Set<String> keys;
    private final String javaSimpleName;
    private final String javaPackageName;

    I18nKeysFileModel(String resource, Set<String> keys) {
        String path = resource.replace(".keys", "");
        int endIndex = path.lastIndexOf("/");
        this.javaSimpleName = path.substring(endIndex + 1) + "I18nHelper";
        this.javaPackageName = path.substring(0, endIndex).replaceAll("/", ".");
        this.keys = Objects.requireNonNull(keys);
    }

    public static I18nKeysFileModel of(ClassLoader classLoader, String fqn, boolean verbose) {
        Set<String> result = new TreeSet<>();
        String resourcePath = fqn.replaceAll("\\.", File.separator) + ".keys";
        URL resource = classLoader.getResource(resourcePath);
        if (resource != null) {
            log.debug(String.format("Found a i18n keys file: %s", resourcePath));
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.openStream()))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    result.add(line.trim());
                }
            } catch (IOException e) {
                throw new CompilerException("Can't load i18n keys at: " + resourcePath, e);
            }
            if (verbose) {
                log.info(String.format("Found %d i18n key(s) in file: %s", result.size(), resourcePath));
            } else {
                log.debug(String.format("Found %d i18n key(s) in file: %s", result.size(), resourcePath));
            }
            return new I18nKeysFileModel(resourcePath, result);
        }
        return null;
    }

    public Set<String> getKeys() {
        return keys;
    }

    public Path getJavaFile(Path targetRoot) {
        Path result = targetRoot;
        for (String part : javaPackageName.split("\\.")) {
            result = result.resolve(part);
        }
        return result.resolve(javaSimpleName + ".java");
    }

    public String getJavaFileContent() {

        List<String> methods = new LinkedList<>();
        for (String i18nKey : getKeys()) {
            StringBuilder methodNameBuilder = new StringBuilder("get");
            for (String keyPart : i18nKey.split("\\.")) {
                methodNameBuilder.append(StringUtils.capitalize(keyPart));
            }
            String methodName = methodNameBuilder.toString();
            methods.add(String.format(METHOD_PATTERN, methodName, i18nKey));
        }
        return String.format(FILE_PATTERN, javaPackageName, new Date(), javaSimpleName, String.join("\n", methods));
    }

    public File generateI18nJavaFile(Path targetRoot) throws IOException {
        Path javaFile = getJavaFile(targetRoot);
        String javaFileContent = getJavaFileContent();
        if (Files.notExists(javaFile.getParent())) {
            Files.createDirectories(javaFile.getParent());
        }
        Files.write(javaFile, javaFileContent.getBytes(StandardCharsets.UTF_8));
        return javaFile.toFile();
    }
}
