/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.binding.TypeParser;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.action.JAXXObjectActionSupport;
import org.w3c.dom.Element;

import javax.swing.AbstractButton;
import java.lang.reflect.Modifier;
import java.util.Set;

public class AbstractButtonHandler extends DefaultComponentHandler {

    private static final Logger log = LogManager.getLogger(AbstractButtonHandler.class);

    private final Set<ActionCandidateResolver> actionCandidateResolvers;

    public AbstractButtonHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, AbstractButton.class);
        actionCandidateResolvers = ImmutableSet.<ActionCandidateResolver>builder()
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + StringUtils.capitalize(objectId))
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + StringUtils.capitalize(objectId + "Action"))
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + jaxxFileName + StringUtils.capitalize(objectId))
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + jaxxFileName + StringUtils.capitalize(objectId + "Action"))
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + StringUtils.capitalize(objectId.replace("Action", "")))
                .add((packageName, jaxxFileName, objectId) -> packageName + ".actions." + jaxxFileName + StringUtils.capitalize(objectId.replace("Action", "")))
                .build();
    }

    @Override
    protected void setDefaults(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        super.setDefaults(object, tag, compiler);
        if (compiler.getConfiguration().isDetectAction()) {

            ClassDescriptor classDescriptor = getAction(compiler.getJavaFile().getPackageName(), compiler.getJavaFile().getSimpleName(), compiler, object);
            if (!object.isOverride() && classDescriptor == null) {
                // try in inheritance
                if (compiler.isSuperClassAware(JAXXObject.class)) {
                    ClassDescriptor objectClass = compiler.getRootObject().getObjectClass();
                    while (objectClass != null && classDescriptor == null) {
                        classDescriptor = getAction(objectClass.getPackageName(), objectClass.getSimpleName(), compiler, object);
                        if (classDescriptor == null) {
                            objectClass = objectClass.getSuperclass();
                        }
                    }
                }
            }

            if (classDescriptor != null) {
                if (compiler.getConfiguration().isVerbose()) {
                    log.info(String.format("Detect auto action: %s for %s.%s", classDescriptor.getName(), compiler.getJavaFile().getName(), object.getId()));
                }
                addDefaultAction(classDescriptor, object, compiler);
            }
        }
    }

    private ClassDescriptor getAction(String packageName, String jaxxFileName, JAXXCompiler compiler, CompiledObject compiledObject) {
        for (ActionCandidateResolver actionCandidateResolver : actionCandidateResolvers) {
            ClassDescriptor classDescriptor = actionCandidateResolver.resolve(packageName, jaxxFileName, compiler, compiledObject);
            if (classDescriptor != null) {
                return classDescriptor;
            }
        }
        return null;
    }

    @Override
    public void setAttribute(CompiledObject object, String propertyName, String stringValue, boolean inline, JAXXCompiler compiler) {
        if (propertyName.equals("action") && compiler.getConfiguration().isDetectAction()) {

            String actionInstance = compiler.checkJavaCode(stringValue, false);
            ClassDescriptor classDescriptor = new TypeParser(compiler).parseType(actionInstance);
            if (ClassDescriptorHelper.getClassDescriptor(JAXXObjectActionSupport.class).isAssignableFrom(classDescriptor)) {
                if (compiler.getConfiguration().isVerbose()) {
                    log.info(String.format("Detect auto action instance: %s for %s.%s : %s", classDescriptor.getName(), compiler.getJavaFile().getName(), object.getId(), actionInstance));
                }
                addActionInstance(classDescriptor, object, compiler, actionInstance);
                return;
            }
        }
        if (propertyName.equals(JAXXObjectActionSupport.ACTION_TYPE)) {
            if (compiler.getConfiguration().isDetectAction()) {
                compiler.reportWarning(String.format("Should not used actionType while using detect action mode: %s.%s", compiler.getOutputClassName(), object.getId()));
            }
            String type = compiler.checkJavaCode(stringValue).replace(".class", "");
            ClassDescriptor classDescriptor;
            try {
                classDescriptor = ClassDescriptorHelper.getClassDescriptor(type, compiler.getClassLoader());
            } catch (ClassNotFoundException e) {
                try {
                    String fqn = compiler.getImportedTypeForSimpleName(type);
                    classDescriptor = ClassDescriptorHelper.getClassDescriptor(fqn, compiler.getClassLoader());
                } catch (ClassNotFoundException ex) {
                    compiler.reportError("Can't find class for type: " + type);
                    return;
                }
            }
            if (compiler.getConfiguration().isVerbose()) {
                log.info(String.format("Found action: %s for %s.%s", classDescriptor.getName(), compiler.getJavaFile().getName(), object.getId()));
            }
            addAction(classDescriptor, object, compiler);

        } else {
            super.setAttribute(object, propertyName, stringValue, inline, compiler);
        }
    }

    private void addAction(ClassDescriptor classDescriptor, CompiledObject object, JAXXCompiler compiler) {
        compiler.addImport(classDescriptor.getName());
        compiler.appendActionInitializer(JAXXCompiler.getLineSeparator() + String.format("%s.init(this, %s, %s.class);", classDescriptor.getSimpleName(), object.getId(), classDescriptor.getSimpleName()));
    }

    private void addDefaultAction(ClassDescriptor classDescriptor, CompiledObject object, JAXXCompiler compiler) {
        compiler.addImport(classDescriptor.getName());
        MethodDescriptor[] constructors = classDescriptor.getConstructorDescriptors();
        boolean useGeneric = !classDescriptor.getTypeParameters().isEmpty();
        String constructorValue = String.format("new %s%s()", classDescriptor.getSimpleName(),useGeneric?"<>":"");

        for (MethodDescriptor constructor : constructors) {
            if (!Modifier.isPublic(constructor.getModifiers())) {
                continue;
            }
            ClassDescriptor[] parameterTypes = constructor.getParameterTypes();
            if (parameterTypes.length == 0) {
                // default constructor
                break;
            }
            if (parameterTypes.length == 1) {
                ClassDescriptor parameterType = parameterTypes[0];
                if (compiler.getOutputClassName().equals(parameterType.getName())) {
                    constructorValue = String.format("new %s%s(this)", classDescriptor.getSimpleName(),useGeneric?"<>":"");
                }
            }
        }
        compiler.appendActionInitializer(JAXXCompiler.getLineSeparator() + String.format("%s.init(this, %s, %s);", classDescriptor.getSimpleName(), object.getId(), constructorValue));
    }

    private void addActionInstance(ClassDescriptor classDescriptor, CompiledObject object, JAXXCompiler compiler, String value) {
        compiler.addImport(classDescriptor.getName());
        compiler.appendActionInitializer(JAXXCompiler.getLineSeparator() + String.format("%s.init(this, %s, %s);", classDescriptor.getSimpleName(), object.getId(), value));
    }

    interface ActionCandidateResolver {

        String getActionCandidateName(String packageName, String jaxxFileName, String objectId);

        default ClassDescriptor resolve(String packageName, String jaxxFileName, JAXXCompiler compiler, CompiledObject object) {

            String objectId = object.getId();
            String actionCandidateName = getActionCandidateName(packageName, jaxxFileName, objectId);
            try {
                return ClassDescriptorHelper.getClassDescriptor(actionCandidateName, compiler.getClassLoader());
            } catch (ClassNotFoundException e) {
                return null;
            }
        }
    }

}
