/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.I18nHelper;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.TabInfo;
import org.nuiton.jaxx.validator.swing.tab.TabInfoWithValidator;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.swing.Icon;
import javax.swing.JTabbedPane;
import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TabWithValidatorHandler extends DefaultObjectHandler {

    public static final String TAG_NAME = "tabWithValidator";

    public static final String ATTRIBUTE_ID = "id";

    public static final String ATTRIBUTE_TITLE = "title";

    public static final String ATTRIBUTE_TOOL_TIP_TEXT = "toolTipText";

    public static final String ATTRIBUTE_ICON = "icon";

    public static final String ATTRIBUTE_ENABLED = "enabled";

    public static final String ATTRIBUTE_DISABLED_ICON = "disabledIcon";

    public static final String ATTRIBUTE_MNEMONIC = "mnemonic";

    public static final String ATTRIBUTE_DISPLAYED_MNEMONIC_INDEX = "displayedMnemonicIndex";

    public static final String ATTRIBUTE_FOREGROUND = "foreground";

    public static final String ATTRIBUTE_BACKGROUND = "background";

    public static final String ATTRIBUTE_TAB_COMPONENT = "tabComponent";

    public static final String ATTRIBUTE_VALIDATOR_ID = "validatorId";

    protected static final Map<JAXXCompiler, List<TabInfoWithValidator>> tabs = new HashMap<>();

    /**
     * Creates a new <code>DefaultObjectHandler</code> which provides support for the specified class.  The
     * class is not actually introspected until the {@link #compileFirstPass} method is invoked.
     *
     * @param beanClass the class which this handler supports
     */
    public TabWithValidatorHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, TabInfoWithValidator.class);
    }

    public static void setAttribute(CompiledObject compiledTabInfo, TabInfoWithValidator tabInfo, String name, String value, JAXXCompiler compiler) throws CompilerException {
        value = value.trim();
        String id = tabInfo.getId();
        String binding = compiler.processDataBindings(value);
        if (binding != null) {
            compiler.getBindingHelper().registerDataBinding(id + "." + name, binding, id + ".set" + StringUtils.capitalize(name) + "(" + binding + ");");
            return;
        }

        String valueCode = TypeManager.getJavaCode(value);

        // add i18n support
        if (name.equals(I18N_PROPERTY_ATTRIBUTE)) {
            if ("skip".equals(value)) {
                return;
            }
            compiledTabInfo.setI18nProperty(StringUtils.isEmpty(value) ? id : value);
            String i18nKey = compiler.computeI18nProperty(compiledTabInfo);
            String i18nProperty = compiler.getI18nProperty(compiledTabInfo);
            setAttribute(compiledTabInfo, tabInfo, i18nProperty, i18nKey, compiler);
            return;
        }
        if (I18nHelper.isI18nableAttribute(name, compiler)) {
            value = valueCode = I18nHelper.addI18nInvocation(id, name, valueCode, compiler);
        }
        if (name.equals(ATTRIBUTE_ID)) {
            // ignore, already handled
        } else if (name.equals(ATTRIBUTE_TITLE)) {
            tabInfo.setTitle(value);
            compiledTabInfo.appendInitializationCode(id + ".setTitle(" + valueCode + ");");
        } else if (name.equals(ATTRIBUTE_TOOL_TIP_TEXT)) {
            tabInfo.setToolTipText(value);
            compiledTabInfo.appendInitializationCode(id + ".setToolTipText(" + valueCode + ");");
        } else if (name.equals(ATTRIBUTE_ICON)) {
            Icon icon = (Icon) TypeManager.convertFromString(value, Icon.class);
            tabInfo.setIcon(icon);
            compiledTabInfo.appendInitializationCode(id + ".setIcon(" + TypeManager.getJavaCode(icon) + ");");
        } else if (name.equals(ATTRIBUTE_ENABLED)) {
            boolean enabled = (Boolean) TypeManager.convertFromString(value, Boolean.class);
            tabInfo.setEnabled(enabled);
            compiledTabInfo.appendInitializationCode(id + ".setEnabled(" + enabled + ");");
        } else if (name.equals(ATTRIBUTE_DISABLED_ICON)) {
            Icon disabledIcon = (Icon) TypeManager.convertFromString(value, Icon.class);
            tabInfo.setDisabledIcon(disabledIcon);
            compiledTabInfo.appendInitializationCode(id + ".setDisabledIcon(" + TypeManager.getJavaCode(disabledIcon) + ");");
        } else if (name.equals(ATTRIBUTE_MNEMONIC)) {
            int mnemonic = (Character) TypeManager.convertFromString(value, char.class);
            tabInfo.setMnemonic(mnemonic);
            compiledTabInfo.appendInitializationCode(id + ".setMnemonic(" + mnemonic + ");");
        } else if (name.equals(ATTRIBUTE_DISPLAYED_MNEMONIC_INDEX)) {
            int displayedMnemonicIndex = (Integer) TypeManager.convertFromString(value, int.class);
            tabInfo.setDisplayedMnemonicIndex(displayedMnemonicIndex);
            compiledTabInfo.appendInitializationCode(id + ".setDisplayedMnemonicIndex(" + displayedMnemonicIndex + ");");
        } else if (name.equals(ATTRIBUTE_FOREGROUND)) {
            Color foreground = (Color) TypeManager.convertFromString(value, Color.class);
            tabInfo.setForeground(foreground);
            compiledTabInfo.appendInitializationCode(id + ".setForeground(" + TypeManager.getJavaCode(foreground) + ");");
        } else if (name.equals(ATTRIBUTE_BACKGROUND)) {
            Color background = (Color) TypeManager.convertFromString(value, Color.class);
            tabInfo.setBackground(background);
            compiledTabInfo.appendInitializationCode(id + ".setBackground(" + TypeManager.getJavaCode(background) + ");");
        } else if (name.equals(ATTRIBUTE_TAB_COMPONENT)) {
            tabInfo.setTabComponentStr(TypeManager.getJavaCode(value));
            compiledTabInfo.appendInitializationCode(id + ".setTabComponent(" + TypeManager.getJavaCode(value) + ");");
        } else if (name.equals(ATTRIBUTE_VALIDATOR_ID)) {
            tabInfo.setValidatorId(value);
            compiledTabInfo.appendInitializationCode(id + ".setValidatorId(\"" + value + "\");");
        } else {
            compiler.reportError("The <tabWithValidator> tag does not support the attribute '" + name + "'");
        }
    }

    public static List<TabInfoWithValidator> getTabs(JAXXCompiler compiler) {
        return tabs.getOrDefault(compiler, List.of());
    }

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compileChildrenFirstPass(tag, compiler);
    }

    protected CompiledObject createCompiledObject(String id,
                                                  JAXXCompiler compiler) {
        return new CompiledObject(id, getBeanClass(), compiler) {
            @Override
            public void finalizeCompiler(JAXXCompiler compiler) {

                TabInfoWithValidator tabInfo = Objects.requireNonNull(getTabInfoWithValidator(this, compiler));
                if (tabInfo.getValidatorId() == null) {
                    compiler.reportError("tabWithValidator " + getId() + "requires a validatorId value");
                }
                super.finalizeCompiler(compiler);
            }
        };
    }

    @Override
    public void compileSecondPass(Element tag, final JAXXCompiler compiler) throws CompilerException, IOException {
        if (!ClassDescriptorHelper.getClassDescriptor(JTabbedPane.class).isAssignableFrom(compiler.getOpenComponent().getObjectClass())) {
            compiler.reportError("tab tag may only appear within JTabbedPane tag");
            return;
        }

        JTabbedPaneHandler.CompiledTabbedPane tabs = (JTabbedPaneHandler.CompiledTabbedPane) compiler.getOpenComponent();
        CompiledObject compiledTabInfo = objectMap.get(tag);
        String id = tag.getAttribute(ATTRIBUTE_ID);
        if (id == null || id.length() == 0) {
            id = compiler.getAutoId(TabInfo.class.getSimpleName());
        }
        TabInfoWithValidator tabInfo = new TabInfoWithValidator(id);
        compiledTabInfo.setConstructorParams("\"" + id + "\"");
        compiler.appendLateInitializer(id + ".init(this, " + tabs.getId() + ");" + JAXXCompiler.getLineSeparator());
//        tabs.tabInfo = tabInfo;
        List<TabInfoWithValidator> list = TabWithValidatorHandler.tabs.getOrDefault(compiler, new LinkedList<>());
        list.add(tabInfo);
        TabWithValidatorHandler.tabs.put(compiler, list);
        setAttributes(compiledTabInfo, tabInfo, tag, compiler);
        compileChildrenSecondPass(tag, compiler);


//        tabs.tabInfo = null;
    }

    @Override
    public void setAttribute(CompiledObject object, String propertyName, String stringValue, boolean inline, JAXXCompiler compiler) {
        TabInfoWithValidator tabInfo = getTabInfoWithValidator(object, compiler);
        if (tabInfo == null) return;
        setAttribute(object, tabInfo, propertyName, stringValue, compiler);
    }

    private TabInfoWithValidator getTabInfoWithValidator(CompiledObject object, JAXXCompiler compiler) {
        List<TabInfoWithValidator> list = TabWithValidatorHandler.tabs.get(compiler);
        TabInfoWithValidator tabInfo = null;
        for (TabInfoWithValidator tabInfoWithValidator : list) {
            if (object.getId().equals(tabInfoWithValidator.getId())) {
                tabInfo = tabInfoWithValidator;
                break;
            }
        }
        if (tabInfo == null) {
            compiler.reportError("Can't find tabInfoWithValidator with id: " + object.getId());
            return null;
        }
        return tabInfo;
    }

    public void setAttributes(CompiledObject compiledTabInfo, TabInfo tabInfo, Element tag, JAXXCompiler compiler) throws CompilerException {
        NamedNodeMap children = tag.getAttributes();
        for (int i = 0; i < children.getLength(); i++) {
            Attr attribute = (Attr) children.item(i);
            String name = attribute.getName();
            String value = attribute.getValue();
            if (!name.startsWith("xmlns") && !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                setAttribute(compiledTabInfo, (TabInfoWithValidator) tabInfo, name, value, compiler);
            }
        }
    }

    protected void compileChildrenFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagFirstPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    protected void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }

}
