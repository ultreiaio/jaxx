/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.BeanScope;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.I18nHelper;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.bean.BeanScopeAware;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerListener;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Optional;

public class DefaultComponentHandler extends DefaultObjectHandler {


    public static final String BORDER_ATTRIBUTE = "border";

    public static final String TITLED_BORDER_ATTRIBUTE = "titledBorder";

    public static final String ACTION_ICON_ATTRIBUTE = "actionIcon";

    public static final String I18N_MNEMONIC_ATTRIBUTE = "i18nMnemonic";

    public static final String CONTAINER_DELEGATE_ATTRIBUTE = "containerDelegate";

    public static final String FONT_FACE_ATTRIBUTE = "font-face";

    public static final String FONT_SIZE_ATTRIBUTE = "font-size";

    public static final String FONT_STYLE_ATTRIBUTE = "font-style";

    public static final String FONT_WEIGHT_ATTRIBUTE = "font-weight";

    public static final String HEIGHT_ATTRIBUTE = "height";

    public static final String ICON_ATTRIBUTE = "icon";

    public static final String ICON_IMAGE_ATTRIBUTE = "iconImage";

    public static final String NAME_ATTRIBUTE = "name";

    public static final String WIDTH_ATTRIBUTE = "width";

    public static final String X_ATTRIBUTE = "x";

    public static final String Y_ATTRIBUTE = "y";

    public static final String BEAN_SCOPE_ATTRIBUTE = "beanScope";

    public static final String ADD_TO_CONTAINER_ATTRIBUTE = "addToContainer";

    public static final String FORCE_OVERRIDE_ATTRIBUTE = "forceOverride";

    /** Logger */
    protected static final Logger log =
            LogManager.getLogger(DefaultComponentHandler.class);

    public static final String BORDER_FACTORY_PREFIX = BorderFactory.class.getSimpleName() + ".";

    public static final String TITLED_BORDER_PREFIX = TitledBorder.class.getSimpleName() + "(";

    /** container delegate (if any) */
    private String containerDelegate;

    public DefaultComponentHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, Component.class);
    }

    @Override
    protected void init() throws IntrospectionException {
        if (jaxxBeanInfo == null) {
            super.init();

            containerDelegate = (String) getJAXXBeanInfo().getJAXXBeanDescriptor().getValue(CONTAINER_DELEGATE_ATTRIBUTE);
            if (containerDelegate == null && ClassDescriptorHelper.getClassDescriptor(Container.class).isAssignableFrom(getBeanClass().getSuperclass())) {
                containerDelegate = ((DefaultComponentHandler) TagManager.getTagHandler(getBeanClass().getSuperclass())).getContainerDelegate();
            }
        }
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("hasFocus", FocusListener.class);
        addProxyEventInfo("isVisible", ComponentListener.class);
        addProxyEventInfo("getBounds", ComponentListener.class);
        addProxyEventInfo("getLocation", ComponentListener.class);
        addProxyEventInfo("getLocationOnScreen", ComponentListener.class);
        addProxyEventInfo("getSize", ComponentListener.class);
        addProxyEventInfo("getX", ComponentListener.class);
        addProxyEventInfo("getY", ComponentListener.class);
        addProxyEventInfo("getWidth", ComponentListener.class);
        addProxyEventInfo("getHeight", ComponentListener.class);
        if (ClassDescriptorHelper.getClassDescriptor(Container.class).isAssignableFrom(getBeanClass())) {
            addProxyEventInfo("getComponentCount", ContainerListener.class);
        }
    }

    @Override
    protected void setDefaults(CompiledObject object,
                               Element tag,
                               JAXXCompiler compiler) throws CompilerException {
        super.setDefaults(object, tag, compiler);
        setAttribute(object, NAME_ATTRIBUTE, object.getId(), false, compiler);
        openComponent(object, tag, compiler);
    }

    @Override
    public void compileFirstPass(Element tag,
                                 JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag,
                                  JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileSecondPass(tag, compiler);
        closeComponent(compiler.getOpenComponent(), tag, compiler);
    }

    protected void openComponent(CompiledObject object,
                                 Element tag,
                                 JAXXCompiler compiler) throws CompilerException {
        if (object.isOverride()) {
            String addToContainer = tag.getAttribute(ADD_TO_CONTAINER_ATTRIBUTE).trim();
            object.setAddToContainer(Objects.equals("true", addToContainer));
        }
        String beanScopeAttribute = tag.getAttribute(BEAN_SCOPE_ATTRIBUTE);
        if (beanScopeAttribute!=null && !beanScopeAttribute.isEmpty()) {
            object.setBeanScope(new BeanScope(beanScopeAttribute, object));
        }
        String constraints = tag.getAttribute(CONSTRAINTS_ATTRIBUTE);
        if (constraints != null && constraints.length() > 0) {
            compiler.openComponent(object, constraints);
        } else {
            compiler.openComponent(object);
        }
        Optional<BeanScope> beanScope = compiler.getBeanScope();
        if (beanScope.isPresent() && ClassDescriptorHelper.isAssignableFrom(object.getObjectClass(), BeanScopeAware.class)) {
            // apply bean scope
            String beanScopeValue = beanScope.get().getBeanScope();
            log.debug(String.format("Apply bean '%s' to %s", beanScopeValue, object.getId()));
            setAttribute(object, "bean", "{" + beanScopeValue + "}", true, compiler);
        }
    }

    protected void closeComponent(CompiledObject object,
                                  Element tag,
                                  JAXXCompiler compiler) throws CompilerException {
        compiler.closeComponent(object);
    }

    @Override
    public boolean isPropertyInherited(String property) throws UnsupportedAttributeException {
        return property.equals("font") || property.startsWith("font-") || property.equals("foreground");
        //tchemit-2019-11-27 Do not any longer inherit enabled property, otherwise in menus, menu items have the enabled binding of his parent
        //tchemit-2019-11-27 This is not science, none science, it is none sense...
    }

    @Override
    public ClassDescriptor getPropertyType(CompiledObject object,
                                           String propertyName,
                                           JAXXCompiler compiler) throws CompilerException {
        if (X_ATTRIBUTE.equals(propertyName) ||
                Y_ATTRIBUTE.equals(propertyName) ||
                WIDTH_ATTRIBUTE.equals(propertyName) ||
                HEIGHT_ATTRIBUTE.equals(propertyName) ||
                FONT_SIZE_ATTRIBUTE.equals(propertyName)) {
            return ClassDescriptorHelper.getClassDescriptor(Integer.class);
        }
        if (FONT_FACE_ATTRIBUTE.equals(propertyName) ||
                FONT_STYLE_ATTRIBUTE.equals(propertyName) ||
                FONT_WEIGHT_ATTRIBUTE.equals(propertyName)) {
            return ClassDescriptorHelper.getClassDescriptor(String.class);
        }
        return super.getPropertyType(object, propertyName, compiler);
    }

    @Override
    public String getGetPropertyCode(String id,
                                     String name,
                                     JAXXCompiler compiler) throws CompilerException {
        if (FONT_FACE_ATTRIBUTE.equals(name)) {
            return id + ".getFont().getFontName()";
        }
        if (FONT_SIZE_ATTRIBUTE.equals(name)) {
            return id + ".getFont().getSize()";
        }
        if (FONT_WEIGHT_ATTRIBUTE.equals(name)) {
            compiler.addImport(Font.class);
            return "(" + id + ".getFont().getStyle() & Font.BOLD) != 0 ? \"bold\" : \"normal\"";
        }
        if (FONT_STYLE_ATTRIBUTE.equals(name)) {
            compiler.addImport(Font.class);
            return "(" + id + ".getFont().getStyle() & Font.ITALIC) != 0 ? \"italic\" : \"normal\"";
        }
        return super.getGetPropertyCode(id, name, compiler);
    }

    @Override
    public String getSetPropertyCode(String id,
                                     String name,
                                     String valueCode,
                                     JAXXCompiler compiler) throws CompilerException {
        if (X_ATTRIBUTE.equals(name)) {
            return id + ".setLocation(" + valueCode + ", " + id + ".getY());";
        }
        if (Y_ATTRIBUTE.equals(name)) {
            return id + ".setLocation(" + id + ".getX(), " + valueCode + ");";
        }
        if (WIDTH_ATTRIBUTE.equals(name)) {
            String type = compiler.getImportedType(SwingUtil.class);
//            compiler.setNeedSwingUtil(true);
            // need to optimize case when both width and height are being assigned
            return type + ".setComponentWidth(" + id + "," + valueCode + ");";
        }
        if (HEIGHT_ATTRIBUTE.equals(name)) {
//            compiler.setNeedSwingUtil(true);
            String type = compiler.getImportedType(SwingUtil.class);
            return type + ".setComponentHeight(" + id + "," + valueCode + ");";
        }
        if (FONT_FACE_ATTRIBUTE.equals(name)) {
            return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(new Font(" + valueCode + ", " + id + ".getFont().getStyle(), " + id + ".getFont().getSize()));\n}";
        }
        if (FONT_SIZE_ATTRIBUTE.equals(name)) {
            return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(" + id + ".getFont().deriveFont((float) " + valueCode + "));\n}";
        }
        if (FONT_WEIGHT_ATTRIBUTE.equals(name)) {
            if (valueCode.equals("\"bold\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() | " + type + ".BOLD));\n}";
            }
            if (valueCode.equals("\"normal\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() & ~" + type + ".BOLD));\n}";
            }
            if (!valueCode.startsWith("\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    if ((" + valueCode + ").equals(\"bold\")) {\n        " +
                        id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() | " + type + ".BOLD));\n    } else {\n        " +
                        id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() & ~" + type + ".BOLD));\n    }\n}";
            }
            compiler.reportError("font-weight must be either \"normal\" or \"bold\", found " + valueCode);
            return "";
        }
        if (FONT_STYLE_ATTRIBUTE.equals(name)) {
            if (valueCode.equals("\"italic\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() | " + type + ".ITALIC));\n}";
            }
            if (valueCode.equals("\"normal\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    " + id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() & ~" + type + ".ITALIC));\n}";
            }
            if (!valueCode.startsWith("\"")) {
                String type = compiler.getImportedType(Font.class);
                return "if (" + id + ".getFont() != null) {\n    if ((" + valueCode + ").equals(\"italic\")) {\n        " +
                        id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() | " + type + ".ITALIC));\n    } else {\n        " +
                        id + ".setFont(" + id + ".getFont().deriveFont(" + id + ".getFont().getStyle() & ~" + type + ".ITALIC));\n    }\n}";
            }
            compiler.reportError("font-style must be either \"normal\" or \"italic\", found " + valueCode);
            return "";
        }
        if (ClassDescriptorHelper.getClassDescriptor(Container.class).isAssignableFrom(getBeanClass()) && name.equals(LAYOUT_ATTRIBUTE)) {
            // handle containerDelegate (e.g. contentPane on JFrame)
            String cDelegate = (String) getJAXXBeanInfo().getJAXXBeanDescriptor().getValue(CONTAINER_DELEGATE_ATTRIBUTE);
            if (cDelegate != null) {
                return id + '.' + cDelegate + "().setLayout(" + valueCode + ");";
            }
        }
//        // ajout du support i18n
//        if (I18nHelper.isI18nableAttribute(name, compiler)) {
//            if (compiler.getCompiledObject(valueCode) == null) {
//                valueCode = I18nHelper.addI18nInvocation(id, name, valueCode, compiler);
//            }
//        }

        return super.getSetPropertyCode(id, name, valueCode, compiler);
    }

    @Override
    public void setAttribute(CompiledObject object,
                             String propertyName,
                             String stringValue,
                             boolean inline,
                             JAXXCompiler compiler) {

        if (propertyName.equals(BEAN_SCOPE_ATTRIBUTE)) {
            return;
        }
        if (propertyName.startsWith("_")) {
            // client property
            if (stringValue.startsWith("{")) {
                stringValue = stringValue.substring(1, stringValue.length() - 1);
                I18nHelper.tryToRegisterI18nInvocation(compiler,stringValue);
            }
            object.addClientProperty(propertyName.substring(1), stringValue);
            //TC-20090327 rather not generating code here
            //object.appendAdditionCode(object.getJavaCode() + ".putClientProperty(\"" + propertyName.substring(1) + "\", " + stringValue + ");");
            return;
        }
        if (TITLED_BORDER_ATTRIBUTE.equals(propertyName)) {
            compiler.addImport(TitledBorder.class);
            String attributeValue;
            if (object.isUseComputeI18n()) {
                String i18nKey = "\""+compiler.computeI18nProperty(object)+"\"";
                attributeValue = I18nHelper.addI18nInvocation(object.getId(), propertyName, i18nKey, compiler);
            } else {
                attributeValue = "\""+stringValue +"\"";
            }
            super.setAttribute(object, BORDER_ATTRIBUTE, "{new TitledBorder(" + attributeValue + ")}", false, compiler);
            return;
        }

        if (BORDER_ATTRIBUTE.equals(propertyName)) {
            if (stringValue.contains(BORDER_FACTORY_PREFIX)) {
                compiler.addImport(BorderFactory.class);
            } else if (stringValue.contains(TITLED_BORDER_PREFIX)) {
                compiler.addImport(TitledBorder.class);
            }
        }
        if (ICON_ATTRIBUTE.equals(propertyName)) {
            if (!(stringValue.startsWith("{") || stringValue.endsWith("}"))) {
                // this is a customized icon, add the icon creation code
//                compiler.setNeedSwingUtil(true);
                String type = compiler.getImportedType(SwingUtil.class);

                if (compiler.getConfiguration().isUseUIManagerForIcon()) {
                    stringValue = "{" + type + ".getUIManagerIcon(\"" + stringValue + "\")}";
                } else {
                    stringValue = "{" + type + ".createImageIcon(\"" + stringValue + "\")}";
                }
            }
        } else if (ICON_IMAGE_ATTRIBUTE.equals(propertyName)) {
            if (!(stringValue.startsWith("{") || stringValue.endsWith("}"))) {
                // this is a customized icon, add the icon creation code
//                compiler.setNeedSwingUtil(true);
                String type =
                        compiler.getImportedType(SwingUtil.class);

                if (compiler.getConfiguration().isUseUIManagerForIcon()) {
                    stringValue = "{" + type + ".getUIManagerIcon(\"" + stringValue + "\").getImage()}";
                } else {
                    stringValue = "{" + type + ".createImageIcon(\"" + stringValue + "\").getImage()}";
                }
            }
        } else if (ACTION_ICON_ATTRIBUTE.equals(propertyName)) {
            // customized actionIcon property
            if (stringValue.startsWith("{") && stringValue.endsWith("}")) {
                // there is a script to define the action icon, this is forbidden
                compiler.reportError("the actionIcon does not support script, remove braces..., fix the file " + compiler.getOutputClassName());
                return;
            }
            propertyName = ICON_ATTRIBUTE;
//            compiler.setNeedSwingUtil(true);
            String type = compiler.getImportedType(SwingUtil.class);
            if (compiler.getConfiguration().isUseUIManagerForIcon()) {
                stringValue = "{" + type + ".getUIManagerActionIcon(\"" + stringValue + "\")}";
            } else {
                stringValue = "{" + type + ".createActionIcon(\"" + stringValue + "\")}";
            }
            inline = true;
        } else if (I18N_MNEMONIC_ATTRIBUTE.equals(propertyName)) {
            propertyName = MNEMONIC_ATTRIBUTE;
            stringValue = I18nHelper.addI18nMnemonicInvocation(
                    object,
                    I18N_MNEMONIC_ATTRIBUTE,
                    TypeManager.getJavaCode(stringValue),
                    compiler);
        }
        super.setAttribute(object, propertyName, stringValue, inline, compiler);
    }

    @Override
    protected void scanAttributesForDependencies(Element tag,
                                                 JAXXCompiler compiler) {
        super.scanAttributesForDependencies(tag, compiler);
        // check for clientProperty attributes
        //FIXME make this works,... it seems jaxx compiler does not come here ?
        //FIXME see the the firstPassHandler in JAXXCompiler ?
        NamedNodeMap children = tag.getAttributes();
        for (int i = 0, max = children.getLength(); i < max; i++) {
            Attr attr = (Attr) children.item(i);
            String name = attr.getName();
            if (!name.startsWith("_")) {
                continue;
            }
            String value = attr.getValue();
            if (value.startsWith("{")) {
                compiler.reportWarning(tag, "an clientProperty attribute " + name.substring(1) + " does not required curly value but was : " + value, 0);
            }
        }

    }

    /**
     * Maps string values onto integers, so that int-valued enumeration properties can be specified by strings.  For
     * example, when passed a key of 'alignment', this method should normally map the values 'left', 'center', and
     * 'right' onto SwingConstants.LEFT, SwingConstants.CENTER, and SwingConstants.RIGHT respectively.
     * <p>
     * You do not normally need to call this method yourself; it is invoked by {@link #convertFromString} when an
     * int-valued property has a value which is not a valid number.  By default, this method looks at the
     * <code>enumerationValues</code> value of the <code>JAXXPropertyDescriptor</code>.
     *
     * @param key   the name of the int-typed property
     * @param value the non-numeric value that was specified for the property
     * @throws IllegalArgumentException if the property is an enumeration, but the value is not valid
     * @throws NumberFormatException    if the property is not an enumeration
     */
    @Override
    protected int constantValue(String key, String value) {
        if (key.equals(MNEMONIC_ATTRIBUTE) ||
                key.equals(DISPLAYED_MNEMONIC_ATTRIBUTE)) {
            if (value.length() == 1) {
                return value.charAt(0);
            }
            try {
                Field vk = KeyEvent.class.getField(value);
                return (Integer) vk.get(null);
            } catch (NoSuchFieldException e) {
                throw new IllegalArgumentException("mnemonics must be either a single character or the name of a field in KeyEvent (found: '" + value + "')");
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return super.constantValue(key, value);
    }

    /**
     * Returns <code>true</code> if this component can contain other components.  For children to be
     * allowed, the component must be a subclass of <code>Container</code> and its <code>JAXXBeanInfo</code>
     * must not have the value <code>false</code> for its <code>isContainer</code> value.
     *
     * @return <code>true</code> if children are allowed
     */
    public boolean isContainer() {
        boolean container = ClassDescriptorHelper.getClassDescriptor(Container.class).isAssignableFrom(getBeanClass());
//        if (container) {
//            try {
//                init();
//                if (Boolean.FALSE.equals(getJAXXBeanInfo().getJAXXBeanDescriptor().getValue("isContainer"))) {
//                    container = false;
//                }
//            } catch (IntrospectionException e) {
//                throw new RuntimeException(e);
//            }
//        }
        safeInit();
        if (container) {
            if (Boolean.FALSE.equals(getJAXXBeanInfo().getJAXXBeanDescriptor().getValue("isContainer"))) {
                container = false;
            }
        }
        return container;
    }

    public String getContainerDelegate() {
//        try {
//            init();
//            return containerDelegate;
//        } catch (IntrospectionException e) {
//            throw new RuntimeException(e);
//        }
        safeInit();
        return containerDelegate;

    }
}
