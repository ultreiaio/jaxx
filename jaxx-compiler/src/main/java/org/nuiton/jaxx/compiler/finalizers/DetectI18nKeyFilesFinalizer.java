package org.nuiton.jaxx.compiler.finalizers;

/*-
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Created on 08/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@AutoService(JAXXCompilerFinalizer.class)
@Component(hint = "i18n-detect-keys-file", role = JAXXCompilerFinalizer.class)
public class DetectI18nKeyFilesFinalizer extends AbstractFinalizer {

    private static final Logger log = LogManager.getLogger(DetectI18nKeyFilesFinalizer.class);

    @Override
    public boolean accept(JAXXCompiler compiler) {
        return compiler.getConfiguration().isGenerateI18nHelper() && compiler.getI18nKeysFileModel(true) != null;
    }

    @Override
    public void finalizeCompiler(CompiledObject root, JAXXCompiler compiler, JavaFile javaFile, String packageName, String className) {
    }

    @Override
    public void prepareJavaFile(CompiledObject root, JAXXCompiler compiler, JavaFile javaFile, String packageName, String className) throws IOException {
        I18nKeysFileModel keysFileModel = compiler.getI18nKeysFileModel(false);
        if (keysFileModel == null) {
            return;
        }
        Path targetRoot = compiler.getEngine().getConfiguration().getTargetDirectory().toPath();
        File file = keysFileModel.generateI18nJavaFile(targetRoot);
        log.info(String.format("Generate i18n helper file (with %d key(s)): %s", keysFileModel.getKeys().size(), file));
    }

}
