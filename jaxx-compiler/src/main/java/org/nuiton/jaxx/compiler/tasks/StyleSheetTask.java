/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;

/**
 * Task to apply css stylesheet on objects after second round of compilation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class StyleSheetTask extends JAXXEngineTask {

    /** Logger */
    private static final Logger log = LogManager.getLogger(StyleSheetTask.class);

    /** Task name */
    public static final String TASK_NAME = "StyleSheet";

    public StyleSheetTask() {
        super(TASK_NAME);
    }

    @Override
    public boolean perform(JAXXEngine engine) {

        // check all files are attached to a compiler
        checkAllFilesCompiled(engine);

        boolean success = true;
        boolean isVerbose = engine.getConfiguration().isVerbose();

        JAXXCompilerFile[] files = engine.getCompiledFiles();

        for (JAXXCompilerFile jaxxFile : files) {
            String className = jaxxFile.getClassName();
            if (isVerbose) {
                log.info("start " + className);
            }

            JAXXCompiler compiler = jaxxFile.getCompiler();
            addStartProfileTime(engine, compiler);
            compiler.applyStylesheets();
            addEndProfileTime(engine, compiler);

            if (compiler.isFailed()) {
                success = false;
            }
        }
        return success;
    }

}
