/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.compiler.tags.validator.BeanValidatorHandler.CompiledBeanValidator;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.w3c.dom.Element;

public class FieldValidatorHandler implements TagHandler {

    public static final String TAG = "field";

    public static final String NAME_ATTRIBUTE = "name";

    public static final String COMPONENT_ATTRIBUTE = "component";

    static private final Logger log = LogManager.getLogger(FieldValidatorHandler.class);

    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException {
        if (compiler.getConfiguration().isVerbose()) {
            log.debug(tag);
        }
        //todo check there is no child
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException {
        if (compiler.getConfiguration().isVerbose()) {
            log.debug(tag);
        }

        ClassDescriptor descriptor = ClassDescriptorHelper.getClassDescriptor(SwingValidator.class);
        if (!descriptor.isAssignableFrom(compiler.getOpenComponent().getObjectClass())) {
            compiler.reportError(String.format("%s tag may only appear within %s tag but was %s", TAG, BeanValidatorHandler.TAG, tag));
            return;
        }

        CompiledBeanValidator info =
                (CompiledBeanValidator) compiler.getOpenComponent();

        String name = tag.getAttribute(NAME_ATTRIBUTE);
        if (StringUtils.isEmpty(name)) {
            compiler.reportError(String.format("%s tag requires a %s attribute", TAG, NAME_ATTRIBUTE));
            return;
        }
        name = name.trim();

        String component = tag.getAttribute(COMPONENT_ATTRIBUTE);
        if (StringUtils.isEmpty(component)) {
            // try to use the name as component
            if (!compiler.checkReference(tag, name, false, name)) {
                compiler.reportError(String.format("%s tag requires a %s attribute, try to use the name attribute [%s] for the component, but no such component found", TAG, COMPONENT_ATTRIBUTE, name));
                return;
            }
            component = name;
        }
        component = component.trim();

        boolean complexType = false;
        if (component.startsWith("{") && component.endsWith("}")) {
            complexType = true;
            // means a complex reference (says a java Code in facts)
            component = compiler.preprocessScript(component.substring(1, component.length() - 1));
        }
        log.debug(String.format("Check '%s' reference", component));

        if (complexType) {
            String binding = compiler.processDataBindings(component);
            boolean withBinding = binding != null;
            log.debug(String.format("apply data binding on [%s] : %s", component, withBinding));
            // this means reference is ok, can safely add field
            // add a field
            info.registerField(name, component, compiler);
            return;
        }

        // simple reference, check it directly on compiler

        // check component exist (again perhaps, but let the error knows
        // exactly which tag failed...)
        if (compiler.checkReference(tag, component, true, COMPONENT_ATTRIBUTE)) {
            // add a field
            info.registerField(name, component, compiler);
        }
    }
}
