/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.jaxx.compiler.CompilerConfiguration;
import org.nuiton.jaxx.compiler.JAXXEngine;
import org.nuiton.jaxx.compiler.JAXXFactory;

/**
 * The init task to be launched first.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class InitTask extends JAXXEngineTask {

    /** Logger */
    private static final Logger log = LogManager.getLogger(InitTask.class);

    /** Task name */
    public static final String TASK_NAME = "Init";

    public InitTask() {
        super(TASK_NAME);
    }

    @Override
    public boolean perform(JAXXEngine engine) {
        boolean success = true;

        CompilerConfiguration configuration = engine.getConfiguration();

        // check initializers
        if (configuration.getInitializers() == null) {
            throw new NullPointerException(
                    "no initializers found in configuration.");
        }

        // check decorators
        if (configuration.getDecorators() == null) {

            throw new NullPointerException(
                    "no decorators found in configuration.");
        }

        // check finalizers
        if (configuration.getFinalizers() == null) {

            throw new NullPointerException(
                    "no finalizers found in configuration.");
        }

        if (configuration.isVerbose()) {
            log.info("Will init " + JAXXFactory.class.getName());
        }
        JAXXFactory.initFactory();

        engine.clearReports();

        return success;
    }
}
