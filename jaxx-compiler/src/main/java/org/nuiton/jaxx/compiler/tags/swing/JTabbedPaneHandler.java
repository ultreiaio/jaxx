/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.runtime.swing.TabInfo;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;
import java.awt.event.ContainerListener;

public class JTabbedPaneHandler extends DefaultComponentHandler {

//    public static final String ATTRIBUTE_TITLE = "title";
//
//    public static final String ATTRIBUTE_TOOL_TIP_TEXT = "toolTipText";

    public JTabbedPaneHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JTabbedPane.class);
    }

    public class CompiledTabbedPane extends CompiledObject {

        int tabCount;

        TabInfo tabInfo;


        public CompiledTabbedPane(String id, ClassDescriptor objectClass, JAXXCompiler compiler) throws CompilerException {
            super(id, objectClass, compiler);
        }

        @Override
        public void addChild(CompiledObject child, String constraints, JAXXCompiler compiler) throws CompilerException {
            if (constraints != null) {
                compiler.reportError("JTabbedPane tabs may not have constraints");
            }

            super.addChild(child, constraints, compiler, JTabbedPaneHandler.this);
            if (tabInfo == null) {
                compiler.reportError("JTabbedPaneHandler may only have 'tab' tags as children (found " + child.getObjectClass() + ")");
                return;
            }

            int tabIndex = ++tabCount - 1;
            appendAdditionCode(String.format("%s.install(%s, %d);", tabInfo.getId(), getId(), tabIndex));

//            String title = tabInfo.getTitle();
//            if (title != null) {
//                if (I18nHelper.isI18nAttribute(ATTRIBUTE_TITLE)) {
//                    if (!title.startsWith("t(\"")) {
//                        // we did not have the invocation code, add it
//                        title = I18nHelper.addI18nInvocation(getId(), ATTRIBUTE_TITLE, TypeManager.getJavaCode(title), compiler);
//                    }
//                } else {
//                    title = TypeManager.getJavaCode(title);
//                }
//                appendAdditionCode(tabInfo.getId() + ".setTitle(" + title + ");");
//            }
//
//            String toolTipText = tabInfo.getToolTipText();
//            if (toolTipText != null) {
//                if (I18nHelper.isI18nAttribute(ATTRIBUTE_TOOL_TIP_TEXT)) {
//                    if (!toolTipText.startsWith("t(\"")) {
//                        // we did not have the invocation code, add it
//                        toolTipText = I18nHelper.addI18nInvocation(getId(), ATTRIBUTE_TOOL_TIP_TEXT, TypeManager.getJavaCode(toolTipText), compiler);
//                    }
//                } else {
//                    toolTipText = TypeManager.getJavaCode(toolTipText);
//                }
//                appendAdditionCode(tabInfo.getId() + ".setToolTipText(" + toolTipText + ");");
//            }
//
//            boolean enabled = tabInfo.isEnabled();
//            if (!enabled) {
//                appendAdditionCode(tabInfo.getId() + ".setEnabled(false);");
//            }
//
//            Color foreground = tabInfo.getForeground();
//            if (foreground != null) {
//                appendAdditionCode(getId() + ".setForegroundAt(" + tabIndex + ", " + TypeManager.getJavaCode(foreground) + ");");
//            }
//
//            Color background = tabInfo.getBackground();
//            if (background != null) {
//                appendAdditionCode(getId() + ".setBackgroundAt(" + tabIndex + ", " + TypeManager.getJavaCode(background) + ");");
//            }
//
//            int mnemonic = tabInfo.getMnemonic();
//            if (mnemonic != -1) {
//                appendAdditionCode(tabInfo.getId() + ".setMnemonic(" + mnemonic + ");");
//            }
//
//            int displayedMnemonicIndex = tabInfo.getDisplayedMnemonicIndex();
//            if (displayedMnemonicIndex != -1) {
//                appendAdditionCode(tabInfo.getId() + ".setDisplayedMnemonicIndex("  + displayedMnemonicIndex + ");");
//            }
//
//            Icon icon = tabInfo.getIcon();
//            if (icon != null) {
//                appendAdditionCode(getId() + ".setIconAt(" + tabIndex + ", " + icon + ");");
//            }
//
//            Icon disabledIcon = tabInfo.getDisabledIcon();
//            if (disabledIcon != null) {
//                appendAdditionCode(getId() + ".setDisabledIconAt(" + tabIndex + ", " + disabledIcon + ");");
//            }
//
//            String tabComponent = tabInfo.getTabComponentStr();
//            if (tabComponent != null) {
//                appendAdditionCode(tabInfo.getId() + ".setTabComponent(" + tabComponent + ");");
//            }

            tabInfo = null;
        }
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledTabbedPane(id, getBeanClass(), compiler);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getSelectedIndex", ChangeListener.class);
        addProxyEventInfo("getSelectedComponent", ChangeListener.class);
        addProxyEventInfo("getTabCount", ContainerListener.class);
    }
}
