/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.TabInfo;
import org.w3c.dom.Element;

import java.io.IOException;

public class TabHandler extends DefaultObjectHandler {

    public static final String TAG_NAME = "tab";

    public static final String ATTRIBUTE_TAB_COMPONENT = "tabComponent";

    public TabHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, TabInfo.class);
    }

    public static class CompiledTabbedTab extends CompiledObject {
        final TabInfo tabInfo;

        public CompiledTabbedTab(String id, ClassDescriptor objectClass, JAXXCompiler compiler) {
            super(id, objectClass, compiler);
            tabInfo = new TabInfo(id);
            setConstructorParams("\"" + id + "\"");
        }

    }

    @Override
    protected CompiledObject createCompiledObject(String id, JAXXCompiler compiler) {
        return new CompiledTabbedTab(id, getBeanClass(), compiler);
    }

    @Override
    protected CompiledObject getSafeCompiledObject(Element tag, JAXXCompiler compiler) {
        CompiledObject compiledObject = super.getSafeCompiledObject(tag, compiler);
        if (!(compiledObject instanceof CompiledTabbedTab)) {
            String message = String.format("Should have found a tab compiled object, but found: %s", compiledObject);
            compiler.reportError(tag, message);
            throw new IllegalStateException(message);
        }
        ((JTabbedPaneHandler.CompiledTabbedPane) compiler.getOpenComponent()).tabInfo = ((CompiledTabbedTab) compiledObject).tabInfo;
        return compiledObject;
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        CompiledObject openComponent = compiler.getOpenComponent();

        if (!(openComponent instanceof JTabbedPaneHandler.CompiledTabbedPane)) {
            compiler.reportError(tag, "tab tag may only appear within JTabbedPane tag, but found: " + openComponent);
            return;
        }

        super.compileSecondPass(tag, compiler);
        ((JTabbedPaneHandler.CompiledTabbedPane) openComponent).tabInfo = null;
    }

    @Override
    public void setAttribute(CompiledObject object, String propertyName, String stringValue, boolean inline, JAXXCompiler compiler) {
        if (propertyName.equals(ATTRIBUTE_TAB_COMPONENT)) {
            ((CompiledTabbedTab) object).tabInfo.setTabComponentStr(TypeManager.getJavaCode(stringValue));
        }
        super.setAttribute(object, propertyName, stringValue, inline, compiler);
    }

}
